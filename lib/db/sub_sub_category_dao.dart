import 'package:biomart/db/common/ps_dao.dart';
import 'package:biomart/viewobject/sub_sub_category.dart';
import 'package:sembast/sembast.dart';

class SubSubCategoryDao extends PsDao<SubSubCategory> {
  SubSubCategoryDao() {
    init(SubSubCategory());
  }

  static const String STORE_NAME = 'SubSubCategory';
  final String _primaryKey = 'id';

  @override
  String getPrimaryKey(SubSubCategory object) {
    return object.id;
  }

  @override
  String getStoreName() {
    return STORE_NAME;
  }

  @override
  Filter getFilter(SubSubCategory object) {
    return Filter.equals(_primaryKey, object.id);
  }
}
