import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:flutter/material.dart';

PreferredSizeWidget customAppBar({
  String title,
  List<Widget> actions,
  Widget leading,
  bool centerTitle,
  // bool isAutomaticallyImplyLeading,
}) {
  return AppBar(
    backgroundColor: PsColors.coreBackgroundColor,
    automaticallyImplyLeading: false,
    centerTitle: centerTitle ?? false,
    elevation: 0,
    actions: actions,
    leading: leading,
    iconTheme: IconThemeData(color: PsColors.textPrimaryColor),
    title: Container(
      margin: EdgeInsets.only(left: leading == null ? 17 : 0),
      child: Text(
        title,
        style: TextStyle(
          color: PsColors.textPrimaryColor,
          fontSize: PsDimens.space24,
          fontWeight: FontWeight.w500,
          //fontFamily: PsConfig.ps_default_font_family,
        ),
      ),
    ),
  );
}
