import 'dart:async';

import 'package:biomart/api/common/ps_resource.dart';
import 'package:biomart/api/common/ps_status.dart';
import 'package:biomart/api/ps_api_service.dart';
import 'package:biomart/viewobject/delivery_cost.dart';
import 'package:flutter/material.dart';

import 'Common/ps_repository.dart';

class DeliveryCostRepository extends PsRepository {
  DeliveryCostRepository({
    @required PsApiService psApiService,
  }) {
    _psApiService = psApiService;
  }
  String primaryKey = 'id';
  PsApiService _psApiService;

  Future<PsResource<DeliveryCost>> postDeliveryCheckingAndCalculating(
      Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet,
      PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<DeliveryCost> _resource =
        await _psApiService.postDeliveryCheckingAndCalculating(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
      return _resource;
    } else {
      final Completer<PsResource<DeliveryCost>> completer =
          Completer<PsResource<DeliveryCost>>();
      completer.complete(_resource);
      return completer.future;
    }
  }
}
