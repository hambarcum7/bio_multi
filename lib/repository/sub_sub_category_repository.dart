import 'dart:async';

import 'package:biomart/api/common/ps_resource.dart';
import 'package:biomart/api/common/ps_status.dart';
import 'package:biomart/api/ps_api_service.dart';
import 'package:biomart/constant/ps_constants.dart';
import 'package:biomart/db/sub_sub_category_dao.dart';
import 'package:biomart/repository/Common/ps_repository.dart';
import 'package:biomart/viewobject/sub_sub_category.dart';
import 'package:flutter/material.dart';
import 'package:sembast/sembast.dart';

class SubSubCategoryRepository extends PsRepository {
  SubSubCategoryRepository(
      {@required PsApiService psApiService,
      @required SubSubCategoryDao subSubCategoryDao}) {
    _psApiService = psApiService;
    _subSubCategoryDao = subSubCategoryDao;
  }

  PsApiService _psApiService;
  SubSubCategoryDao _subSubCategoryDao;
  final String _primaryKey = 'id';

  Future<dynamic> insert(SubSubCategory subSubCategory) async {
    return _subSubCategoryDao.insert(_primaryKey, subSubCategory);
  }

  Future<dynamic> update(SubSubCategory subSubCategory) async {
    return _subSubCategoryDao.update(subSubCategory);
  }

  Future<dynamic> delete(SubSubCategory subSubCategory) async {
    return _subSubCategoryDao.delete(subSubCategory);
  }

  Future<dynamic> getSubSubCategoryListBySubCategoryId(
      StreamController<PsResource<List<SubSubCategory>>>
          subSubCategoryListStream,
      bool isConnectedToIntenet,
      int limit,
      int offset,
      PsStatus status,
      String subCategoryId,
      {bool isLoadFromServer = true}) async {
    final Finder finder =
        Finder(filter: Filter.equals('cat_id', subCategoryId));

    subSubCategoryListStream.sink
        .add(await _subSubCategoryDao.getAll(finder: finder, status: status));

    final PsResource<List<SubSubCategory>> _resource =
        await _psApiService.getSubSubCategoryList(limit, offset, subCategoryId);
        
    for (var i in _resource.data) {
      if (i.subCatId == subCategoryId) {
        print('${i.name}  qqqqqqqqqqqqqqqqqqqqq');
      }
    }

    if (_resource.status == PsStatus.SUCCESS) {
      await _subSubCategoryDao.deleteWithFinder(finder);
      await _subSubCategoryDao.insertAll(_primaryKey, _resource.data);
    } else {
      if (_resource.errorCode == PsConst.ERROR_CODE_10001) {
        await _subSubCategoryDao.deleteWithFinder(finder);
      }
    }
    final PsResource<List<SubSubCategory>> allData =
        await _subSubCategoryDao.getAll(finder: finder);
    print('${allData.data[0].name}  ooooooooooooooooooooooooooooooooooooooooooooooooo');

    subSubCategoryListStream.sink.add(allData);
    // subSubCategoryListStream.sink
    //     .add(await _subSubCategoryDao.getAll(finder: finder));
  }

  Future<dynamic> getAllSubSubCategoryListBySubCategoryId(
      StreamController<PsResource<List<SubSubCategory>>>
          subSubCategoryListStream,
      bool isConnectedToIntenet,
      PsStatus status,
      String subCategoryId,
      {bool isLoadFromServer = true}) async {
    final Finder finder =
        Finder(filter: Filter.equals('cat_id', subCategoryId));

    subSubCategoryListStream.sink
        .add(await _subSubCategoryDao.getAll(finder: finder, status: status));

    final PsResource<List<SubSubCategory>> _resource =
        await _psApiService.getAllSubSubCategoryList(subCategoryId);

    if (_resource.status == PsStatus.SUCCESS) {
      await _subSubCategoryDao.deleteWithFinder(finder);
      await _subSubCategoryDao.insertAll(_primaryKey, _resource.data);
    } else {
      if (_resource.errorCode == PsConst.ERROR_CODE_10001) {
        await _subSubCategoryDao.deleteWithFinder(finder);
      }
    }
    subSubCategoryListStream.sink
        .add(await _subSubCategoryDao.getAll(finder: finder));
  }

  Future<dynamic> getNextPageSubSubCategoryList(
      StreamController<PsResource<List<SubSubCategory>>>
          subSubCategoryListStream,
      bool isConnectedToIntenet,
      int limit,
      int offset,
      PsStatus status,
      String subCategoryId,
      {bool isLoadFromServer = true}) async {
    final Finder finder =
        Finder(filter: Filter.equals('cat_id', subCategoryId));
    subSubCategoryListStream.sink
        .add(await _subSubCategoryDao.getAll(finder: finder, status: status));

    final PsResource<List<SubSubCategory>> _resource =
        await _psApiService.getSubSubCategoryList(limit, offset, subCategoryId);

    if (_resource.status == PsStatus.SUCCESS) {
      _subSubCategoryDao
          .insertAll(_primaryKey, _resource.data)
          .then((dynamic data) async {
        subSubCategoryListStream.sink
            .add(await _subSubCategoryDao.getAll(finder: finder));
      });
    } else {
      subSubCategoryListStream.sink
          .add(await _subSubCategoryDao.getAll(finder: finder));
    }
  }
}
