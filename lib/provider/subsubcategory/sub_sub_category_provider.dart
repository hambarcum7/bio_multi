import 'dart:async';

import 'package:biomart/api/common/ps_resource.dart';
import 'package:biomart/api/common/ps_status.dart';
import 'package:biomart/provider/common/ps_provider.dart';
import 'package:biomart/repository/sub_sub_category_repository.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/common/ps_value_holder.dart';
import 'package:biomart/viewobject/holder/product_parameter_holder.dart';
import 'package:biomart/viewobject/sub_sub_category.dart';
import 'package:flutter/material.dart';

class SubSubCategoryProvider extends PsProvider {
  SubSubCategoryProvider(
      {@required SubSubCategoryRepository repo, int limit = 0, this.psValueHolder})
      : super(repo, limit) {
    _repo = repo;
    print('SubCategory Provider: $hashCode');

    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
    });

    subSubCategoryListStream =
        StreamController<PsResource<List<SubSubCategory>>>.broadcast();
    subscription = subSubCategoryListStream.stream.listen((dynamic resource) {
      updateOffset(resource.data.length);

      _subSubCategoryList = resource;

      if (resource.status != PsStatus.BLOCK_LOADING &&
          resource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });
  }

  StreamController<PsResource<List<SubSubCategory>>> subSubCategoryListStream;
  SubSubCategoryRepository _repo;
  PsValueHolder psValueHolder;

  PsResource<List<SubSubCategory>> _subSubCategoryList =
      PsResource<List<SubSubCategory>>(PsStatus.NOACTION, '', <SubSubCategory>[]);

  PsResource<List<SubSubCategory>> get subSubCategoryList => _subSubCategoryList;
  StreamSubscription<PsResource<List<SubSubCategory>>> subscription;

  String subCategoryId = '';

  ProductParameterHolder subSubCategoryBySubCatIdParamenterHolder =
      ProductParameterHolder().getSubSubCategoryBySubCatIdParameterHolder();

  @override
  void dispose() {
    subscription.cancel();
    isDispose = true;
    print('SubCategory Provider Dispose: $hashCode');
    super.dispose();
  }

  Future<dynamic> loadSubSubCategoryList(String subCategoryId) async {
    isLoading = true;
    isConnectedToInternet = await Utils.checkInternetConnectivity();
    await _repo.getSubSubCategoryListBySubCategoryId(
        subSubCategoryListStream,
        isConnectedToInternet,
        //limit,
        200,
        offset,
        PsStatus.PROGRESS_LOADING,
        subCategoryId);
  }

  Future<dynamic> loadAllSubSubCategoryList(String subCategoryId) async {
    isLoading = true;
    isConnectedToInternet = await Utils.checkInternetConnectivity();
    await _repo.getAllSubSubCategoryListBySubCategoryId(subSubCategoryListStream,
        isConnectedToInternet, PsStatus.PROGRESS_LOADING, subCategoryId);
  }

  Future<dynamic> nextSubSubCategoryList(String subCategoryId) async {
    isConnectedToInternet = await Utils.checkInternetConnectivity();
    if (!isLoading && !isReachMaxData) {
      super.isLoading = true;

      await _repo.getNextPageSubSubCategoryList(
          subSubCategoryListStream,
          isConnectedToInternet,
          limit,
          offset,
          PsStatus.PROGRESS_LOADING,
          subCategoryId);
    }
  }

  Future<void> resetSubSubCategoryList(String subCategoryId) async {
    isConnectedToInternet = await Utils.checkInternetConnectivity();

    isLoading = true;

    updateOffset(0);

    await _repo.getSubSubCategoryListBySubCategoryId(
        subSubCategoryListStream,
        isConnectedToInternet,
        limit,
        offset,
        PsStatus.PROGRESS_LOADING,
        subCategoryId);

    isLoading = false;
  }
}
