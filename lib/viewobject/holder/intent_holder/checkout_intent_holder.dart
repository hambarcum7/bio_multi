import 'package:biomart/provider/basket/basket_provider.dart';
import 'package:biomart/provider/coupon_discount/coupon_discount_provider.dart';
import 'package:biomart/viewobject/basket.dart';
import 'package:flutter/cupertino.dart';

class CheckoutIntentHolder {
  const CheckoutIntentHolder({
    @required this.basketList,
    @required this.basketProvider,
  });
  final List<Basket> basketList;
  final BasketProvider basketProvider;
}
