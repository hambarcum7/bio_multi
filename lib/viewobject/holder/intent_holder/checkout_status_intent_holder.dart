import 'package:biomart/viewobject/transaction_header.dart';
import 'package:flutter/cupertino.dart';

class CheckoutStatusIntentHolder {
  const CheckoutStatusIntentHolder({
    @required this.transactionHeader,
  });

  final TransactionHeader transactionHeader;
}
