import 'package:biomart/viewobject/customized_detail.dart';
import 'package:biomart/viewobject/product.dart';
import 'package:flutter/material.dart';

class AttributeDetailIntentHolder {
  const AttributeDetailIntentHolder({
    @required this.product,
    @required this.attributeDetail,
  });
  final Product product;
  final List<CustomizedDetail> attributeDetail;
}
