import 'package:biomart/viewobject/holder/shop_parameter_holder.dart';
import 'package:flutter/material.dart';

class ShopListIntentHolder {
  const ShopListIntentHolder({
    @required this.shopParameterHolder,
    @required this.appBarTitle,
  });
  final ShopParameterHolder shopParameterHolder;
  final String appBarTitle;
}
