import 'dart:ui';

class Language {
  Language({
    this.languageCode,
    this.countryCode,
    this.name,
    this.flagPath,
  });
  final String languageCode;
  final String countryCode;
  final String name;
  final String flagPath;

  Locale toLocale() {
    return Locale(languageCode, countryCode);
  }

  @override
  bool operator ==(other) {
    return languageCode == other.languageCode &&
        countryCode == other.countryCode;
  }

  @override
  // TODO: implement hashCode
  int get hashCode => super.hashCode;
}
