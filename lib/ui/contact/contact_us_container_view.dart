import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/ui/contact/contact_us_view.dart';
import 'package:biomart/ui/dashboard/view_pages/call_bottom_view.dart';
import 'package:biomart/utils/app_bar_widget.dart';
import 'package:biomart/utils/utils.dart';
import 'package:flutter/material.dart';

class ContactUsContainerView extends StatefulWidget {
  ContactUsContainerView({
    Key key,
    this.isPhoneChecked = false,
  }) : super(key: key);
  bool isPhoneChecked;
  @override
  _ContactUsContainerViewState createState() => _ContactUsContainerViewState();
}

class _ContactUsContainerViewState extends State<ContactUsContainerView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print(
        '............................Build UI Again ............................');
    return Scaffold(
      bottomNavigationBar: const BottomBarViewWidget(
        currentIdex: 2,
      ),
      backgroundColor: PsColors.coreBackgroundColor,
      appBar: customAppBar(
        title: Utils.getString(context, 'home__menu_drawer_contact_us'),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back_ios,
            size: 17.0,
            color: PsColors.textPrimaryColor,
          ),
        ),
      ),
      body: Container(
        color: PsColors.coreBackgroundColor,
        height: double.infinity,
        child: ContactUsView(
          animationController: animationController,
          isPhoneChecked: widget.isPhoneChecked,
        ),
      ),
    );
  }
}
