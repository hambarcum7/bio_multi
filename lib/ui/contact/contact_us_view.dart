import 'package:biomart/api/common/ps_resource.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/provider/contact/contact_us_provider.dart';
import 'package:biomart/repository/contact_us_repository.dart';
import 'package:biomart/ui/common/dialog/error_dialog.dart';
import 'package:biomart/ui/common/dialog/success_dialog.dart';
import 'package:biomart/ui/common/ps_button_widget.dart';
import 'package:biomart/ui/common/ps_textfield_widget.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/api_status.dart';
import 'package:biomart/viewobject/holder/contact_us_holder.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ContactUsView extends StatefulWidget {
  ContactUsView({
    Key key,
    @required this.animationController,
    this.isPhoneChecked,
  }) : super(key: key);
  final AnimationController animationController;
  bool isPhoneChecked = false;
  @override
  _ContactUsViewState createState() => _ContactUsViewState();
}

class _ContactUsViewState extends State<ContactUsView> {
  ContactUsRepository contactUsRepo;
  TextEditingController nameController;
  TextEditingController emailController;
  TextEditingController phoneController;
  TextEditingController messageController;

  @override
  void initState() {
    nameController = TextEditingController();
    messageController = TextEditingController();
    widget.isPhoneChecked
        ? phoneController = TextEditingController()
        : emailController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Animation<double> animation = Tween<double>(begin: 0.0, end: 1.0)
        .animate(CurvedAnimation(
            parent: widget.animationController,
            curve: const Interval(0.5 * 1, 1.0, curve: Curves.fastOutSlowIn)));
    widget.animationController.forward();
    contactUsRepo = Provider.of<ContactUsRepository>(context);
    const Widget _largeSpacingWidget = SizedBox(
      height: PsDimens.space8,
    );
    return ChangeNotifierProvider<ContactUsProvider>(
        lazy: false,
        create: (BuildContext context) {
          final ContactUsProvider contactUsProvide =
              ContactUsProvider(repo: contactUsRepo);
          return contactUsProvide;
        },
        child: Consumer<ContactUsProvider>(
          builder:
              (BuildContext context, ContactUsProvider provider, Widget child) {
            return AnimatedBuilder(
                animation: widget.animationController,
                child: SingleChildScrollView(
                    child: Container(
                  padding: const EdgeInsets.all(PsDimens.space8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      PsTextFieldWidget(
                          titleText: Utils.getString(
                              context, 'contact_us__contact_name'),
                          textAboutMe: false,
                          hintText: Utils.getString(
                              context, 'contact_us__contact_name_hint'),
                          textEditingController: nameController),
                      if (!widget.isPhoneChecked)
                        PsTextFieldWidget(
                            titleText: Utils.getString(
                                context, 'contact_us__contact_email'),
                            textAboutMe: false,
                            hintText: Utils.getString(
                                context, 'contact_us__contact_email_hint'),
                            textEditingController: emailController),
                      if (widget.isPhoneChecked)
                        PsTextFieldWidget(
                            titleText: Utils.getString(
                                context, 'contact_us__contact_phone'),
                            textAboutMe: false,
                            hintText: Utils.getString(
                                context, 'contact_us__contact_phone_hint'),
                            keyboardType: TextInputType.phone,
                            phoneInputType: true,
                            textEditingController: phoneController),
                      PsTextFieldWidget(
                          titleText: Utils.getString(
                              context, 'contact_us__contact_message'),
                          textAboutMe: false,
                          height: PsDimens.space160,
                          hintText: Utils.getString(
                              context, 'contact_us__contact_message_hint'),
                          textEditingController: messageController),
                      _largeSpacingWidget,
                      Center(
                        child: PsButtonWidget(
                          provider: provider,
                          nameText: nameController,
                          emailText: emailController,
                          messageText: messageController,
                          phoneText: phoneController,
                        ),
                      ),
                      _largeSpacingWidget,
                    ],
                  ),
                )),
                builder: (BuildContext context, Widget child) {
                  return FadeTransition(
                      opacity: animation,
                      child: Transform(
                        transform: Matrix4.translationValues(
                            0.0, 100 * (1.0 - animation.value), 0.0),
                        child: child,
                      ));
                });
          },
        ));
  }
}

class PsButtonWidget extends StatelessWidget {
  const PsButtonWidget({
    @required this.nameText,
    @required this.emailText,
    @required this.messageText,
    @required this.phoneText,
    @required this.provider,
  });

  final TextEditingController nameText, emailText, messageText, phoneText;
  final ContactUsProvider provider;

  @override
  Widget build(BuildContext context) {
    return PSButtonWidget(
        hasShadow: true,
        width: double.infinity,
        titleText: Utils.getString(context, 'contact_us__submit'),
        onPressed: () async {
          if (nameText.text != '' &&
              emailText?.text != '' &&
              messageText.text != '' &&
              phoneText?.text != '') {
            if (await Utils.checkInternetConnectivity()) {
              final ContactUsParameterHolder contactUsParameterHolder =
                  ContactUsParameterHolder(
                name: nameText.text,
                email: emailText == null ? '.' : emailText.text,
                message: messageText.text,
                phone: phoneText == null ? '' : phoneText.text,
              );

              final PsResource<ApiStatus> _apiStatus = await provider
                  .postContactUs(contactUsParameterHolder.toMap());

              if (_apiStatus.data != null) {
                print('Success');
                nameText.clear();
                emailText?.clear();
                messageText.clear();
                phoneText?.clear();
                showDialog<dynamic>(
                    context: context,
                    builder: (BuildContext context) {
                      if (_apiStatus.data.status == 'success') {
                        return SuccessDialog(
                          message: _apiStatus.data.status,
                        );
                      } else {
                        return ErrorDialog(
                          message: _apiStatus.data.status,
                        );
                      }
                    });
              }
            } else {
              showDialog<dynamic>(
                  context: context,
                  builder: (BuildContext context) {
                    return ErrorDialog(
                      message:
                          Utils.getString(context, 'error_dialog__no_internet'),
                    );
                  });
            }
          } else {
            print('Fail');
            showDialog<dynamic>(
                context: context,
                builder: (BuildContext context) {
                  return ErrorDialog(
                    message: Utils.getString(context, 'contact_us__fail'),
                  );
                });
          }
        });
  }
}
