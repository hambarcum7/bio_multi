import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/viewobject/sub_category.dart';
import 'package:flutter/material.dart';

class SubCategoryVerticalListItem extends StatefulWidget {
  SubCategoryVerticalListItem({
    Key key,
    @required this.subCategory,
    this.onTap,
    this.boxDecoration,
  }) : super(key: key);

  final SubCategory subCategory;
  final Function onTap;
  BoxDecoration boxDecoration;

  @override
  State<SubCategoryVerticalListItem> createState() =>
      _SubCategoryVerticalListItemState();
}

class _SubCategoryVerticalListItemState
    extends State<SubCategoryVerticalListItem> {
  bool isSelected = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Container(
        // height: 37.0,
        alignment: Alignment.center,
        margin: const EdgeInsets.symmetric(
          horizontal: 8.0,
        ),
        padding: const EdgeInsets.symmetric(
          horizontal: 16.0,
        ),
        decoration: widget.boxDecoration,
        child: Text(
          widget.subCategory.name,
          textAlign: TextAlign.start,
          style: Theme.of(context).textTheme.bodyText2.copyWith(
                fontWeight: FontWeight.w400,
                color: PsColors.white,
                fontSize: 18.0,
              ),
        ),
      ),
    );
    // return GestureDetector(
    //     onTap: onTap,
    //     child: Card(
    //       elevation: 0.3,
    //       margin: const EdgeInsets.symmetric(
    //           horizontal: PsDimens.space16, vertical: PsDimens.space4),
    //       child: Container(
    //         padding: const EdgeInsets.all(PsDimens.space16),
    //         child: Row(
    //           children: <Widget>[
    //             const SizedBox(
    //               height: PsDimens.space4,
    //             ),
    //             PsNetworkImage(
    //               photoKey: '',
    //               defaultPhoto: subCategory.defaultPhoto,
    //               width: PsDimens.space44,
    //               height: PsDimens.space44,
    //               onTap: () {
    //                 Utils.psPrint(subCategory.defaultPhoto.imgParentId);
    //               },
    //             ),
    //             const SizedBox(
    //               height: PsDimens.space8,
    //             ),
    //             Column(
    //               crossAxisAlignment: CrossAxisAlignment.start,
    //               children: <Widget>[
    //                 const SizedBox(
    //                   height: PsDimens.space4,
    //                 ),
    //                 Text(
    //                   subCategory.name,
    //                   textAlign: TextAlign.start,
    //                   style: TextStyle(
    //                       fontWeight: Theme.of(context)
    //                           .textTheme
    //                           .bodyText2
    //                           .copyWith(fontWeight: FontWeight.bold)
    //                           .fontWeight),
    //                 ),
    //                 const SizedBox(
    //                   height: PsDimens.space4,
    //                 ),
    //                 Text(subCategory.addedDate,
    //                     maxLines: 2, overflow: TextOverflow.ellipsis),
    //                 const SizedBox(
    //                   height: PsDimens.space4,
    //                 ),
    //               ],
    //             )
    //           ],
    //         ),
    //       ),
    //     ));
  }
}
