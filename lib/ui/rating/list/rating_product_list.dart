import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_constants.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/constant/route_paths.dart';
import 'package:biomart/provider/product/product_provider.dart';
import 'package:biomart/provider/rating/rating_provider.dart';
import 'package:biomart/repository/product_repository.dart';
import 'package:biomart/repository/rating_repository.dart';
import 'package:biomart/ui/common/base/ps_widget_custom_class.dart';
import 'package:biomart/ui/common/dialog/error_dialog.dart';
import 'package:biomart/ui/common/ps_button_widget.dart';
import 'package:biomart/ui/rating/entry/rating_input_dialog.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/common/ps_value_holder.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../item/rating_list_item.dart';

class ProductRatingListView extends StatefulWidget {
  const ProductRatingListView({
    Key key,
    @required this.productDetailid,
  }) : super(key: key);

  final String productDetailid;
  @override
  _ProductRatingListViewState createState() => _ProductRatingListViewState();
}

class _ProductRatingListViewState extends State<ProductRatingListView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  RatingRepository ratingRepo;
  RatingProvider ratingProvider;
  ProductDetailProvider productDetailProvider;
  ProductRepository productRepository;
  PsValueHolder psValueHolder;

  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  bool isConnectedToInternet = false;
  bool isSuccessfullyLoaded = true;

  void checkConnection() {
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
      if (isConnectedToInternet && PsConfig.showAdMob) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!isConnectedToInternet && PsConfig.showAdMob) {
      print('loading ads....');
      checkConnection();
    }
    
    ratingRepo = Provider.of<RatingRepository>(context);
    productRepository = Provider.of<ProductRepository>(context);
    psValueHolder = Provider.of<PsValueHolder>(context);
    return WidgetWithrWithTwoProvider<RatingProvider, ProductDetailProvider>(
      initProvider1: () {
        ratingProvider = RatingProvider(repo: ratingRepo);
        return ratingProvider;
      },
      onProviderReady1: (RatingProvider provider) {
        provider.loadRatingList(widget.productDetailid);
      },
      initProvider2: () {
        productDetailProvider = ProductDetailProvider(
            repo: productRepository, psValueHolder: psValueHolder);
        return productDetailProvider;
      },
      onProviderReady2: (ProductDetailProvider productDetailProvider) {
        productDetailProvider.loadProduct(
            widget.productDetailid, psValueHolder.loginUserId);
      },
      child: Consumer<RatingProvider>(
        builder: (BuildContext context, RatingProvider ratingProvider,
            Widget child) {
          return Container(
            color: PsColors.coreBackgroundColor,
            child: RefreshIndicator(
              child: Column(
                children: <Widget>[
                  Container(
                    height: 30,
                    child: HeaderWidget(
                      productDetailId: widget.productDetailid,
                      ratingProvider: ratingProvider,
                    ),
                  ),
               
                    Container(
                      height: PsDimens.space200,
                      child: ListView.builder(
                        itemCount: ratingProvider.ratingList.data.length,
                        scrollDirection: Axis.horizontal,
                        physics: const PageScrollPhysics(),
                        itemBuilder: (BuildContext context, int index) {
                          return RatingListItem(
                            rating: ratingProvider.ratingList.data[index],
                            onTap: () {},
                          );
                        },
                      ),
                    ),
                    
                  Container(
                    margin: const EdgeInsets.all(2),
                    child: _WriteReviewButtonWidget(
                      productprovider: productDetailProvider,
                      ratingProvider: ratingProvider,
                      productId: widget.productDetailid,
                    ),
                  ),
                ],
              ),
              onRefresh: () {
                return ratingProvider
                    .refreshRatingList(widget.productDetailid);
              },
            ),
          );
        },
      ),
    );
  }
}

class HeaderWidget extends StatefulWidget {
  const HeaderWidget({
    Key key,
    @required this.productDetailId,
    @required this.ratingProvider,
  }) : super(key: key);
  final String productDetailId;
  final RatingProvider ratingProvider;

  @override
  _HeaderWidgetState createState() => _HeaderWidgetState();
}

class _HeaderWidgetState extends State<HeaderWidget> {
  ProductRepository repo;
  PsValueHolder psValueHolder;

  @override
  Widget build(BuildContext context) {
    repo = Provider.of<ProductRepository>(context);
    psValueHolder = Provider.of<PsValueHolder>(context);
    dynamic result;

    return Consumer<ProductDetailProvider>(builder: (BuildContext context,
        ProductDetailProvider productDetailProvider, Widget child) {
      if (productDetailProvider.productDetail != null &&
          productDetailProvider.productDetail.data != null &&
          productDetailProvider.productDetail.data.ratingDetail != null) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Container(
            color: PsColors.coreBackgroundColor,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  '${productDetailProvider.productDetail.data.ratingDetail.totalRatingCount} ${Utils.getString(context, 'rating_list__customer_reviews')}',
                  style: TextStyle(
                    color: PsColors.textPrimaryColor,
                    fontSize: PsDimens.space16,
                  ),
                ),
                InkWell(
                  onTap: () async {
                    result = await Navigator.pushNamed(
                        context, RoutePaths.ratingList,
                        arguments: productDetailProvider.productDetail.data.id);

                    if (result != null && result) {
                      setState(() {
                        productDetailProvider.loadProduct(
                            productDetailProvider.productDetail.data.id,
                            productDetailProvider.psValueHolder.loginUserId);
                      });
                    }
                  },
                  child: Row(
                    children: <Widget>[
                      InkWell(
                        onTap: () async {
                          result = await Navigator.pushNamed(
                              context, RoutePaths.ratingList,
                              arguments:
                                  productDetailProvider.productDetail.data.id);

                          if (result != null && result) {
                            productDetailProvider.loadProduct(
                                productDetailProvider.productDetail.data.id,
                                productDetailProvider
                                    .psValueHolder.loginUserId);
                          }
                        },
                        child: Text(
                          ' ${Utils.getString(context, 'customer_review__view_all')}',
                          style: TextStyle(
                            color: PsColors.textPrimaryColor,
                            fontSize: PsDimens.space16,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      } else {
        return Container(
          color: PsColors.coreBackgroundColor,
        );
      }
    });
  }
}

// class _RatingWidget extends StatelessWidget {
//   const _RatingWidget({
//     Key key,
//     @required this.starCount,
//     @required this.value,
//     @required this.percentage,
//   }) : super(key: key);

//   final String starCount;
//   final double value;
//   final String percentage;

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       margin: const EdgeInsets.only(top: PsDimens.space4),
//       width: MediaQuery.of(context).size.width,
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         mainAxisSize: MainAxisSize.max,
//         children: <Widget>[
//           Text(
//             starCount,
//             style: Theme.of(context).textTheme.subtitle2,
//           ),
//           const SizedBox(
//             width: PsDimens.space12,
//           ),
//           Expanded(
//             flex: 5,
//             child: LinearProgressIndicator(
//               value: value,
//             ),
//           ),
//           const SizedBox(
//             width: PsDimens.space12,
//           ),
//           Container(
//             width: PsDimens.space68,
//             child: Text(
//               percentage,
//               style: Theme.of(context).textTheme.bodyText2,
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }

class _WriteReviewButtonWidget extends StatelessWidget {
  const _WriteReviewButtonWidget({
    Key key,
    @required this.productprovider,
    @required this.ratingProvider,
    @required this.productId,
  }) : super(key: key);

  final ProductDetailProvider productprovider;
  final RatingProvider ratingProvider;
  final String productId;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: PSButtonWidget(
        hasShadow: true,
        width: double.infinity,
        titleText: Utils.getString(context, 'rating_list__write_review'),
        onPressed: () async {
          if (await Utils.checkInternetConnectivity()) {
            Utils.navigateOnUserVerificationView(context, () async {
              await showDialog<dynamic>(
                  context: context,
                  builder: (BuildContext context) {
                    return RatingInputDialog(
                        productprovider: productprovider,
                        flag: PsConst.PRODUCT_RATING);
                  });

              ratingProvider.refreshRatingList(productId);
              await productprovider.loadProduct(
                  productId, productprovider.psValueHolder.loginUserId);
            });
          } else {
            showDialog<dynamic>(
                context: context,
                builder: (BuildContext context) {
                  return ErrorDialog(
                    message:
                        Utils.getString(context, 'error_dialog__no_internet'),
                  );
                });
          }
        },
      ),
    );
  }
}
