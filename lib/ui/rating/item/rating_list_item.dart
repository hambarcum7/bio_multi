import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/ui/common/ps_ui_widget.dart';
import 'package:biomart/ui/common/smooth_star_rating_widget.dart';
import 'package:biomart/viewobject/rating.dart';
import 'package:flutter/material.dart';

class RatingListItem extends StatelessWidget {
  const RatingListItem({
    Key key,
    @required this.rating,
    this.onTap,
  }) : super(key: key);

  final Rating rating;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.symmetric(
          horizontal: PsDimens.space10,
          vertical: PsDimens.space10,
        ),
        width: MediaQuery.of(context).size.width * 0.95,
        decoration: BoxDecoration(
          color: PsColors.greyColorForCustomWidget,
          borderRadius: BorderRadius.circular(PsDimens.space12),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            ImageAndTextWidget(
              rating: rating,
            ),
            _RatingListDataWidget(
              rating: rating,
            ),
          ],
        ),
      ),
    );
  }
}

class _RatingListDataWidget extends StatelessWidget {
  const _RatingListDataWidget({
    Key key,
    @required this.rating,
  }) : super(key: key);

  final Rating rating;

  @override
  Widget build(BuildContext context) {
    const Widget _spacingWidget = SizedBox(
      height: PsDimens.space8,
    );
    final Widget _titleTextWidget = Text(
      rating.title,
      style: TextStyle(
        color: PsColors.textPrimaryColor,
        fontSize: PsDimens.space18,
        fontWeight: FontWeight.w600,
      ),
    );
    final Widget _descriptionTextWidget = Text(
      rating.description,
      maxLines: 3,
      overflow: TextOverflow.ellipsis,
      style: TextStyle(
        color: PsColors.textPrimaryColor,
        fontSize: PsDimens.space15,
        fontWeight: FontWeight.w500,
      ),
    );
    return Container(
      height: PsDimens.space100,
      padding: const EdgeInsets.all(PsDimens.space12),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            _spacingWidget,
            _titleTextWidget,
            _spacingWidget,
            _descriptionTextWidget,
          ],
        ),
      ),
    );
  }
}

class ImageAndTextWidget extends StatelessWidget {
  const ImageAndTextWidget({
    Key key,
    @required this.rating,
  }) : super(key: key);

  final Rating rating;

  @override
  Widget build(BuildContext context) {
    final Widget _ratingStarsWidget = SmoothStarRating(
      key: Key(rating.rating),
      rating: double.parse(rating.rating),
      isReadOnly: true,
      allowHalfRating: false,
      starCount: 5,
      size: PsDimens.space16,
      color: PsColors.ratingColor,
      borderColor: PsColors.grey,
      spacing: 0.0,
    );
    final Widget _imageWidget = ClipRRect(
      borderRadius: BorderRadius.circular(50),
      child: Container(
        width: PsDimens.space44,
        height: PsDimens.space44,
        child: PsNetworkImageWithUrl(
          photoKey: '',
          imagePath: rating.user.userProfilePhoto,
        ),
      ),
    );
    final Widget _personNameTextWidget = Text(
      rating.user.userName,
      style: TextStyle(
          fontSize: PsDimens.space14,
          color: PsColors.textPrimaryColor,
          fontWeight: FontWeight.w500),
    );

    final Widget _timeWidget = Text(
      rating.addedDateStr,
      style: TextStyle(
        fontSize: PsDimens.space12,
        color: PsColors.textPrimaryColor,
        fontWeight: FontWeight.w400,
      ),
    );
    return Padding(
      padding: const EdgeInsets.all(PsDimens.space12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              _imageWidget,
              const SizedBox(
                width: PsDimens.space15,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _personNameTextWidget,
                  const SizedBox(
                    height: PsDimens.space8,
                  ),
                  _timeWidget,
                ],
              ),
            ],
          ),
          _ratingStarsWidget,
        ],
      ),
    );
  }
}
