import 'dart:async';
import 'dart:io';
import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/constant/route_paths.dart';
import 'package:biomart/provider/common/notification_provider.dart';
import 'package:biomart/provider/product/favourite_product_provider.dart';
import 'package:biomart/provider/user/user_provider.dart';
import 'package:biomart/repository/Common/notification_repository.dart';
import 'package:biomart/repository/product_repository.dart';
import 'package:biomart/repository/user_repository.dart';
import 'package:biomart/ui/common/base/ps_widget_with_multi_provider.dart';
import 'package:biomart/ui/common/ps_ui_widget.dart';
import 'package:biomart/ui/dashboard/view_pages/call_bottom_view.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/common/ps_value_holder.dart';
import 'package:biomart/viewobject/holder/intent_holder/blog_intent_holder.dart';
import 'package:biomart/viewobject/holder/intent_holder/privacy_policy_intent_holder.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingView extends StatefulWidget {
  const SettingView({
    Key key,
    @required this.animationController,
    @required this.userId,
    //@required this.currentIndex,
  }) : super(key: key);
  final AnimationController animationController;
  final String userId;
  //final int currentIndex;
  @override
  _SettingViewState createState() => _SettingViewState();
}

class _SettingViewState extends State<SettingView> {
  bool isConnectedToInternet = false;
  bool isSuccessfullyLoaded = true;
  bool isReadyToShowAppBarIcons = false;
  bool isSwitched = true;
  PsValueHolder valueHolder;
  ProductRepository repo1;
  NotificationRepository notificationRepository;
  NotificationProvider notiProvider;
  UserRepository userRepository;
  FavouriteProductProvider _favouriteProductProvider;
  // final FirebaseMessaging _fcm = FirebaseMessaging.instance;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  bool isVisible = false;

  void checkConnection() {
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
      if (isConnectedToInternet && PsConfig.showAdMob) {
        setState(() {});
      }
    });
  }

  void awaitMoreSeconds() {
    if (widget.userId != null && widget.userId != '') {
      setState(() {});
    }
  }

  @override
  void initState() {
    //awaitMoreSeconds();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    repo1 = Provider.of<ProductRepository>(context);
    userRepository = Provider.of<UserRepository>(context);
    valueHolder = Provider.of<PsValueHolder>(context);
    notificationRepository = Provider.of<NotificationRepository>(context);

    final Animation<double> animation = Tween<double>(begin: 0.0, end: 1.0)
        .animate(CurvedAnimation(
            parent: widget.animationController,
            curve: const Interval(0.5 * 1, 1.0, curve: Curves.fastOutSlowIn)));
    widget.animationController.forward();

    if (!isConnectedToInternet && PsConfig.showAdMob) {
      print('loading ads....');
      checkConnection();
    }
    if (!isReadyToShowAppBarIcons) {
      Timer(const Duration(milliseconds: 800), () {
        setState(() {
          isReadyToShowAppBarIcons = true;
        });
      });
    }

    return PsWidgetWithMultiProvider(
      child: MultiProvider(
        providers: <SingleChildWidget>[
          ChangeNotifierProvider<NotificationProvider>(
            lazy: false,
            create: (BuildContext context) {
              final NotificationProvider provider = NotificationProvider(
                  repo: notificationRepository, psValueHolder: valueHolder);
              if (provider.psValueHolder.notiSetting != null) {
                isSwitched = provider.psValueHolder.notiSetting;
              }
              if (provider.psValueHolder.deviceToken == null ||
                  provider.psValueHolder.deviceToken == '') {
                final FirebaseMessaging _fcm = FirebaseMessaging.instance;
                Utils.saveDeviceToken(_fcm, provider);
              } else {
                print(
                    'Notification Token is already registered. Notification Setting : true.');
              }
              notiProvider = provider;
              return provider;
            },
          ),
          ChangeNotifierProvider<FavouriteProductProvider>(
              create: (BuildContext context) {
            final FavouriteProductProvider provider = FavouriteProductProvider(
                repo: repo1, psValueHolder: valueHolder);
            provider.loadFavouriteProductList();
            _favouriteProductProvider = provider;

            return _favouriteProductProvider;
          }),
        ],
        child: Consumer<FavouriteProductProvider>(
          builder: (BuildContext context, FavouriteProductProvider provider,
              Widget child) {
            return ChangeNotifierProvider<UserProvider>(
              lazy: false,
              create: (BuildContext context) {
                final UserProvider provider = UserProvider(
                    repo: userRepository, psValueHolder: valueHolder);
                if (provider.psValueHolder.loginUserId == null ||
                    provider.psValueHolder.loginUserId == '') {
                  provider.getUser(widget.userId);
                } else {
                  provider.getUser(provider.psValueHolder.loginUserId);
                }
                return provider;
              },
              child: Consumer<UserProvider>(
                builder: (
                  BuildContext context,
                  UserProvider provider,
                  Widget child,
                ) {
                  return Scaffold(
                    key: scaffoldKey,
                    bottomNavigationBar: BottomBarViewWidget(
                        currentIdex: 2), // Don't set const keyword here
                    backgroundColor: PsColors.coreBackgroundColor,
                    // appBar: customAppBar(
                    //   title:
                    //       Utils.getString(context, 'setting__toolbar_name'),
                    // ),
                    body: AnimatedBuilder(
                      animation: widget.animationController,
                      child: CustomScrollView(
                        shrinkWrap: true,
                        slivers: <Widget>[
                          // SliverAppBar(
                          //   pinned: true,
                          //   floating: false,
                          //   snap: false,
                          //   automaticallyImplyLeading: false,
                          //   expandedHeight: 180.0,
                          //   flexibleSpace: FlexibleSpaceBar(
                          //     background: Image.network(
                          //       'https://i.imgur.com/sfRcRTQ.jpeg',
                          //       fit: BoxFit.cover,
                          //     ),
                          //   ),
                          //   backgroundColor: PsColors.mainColor,
                          //   toolbarHeight: 0,
                          //   leading: PsBackButtonWithCircleBgWidget(
                          //     //TODO poxvelua
                          //     isReadyToShow: isReadyToShowAppBarIcons,
                          //   ),
                          //   title: Text(
                          //     Utils.getString(
                          //         context, 'setting__toolbar_name'),
                          //     style: TextStyle(
                          //       fontSize: 24,
                          //       fontWeight: FontWeight.w500,
                          //       color: PsColors.coreBackgroundColor,
                          //     ),
                          //   ),
                          // ),
                          SliverList(
                            delegate: SliverChildListDelegate(
                              <Widget>[
                                //LoginItem(userProvider: provider),
                                SizedBox(
                                    height: 20.0 +
                                        MediaQuery.of(context).viewPadding.top),
                                _ImageAndTextWidget(
                                  userProvider: provider,
                                  onTap: () {
                                    // Navigator.pushNamed(
                                    //   context,
                                    //   RoutePaths.editProfile,
                                    // );
                                    Navigator.pushNamed(
                                      context,
                                      RoutePaths.viewProfile,
                                    );
                                  },
                                ),
                                const SizedBox(height: 20.0),
                                Padding(
                                  padding: const EdgeInsets.only(left: 28.0),
                                  child: Text(
                                    Utils.getString(
                                        context, 'setting__toolbar_name'),
                                    style: TextStyle(
                                      color: PsColors.textPrimaryColor,
                                      fontSize: 22.0,
                                      fontWeight: FontWeight.w600,
                                      fontFamily:
                                          PsConfig.ps_default_font_family,
                                    ),
                                  ),
                                ),
                                const SizedBox(height: PsDimens.space10),
                                const Divider(
                                  height: 1,
                                ),
                                const SizedBox(height: PsDimens.space10),
                                SettingItem(
                                  title: Utils.getString(
                                      context, 'profile__favourite'),
                                  iconPath: CupertinoIcons.heart,
                                  trailingText: (_favouriteProductProvider !=
                                              null &&
                                          _favouriteProductProvider
                                                  .favouriteProductList !=
                                              null &&
                                          _favouriteProductProvider
                                                  ?.favouriteProductList
                                                  ?.data !=
                                              null)
                                      ? '${_favouriteProductProvider.favouriteProductList.data.length}'
                                      : '0',
                                  onItemTap: () {
                                    Navigator.pushNamed(context,
                                        RoutePaths.favouriteProductList);
                                  },
                                ),
                                const SizedBox(height: PsDimens.space8),
                                SettingItem(
                                  title: Utils.getString(
                                      context, 'home__menu_drawer_blog'),
                                  iconPath: CupertinoIcons.book,
                                  onItemTap: () {
                                    Navigator.pushNamed(
                                      context,
                                      RoutePaths.blogList,
                                      arguments: const BlogIntentHolder(
                                          noBlogListForShop: true),
                                    );
                                  },
                                ),
                                const SizedBox(height: PsDimens.space8),
                                if (provider.user != null &&
                                    provider.user.data != null)
                                  Column(
                                    children: <Widget>[
                                      SettingItem(
                                        title: Utils.getString(
                                            context, 'noti_list__toolbar_name'),
                                        iconPath: CupertinoIcons.bell,
                                        onItemTap: () {
                                          Navigator.pushNamed(
                                            context,
                                            RoutePaths.notiList,
                                          );
                                        },
                                      ),
                                      const SizedBox(height: PsDimens.space8),
                                    ],
                                  ),

                                SettingItem(
                                  title: Utils.getString(
                                      context, 'home__menu_drawer_language'),
                                  iconPath: CupertinoIcons.globe,
                                  onItemTap: () {
                                    Navigator.pushNamed(
                                      context,
                                      RoutePaths.languageList,
                                      arguments: scaffoldKey,
                                    );
                                  },
                                ),
                                const SizedBox(height: PsDimens.space8),
                                SettingItem(
                                  title: Utils.getString(
                                      context, 'setting__currency'),
                                  iconPath: CupertinoIcons.money_dollar_circle,
                                  onItemTap: () {},
                                ),
                                const SizedBox(height: PsDimens.space8),
                                CustomSwitchItem(
                                  onTitle: Utils.getString(
                                      context, 'setting__change_mode'),
                                  offTitle: Utils.getString(
                                      context, 'setting__change_mode_light'),
                                  onIconPath: CupertinoIcons.moon,
                                  offIconPath: CupertinoIcons.sun_min,
                                  initialValueIsOn: !Utils.isLightMode(context),
                                  onSwitchValueChanged: (bool value) {
                                    changeBrightness(context);
                                  },
                                ),
                                const SizedBox(height: PsDimens.space8),
                                if (provider.user != null &&
                                    provider.user.data != null)
                                  Column(
                                    children: <Widget>[
                                      SettingItem(
                                        title: Utils.getString(context,
                                            'more__your_transction_history'),
                                        iconPath: CupertinoIcons.time,
                                        onItemTap: () {
                                          Navigator.pushNamed(
                                            context,
                                            RoutePaths.transactionList,
                                          );
                                        },
                                      ),
                                      const SizedBox(height: PsDimens.space8),
                                    ],
                                  ),
                                SettingItem(
                                  title: Utils.getString(context,
                                      'home__menu_drawer_rate_this_app'),
                                  iconPath: CupertinoIcons.star,
                                  onItemTap: () {
                                    if (Platform.isIOS) {
                                      Utils.launchAppStoreURL(
                                          iOSAppId: PsConfig.iOSAppStoreId,
                                          writeReview: true);
                                    } else {
                                      Utils.launchURL();
                                    }
                                  },
                                ),
                                const SizedBox(height: PsDimens.space8),
                                SettingItem(
                                  title: Utils.getString(
                                      context, 'setting__privacy_policy'),
                                  iconPath: CupertinoIcons.doc_text,
                                  onItemTap: () {
                                    Navigator.pushNamed(
                                        context, RoutePaths.privacyPolicy,
                                        arguments: PrivacyPolicyIntentHolder(
                                            title: Utils.getString(context,
                                                'privacy_policy__toolbar_name'),
                                            description: ''));
                                  },
                                ),
                                const SizedBox(height: PsDimens.space8),
                                SettingItem(
                                  title: Utils.getString(
                                      context, 'setting__app_info'),
                                  iconPath: CupertinoIcons.info,
                                  onItemTap: () {
                                    Navigator.pushNamed(
                                      context,
                                      RoutePaths.appinfo,
                                    );
                                  },
                                ),
                                const SizedBox(height: PsDimens.space40),
                                // CustomSwitchItem(
                                //   ////////// callback-@ pti noric nayvi
                                //   title: Utils.getString(context,
                                //       'setting__notification_setting'),
                                //   onIconPath: 'assets/setting_icons/notification.png',
                                //   offIconPath: Icons.notifications_off,
                                //   initialValueIsOn: isSwitched,
                                //   onSwitchValueChanged: (bool value) {
                                //     setState(
                                //       () async {
                                //         isSwitched = value;
                                //         provider.psValueHolder.notiSetting =
                                //             value;
                                //         await provider
                                //             .replaceNotiSetting(value);
                                //       },
                                //     );
                                //     if (isSwitched == true) {
                                //       _fcm.subscribeToTopic('broadcast');
                                //       if (notiProvider.psValueHolder
                                //                   .deviceToken !=
                                //               null &&
                                //           notiProvider.psValueHolder
                                //                   .deviceToken !=
                                //               '') {
                                //         final NotiRegisterParameterHolder
                                //             notiRegisterParameterHolder =
                                //             NotiRegisterParameterHolder(
                                //           platformName: PsConst.PLATFORM,
                                //           deviceId: notiProvider
                                //               .psValueHolder.deviceToken,
                                //           loginUserId:
                                //               Utils.checkUserLoginId(
                                //                   notiProvider
                                //                       .psValueHolder),
                                //         );
                                //         notiProvider.rawRegisterNotiToken(
                                //           notiRegisterParameterHolder
                                //               .toMap(),
                                //         );
                                //       }
                                //     } else {
                                //       _fcm.unsubscribeFromTopic(
                                //           'broadcast');
                                //       if (notiProvider.psValueHolder
                                //                   .deviceToken !=
                                //               null &&
                                //           notiProvider.psValueHolder
                                //                   .deviceToken !=
                                //               '') {
                                //         final NotiUnRegisterParameterHolder
                                //             notiUnRegisterParameterHolder =
                                //             NotiUnRegisterParameterHolder(
                                //           platformName: PsConst.PLATFORM,
                                //           deviceId: notiProvider
                                //               .psValueHolder.deviceToken,
                                //           loginUserId:
                                //               Utils.checkUserLoginId(
                                //                   notiProvider
                                //                       .psValueHolder),
                                //         );
                                //         notiProvider.rawUnRegisterNotiToken(
                                //           notiUnRegisterParameterHolder
                                //               .toMap(),
                                //         );
                                //       }
                                //     }
                                //   },
                                // ),
                              ],
                            ),
                          ),

                          //_SettingPrivacyWidget(),
                          //SizedBox(height: PsDimens.space8),
                          //_SettingNotificationWidget(),
                          //SizedBox(height: PsDimens.space8),
                          //_IntroSliderWidget(),
                          //SizedBox(height: PsDimens.space8),
                          //_SettingDarkAndWhiteModeWidget(
                          //animationController: widget.animationController),
                          //SizedBox(height: PsDimens.space8),
                          //_SettingAppInfoWidget(),
                          //SizedBox(height: PsDimens.space8),
                          //_SettingAppVersionWidget(),
                          // PsAdMobBannerWidget(
                          //   admobSize: NativeAdmobType.full,
                          // ),
                        ],
                      ),

                      //   floatingActionButton: FloatingActionButton(
                      //     onPressed: () {
                      //       Navigator.pushNamed(
                      //         context,
                      //         RoutePaths.contactUs,
                      //       );
                      //     },
                      //     backgroundColor: PsColors.mainColor,
                      //     child: Padding(
                      //       padding: const EdgeInsets.all(17.0),
                      //       child:
                      //           Image.asset('assets/setting_icons/message.png'),
                      //     ),
                      //   ),
                      // ),

                      builder: (BuildContext context, Widget child) {
                        return FadeTransition(
                          opacity: animation,
                          child: Transform(
                            transform: Matrix4.translationValues(
                                0.0, 100 * (1.0 - animation.value), 0.0),
                            child: child,
                          ),
                        );
                      },
                    ),
                    floatingActionButton: Stack(
                      children: <Widget>[
                        Positioned(
                          bottom: 60.0,
                          right: 10,
                          child: FloatingActionButton(
                            onPressed: () {
                              setState(() {
                                isVisible = !isVisible;
                              });
                            },
                            backgroundColor: PsColors.mainColor,
                            child: isVisible
                                ? const Icon(
                                    Icons.close,
                                  )
                                : Image.asset(
                                    'assets/setting_icons/message.png',
                                    height: 22,
                                  ),
                          ),
                        ),
                        Positioned(
                          bottom: 70,
                          right: 100,
                          child: Visibility(
                            visible: isVisible,
                            child: FloatingActionButton(
                              onPressed: () async {
                                const String url =
                                    'https://t.me/${PsConfig.TELEGRAM_URL}';
                                
                                print('launc url:  $url');
                                if (await canLaunch(url)) {
                                  await launch(url);
                                } else {
                                  print('Could not launch $url');
                                }

                                setState(() {
                                  isVisible = false;
                                });
                                // Navigator.pushNamed(
                                //   context,
                                //   RoutePaths.contactUs,
                                //   arguments: true,
                                // );
                              },
                              backgroundColor: PsColors.mainColor,
                              child: Image.asset(
                                'assets/setting_icons/telegram.png',
                                height: 30,
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 135,
                          right: 35,
                          child: Visibility(
                            visible: isVisible,
                            child: FloatingActionButton(
                              onPressed: () async {
                                const String url =
                                    'mailto:Shamxalov001@mail.ru?subject=Report&body=';
                                if (await canLaunch(url)) {
                                  await launch(url);
                                } else {
                                  print('Could not launch $url');
                                }
                                setState(() {
                                  isVisible = false;
                                });
                                // Navigator.pushNamed(
                                //   context,
                                //   RoutePaths.contactUs,
                                // );
                              },
                              backgroundColor: PsColors.mainColor,
                              child: Image.asset(
                                'assets/setting_icons/message_icon.png',
                                height: 35,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    floatingActionButtonLocation:
                        FloatingActionButtonLocation.miniEndDocked,
                  );
                },
              ),
            );
          },
        ),
      ),
    );
  }
}

// class MyFloatingActionButton extends StatefulWidget {
//   const MyFloatingActionButton({Key key}) : super(key: key);

//   @override
//   _MyFloatingActionButtonState createState() => _MyFloatingActionButtonState();
// }

// class _MyFloatingActionButtonState extends State<MyFloatingActionButton> {
//   @override
//   Widget build(BuildContext context) {
//     return Builder(
//       builder: (BuildContext context) => FabCircularMenu(
//         // Cannot be `Alignment.center`
//         alignment: Alignment.bottomRight,
//         ringColor: PsColors.mainColor,
//         ringDiameter: 328.0,
//         ringWidth: 70.0,
//         fabSize: 56.0,
//         //fabElevation: 8.0,
//         fabIconBorder: const CircleBorder(),
//         //fabOpenColor: PsColors.mainColor,
//         // fabCloseColor: Colors.white
//         // These properties take precedence over fabColor
//         fabColor: PsColors.mainColor,
//         fabOpenIcon: Padding(
//           padding: const EdgeInsets.all(17.0),
//           child: Image.asset('assets/setting_icons/message.png'),
//         ),
//         fabCloseIcon: Icon(Icons.close, color: PsColors.black),
//         fabMargin: const EdgeInsets.only(right: 35, bottom: 76),
//         animationDuration: const Duration(milliseconds: 400),
//         animationCurve: Curves.easeInOutCirc,
//         onDisplayChange: (bool isOpen) {},
//         children: <Widget>[
//           const SizedBox(),
//           InkWell(
//             customBorder: const CircleBorder(),
//             onTap: () {},
//             child: Icon(
//               Icons.phone_enabled,
//               color: PsColors.coreBackgroundColor,
//               size: 45,
//             ),
//           ),
//           const SizedBox(),
//           const SizedBox(),
//           InkWell(
//             customBorder: const CircleBorder(),
//             onTap: () {},
//             child: Icon(
//               Icons.g_mobiledata,
//               color: PsColors.coreBackgroundColor,
//               size: 45,
//             ),
//           ),
//           const SizedBox(),
//         ],
//       ),
//     );
//   }
// }

/// Animated FloatinActionButon without container
// class MyFloatingActionButton extends StatefulWidget {
//   const MyFloatingActionButton({
//     Key key,
//   }) : super(key: key);
//   @override
//   State<MyFloatingActionButton> createState() => _MyFloatingActionButtonState();
// }

// class _MyFloatingActionButtonState extends State<MyFloatingActionButton> {
//   bool _isShowDial = false;
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.only(bottom: 50),
//       child: SpeedDialMenuButton(
//         isSpeedDialFABsMini: true,
//         isShowSpeedDial: _isShowDial,
//         isMainFABMini: false,
//         paddingBtwSpeedDialButton: 75.0,
//         isEnableAnimation: true,
//         //manually open or close menu
//         updateSpeedDialStatus: (bool isShow) {
//           //return any open or close change within the widget
//           _isShowDial = isShow;
//         },
//         mainMenuFloatingActionButton: MainMenuFloatingActionButton(
//             //heroTag: 'somethingUnique',
//             onPressed: () {},
//             child: Image.asset(
//               'assets/setting_icons/message.png',
//               height: 22,
//             ),
//             closeMenuChild: Icon(
//               Icons.close,
//               color: PsColors.black,
//             ),
//             closeMenuBackgroundColor: PsColors.mainColor,
//             backgroundColor: PsColors.mainColor),
//         floatingActionButtonWidgetChildren: <FloatingActionButton>[
//           FloatingActionButton(
//             backgroundColor: PsColors.mainColor,
//             onPressed: () {
//               setState(() {
//                 _isShowDial = !_isShowDial;
//               });
//               Navigator.pushNamed(
//                 context,
//                 RoutePaths.contactUs,
//                 arguments: true,
//               );
//             },
//             child: const Icon(
//               Icons.phone_enabled,
//               size: 35.0,
//             ),
//           ),
//           FloatingActionButton(
//             onPressed: () {
//               setState(() {
//                 _isShowDial = !_isShowDial;
//               });
//               Navigator.pushNamed(
//                 context,
//                 RoutePaths.contactUs,
//               );
//             },
//             backgroundColor: PsColors.mainColor,
//             child: const Icon(
//               Icons.g_mobiledata,
//               size: 35.0,
//             ),
//           ),
//         ],
//       ),
//     );
//   }

// @override
// Widget build(BuildContext context) {
//   return CircularMenu(
//     animationDuration: const Duration(milliseconds: 150),
//     curve: Curves.easeIn,
//     alignment: const Alignment(0.95, 0.85),
//     startingAngleInRadian: pi,
//     endingAngleInRadian: 5 * pi / 4,
//     toggleButtonSize: 35,
//     toggleButtonAnimatedIconData: AnimatedIcons.menu_close,
//     items: <CircularMenuItem>[
//       CircularMenuItem(
//         enableBadge: true,
//         badgeColor: Colors.amber,
//         badgeLabel: '3',
//         badgeRadius: 15,
//         badgeTextColor: Colors.white,
//         badgeRightOffet: 0,
//         badgeTopOffet: 0,
//         onTap: () {},
//         icon: Icons.g_mobiledata,
//       ),
//       CircularMenuItem(
//         onTap: () {},
//         icon: Icons.phone_enabled,
//       ),
//     ],
//   );
// }

// @override
// Widget build(BuildContext context) {
//   return Padding(
//     padding: const EdgeInsets.only(bottom: 56),
//     child: GestureDetector(
//       onTap: () {
//         print(
//             'asdugggggggggggggggggggggggggggggggggggggggggsfffffffffffffffffffffffffffffffffffffffffffff');
//         if (animationController.isCompleted) {
//           animationController.reverse();
//         } else {
//           animationController.forward();
//         }
//       },
//       child: Stack(
//         alignment: Alignment.center,
//         children: <Widget>[
//           Transform.translate(
//             offset: Offset.fromDirection(
//               getRadiansFromDegree(192.5),
//               animaton.value * 80,
//             ),
//             child: FloatingActionButton(
//               backgroundColor: PsColors.mainColor,
//               onPressed: () {
//                 print(
//                     'sdgdngidsni sidggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg');
//               },
//               child: const Icon(Icons.phone_enabled),
//             ),
//           ),
//           Transform.translate(
//             offset: Offset.fromDirection(
//               getRadiansFromDegree(245.5),
//               animaton.value * 80,
//             ),
//             child: FloatingActionButton(
//               onPressed: () {
//                 print(
//                     'sdgdngidsni sidggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg');
//               },
//               backgroundColor: PsColors.mainColor,
//               child: const Icon(Icons.g_mobiledata),
//             ),
//           ),
//           FloatingActionButton(
//             backgroundColor: PsColors.mainColor,
//             child: animationController.isCompleted
//                 ? Icon(
//                     Icons.close,
//                     color: PsColors.white,
//                   )
//                 : Image.asset(
//                     'assets/setting_icons/message.png',
//                     height: 22,
//                   ),
//             onPressed: () {
//               if (animationController.isCompleted) {
//                 animationController.reverse();
//               } else {
//                 animationController.forward();
//               }
//             },
//           ),
//         ],
//       ),
//     ),
//   );
// }
//}

// class LoginItem extends StatelessWidget {
//   const LoginItem({Key key, this.userProvider}) : super(key: key);
//   final UserProvider userProvider;
//   @override
//   Widget build(BuildContext context) {
//     if (userProvider == null ||
//         userProvider.psValueHolder == null ||
//         userProvider.psValueHolder.loginUserId == null ||
//         userProvider.psValueHolder.loginUserId == '')
//       return SettingItem(
//         title: Utils.getString(context, 'login__title'),
//         icon: Icons.person_sharp,
//         onItemTap: () {
//           Navigator.pushNamed(
//             context,
//             RoutePaths.login_container,
//           );
//         },
//       );
//     final String imagePath = userProvider.user.data.userProfilePhoto;
//     final String fullImagePath = '${PsConfig.ps_app_image_url}$imagePath';
//     final Widget _imageWidget = PsNetworkCircleImageForUser(
//       photoKey: '',
//       imagePath: imagePath,
//       boxfit: BoxFit.cover,
//     );

//     return GestureDetector(
//       onTap: () {}, //TODO go to SignIn()
//       child: Container(
//         //color: PsColors.textPrimaryColor,
//         height: 80.0,
//         padding: const EdgeInsets.symmetric(
//           horizontal: 8.0,
//           vertical: 14.0,
//         ),
//         child: Row(
//           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           children: <Widget>[
//             Row(
//               children: <Widget>[
//                 //_imageWidget,
//                 ClipRRect(
//                   borderRadius: BorderRadius.circular(120),
//                   child: (imagePath == null || imagePath == '')
//                       ? Image.asset('assets/images/placeholder_image.png.png')
//                       : OptimizedCacheImage(
//                           width: 80.0,
//                           height: 80.0,
//                           imageUrl: fullImagePath,
//                           fit: BoxFit.contain,
//                         ),
//                 ),

//                 SizedBox(
//                   width: 210,
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     mainAxisSize: MainAxisSize.min,
//                     children: <Widget>[
//                       Text(
//                         userProvider.user.data.userName,
//                         maxLines: 2,
//                         overflow: TextOverflow.ellipsis,
//                         style: TextStyle(
//                           color: PsColors.textPrimaryColor,
//                           fontSize: 18.0,
//                           fontWeight: FontWeight.w500,
//                         ),
//                       ),
//                       Text(
//                         userProvider.user.data.userEmail,
//                         overflow: TextOverflow.ellipsis,
//                         style: TextStyle(
//                           color: PsColors.textPrimaryColor,
//                           fontSize: 18.0,
//                           fontWeight: FontWeight.w500,
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//             Padding(
//               padding: const EdgeInsets.only(right: 1.0),
//               child: Icon(
//                 Icons.keyboard_arrow_right,
//                 size: 17.0,
//                 color: PsColors.textPrimaryColor,
//               ),
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }

class SettingItem extends StatefulWidget {
  SettingItem({
    Key key,
    @required this.title,
    @required this.iconPath,
    this.isSwitchable = false,
    this.trailingText,
    this.iconSize = 28.0,
    this.height = 63,
    @required this.onItemTap,
    this.onSwitchValueChanged,
  }) : super(key: key);

  final String title;
  final IconData iconPath;
  final bool isSwitchable;
  final String trailingText;
  double iconSize;
  double height;
  final void Function() onItemTap;
  final void Function(bool value) onSwitchValueChanged;

  @override
  _SettingItemState createState() => _SettingItemState();
}

class _SettingItemState extends State<SettingItem> {
  bool isOn = false;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onItemTap,
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 8.0),
        color: PsColors.coreBackgroundColor,
        height: widget.height,
        child: Row(
          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Icon(
                widget.iconPath,
                size: widget.iconSize,
                color: PsColors.textPrimaryColor,
              ),
            ),
            Container(
              //width: widget.trailingText != null ? 185 : 240,
              color: PsColors.coreBackgroundColor,
              padding: const EdgeInsets.only(left: 20, right: 8),
              // margin: const EdgeInsets.only(right: 25),
              child: Text(
                widget.title,
                maxLines: 2,
                style: TextStyle(
                  // overflow: TextOverflow.ellipsis,
                  color: PsColors.textPrimaryColor,
                  fontSize: 18.0,
                  fontWeight: FontWeight.w500,
                  fontFamily: PsConfig.ps_default_font_family,
                ),
              ),
            ),
            const Spacer(),
            if (widget.trailingText != null)
              Row(
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 12.0,
                      vertical: 4.0,
                    ),
                    margin: const EdgeInsets.only(right: 28.0),
                    decoration: BoxDecoration(
                      color: PsColors.mainColor,
                      borderRadius: BorderRadius.circular(24.0),
                    ),
                    child: FittedBox(
                      child: Text(
                        widget.trailingText,
                        style: const TextStyle(
                          color: Colors.white,
                          fontSize: 14.0,
                          fontWeight: FontWeight.w400,
                          fontFamily: 'Roboto',
                        ),
                      ),
                    ),
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    size: 17.0,
                    color: PsColors.textPrimaryColor,
                  ),
                ],
              )
            else
              Icon(
                Icons.arrow_forward_ios,
                size: 17.0,
                color: PsColors.textPrimaryColor,
              ),
          ],
        ),
      ),
    );
  }
}

class CustomSwitchItem extends StatefulWidget {
  CustomSwitchItem({
    Key key,
    @required this.onTitle,
    @required this.offTitle,
    @required this.onIconPath,
    @required this.offIconPath,
    @required this.initialValueIsOn,
    this.onSwitchValueChanged,
  }) : super(key: key);

  final String onTitle;
  final String offTitle;
  final IconData onIconPath;
  IconData offIconPath;
  final bool initialValueIsOn;
  final void Function(bool value) onSwitchValueChanged;

  @override
  _CustomSwitchItemState createState() => _CustomSwitchItemState();
}

class _CustomSwitchItemState extends State<CustomSwitchItem> {
  bool isOn;
  IconData icon;
  String title;

  @override
  void initState() {
    isOn = widget.initialValueIsOn;
    icon = widget.initialValueIsOn ? widget.onIconPath : widget.offIconPath;
    title = widget.initialValueIsOn ? widget.onTitle : widget.offTitle;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8.0),
      height: 63.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Icon(
                  icon,
                  size: 28.0,
                  color: PsColors.textPrimaryColor,
                ),
              ),
              Container(
                width: 205,
                padding: const EdgeInsets.only(left: 20),
                child: Text(
                  title,
                  maxLines: 3,
                  style: TextStyle(
                    color: PsColors.textPrimaryColor,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w500,
                    fontFamily: PsConfig.ps_default_font_family,
                  ),
                ),
              ),
            ],
          ),
          Switch(
            value: isOn,
            onChanged: (bool val) {
              setState(() {
                isOn = val;
                icon = icon == widget.onIconPath
                    ? widget.offIconPath
                    : widget.onIconPath;
                title =
                    title == widget.onTitle ? widget.offTitle : widget.onTitle;
                widget.onSwitchValueChanged(val);
              });
            },
            activeTrackColor: const Color(0xff053A35),
            activeColor: PsColors.mainColor,
            inactiveTrackColor: const Color(0xff6C6E7A),
          ),
        ],
      ),
    );
  }
}

void changeBrightness(BuildContext context) {
  DynamicTheme.of(context).setBrightness(
      Utils.isLightMode(context) ? Brightness.dark : Brightness.light);
}

class _ImageAndTextWidget extends StatefulWidget {
  const _ImageAndTextWidget({this.userProvider, this.onTap});
  final UserProvider userProvider;
  final Function() onTap;

  @override
  State<_ImageAndTextWidget> createState() => _ImageAndTextWidgetState();
}

class _ImageAndTextWidgetState extends State<_ImageAndTextWidget> {
  @override
  Widget build(BuildContext context) {
    final Widget _imageWidget = PsNetworkCircleImageForUser(
      photoKey: '',
      imagePath: widget.userProvider.user?.data?.userProfilePhoto,
      boxfit: BoxFit.fill,
    );
    const Widget _spacingWidget = SizedBox(
      height: PsDimens.space4,
    );
    if (widget.userProvider.user != null &&
        widget.userProvider.user.data != null)
      return GestureDetector(
        onTap: widget.onTap,
        child: Container(
          width: double.infinity,
          height: PsDimens.space68,
          margin: const EdgeInsets.only(right: 8.0),
          //color: PsColors.coreBackgroundColor,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                children: <Widget>[
                  const SizedBox(width: PsDimens.space16),
                  Container(
                    width: PsDimens.space60,
                    height: PsDimens.space60,
                    decoration: BoxDecoration(
                      color: PsColors.textPrimaryColor,
                      border: Border.all(
                          width: 1, color: PsColors.coreBackgroundColor),
                      shape: BoxShape.circle,
                    ),
                    child: _imageWidget,
                  ),
                  const SizedBox(width: PsDimens.space16),
                ],
              ),
              Expanded(
                flex: 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    FittedBox(
                      child: Text(
                        widget.userProvider.user.data.userName,
                        textAlign: TextAlign.start,
                        style: Theme.of(context).textTheme.headline6,
                        maxLines: 2,
                      ),
                    ),
                    _spacingWidget,
                    FittedBox(
                      child: Text(
                        widget.userProvider.user.data.userEmail != ''
                            ? widget.userProvider.user.data.userEmail
                            : Utils.getString(context, 'profile__phone_no'),
                        style: Theme.of(context)
                            .textTheme
                            .bodyText2
                            .copyWith(color: PsColors.textPrimaryColor),
                        maxLines: 2,
                      ),
                    ),
                    _spacingWidget,

                    // Text(
                    //   userProvider.user.data.userAboutMe != ''
                    //       ? userProvider.user.data.userAboutMe
                    //       : Utils.getString(context, 'profile__about_me'),
                    //   style: Theme.of(context)
                    //       .textTheme
                    //       .caption
                    //       .copyWith(color: PsColors.textPrimaryLightColor),
                    //   maxLines: 2,
                    //   overflow: TextOverflow.ellipsis,
                    // ),
                  ],
                ),
              ),
              const Spacer(),
              Icon(
                Icons.keyboard_arrow_right,
                size: 30.0,
                color: PsColors.textPrimaryColor,
              ),
            ],
          ),
        ),
      );
    else
      return GestureDetector(
        onTap: () {
          Navigator.pushNamed(
            context,
            RoutePaths.login_container,
          );
        },
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 8.0),
          color: PsColors.coreBackgroundColor,
          height: PsDimens.space60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 12),
                    decoration: BoxDecoration(
                      color: PsColors.greyColorForCustomWidget,
                      shape: BoxShape.circle,
                    ),
                    child: Image.asset(
                      'assets/images/user_profile.png',
                      height: PsDimens.space60,
                      color: PsColors.textPrimaryColor,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Container(
                    color: PsColors.coreBackgroundColor,
                    padding: const EdgeInsets.only(left: 10, right: 8),
                    child: Text(
                      Utils.getString(context, 'login__title'),
                      maxLines: 3,
                      style: TextStyle(
                        overflow: TextOverflow.ellipsis,
                        color: PsColors.textPrimaryColor,
                        fontSize: 18.0,
                        fontWeight: FontWeight.w500,
                        fontFamily: PsConfig.ps_default_font_family,
                      ),
                    ),
                  ),
                ],
              ),
              Icon(
                Icons.arrow_forward_ios,
                size: 17.0,
                color: PsColors.textPrimaryColor,
              ),
            ],
          ),
        ),
      );
  }
}








// class _SettingPrivacyWidget extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: () {
//         print('App Info');
//         Navigator.pushNamed(context, RoutePaths.privacyPolicy,
//             arguments: PrivacyPolicyIntentHolder(
//                 title: Utils.getString(context, 'privacy_policy__toolbar_name'),
//                 description: ''));
//       },
//       child: Container(
//         color: PsColors.coreBackgroundColor,
//         padding: const EdgeInsets.all(PsDimens.space16),
//         child: Row(
//           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           children: <Widget>[
//             Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: <Widget>[
//                 Text(
//                   Utils.getString(context, 'setting__privacy_policy'),
//                   style: Theme.of(context).textTheme.subtitle1,
//                 ),
//                 const SizedBox(
//                   height: PsDimens.space10,
//                 ),
//                 Text(
//                   Utils.getString(context, 'setting__policy_statement'),
//                   style: Theme.of(context).textTheme.caption,
//                 ),
//               ],
//             ),
//             Icon(
//               Icons.arrow_forward_ios,
//               color: PsColors.mainColor,
//               size: PsDimens.space12,
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

// class _SettingNotificationWidget extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: () {
//         print('Notification Setting');
//         Navigator.pushNamed(context, RoutePaths.notiSetting);
//       },
//       child: Container(
//         color: PsColors.coreBackgroundColor,
//         padding: const EdgeInsets.all(PsDimens.space16),
//         child: Row(
//           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           children: <Widget>[
//             Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: <Widget>[
//                 Text(
//                   Utils.getString(context, 'setting__notification_setting'),
//                   style: Theme.of(context).textTheme.subtitle1,
//                 ),
//                 const SizedBox(
//                   height: PsDimens.space10,
//                 ),
//                 Text(
//                   Utils.getString(context, 'setting__control_setting'),
//                   style: Theme.of(context).textTheme.caption,
//                 ),
//               ],
//             ),
//             Icon(
//               Icons.arrow_forward_ios,
//               color: PsColors.mainColor,
//               size: PsDimens.space12,
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

// class _IntroSliderWidget extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return InkWell(
//       onTap: () {
//         print('Slider Guide');
//         Navigator.pushNamed(context, RoutePaths.introSlider, arguments: 1);
//       },
//       child: Container(
//         color: PsColors.coreBackgroundColor,
//         padding: const EdgeInsets.all(PsDimens.space16),
//         child: Ink(
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: <Widget>[
//               Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: <Widget>[
//                   Text(
//                     Utils.getString(context, 'intro_slider_setting'),
//                     style: Theme.of(context).textTheme.subtitle1,
//                   ),
//                   const SizedBox(
//                     height: PsDimens.space10,
//                   ),
//                   Text(
//                     Utils.getString(context, 'intro_slider_setting_description'),
//                     style: Theme.of(context).textTheme.caption,
//                   ),
//                 ],
//               ),
//               Icon(
//                 Icons.arrow_forward_ios,
//                 color: PsColors.mainColor,
//                 size: PsDimens.space12,
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

// class _SettingDarkAndWhiteModeWidget extends StatefulWidget {
//   const _SettingDarkAndWhiteModeWidget({Key key, this.animationController})
//       : super(key: key);
//   final AnimationController animationController;
//   @override
//   __SettingDarkAndWhiteModeWidgetState createState() =>
//       __SettingDarkAndWhiteModeWidgetState();
// }

// class __SettingDarkAndWhiteModeWidgetState
//     extends State<_SettingDarkAndWhiteModeWidget> {
//   bool checkClick = false;
//   bool isDarkOrWhite = false;
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//         width: double.infinity,
//         color: PsColors.coreBackgroundColor,
//         padding: const EdgeInsets.only(
//             top: PsDimens.space16,
//             left: PsDimens.space16,
//             bottom: PsDimens.space12,
//             right: PsDimens.space12),
//         child: Row(
//           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           mainAxisSize: MainAxisSize.max,
//           children: <Widget>[
//             Text(
//               Utils.getString(context, 'setting__change_mode'),
//               style: Theme.of(context).textTheme.subtitle1,
//             ),
//             if (checkClick)
//               Switch(
//                 value: isDarkOrWhite,
//                 onChanged: (bool value) {
//                   setState(() {
//                     PsColors.loadColor2(value);
//                     isDarkOrWhite = value;

//                     changeBrightness(context);
//                   });
//                 },
//                 activeTrackColor: PsColors.mainColor,
//                 activeColor: PsColors.mainColor,
//               )
//             else
//               Switch(
//                 value: isDarkOrWhite,
//                 onChanged: (bool value) {
//                   setState(() {
//                     PsColors.loadColor2(value);
//                     isDarkOrWhite = value;

//                     changeBrightness(context);
//                   });
//                 },
//                 activeTrackColor: PsColors.mainColor,
//                 activeColor: PsColors.mainColor,
//               ),
//           ],
//         ));
//   }
// }



// class _SettingAppInfoWidget extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return InkWell(
//       onTap: () {
//         print('App Info');
//         Navigator.pushNamed(context, RoutePaths.appinfo, arguments: 1);
//       },
//       child: Container(
//         color: PsColors.coreBackgroundColor,
//         padding: const EdgeInsets.all(PsDimens.space16),
//         child: Ink(
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: <Widget>[
//               Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: <Widget>[
//                   Text(
//                     Utils.getString(context, 'setting__app_info'),
//                     style: Theme.of(context).textTheme.subtitle1,
//                   ),
//                   const SizedBox(
//                     height: PsDimens.space10,
//                   ),
//                   Text(
//                     Utils.getString(context, 'setting__app_info'),
//                     style: Theme.of(context).textTheme.caption,
//                   ),
//                 ],
//               ),
//               Icon(
//                 Icons.arrow_forward_ios,
//                 color: PsColors.mainColor,
//                 size: PsDimens.space12,
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

// class _SettingAppVersionWidget extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: () {
//         print('App Info');
//       },
//       child: Container(
//         width: double.infinity,
//         color: PsColors.coreBackgroundColor,
//         padding: const EdgeInsets.all(PsDimens.space16),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           mainAxisSize: MainAxisSize.min,
//           children: <Widget>[
//             Text(
//               Utils.getString(context, 'setting__app_version'),
//               style: Theme.of(context).textTheme.subtitle1,
//             ),
//             const SizedBox(
//               height: PsDimens.space10,
//             ),
//             Text(
//               PsConfig.app_version,
//               style: Theme.of(context).textTheme.bodyText2,
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
