import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/ui/common/dialog/confirm_dialog_view.dart';
import 'package:biomart/ui/setting/setting_view.dart';
import 'package:biomart/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SettingContainerView extends StatefulWidget {
  SettingContainerView({this.userId});
  String userId;
  @override
  _SettingContainerViewState createState() => _SettingContainerViewState();
}

class _SettingContainerViewState extends State<SettingContainerView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;

  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Future<bool> _onWillPop() {
      return showDialog<dynamic>(
            context: context,
            builder: (BuildContext context) {
              return ConfirmDialogView(
                description:
                    Utils.getString(context, 'home__quit_dialog_description'),
                leftButtonText:
                    Utils.getString(context, 'app_info__cancel_button_name'),
                rightButtonText: Utils.getString(context, 'dialog__ok'),
                onAgreeTap: () {
                  SystemNavigator.pop();
                },
              );
            },
          ) ??
          false;
    }

    print(
        '............................Build UI Again ............................');
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Container(
        color: PsColors.coreBackgroundColor,
        height: double.infinity,
        child: SettingView(
          animationController: animationController,
          userId: widget.userId,
        ),
      ),
      // Scaffold(
      //   backgroundColor: PsColors.coreBackgroundColor,
      //   appBar: AppBar(
      //     systemOverlayStyle: SystemUiOverlayStyle(
      //         statusBarIconBrightness: Utils.getBrightnessForAppBar(context)),
      //     iconTheme: Theme.of(context)
      //         .iconTheme
      //         .copyWith(color: PsColors.mainColorWithWhite),
      //     title: Text(
      //       Utils.getString(context, 'Setting'),
      //       textAlign: TextAlign.center,
      //       style: Theme.of(context).textTheme.headline6.copyWith(
      //           fontWeight: FontWeight.bold,
      //           color: PsColors.mainColorWithWhite),
      //     ),
      //     elevation: 0,
      //   ),
      //   body:
      // Container(
      //     color: PsColors.coreBackgroundColor,
      //     height: double.infinity,
      //     child: SettingView(
      //       animationController: animationController,
      //       userId: widget.userId,
      //     ),
      //   ),

      // ),
    );
  }
}
