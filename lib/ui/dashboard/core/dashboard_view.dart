//import 'package:flutter_native_admob/flutter_native_admob.dart';
import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_constants.dart';
import 'package:biomart/constant/route_paths.dart';
import 'package:biomart/provider/common/notification_provider.dart';
import 'package:biomart/provider/delete_task/delete_task_provider.dart';
import 'package:biomart/provider/shop_info/shop_info_provider.dart';
import 'package:biomart/provider/user/user_provider.dart';
import 'package:biomart/repository/Common/notification_repository.dart';
import 'package:biomart/repository/basket_repository.dart';
import 'package:biomart/repository/delete_task_repository.dart';
import 'package:biomart/repository/product_repository.dart';
import 'package:biomart/repository/shop_info_repository.dart';
import 'package:biomart/repository/user_repository.dart';
import 'package:biomart/ui/basket/list/basket_list_view.dart';
import 'package:biomart/ui/collection/header_list/collection_header_list_view.dart';
import 'package:biomart/ui/common/dialog/confirm_dialog_view.dart';
import 'package:biomart/ui/contact/contact_us_view.dart';
import 'package:biomart/ui/dashboard/home/main_dashboard_view.dart';
import 'package:biomart/ui/dashboard/view_pages/call_login.dart';
import 'package:biomart/ui/dashboard/view_pages/call_verify_email.dart';
import 'package:biomart/ui/dashboard/view_pages/call_verify_phone.dart';
import 'package:biomart/ui/history/list/history_list_view.dart';
import 'package:biomart/ui/language/setting/language_setting_view.dart';
import 'package:biomart/ui/product/favourite/favourite_product_list_view.dart';
import 'package:biomart/ui/reservation/entry/create_reservation_view.dart';
import 'package:biomart/ui/reservation/list/reservation_list_view.dart';
import 'package:biomart/ui/search/home_item_search_view.dart';
import 'package:biomart/ui/setting/setting_view.dart';
import 'package:biomart/ui/terms_and_conditions/terms_and_conditions_view.dart';
import 'package:biomart/ui/transaction/list/transaction_list_view.dart';
import 'package:biomart/ui/user/forgot_password/forgot_password_view.dart';
import 'package:biomart/ui/user/login/login_view.dart';
import 'package:biomart/ui/user/phone/sign_in/phone_sign_in_view.dart';
import 'package:biomart/ui/user/profile/profile_view.dart';
import 'package:biomart/ui/user/register/register_view.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/common/ps_value_holder.dart';
import 'package:biomart/viewobject/holder/intent_holder/product_detail_intent_holder.dart';
import 'package:biomart/viewobject/holder/product_parameter_holder.dart';
import 'package:biomart/viewobject/user.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart' show timeDilation;
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class DashboardView extends StatefulWidget {
  @override
  HomeViewState createState() => HomeViewState();
}

class HomeViewState extends State<DashboardView>
    with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  static AnimationController animationController;

  Animation<double> animation;
  BasketRepository basketRepository;

  String appBarTitle = 'Home';
  int _currentIndex = PsConst.REQUEST_CODE__MENU_HOME_FRAGMENT;
  String _userId = '';
  bool isLogout = false;
  bool isFirstTime = true;
  String phoneUserName = '';
  String phoneNumber = '';
  String phoneId = '';

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  ShopInfoProvider shopInfoProvider;
  final FirebaseMessaging _fcm = FirebaseMessaging.instance;
  bool isResumed = false;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      isResumed = true;
      initDynamicLinks(context);
    }
  }

  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    initDynamicLinks(context);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  Future<void> initDynamicLinks(BuildContext context) async {
    Future<dynamic>.delayed(const Duration(seconds: 3)); //recomme
    String itemId = '';
    if (!isResumed) {
      final PendingDynamicLinkData data =
          await FirebaseDynamicLinks.instance.getInitialLink();

      if (data != null && data?.link != null) {
        final Uri deepLink = data?.link;
        if (deepLink != null) {
          final String path = deepLink.path;
          final List<String> pathList = path.split('=');
          itemId = pathList[1];
          final ProductDetailIntentHolder holder = ProductDetailIntentHolder(
            productId: itemId,
            heroTagImage: '-1' + pathList[1] + PsConst.HERO_TAG__IMAGE,
            heroTagTitle: '-1' + pathList[1] + PsConst.HERO_TAG__TITLE,
            heroTagOriginalPrice:
                '-1' + pathList[1] + PsConst.HERO_TAG__ORIGINAL_PRICE,
            heroTagUnitPrice: '-1' + pathList[1] + PsConst.HERO_TAG__UNIT_PRICE,
          );
          Navigator.pushNamed(context, RoutePaths.productDetail,
              arguments: holder);
        }
      }
    }

    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData dynamicLink) async {
      final Uri deepLink = dynamicLink?.link;
      if (deepLink != null) {
        final String path = deepLink.path;
        final List<String> pathList = path.split('=');
        if (itemId == '') {
          final ProductDetailIntentHolder holder = ProductDetailIntentHolder(
              productId: pathList[1],
              heroTagImage: '-1' + pathList[1] + PsConst.HERO_TAG__IMAGE,
              heroTagTitle: '-1' + pathList[1] + PsConst.HERO_TAG__TITLE);
          Navigator.pushNamed(context, RoutePaths.productDetail,
              arguments: holder);
        }
      }
      debugPrint('DynamicLinks onLink $deepLink');
    }, onError: (OnLinkErrorException e) async {
      debugPrint('DynamicLinks onError $e');
    });
  }

  ShopInfoRepository shopInfoRepository;
  UserRepository userRepository;
  ProductRepository productRepository;
  PsValueHolder valueHolder;
  NotificationRepository notificationRepository;
  DeleteTaskRepository deleteTaskRepository;
  DeleteTaskProvider deleteTaskProvider;

  @override
  Widget build(BuildContext context) {
    shopInfoRepository = Provider.of<ShopInfoRepository>(context);
    userRepository = Provider.of<UserRepository>(context);
    valueHolder = Provider.of<PsValueHolder>(context);
    notificationRepository = Provider.of<NotificationRepository>(context);
    productRepository = Provider.of<ProductRepository>(context);
    basketRepository = Provider.of<BasketRepository>(context);
    deleteTaskRepository = Provider.of<DeleteTaskRepository>(context);

    timeDilation = 1.0;

    if (isFirstTime) {
      appBarTitle = Utils.getString(context, 'app_name');

      Utils.subscribeToTopic(valueHolder.notiSetting ?? true);

      Utils.fcmConfigure(context, _fcm, valueHolder.loginUserId);
      isFirstTime = false;
    }

    Future<void> updateSelectedIndexWithAnimation(
        String title, int index) async {
      await animationController.reverse().then<dynamic>((void data) {
        if (!mounted) {
          return;
        }

        setState(() {
          appBarTitle = title;
          _currentIndex = index;
        });
      });
    }

    Future<void> updateSelectedIndexWithAnimationUserId(
        String title, int index, String userId) async {
      await animationController.reverse().then<dynamic>((void data) {
        if (!mounted) {
          return;
        }
        if (userId != null) {
          _userId = userId;
        }
        setState(() {
          appBarTitle = title;
          _currentIndex = index;
        });
      });
    }

    Future<bool> _onWillPop() {
      return showDialog<dynamic>(
            context: context,
            builder: (BuildContext context) {
              return ConfirmDialogView(
                description:
                    Utils.getString(context, 'home__quit_dialog_description'),
                leftButtonText:
                    Utils.getString(context, 'app_info__cancel_button_name'),
                rightButtonText: Utils.getString(context, 'dialog__ok'),
                onAgreeTap: () {
                  SystemNavigator.pop();
                },
              );
            },
          ) ??
          false;
    }

    final Animation<double> animation =
        Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: animationController,
        curve: const Interval(0.5 * 1, 1.0, curve: Curves.fastOutSlowIn),
      ),
    );

    return WillPopScope(
      onWillPop: _onWillPop,
      
        // drawer: Drawer(
        //   backgroundColor: PsColors.coreBackgroundColor,
        //   child: MultiProvider(
        //     providers: <SingleChildWidget>[
        //       ChangeNotifierProvider<UserProvider>(
        //           lazy: false,
        //           create: (BuildContext context) {
        //             return UserProvider(
        //                 repo: userRepository, psValueHolder: valueHolder);
        //           }),
        //       ChangeNotifierProvider<DeleteTaskProvider>(
        //           lazy: false,
        //           create: (BuildContext context) {
        //             deleteTaskProvider = DeleteTaskProvider(
        //                 repo: deleteTaskRepository, psValueHolder: valueHolder);
        //             return deleteTaskProvider;
        //           }),
        //     ],
        //     child: Consumer<UserProvider>(
        //       builder:
        //           (BuildContext context, UserProvider provider, Widget child) {
        //         print(provider.psValueHolder.loginUserId);
        //         return ListView(padding: EdgeInsets.zero, children: <Widget>[
        //           _DrawerHeaderWidget(),
        //           ListTile(
        //             title: Text(
        //                 Utils.getString(context, 'home__drawer_menu_home')),
        //           ),
        //           _DrawerMenuWidget(
        //               icon: Icons.store,
        //               title: Utils.getString(context, 'home__drawer_menu_home'),
        //               index: PsConst.REQUEST_CODE__MENU_HOME_FRAGMENT,
        //               onTap: (String title, int index) {
        //                 Navigator.pop(context);
        //                 updateSelectedIndexWithAnimation(
        //                     Utils.getString(context, 'app_name'), index);
        //               }),
        //           _DrawerMenuWidget(
        //               icon: Icons.category,
        //               title: Utils.getString(
        //                   context, 'home__drawer_menu_category'),
        //               index: PsConst.REQUEST_CODE__MENU_CATEGORY_FRAGMENT,
        //               onTap: (String title, int index) {
        //                 Navigator.pop(context);
        //                 updateSelectedIndexWithAnimation(title, index);
        //               }),
        //           _DrawerMenuWidget(
        //               icon: Icons.schedule,
        //               title: Utils.getString(
        //                   context, 'home__drawer_menu_latest_product'),
        //               index: PsConst.REQUEST_CODE__MENU_LATEST_PRODUCT_FRAGMENT,
        //               onTap: (String title, int index) {
        //                 Navigator.pop(context);
        //                 updateSelectedIndexWithAnimation(title, index);
        //               }),
        //           _DrawerMenuWidget(
        //               icon: Feather.percent,
        //               title: Utils.getString(
        //                   context, 'home__drawer_menu_discount_product'),
        //               index:
        //                   PsConst.REQUEST_CODE__MENU_DISCOUNT_PRODUCT_FRAGMENT,
        //               onTap: (String title, int index) {
        //                 Navigator.pop(context);
        //                 updateSelectedIndexWithAnimation(title, index);
        //               }),
        //           _DrawerMenuWidget(
        //               icon: FontAwesome5.gem,
        //               title: Utils.getString(
        //                   context, 'home__menu_drawer_featured_product'),
        //               index:
        //                   PsConst.REQUEST_CODE__MENU_FEATURED_PRODUCT_FRAGMENT,
        //               onTap: (String title, int index) {
        //                 Navigator.pop(context);
        //                 updateSelectedIndexWithAnimation(title, index);
        //               }),
        //           _DrawerMenuWidget(
        //               icon: Icons.trending_up,
        //               title: Utils.getString(
        //                   context, 'home__drawer_menu_trending_product'),
        //               index:
        //                   PsConst.REQUEST_CODE__MENU_TRENDING_PRODUCT_FRAGMENT,
        //               onTap: (String title, int index) {
        //                 Navigator.pop(context);
        //                 updateSelectedIndexWithAnimation(title, index);
        //               }),
        //           _DrawerMenuWidget(
        //               icon: Icons.folder_open,
        //               title: Utils.getString(
        //                   context, 'home__menu_drawer_collection'),
        //               index: PsConst.REQUEST_CODE__MENU_COLLECTION_FRAGMENT,
        //               onTap: (String title, int index) {
        //                 Navigator.pop(context);
        //                 updateSelectedIndexWithAnimation(title, index);
        //               }),
        //           const Divider(
        //             height: PsDimens.space1,
        //           ),
        //           ListTile(
        //             title: Text(Utils.getString(
        //                 context, 'home__menu_drawer_user_info')),
        //           ),
        //           _DrawerMenuWidget(
        //               icon: Icons.person,
        //               title:
        //                   Utils.getString(context, 'home__menu_drawer_profile'),
        //               index:
        //                   PsConst.REQUEST_CODE__MENU_SELECT_WHICH_USER_FRAGMENT,
        //               onTap: (String title, int index) {
        //                 Navigator.pop(context);
        //                 title = (valueHolder == null ||
        //                         valueHolder.userIdToVerify == null ||
        //                         valueHolder.userIdToVerify == '')
        //                     ? Utils.getString(
        //                         context, 'home__menu_drawer_profile')
        //                     : Utils.getString(
        //                         context, 'home__bottom_app_bar_verify_email');
        //                 updateSelectedIndexWithAnimation(title, index);
        //               }),
        //           // if (provider != null)
        //           //   if (provider.psValueHolder.loginUserId != null &&
        //           //       provider.psValueHolder.loginUserId != '')
        //           //     Visibility(
        //           //       visible: true,
        //           //       child: _DrawerMenuWidget(
        //           //           icon: Icons.favorite_border,
        //           //           title: Utils.getString(
        //           //               context, 'home__menu_drawer_favourite'),
        //           //           index:
        //           //               PsConst.REQUEST_CODE__MENU_FAVOURITE_FRAGMENT,
        //           //           onTap: (String title, int index) {
        //           //             Navigator.pop(context);
        //           //             updateSelectedIndexWithAnimation(title, index);
        //           //           }),
        //           //     ),

        //           // if (provider != null)
        //           //   if (provider.psValueHolder.loginUserId != null &&
        //           //       provider.psValueHolder.loginUserId != '')
        //           //     Visibility(
        //           //       visible: true,
        //           //       child: _DrawerMenuWidget(
        //           //         icon: Icons.swap_horiz,
        //           //         title: Utils.getString(
        //           //             context, 'home__menu_drawer_transaction'),
        //           //         index:
        //           //             PsConst.REQUEST_CODE__MENU_TRANSACTION_FRAGMENT,
        //           //         onTap: (String title, int index) {
        //           //           Navigator.pop(context);
        //           //           updateSelectedIndexWithAnimation(title, index);
        //           //         },
        //           //       ),
        //           //     ),

        //           // if (provider != null)
        //           //   if (provider.psValueHolder.loginUserId != null &&
        //           //       provider.psValueHolder.loginUserId != '')
        //           //     Visibility(
        //           //       visible: true,
        //           //       child: _DrawerMenuWidget(
        //           //           icon: Icons.book,
        //           //           title: Utils.getString(
        //           //               context, 'home__menu_drawer_user_history'),
        //           //           index: PsConst
        //           //               .REQUEST_CODE__MENU_USER_HISTORY_FRAGMENT,
        //           //           onTap: (String title, int index) {
        //           //             Navigator.pop(context);
        //           //             updateSelectedIndexWithAnimation(title, index);
        //           //           }),
        //           //     ),

        //           if (provider != null)
        //             if (provider.psValueHolder.loginUserId != null &&
        //                 provider.psValueHolder.loginUserId != '')
        //               Visibility(
        //                 visible: true,
        //                 child: ListTile(
        //                   leading: Icon(
        //                     Icons.power_settings_new,
        //                     color: PsColors.mainColorWithWhite,
        //                   ),
        //                   title: Text(
        //                     Utils.getString(
        //                         context, 'home__menu_drawer_logout'),
        //                     style: Theme.of(context).textTheme.bodyText2,
        //                   ),
        //                   onTap: () async {
        //                     Navigator.pop(context);
        //                     showDialog<dynamic>(
        //                       context: context,
        //                       builder: (BuildContext context) {
        //                         return ConfirmDialogView(
        //                           description: Utils.getString(context,
        //                               'home__logout_dialog_description'),
        //                           leftButtonText: Utils.getString(context,
        //                               'home__logout_dialog_cancel_button'),
        //                           rightButtonText: Utils.getString(
        //                               context, 'home__logout_dialog_ok_button'),
        //                           onAgreeTap: () async {
        //                             Navigator.of(context).pop();
        //                             setState(() {
        //                               _currentIndex = PsConst
        //                                   .REQUEST_CODE__MENU_HOME_FRAGMENT;
        //                             });
        //                             await provider.replaceLoginUserId('');
        //                             await deleteTaskProvider.deleteTask();
        //                             await FacebookAuth.instance.logOut();
        //                             await GoogleSignIn().signOut();
        //                             await fb_auth.FirebaseAuth.instance
        //                                 .signOut();
        //                           },
        //                         );
        //                       },
        //                     );
        //                   },
        //                 ),
        //               ),
        //           const Divider(
        //             height: PsDimens.space1,
        //           ),
        //           ListTile(
        //             title:
        //                 Text(Utils.getString(context, 'home__menu_drawer_app')),
        //           ),
        //           _DrawerMenuWidget(
        //               icon: Icons.g_translate,
        //               title: Utils.getString(
        //                   context, 'home__menu_drawer_language'),
        //               index: PsConst.REQUEST_CODE__MENU_LANGUAGE_FRAGMENT,
        //               onTap: (String title, int index) {
        //                 Navigator.pop(context);
        //                 updateSelectedIndexWithAnimation('', index);
        //               }),
        //           _DrawerMenuWidget(
        //               icon: Icons.contacts,
        //               title: Utils.getString(
        //                   context, 'home__menu_drawer_contact_us'),
        //               index: PsConst.REQUEST_CODE__MENU_CONTACT_US_FRAGMENT,
        //               onTap: (String title, int index) {
        //                 Navigator.pop(context);
        //                 updateSelectedIndexWithAnimation(title, index);
        //               }),
        //           _DrawerMenuWidget(
        //             icon: Icons.settings,
        //             title:
        //                 Utils.getString(context, 'home__menu_drawer_setting'),
        //             index: PsConst.REQUEST_CODE__MENU_SETTING_FRAGMENT,
        //             onTap: (String title, int index) {
        //               Navigator.pop(context);
        //               updateSelectedIndexWithAnimation(title, index);
        //             },
        //           ),
        //           _DrawerMenuWidget(
        //               icon: Icons.info_outline,
        //               title: Utils.getString(
        //                   context, 'privacy_policy__toolbar_name'),
        //               index: PsConst
        //                   .REQUEST_CODE__MENU_TERMS_AND_CONDITION_FRAGMENT,
        //               onTap: (String title, int index) {
        //                 Navigator.pop(context);
        //                 updateSelectedIndexWithAnimation(title, index);
        //               }),
        //           ListTile(
        //             leading: Icon(
        //               Icons.share,
        //               color: PsColors.mainColorWithWhite,
        //             ),
        //             title: Text(
        //               Utils.getString(
        //                   context, 'home__menu_drawer_share_this_app'),
        //               style: Theme.of(context).textTheme.bodyText2,
        //             ),
        //             onTap: () {
        //               Navigator.pop(context);
        //               showDialog<dynamic>(
        //                   context: context,
        //                   builder: (BuildContext context) {
        //                     return ShareAppDialog(
        //                       onPressed: () {
        //                         Navigator.pop(context, true);
        //                       },
        //                     );
        //                   });
        //             },
        //           ),
        //           ListTile(
        //             leading: Icon(
        //               Icons.star_border,
        //               color: PsColors.mainColorWithWhite,
        //             ),
        //             title: Text(
        //               Utils.getString(
        //                   context, 'home__menu_drawer_rate_this_app'),
        //               style: Theme.of(context).textTheme.bodyText2,
        //             ),
        //             onTap: () {
        //               Navigator.pop(context);
        //               if (Platform.isIOS) {
        //                 Utils.launchAppStoreURL(
        //                     iOSAppId: PsConfig.iOSAppStoreId,
        //                     writeReview: true);
        //               } else {
        //                 Utils.launchURL();
        //               }
        //             },
        //           )
        //         ]);
        //       },
        //     ),
        //   ),
        // ),

        // appBar: AppBar(
        //   backgroundColor: PsColors.coreBackgroundColor,
        //   title: Text(
        //     appBarTitle,
        //     style: Theme.of(context).textTheme.headline6.copyWith(
        //           fontWeight: FontWeight.bold,
        //           color: PsColors.textPrimaryColor,
        //         ),
        //   ),
        //   titleSpacing: 0,
        //   elevation: 0,
        //   iconTheme: IconThemeData(color: PsColors.textPrimaryColor),
        //   toolbarTextStyle: TextStyle(color: PsColors.textPrimaryColor),
        //   systemOverlayStyle: SystemUiOverlayStyle(
        //       statusBarIconBrightness: Utils.getBrightnessForAppBar(context)),
        //   actions: <Widget>[
        //     IconButton(
        //       icon: Icon(
        //         Icons.notifications_none,
        //         color: Theme.of(context).iconTheme.color,
        //       ),
        //       onPressed: () {
        //         Navigator.pushNamed(
        //           context,
        //           RoutePaths.notiList,
        //         );
        //       },
        //     ),
        //     IconButton(
        //       icon: Icon(
        //         Feather.book_open,
        //         color: Theme.of(context).iconTheme.color,
        //       ),
        //       onPressed: () {
        //         Navigator.pushNamed(context, RoutePaths.blogList,
        //             arguments: const BlogIntentHolder(noBlogListForShop: true));
        //       },
        //     ),

        // ChangeNotifierProvider<BasketProvider>(
        // lazy: false,
        // create: (BuildContext context) {
        //   final BasketProvider provider =
        //       BasketProvider(repo: basketRepository);
        //   provider.loadBasketList();
        //   return provider;
        // },
        // child: Consumer<BasketProvider>(
        //   builder: (BuildContext context,
        //     BasketProvider basketProvider, Widget child) {
        //   return InkWell(
        //       child: Stack(
        //         children: <Widget>[
        //           Container(
        //             width: PsDimens.space40,
        //             height: PsDimens.space40,
        //             margin: const EdgeInsets.only(
        //                 top: PsDimens.space8,
        //                 left: PsDimens.space8,
        //                 right: PsDimens.space8),
        //             child: Align(
        //               alignment: Alignment.center,
        //               child: Icon(
        //                 Icons.shopping_basket,
        //                 color: PsColors.mainColor,
        //               ),
        //             ),
        //           ),
        //           Positioned(
        //             right: PsDimens.space4,
        //             top: PsDimens.space1,
        //             child: Container(
        //               width: PsDimens.space28,
        //               height: PsDimens.space28,
        //               decoration: BoxDecoration(
        //                 shape: BoxShape.circle,
        //                 color: PsColors.black.withAlpha(200),
        //               ),
        //               child: Align(
        //                 alignment: Alignment.center,
        //                 child: Text(
        //                   basketProvider.basketList.data.length > 99
        //                       ? '99+'
        //                       : basketProvider.basketList.data.length
        //                           .toString(),
        //                   textAlign: TextAlign.left,
        //                   style: Theme.of(context)
        //                       .textTheme
        //                       .bodyText1
        //                       .copyWith(color: PsColors.white),
        //                   maxLines: 1,
        //                 ),
        //               ),
        //             ),
        //           ),
        //         ],
        //       ),
        //       onTap: () {
        //         Navigator.pushNamed(
        //           context,
        //           RoutePaths.basketList,
        //         );
        //       });
        // })),
        //   ],
        // ),
       

        // // floatingActionButton: _currentIndex ==
        // //             PsConst.REQUEST_CODE__MENU_SETTING_FRAGMENT ||
        // //         _currentIndex == PsConst.REQUEST_CODE__MENU_HOME_FRAGMENT ||
        // //         _currentIndex ==
        // //             PsConst.REQUEST_CODE__DASHBOARD_SHOP_INFO_FRAGMENT ||
        // //         _currentIndex ==
        // //             PsConst
        // //                 .REQUEST_CODE__DASHBOARD_SELECT_WHICH_USER_FRAGMENT ||
        // //         _currentIndex ==
        // //             PsConst.REQUEST_CODE__DASHBOARD_USER_PROFILE_FRAGMENT ||
        // //         _currentIndex ==
        // //             PsConst.REQUEST_CODE__DASHBOARD_FORGOT_PASSWORD_FRAGMENT ||
        // //         _currentIndex ==
        // //             PsConst.REQUEST_CODE__DASHBOARD_REGISTER_FRAGMENT ||
        // //         _currentIndex ==
        // //             PsConst.REQUEST_CODE__DASHBOARD_VERIFY_EMAIL_FRAGMENT ||
        // //         _currentIndex ==
        // //             PsConst.REQUEST_CODE__MENU_FAVOURITE_FRAGMENT ||
        // //         _currentIndex ==
        // //             PsConst.REQUEST_CODE__DASHBOARD_BASKET_FRAGMENT ||
        // //         _currentIndex ==
        // //             PsConst.REQUEST_CODE__DASHBOARD_LOGIN_FRAGMENT ||
        // //         _currentIndex ==
        // //             PsConst.REQUEST_CODE__DASHBOARD_PHONE_SIGNIN_FRAGMENT ||
        // //         _currentIndex ==
        // //             PsConst.REQUEST_CODE__DASHBOARD_PHONE_VERIFY_FRAGMENT
        // //     ? Container(
        // //         height: 54.0,
        // //         width: 54.0,
        // //         decoration: BoxDecoration(
        // //           color: PsColors.mainColor,
        // //           shape: BoxShape.circle,
        // //         ),
        // //         child: InkWell(
        // //           onTap: () {}, //TODO petqa stugvi erevi vor eji hamara
        // //           //vor tarber gorcoxutyunner ani
        // //           child: Image.asset(
        // //             _currentIndex == PsConst.REQUEST_CODE__MENU_SETTING_FRAGMENT
        // //                 ? 'assets/home_images/message.png'
        // //                 : 'assets/home_images/buy.png',
        // //             //fit: BoxFit.cover,
        // //             width: 31.0,
        // //             height: 31.0,
        // //           ),
        // //         ),
        // //       )
        //     : null,
        
       child:  ChangeNotifierProvider<NotificationProvider>(
            lazy: false,
            create: (BuildContext context) {
              final NotificationProvider provider = NotificationProvider(
                  repo: notificationRepository, psValueHolder: valueHolder);

              if (provider.psValueHolder.deviceToken == null ||
                  provider.psValueHolder.deviceToken == '') {
                final FirebaseMessaging _fcm = FirebaseMessaging.instance;
                Utils.saveDeviceToken(_fcm, provider);
              } else {
                print(
                    'Notification Token is already registered. Notification Setting : true.');
              }

              return provider;
            },
            child: Builder(builder: (BuildContext context) {
              if (_currentIndex ==
                  PsConst.REQUEST_CODE__DASHBOARD_SELECT_WHICH_USER_FRAGMENT) {
                return ChangeNotifierProvider<UserProvider>(
                  lazy: false,
                  create: (BuildContext context) {
                    final UserProvider provider = UserProvider(
                        repo: userRepository, psValueHolder: valueHolder);
                    //provider.getUserLogin();
                    return provider;
                  },
                  child: Consumer<UserProvider>(builder: (BuildContext context,
                      UserProvider provider, Widget child) {
                    if (provider == null ||
                        provider.psValueHolder.userIdToVerify == null ||
                        provider.psValueHolder.userIdToVerify == '') {
                      if (provider == null ||
                          provider.psValueHolder == null ||
                          provider.psValueHolder.loginUserId == null ||
                          provider.psValueHolder.loginUserId == '') {
                        return SettingView(
                          animationController: animationController,
                          userId: _userId,
                        );

                        // CallLoginWidget(
                        //     currentIndex: _currentIndex,
                        //     animationController: animationController,
                        //     animation: animation,
                        //     updateCurrentIndex: (String title, int index) {
                        //       if (index != null) {
                        //         updateSelectedIndexWithAnimation(
                        //             title, index);
                        //       }
                        //     },
                        //     updateUserCurrentIndex:
                        //         (String title, int index, String userId) {
                        //       if (index != null) {
                        //         updateSelectedIndexWithAnimation(
                        //             title, index);
                        //       }
                        //       if (userId != null) {
                        //         _userId = userId;
                        //         provider.psValueHolder.loginUserId = userId;
                        //       }
                        //     });
                      } else {
                        return SettingView(
                          animationController: animationController,
                          userId: _userId,
                        );
                      }
                    } else {
                      return CallVerifyEmailWidget(
                          animationController: animationController,
                          animation: animation,
                          currentIndex: _currentIndex,
                          userId: _userId,
                          updateCurrentIndex: (String title, int index) {
                            updateSelectedIndexWithAnimation(title, index);
                          },
                          updateUserCurrentIndex:
                              (String title, int index, String userId) async {
                            if (userId != null) {
                              _userId = userId;
                              provider.psValueHolder.loginUserId = userId;
                            }
                            setState(() {
                              appBarTitle = title;
                              _currentIndex = index;
                            });
                          });
                    }
                  }),
                );
              }

              if (_currentIndex ==
                  PsConst.REQUEST_CODE__DASHBOARD_SEARCH_FRAGMENT) {
                // 2nd Way
                //SearchProductProvider searchProductProvider;

                return CustomScrollView(
                  scrollDirection: Axis.vertical,
                  slivers: <Widget>[
                    HomeItemSearchView(
                        animationController: animationController,
                        animation: animation,
                        productParameterHolder:
                            ProductParameterHolder().getLatestParameterHolder())
                  ],
                );
              } else if (_currentIndex ==
                      PsConst.REQUEST_CODE__DASHBOARD_PHONE_SIGNIN_FRAGMENT ||
                  _currentIndex ==
                      PsConst.REQUEST_CODE__MENU_PHONE_SIGNIN_FRAGMENT) {
                return Stack(children: <Widget>[
                  Container(
                    color: PsColors.coreBackgroundColor,
                    width: double.infinity,
                    height: double.maxFinite,
                  ),
                  CustomScrollView(scrollDirection: Axis.vertical, slivers: <
                      Widget>[
                    PhoneSignInView(
                        animationController: animationController,
                        goToLoginSelected: () {
                          animationController
                              .reverse()
                              .then<dynamic>((void data) {
                            if (!mounted) {
                              return;
                            }
                            if (_currentIndex ==
                                PsConst
                                    .REQUEST_CODE__MENU_PHONE_SIGNIN_FRAGMENT) {
                              updateSelectedIndexWithAnimation(
                                  Utils.getString(context, 'home_login'),
                                  PsConst.REQUEST_CODE__MENU_LOGIN_FRAGMENT);
                            }
                            if (_currentIndex ==
                                PsConst
                                    .REQUEST_CODE__DASHBOARD_PHONE_SIGNIN_FRAGMENT) {
                              updateSelectedIndexWithAnimation(
                                  Utils.getString(context, 'home_login'),
                                  PsConst
                                      .REQUEST_CODE__DASHBOARD_LOGIN_FRAGMENT);
                            }
                          });
                        },
                        phoneSignInSelected:
                            (String name, String phoneNo, String verifyId) {
                          phoneUserName = name;
                          phoneNumber = phoneNo;
                          phoneId = verifyId;
                          if (_currentIndex ==
                              PsConst
                                  .REQUEST_CODE__MENU_PHONE_SIGNIN_FRAGMENT) {
                            updateSelectedIndexWithAnimation(
                                Utils.getString(context, 'home_verify_phone'),
                                PsConst
                                    .REQUEST_CODE__MENU_PHONE_VERIFY_FRAGMENT);
                          } else if (_currentIndex ==
                              PsConst
                                  .REQUEST_CODE__DASHBOARD_PHONE_SIGNIN_FRAGMENT) {
                            updateSelectedIndexWithAnimation(
                                Utils.getString(context, 'home_verify_phone'),
                                PsConst
                                    .REQUEST_CODE__DASHBOARD_PHONE_VERIFY_FRAGMENT);
                          } else {
                            updateSelectedIndexWithAnimation(
                                Utils.getString(context, 'home_verify_phone'),
                                PsConst
                                    .REQUEST_CODE__DASHBOARD_PHONE_VERIFY_FRAGMENT);
                          }
                        })
                  ])
                ]);
              } else if (_currentIndex ==
                      PsConst.REQUEST_CODE__DASHBOARD_PHONE_VERIFY_FRAGMENT ||
                  _currentIndex ==
                      PsConst.REQUEST_CODE__MENU_PHONE_VERIFY_FRAGMENT) {
                return CallVerifyPhoneWidget(
                    userName: phoneUserName,
                    phoneNumber: phoneNumber,
                    phoneId: phoneId,
                    animationController: animationController,
                    animation: animation,
                    currentIndex: _currentIndex,
                    updateCurrentIndex: (String title, int index) {
                      updateSelectedIndexWithAnimation(title, index);
                    },
                    updateUserCurrentIndex:
                        (String title, int index, String userId) async {
                      if (userId != null) {
                        _userId = userId;
                      }
                      setState(() {
                        appBarTitle = title;
                        _currentIndex = index;
                      });
                    });
              } else if (_currentIndex ==
                      PsConst.REQUEST_CODE__DASHBOARD_USER_PROFILE_FRAGMENT ||
                  _currentIndex ==
                      PsConst.REQUEST_CODE__MENU_USER_PROFILE_FRAGMENT) {
                return ProfileView(
                  //scaffoldKey: scaffoldKey,
                  animationController: animationController,
                  flag: _currentIndex,
                  userId: _userId,
                );
              } else if (_currentIndex ==
                      PsConst
                          .REQUEST_CODE__DASHBOARD_FORGOT_PASSWORD_FRAGMENT ||
                  _currentIndex ==
                      PsConst.REQUEST_CODE__MENU_FORGOT_PASSWORD_FRAGMENT) {
                return Stack(children: <Widget>[
                  Container(
                    color: PsColors.coreBackgroundColor,
                    width: double.infinity,
                    height: double.maxFinite,
                  ),
                  CustomScrollView(
                      scrollDirection: Axis.vertical,
                      slivers: <Widget>[
                        ForgotPasswordView(
                          animationController: animationController,
                          goToLoginSelected: () {
                            animationController
                                .reverse()
                                .then<dynamic>((void data) {
                              if (!mounted) {
                                return;
                              }
                              if (_currentIndex ==
                                  PsConst
                                      .REQUEST_CODE__MENU_FORGOT_PASSWORD_FRAGMENT) {
                                updateSelectedIndexWithAnimation(
                                    Utils.getString(context, 'home_login'),
                                    PsConst.REQUEST_CODE__MENU_LOGIN_FRAGMENT);
                              }
                              if (_currentIndex ==
                                  PsConst
                                      .REQUEST_CODE__DASHBOARD_FORGOT_PASSWORD_FRAGMENT) {
                                updateSelectedIndexWithAnimation(
                                    Utils.getString(context, 'home_login'),
                                    PsConst
                                        .REQUEST_CODE__DASHBOARD_LOGIN_FRAGMENT);
                              }
                            });
                          },
                        )
                      ])
                ]);
              } else if (_currentIndex ==
                      PsConst.REQUEST_CODE__DASHBOARD_REGISTER_FRAGMENT ||
                  _currentIndex ==
                      PsConst.REQUEST_CODE__MENU_REGISTER_FRAGMENT) {
                return Stack(children: <Widget>[
                  Container(
                    color: PsColors.coreBackgroundColor,
                    width: double.infinity,
                    height: double.maxFinite,
                  ),
                  CustomScrollView(scrollDirection: Axis.vertical, slivers: <
                      Widget>[
                    RegisterView(
                        animationController: animationController,
                        onRegisterSelected: (User user) {
                          _userId = user.userId;
                          // widget.provider.psValueHolder.loginUserId = userId;
                          if (user.status == PsConst.ONE) {
                            updateSelectedIndexWithAnimationUserId(
                                Utils.getString(
                                    context, 'home__menu_drawer_profile'),
                                PsConst
                                    .REQUEST_CODE__DASHBOARD_USER_PROFILE_FRAGMENT,
                                user.userId);
                          } else {
                            if (_currentIndex ==
                                PsConst.REQUEST_CODE__MENU_REGISTER_FRAGMENT) {
                              updateSelectedIndexWithAnimation(
                                  Utils.getString(
                                      context, 'home__verify_email'),
                                  PsConst
                                      .REQUEST_CODE__MENU_VERIFY_EMAIL_FRAGMENT);
                            } else if (_currentIndex ==
                                PsConst
                                    .REQUEST_CODE__DASHBOARD_REGISTER_FRAGMENT) {
                              updateSelectedIndexWithAnimation(
                                  Utils.getString(
                                      context, 'home__verify_email'),
                                  PsConst
                                      .REQUEST_CODE__DASHBOARD_VERIFY_EMAIL_FRAGMENT);
                            } else {
                              updateSelectedIndexWithAnimationUserId(
                                  Utils.getString(
                                      context, 'home__menu_drawer_profile'),
                                  PsConst
                                      .REQUEST_CODE__DASHBOARD_USER_PROFILE_FRAGMENT,
                                  user.userId);
                            }
                          }
                        },
                        goToLoginSelected: () {
                          animationController
                              .reverse()
                              .then<dynamic>((void data) {
                            if (!mounted) {
                              return;
                            }
                            if (_currentIndex ==
                                PsConst.REQUEST_CODE__MENU_REGISTER_FRAGMENT) {
                              updateSelectedIndexWithAnimation(
                                  Utils.getString(context, 'home_login'),
                                  PsConst.REQUEST_CODE__MENU_LOGIN_FRAGMENT);
                            }
                            if (_currentIndex ==
                                PsConst
                                    .REQUEST_CODE__DASHBOARD_REGISTER_FRAGMENT) {
                              updateSelectedIndexWithAnimation(
                                  Utils.getString(context, 'home_login'),
                                  PsConst
                                      .REQUEST_CODE__DASHBOARD_LOGIN_FRAGMENT);
                            }
                          });
                        })
                  ])
                ]);
              } else if (_currentIndex ==
                      PsConst.REQUEST_CODE__DASHBOARD_VERIFY_EMAIL_FRAGMENT ||
                  _currentIndex ==
                      PsConst.REQUEST_CODE__MENU_VERIFY_EMAIL_FRAGMENT) {
                return CallVerifyEmailWidget(
                    animationController: animationController,
                    animation: animation,
                    currentIndex: _currentIndex,
                    userId: _userId,
                    updateCurrentIndex: (String title, int index) {
                      updateSelectedIndexWithAnimation(title, index);
                    },
                    updateUserCurrentIndex:
                        (String title, int index, String userId) async {
                      if (userId != null) {
                        _userId = userId;
                      }
                      setState(() {
                        appBarTitle = title;
                        _currentIndex = index;
                      });
                    });
              } else if (_currentIndex ==
                      PsConst.REQUEST_CODE__DASHBOARD_LOGIN_FRAGMENT ||
                  _currentIndex == PsConst.REQUEST_CODE__MENU_LOGIN_FRAGMENT) {
                return CallLoginWidget(
                    currentIndex: _currentIndex,
                    animationController: animationController,
                    animation: animation,
                    updateCurrentIndex: (String title, int index) {
                      updateSelectedIndexWithAnimation(title, index);
                    },
                    updateUserCurrentIndex:
                        (String title, int index, String userId) {
                      setState(() {
                        if (index != null) {
                          appBarTitle = title;
                          _currentIndex = index;
                        }
                      });
                      if (userId != null) {
                        _userId = userId;
                      }
                    });
              } else if (_currentIndex ==
                  PsConst.REQUEST_CODE__MENU_SELECT_WHICH_USER_FRAGMENT) {
                return ChangeNotifierProvider<UserProvider>(
                    lazy: false,
                    create: (BuildContext context) {
                      final UserProvider provider = UserProvider(
                          repo: userRepository, psValueHolder: valueHolder);

                      return provider;
                    },
                    child: Consumer<UserProvider>(builder:
                        (BuildContext context, UserProvider provider,
                            Widget child) {
                      if (provider == null ||
                          provider.psValueHolder.userIdToVerify == null ||
                          provider.psValueHolder.userIdToVerify == '') {
                        if (provider == null ||
                            provider.psValueHolder == null ||
                            provider.psValueHolder.loginUserId == null ||
                            provider.psValueHolder.loginUserId == '') {
                          return Stack(
                            children: <Widget>[
                              Container(
                                color: PsColors.coreBackgroundColor,
                                width: double.infinity,
                                height: double.maxFinite,
                              ),
                              CustomScrollView(
                                  scrollDirection: Axis.vertical,
                                  slivers: <Widget>[
                                    LoginView(
                                      animationController: animationController,
                                      animation: animation,
                                      onGoogleSignInSelected: (String userId) {
                                        setState(() {
                                          _currentIndex = PsConst
                                              .REQUEST_CODE__MENU_USER_PROFILE_FRAGMENT;
                                        });
                                        _userId = userId;
                                        provider.psValueHolder.loginUserId =
                                            userId;
                                      },
                                      onFbSignInSelected: (String userId) {
                                        setState(() {
                                          _currentIndex = PsConst
                                              .REQUEST_CODE__MENU_USER_PROFILE_FRAGMENT;
                                        });
                                        _userId = userId;
                                        provider.psValueHolder.loginUserId =
                                            userId;
                                      },
                                      onPhoneSignInSelected: () {
                                        if (_currentIndex ==
                                            PsConst
                                                .REQUEST_CODE__MENU_PHONE_SIGNIN_FRAGMENT) {
                                          updateSelectedIndexWithAnimation(
                                              Utils.getString(
                                                  context, 'home_phone_signin'),
                                              PsConst
                                                  .REQUEST_CODE__MENU_PHONE_SIGNIN_FRAGMENT);
                                        } else if (_currentIndex ==
                                            PsConst
                                                .REQUEST_CODE__DASHBOARD_PHONE_SIGNIN_FRAGMENT) {
                                          updateSelectedIndexWithAnimation(
                                              Utils.getString(
                                                  context, 'home_phone_signin'),
                                              PsConst
                                                  .REQUEST_CODE__DASHBOARD_PHONE_SIGNIN_FRAGMENT);
                                        } else if (_currentIndex ==
                                            PsConst
                                                .REQUEST_CODE__MENU_SELECT_WHICH_USER_FRAGMENT) {
                                          updateSelectedIndexWithAnimation(
                                              Utils.getString(
                                                  context, 'home_phone_signin'),
                                              PsConst
                                                  .REQUEST_CODE__MENU_PHONE_SIGNIN_FRAGMENT);
                                        } else if (_currentIndex ==
                                            PsConst
                                                .REQUEST_CODE__DASHBOARD_SELECT_WHICH_USER_FRAGMENT) {
                                          updateSelectedIndexWithAnimation(
                                              Utils.getString(
                                                  context, 'home_phone_signin'),
                                              PsConst
                                                  .REQUEST_CODE__DASHBOARD_PHONE_SIGNIN_FRAGMENT);
                                        } else {
                                          updateSelectedIndexWithAnimation(
                                              Utils.getString(
                                                  context, 'home_phone_signin'),
                                              PsConst
                                                  .REQUEST_CODE__DASHBOARD_PHONE_SIGNIN_FRAGMENT);
                                        }
                                      },
                                      onProfileSelected: (String userId) {
                                        setState(() {
                                          _currentIndex = PsConst
                                              .REQUEST_CODE__MENU_USER_PROFILE_FRAGMENT;
                                          _userId = userId;
                                          provider.psValueHolder.loginUserId =
                                              userId;
                                        });
                                      },
                                      onForgotPasswordSelected: () {
                                        setState(() {
                                          _currentIndex = PsConst
                                              .REQUEST_CODE__MENU_FORGOT_PASSWORD_FRAGMENT;
                                          appBarTitle = Utils.getString(
                                              context, 'home__forgot_password');
                                        });
                                      },
                                      onSignInSelected: () {
                                        updateSelectedIndexWithAnimation(
                                            Utils.getString(
                                                context, 'home__register'),
                                            PsConst
                                                .REQUEST_CODE__MENU_REGISTER_FRAGMENT);
                                      },
                                    ),
                                  ])
                            ],
                          );
                        } else {
                          return ProfileView(
                            //scaffoldKey: scaffoldKey,
                            animationController: animationController,
                            flag: _currentIndex,
                          );
                        }
                      } else {
                        return CallVerifyEmailWidget(
                            animationController: animationController,
                            animation: animation,
                            currentIndex: _currentIndex,
                            userId: _userId,
                            updateCurrentIndex: (String title, int index) {
                              updateSelectedIndexWithAnimation(title, index);
                            },
                            updateUserCurrentIndex:
                                (String title, int index, String userId) async {
                              if (userId != null) {
                                _userId = userId;
                                provider.psValueHolder.loginUserId = userId;
                              }
                              setState(() {
                                appBarTitle = title;
                                _currentIndex = index;
                              });
                            });
                      }
                    }));
              } else if (_currentIndex ==
                  PsConst.REQUEST_CODE__MENU_FAVOURITE_FRAGMENT) {
                return FavouriteProductListView(
                    animationController: animationController);
              } else if (_currentIndex ==
                  PsConst.REQUEST_CODE__MENU_TRANSACTION_FRAGMENT) {
                return TransactionListView(
                    scaffoldKey: scaffoldKey,
                    animationController: animationController);
              } else if (_currentIndex ==
                  PsConst.REQUEST_CODE__MENU_USER_HISTORY_FRAGMENT) {
                return HistoryListView(
                    animationController: animationController);
              } else if (_currentIndex ==
                  PsConst.REQUEST_CODE__MENU_USER_CREATE_RESERVATION_FRAGMENT) {
                return CreateReservationView(
                    animationController: animationController,
                    shopId: valueHolder.shopId,
                    shopName: valueHolder.shopName);
              } else if (_currentIndex ==
                  PsConst.REQUEST_CODE__MENU_USER_RESERVATION_LIST_FRAGMENT) {
                return ReservationListView(
                    scaffoldKey: scaffoldKey,
                    animationController: animationController);
              } else if (_currentIndex ==
                  PsConst.REQUEST_CODE__MENU_COLLECTION_FRAGMENT) {
                return CollectionHeaderListView(
                    animationController: animationController,
                    shopId: valueHolder.shopId);
              } else if (_currentIndex ==
                  PsConst.REQUEST_CODE__MENU_LANGUAGE_FRAGMENT) {
                return LanguageSettingView(
                    animationController: animationController,
                    languageIsChanged: () {});
              } else if (_currentIndex ==
                  PsConst.REQUEST_CODE__MENU_CONTACT_US_FRAGMENT) {
                return ContactUsView(animationController: animationController);
              } else if (_currentIndex ==
                  PsConst.REQUEST_CODE__MENU_SETTING_FRAGMENT) {
                return SettingView(
                  animationController: animationController,
                  userId: _userId,
                  //currentIndex: _currentIndex,
                );
              } else if (_currentIndex ==
                  PsConst.REQUEST_CODE__MENU_TERMS_AND_CONDITION_FRAGMENT) {
                return TermsAndConditionsView(
                  animationController: animationController,
                );
              } else if (_currentIndex ==
                  PsConst.REQUEST_CODE__DASHBOARD_BASKET_FRAGMENT) {
                return BasketListView(
                  animationController: animationController,
                );
              } else {
                //animationController.forward();
                return MainDashboardViewWidget(
                  context: context,
                );
              }
            })),
      
    );
  }
}
