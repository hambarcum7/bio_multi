import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/constant/route_paths.dart';
import 'package:biomart/provider/basket/basket_provider.dart';
import 'package:biomart/repository/basket_repository.dart';
import 'package:biomart/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BottomBarViewWidget extends StatefulWidget {
  const BottomBarViewWidget({
    Key key,
    @required this.currentIdex,
  }) : super(key: key);

  final int currentIdex;

  @override
  BottomBarViewWidgetState createState() => BottomBarViewWidgetState();
}

class BottomBarViewWidgetState extends State<BottomBarViewWidget> {
  BasketRepository basketRepository;
  int index;

  void _selectTab(int index) {
    switch (index) {
      case 0:
        Navigator.pushReplacementNamed(context, RoutePaths.main);
        break;
      case 1:
        Navigator.pushReplacementNamed(context, RoutePaths.basketList);
        break;
      case 2:
        Navigator.pushReplacementNamed(context, RoutePaths.setting);
        break;
      default:
    }
  }

  void updateIndex(int newIndex) {
    setState(() {
      index = newIndex;
    });
  }

  @override
  void initState() {
    super.initState();
    index = widget.currentIdex;
  }

  @override
  Widget build(BuildContext context) {
    basketRepository = Provider.of<BasketRepository>(context);
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      showUnselectedLabels: false,
      currentIndex: index,
      backgroundColor: PsColors.coreBackgroundColor,
      selectedItemColor: PsColors.mainColor,
      elevation: 0,
      showSelectedLabels: false,
      enableFeedback: false,
      onTap: _selectTab,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: const Hero(
            tag: 'home',
            child: ImageIcon(
              AssetImage(
                'assets/login_icons/home.png',
              ),
            ),
          ),
          label: Utils.getString(context, 'dashboard__home'),
        ),
        BottomNavigationBarItem(
          icon: ChangeNotifierProvider<BasketProvider>(
            lazy: false,
            create: (BuildContext context) {
              final BasketProvider provider =
                  BasketProvider(repo: basketRepository);
              provider.loadBasketList();
              return provider;
            },
            child: Consumer<BasketProvider>(builder: (BuildContext context,
                BasketProvider basketProvider, Widget child) {
              return Container(
                width: 30,
                height: 30,
                color: PsColors.coreBackgroundColor,
                child: Stack(
                  children: <Widget>[
                    const Hero(
                      tag: 'card',
                      child: Align(
                        alignment: Alignment.bottomLeft,
                        child: ImageIcon(
                          AssetImage(
                            'assets/login_icons/card.png',
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: const Alignment(1, -0.8),
                      child: Hero(
                        tag: 'count',
                        child: Container(
                          width: PsDimens.space15,
                          height: PsDimens.space15,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: PsColors.mainColor,
                          ),
                          child: Center(
                            child: Text(
                              basketProvider.basketList.data.length > 99
                                  ? '99+'
                                  : basketProvider.basketList.data.length
                                      .toString(),
                              style: TextStyle(
                                color: PsColors.white,
                                fontSize: 10.0,
                                fontWeight: FontWeight.w400,
                                decoration: TextDecoration.none,
                                fontFamily: 'Roboto',
                              ),
                              maxLines: 1,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            }),
          ),
          label: Utils.getString(context, 'home__bottom_app_bar_basket_list'),
        ),
        BottomNavigationBarItem(
          icon: const Hero(
            tag: 'user',
            child: ImageIcon(
              AssetImage(
                'assets/login_icons/user.png',
              ),
            ),
          ),
          label: Utils.getString(context, 'home__bottom_app_bar_login'),
        ),
      ],
    );
  }
}
