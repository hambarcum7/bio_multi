import 'dart:io';

import 'package:biomart/api/common/ps_status.dart';
import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_constants.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/constant/route_paths.dart';
import 'package:biomart/provider/category/category_provider.dart';
import 'package:biomart/provider/product/discount_product_provider.dart';
import 'package:biomart/provider/product/search_product_provider.dart';
import 'package:biomart/provider/product/trending_product_provider.dart';
import 'package:biomart/provider/shop/new_shop_provider.dart';
import 'package:biomart/provider/shop/trending_shop_provider.dart';
import 'package:biomart/repository/category_repository.dart';
import 'package:biomart/repository/product_collection_repository.dart';
import 'package:biomart/repository/product_repository.dart';
import 'package:biomart/repository/shop_info_repository.dart';
import 'package:biomart/repository/shop_repository.dart';
import 'package:biomart/ui/blog/list/blog_horizonta_list_view.dart';
import 'package:biomart/ui/category/item/category_horizontal_list_item.dart';
import 'package:biomart/ui/common/base/ps_widget_with_multi_provider.dart';
import 'package:biomart/ui/common/dialog/confirm_dialog_view.dart';
import 'package:biomart/ui/common/dialog/rating_dialog/core.dart';
import 'package:biomart/ui/common/dialog/rating_dialog/style.dart';
import 'package:biomart/ui/common/ps_admob_banner_widget.dart';
import 'package:biomart/ui/common/ps_frame_loading_widget.dart';
import 'package:biomart/ui/dashboard/view_pages/call_bottom_view.dart';
import 'package:biomart/ui/product/item/product_horizontal_list_item.dart';
import 'package:biomart/ui/product/list_with_filter/product_list_with_filter_container.dart';
import 'package:biomart/ui/shop_list/item/shop_horizontal_list_item.dart';
import 'package:biomart/ui/user/login/login_view.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/common/ps_value_holder.dart';
import 'package:biomart/viewobject/holder/intent_holder/product_detail_intent_holder.dart';
import 'package:biomart/viewobject/holder/intent_holder/product_list_intent_holder.dart';
import 'package:biomart/viewobject/holder/intent_holder/shop_data_intent_holder.dart';
import 'package:biomart/viewobject/holder/intent_holder/shop_list_intent_holder.dart';
import 'package:biomart/viewobject/holder/product_parameter_holder.dart';
import 'package:biomart/viewobject/holder/shop_parameter_holder.dart';
import 'package:biomart/viewobject/holder/touch_count_parameter_holder.dart';
import 'package:biomart/viewobject/product.dart';
import 'package:biomart/viewobject/product_collection_header.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_admob/flutter_native_admob.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:shimmer/shimmer.dart';

class MainDashboardViewWidget extends StatefulWidget {
  const MainDashboardViewWidget({
    this.context,
  });
  final BuildContext context;

  @override
  _MainDashboardViewWidgetState createState() =>
      _MainDashboardViewWidgetState();
}

class _MainDashboardViewWidgetState extends State<MainDashboardViewWidget>
    with SingleTickerProviderStateMixin {
  PsValueHolder valueHolder;
  CategoryRepository repo1;
  ProductRepository repo2;
  ProductCollectionRepository repo3;
  ShopInfoRepository shopInfoRepository;
  TrendingShopProvider trendingShopProvider;
  NewShopProvider newShopProvider;
  ShopRepository shopRepository;
  TextEditingController searchTextController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TrendingProductProvider _trendingProductProvider;
  DiscountProductProvider _discountProductProvider;
  CategoryProvider _categoryProvider;
  AnimationController animationController;
  final int count = 3;

  final RateMyApp _rateMyApp = RateMyApp(
      preferencesPrefix: 'rateMyApp_',
      minDays: 0,
      minLaunches: 1,
      remindDays: 5,
      remindLaunches: 1);

  @override
  void initState() {
    super.initState();
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    if (_categoryProvider != null) {
      _categoryProvider
          .loadCategoryList(_categoryProvider.latestCategoryParameterHolder);
    }

    if (Platform.isAndroid) {
      _rateMyApp.init().then((_) {
        if (_rateMyApp.shouldOpenDialog) {
          _rateMyApp.showStarRateDialog(
            context,
            title: Utils.getString(context, 'home__menu_drawer_rate_this_app'),
            message: Utils.getString(context, 'rating_popup_dialog_message'),
            ignoreNativeDialog: true,
            actionsBuilder: (BuildContext context, double stars) {
              return <Widget>[
                TextButton(
                  child: Text(
                    Utils.getString(context, 'dialog__ok'),
                  ),
                  onPressed: () async {
                    if (stars != null) {
                      // _rateMyApp.save().then((void v) => Navigator.pop(context));
                      Navigator.pop(context);
                      if (stars < 1) {
                      } else if (stars >= 1 && stars <= 3) {
                        await _rateMyApp
                            .callEvent(RateMyAppEventType.laterButtonPressed);
                        await showDialog<dynamic>(
                            context: context,
                            builder: (BuildContext context) {
                              return ConfirmDialogView(
                                description: Utils.getString(
                                    context, 'rating_confirm_message'),
                                leftButtonText:
                                    Utils.getString(context, 'dialog__cancel'),
                                rightButtonText: Utils.getString(
                                    context, 'home__menu_drawer_contact_us'),
                                onAgreeTap: () {
                                  Navigator.pop(context);
                                  Navigator.pushNamed(
                                    context,
                                    RoutePaths.contactUs,
                                  );
                                },
                              );
                            });
                      } else if (stars >= 4) {
                        await _rateMyApp
                            .callEvent(RateMyAppEventType.rateButtonPressed);
                        if (Platform.isIOS) {
                          Utils.launchAppStoreURL(
                              iOSAppId: PsConfig.iOSAppStoreId,
                              writeReview: true);
                        } else {
                          Utils.launchURL();
                        }
                      }
                    } else {
                      Navigator.pop(context);
                    }
                  },
                )
              ];
            },
            onDismissed: () =>
                _rateMyApp.callEvent(RateMyAppEventType.laterButtonPressed),
            dialogStyle: const DialogStyle(
              titleAlign: TextAlign.center,
              messageAlign: TextAlign.center,
              messagePadding: EdgeInsets.only(bottom: 16.0),
            ),
            starRatingOptions: const StarRatingOptions(),
          );
        }
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    animationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    repo1 = Provider.of<CategoryRepository>(context);
    repo2 = Provider.of<ProductRepository>(context);
    repo3 = Provider.of<ProductCollectionRepository>(context);
    shopInfoRepository = Provider.of<ShopInfoRepository>(context);
    shopRepository = Provider.of<ShopRepository>(context);
    valueHolder = Provider.of<PsValueHolder>(context);

    // final Animation<double> animation = Tween<double>(begin: 0.0, end: 1.0)
    //     .animate(CurvedAnimation(
    //         parent: animationController,
    //         curve: const Interval(0.5 * 1, 1.0, curve: Curves.fastOutSlowIn)));
    // animationController.forward();

    return PsWidgetWithMultiProvider(
      child: MultiProvider(
          providers: <SingleChildWidget>[
            ChangeNotifierProvider<NewShopProvider>(
                lazy: false,
                create: (BuildContext context) {
                  newShopProvider = NewShopProvider(
                      repo: shopRepository, limit: PsConfig.SHOP_LOADING_LIMIT);
                  return newShopProvider;
                }),
            ChangeNotifierProvider<CategoryProvider>(
              lazy: false,
              create: (BuildContext context) {
                _categoryProvider ??= CategoryProvider(
                    repo: repo1,
                    psValueHolder: valueHolder,
                    limit: PsConfig.CATEGORY_LOADING_LIMIT);
                _categoryProvider
                    .loadCategoryList(
                        _categoryProvider.latestCategoryParameterHolder)
                    .then(
                  (dynamic value) {
                    // Utils.psPrint("Is Has Internet " + value);
                    final bool isConnectedToIntenet = value ?? bool;
                    if (!isConnectedToIntenet) {
                      Fluttertoast.showToast(
                          msg: 'No Internet Connectiion. Please try again !',
                          toastLength: Toast.LENGTH_LONG,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.blueGrey,
                          textColor: Colors.white);
                    }
                  },
                );
                return _categoryProvider;
              },
            ),
            ChangeNotifierProvider<SearchProductProvider>(
                lazy: false,
                create: (BuildContext context) {
                  final SearchProductProvider provider = SearchProductProvider(
                      repo: repo2,
                      limit: PsConfig.LATEST_PRODUCT_LOADING_LIMIT);
                  // provider.latestProductParameterHolder.shopId = shopId;
                  provider.loadProductListByKey(
                      provider.latestProductParameterHolder);
                  return provider;
                }),
            ChangeNotifierProvider<DiscountProductProvider>(
                lazy: false,
                create: (BuildContext context) {
                  _discountProductProvider = DiscountProductProvider(
                      repo: repo2,
                      limit: PsConfig.DISCOUNT_PRODUCT_LOADING_LIMIT);
                  // provider.discountProductParameterHolder.shopId = shopId;
                  _discountProductProvider.loadProductList(
                      _discountProductProvider.discountProductParameterHolder);
                  return _discountProductProvider;
                }),
            ChangeNotifierProvider<TrendingProductProvider>(
                lazy: false,
                create: (BuildContext context) {
                  _trendingProductProvider = TrendingProductProvider(
                      repo: repo2,
                      limit: PsConfig.TRENDING_PRODUCT_LOADING_LIMIT);
                  // provider.trendingProductParameterHolder.shopId = shopId;
                  _trendingProductProvider.loadProductList(
                      _trendingProductProvider.trendingProductParameterHolder);
                  return _trendingProductProvider;
                }),
            ChangeNotifierProvider<TrendingShopProvider>(
                lazy: false,
                create: (BuildContext context) {
                  trendingShopProvider = TrendingShopProvider(
                      repo: shopRepository, limit: PsConfig.SHOP_LOADING_LIMIT);
                  trendingShopProvider.loadShopList();
                  return trendingShopProvider;
                }),
          ],
          child: Scaffold(
            backgroundColor: PsColors.coreBackgroundColor,
            bottomNavigationBar: const BottomBarViewWidget(
              currentIdex: 0,
            ),
            appBar: AppBar(
              automaticallyImplyLeading: false,
              backgroundColor: PsColors.coreBackgroundColor,
              elevation: 0,
              title: const AppName(),
              centerTitle: true,
              actions: <Widget>[
                GestureDetector(
                  onTap: () {
                    final ProductParameterHolder productParameterHolder =
                        ProductParameterHolder().getLatestParameterHolder();
                    productParameterHolder.searchTerm = addressController.text;
                    Utils.psPrint(productParameterHolder.searchTerm);
                    Navigator.pushNamed(
                      context,
                      RoutePaths.dashboardsearchFood,
                      arguments: ProductListIntentHolder(
                          appBarTitle: Utils.getString(
                              context, 'home_search__app_bar_title'),
                          productParameterHolder: productParameterHolder),
                    );
                  },
                  child: Container(
                    margin: const EdgeInsets.only(right: 11.0),
                    width: 24.0,
                    height: 24.0,
                    child: Hero(
                      tag: 'search',
                      child: Icon(
                        Icons.search,
                        color: PsColors.textPrimaryColor,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            body: Container(
              color: PsColors.coreBackgroundColor,
              child: RefreshIndicator(
                onRefresh: () {
                  _trendingProductProvider.resetTrendingProductList(
                      _trendingProductProvider.trendingProductParameterHolder);
                  _discountProductProvider.resetDiscountProductList(
                      _discountProductProvider.discountProductParameterHolder);
                  trendingShopProvider.refreshShopList();
                  return _categoryProvider
                      .resetCategoryList(
                          _categoryProvider.latestCategoryParameterHolder)
                      .then((dynamic value) {
                    // Utils.psPrint("Is Has Internet " + value);
                    final bool isConnectedToIntenet = value ?? bool;
                    if (!isConnectedToIntenet) {
                      Fluttertoast.showToast(
                          msg: 'No Internet Connectiion. Please try again !',
                          toastLength: Toast.LENGTH_LONG,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.blueGrey,
                          textColor: Colors.white);
                    }
                  });
                },
                child: CustomScrollView(
                  physics: const AlwaysScrollableScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  slivers: <Widget>[
                    HomeCategoryHorizontalListWidget(
                      psValueHolder: valueHolder,
                    ),
                    SliderList(
                        // animationController: animationController,
                        // animation: Tween<double>(begin: 0.0, end: 1.0).animate(
                        //   CurvedAnimation(
                        //     parent: animationController,
                        //     curve: Interval((1 / count) * 3, 1.0,
                        //         curve: Curves.fastOutSlowIn),
                        //   ),
                        // ),
                        ),
                    const HomeTrendingProductHorizontalListWidget(
                        // animationConat_id":"cataa6aa646bbtroller:
                        //     animationController, //animationController,
                        // animation: Tween<double>(begin: 0.0, end: 1.0).animate(
                        //   CurvedAnimation(
                        //     parent: animationController,
                        //     curve: Interval((1 / count) * 3, 1.0,
                        //         curve: Curves.fastOutSlowIn),
                        //   ),
                        // ), //animation
                        ),
                    const _DiscountProductHorizontalListWidget(
                        // animationController:
                        //     animationController, //animationController,
                        // animation: Tween<double>(begin: 0.0, end: 1.0).animate(
                        //   CurvedAnimation(
                        //     parent: animationController,
                        //     curve: Interval((1 / count) * 3, 1.0,
                        //         curve: Curves.fastOutSlowIn),
                        //   ),
                        // ), //animation
                        ),
                    // HomeTrendingProductHorizontalListWidget(
                    //   animationController:
                    //       animationController, //animationController,
                    //   animation: Tween<double>(begin: 0.0, end: 1.0).animate(
                    //     CurvedAnimation(
                    //       parent: animationController,
                    //       curve: Interval((1 / count) * 4, 1.0,
                    //           curve: Curves.fastOutSlowIn),
                    //     ),
                    //   ), //animation
                    // ),
                    const BlogHorizontalListView(
                        // animationController: animationController,
                        // animation: Tween<double>(begin: 0.0, end: 1.0).animate(
                        //   CurvedAnimation(
                        //     parent: animationController,
                        //     curve: Interval((1 / count) * 3, 1.0,
                        //         curve: Curves.fastOutSlowIn),
                        //   ),
                        // ),
                        ),
                    const SliverToBoxAdapter(
                      child: SizedBox(
                        height: 40,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )),
    );
  }
}

class SliderList extends StatelessWidget {
  SliderList({
    Key key,
    // @required this.animationController,
    // @required this.animation,
  }) : super(key: key);

  // final AnimationController animationController;
  // final Animation<double> animation;
  //TODO back-ic petqa ga
  final List<String> pathList = <String>[
    'https://i.imgur.com/sfRcRTQ.jpeg',
    'https://i.imgur.com/bBfTTym.jpeg'
  ];

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Container(
        margin: const EdgeInsets.only(top: 8.0),
        height: 135.0,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          physics: const PageScrollPhysics(),
          itemCount: pathList.length,
          itemBuilder: (BuildContext context, int index) {
            return _SliderCard(networkImagePath: pathList[index]);
          },
        ),
      ),
    );
  }
}

class _SliderCard extends StatelessWidget {
  const _SliderCard({
    Key key,
    @required this.networkImagePath,
  }) : super(key: key);
  final String networkImagePath;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 8),
        height: 135.0,
        width: MediaQuery.of(context).size.width - 16.0,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(8.0),
          child: Image.network(
            networkImagePath,
            fit: BoxFit.fitWidth,
            loadingBuilder: (BuildContext context, Widget child,
                ImageChunkEvent loadingProgress) {
              if (loadingProgress == null) {
                return child;
              }
              return Center(
                child: CircularProgressIndicator(
                  color: Colors.blue,
                  value: loadingProgress.expectedTotalBytes != null
                      ? loadingProgress.cumulativeBytesLoaded /
                          loadingProgress.expectedTotalBytes
                      : null,
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

class _HomeNewShopHorizontalListWidget extends StatefulWidget {
  const _HomeNewShopHorizontalListWidget(
      {Key key,
      @required this.animationController,
      @required this.animation,
      @required this.psValueHolder})
      : super(key: key);

  final AnimationController animationController;
  final Animation<double> animation;
  final PsValueHolder psValueHolder;

  @override
  __HomeNewShopHorizontalListWidgetState createState() =>
      __HomeNewShopHorizontalListWidgetState();
}

class __HomeNewShopHorizontalListWidgetState
    extends State<_HomeNewShopHorizontalListWidget> {
  bool isConnectedToInternet = false;
  bool isSuccessfullyLoaded = true;

  void checkConnection() {
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
      if (isConnectedToInternet && PsConst.SHOW_ADMOB) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!isConnectedToInternet && PsConst.SHOW_ADMOB) {
      print('loading ads....');
      checkConnection();
    }
    return SliverToBoxAdapter(
      child: Consumer<NewShopProvider>(
        builder: (BuildContext context, NewShopProvider newShopProvider,
            Widget child) {
          return AnimatedBuilder(
              animation: widget.animationController,
              child: (newShopProvider.shopList.data != null &&
                      newShopProvider.shopList.data.isNotEmpty)
                  ? Column(children: <Widget>[
                      _MyHeaderWidget(
                        headerName: Utils.getString(
                            context, 'shop_dashboard__shop_near_you'),
                        viewAllClicked: () {
                          Navigator.pushNamed(context, RoutePaths.shopList,
                              arguments: ShopListIntentHolder(
                                appBarTitle: Utils.getString(
                                    context, 'shop_dashboard__shop_near_you'),
                                shopParameterHolder:
                                    newShopProvider.shopNearYouParameterHolder,
                              ));
                        },
                      ),
                      // Padding(
                      //   padding: const EdgeInsets.only(
                      //       left: PsDimens.space16,
                      //       right: PsDimens.space16,
                      //       bottom: PsDimens.space16),
                      // child:
                      Container(
                          height: PsDimens.space320,
                          width: MediaQuery.of(context).size.width,
                          child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              padding:
                                  const EdgeInsets.only(left: PsDimens.space16),
                              itemCount: newShopProvider.shopList.data.length,
                              itemBuilder: (BuildContext context, int index) {
                                if (newShopProvider.shopList.status ==
                                    PsStatus.BLOCK_LOADING) {
                                  return Shimmer.fromColors(
                                      baseColor: PsColors.grey,
                                      highlightColor: PsColors.white,
                                      child: Row(children: const <Widget>[
                                        PsFrameUIForLoading(),
                                      ]));
                                } else {
                                  return ShopHorizontalListItem(
                                    shop: newShopProvider.shopList.data[index],
                                    onTap: () async {
                                      final String loginUserId =
                                          Utils.checkUserLoginId(
                                              widget.psValueHolder);

                                      final TouchCountParameterHolder
                                          touchCountParameterHolder =
                                          TouchCountParameterHolder(
                                              typeId: newShopProvider
                                                  .shopList.data[index].id,
                                              typeName: PsConst
                                                  .FILTERING_TYPE_NAME_SHOP,
                                              userId: loginUserId,
                                              shopId: newShopProvider
                                                  .shopList.data[index].id);
                                      newShopProvider.postTouchCount(
                                          touchCountParameterHolder.toMap());

                                      await newShopProvider.replaceShop(
                                          newShopProvider
                                              .shopList.data[index].id,
                                          newShopProvider
                                              .shopList.data[index].name);
                                      Navigator.pushNamed(
                                          context, RoutePaths.shop_dashboard,
                                          arguments: ShopDataIntentHolder(
                                              shopId: newShopProvider
                                                  .shopList.data[index].id,
                                              shopName: newShopProvider
                                                  .shopList.data[index].name));
                                    },
                                  );
                                }
                              })),
                      // ),
                      const PsAdMobBannerWidget(
                        admobSize: NativeAdmobType.full,
                      ),
                    ])
                  : Container(
                      color: PsColors.coreBackgroundColor,
                    ),
              builder: (BuildContext context, Widget child) {
                return FadeTransition(
                  opacity: widget.animation,
                  child: Transform(
                    transform: Matrix4.translationValues(
                        0.0, 100 * (1.0 - widget.animation.value), 0.0),
                    child: child,
                  ),
                );
              });
        },
      ),
    );
  }
}

class _HomePopularShopHorizontalListWidget extends StatefulWidget {
  const _HomePopularShopHorizontalListWidget(
      {Key key,
      @required this.animationController,
      @required this.animation,
      @required this.psValueHolder})
      : super(key: key);

  final AnimationController animationController;
  final Animation<double> animation;
  final PsValueHolder psValueHolder;

  @override
  __HomePopularShopHorizontalListWidgetState createState() =>
      __HomePopularShopHorizontalListWidgetState();
}

class __HomePopularShopHorizontalListWidgetState
    extends State<_HomePopularShopHorizontalListWidget> {
  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Consumer<TrendingShopProvider>(
        builder: (BuildContext context, TrendingShopProvider shopProvider,
            Widget child) {
          return AnimatedBuilder(
            animation: widget.animationController,
            child: (shopProvider.shopList.data != null &&
                    shopProvider.shopList.data.isNotEmpty)
                ? Column(children: <Widget>[
                    _MyHeaderWidget(
                      headerName: Utils.getString(
                          context, 'shop_dashboard__trending_shop'),
                      viewAllClicked: () {
                        Navigator.pushNamed(context, RoutePaths.shopList,
                            arguments: ShopListIntentHolder(
                              appBarTitle: Utils.getString(
                                  context, 'shop_dashboard__trending_shop'),
                              shopParameterHolder: ShopParameterHolder()
                                  .getTrendingShopParameterHolder(),
                            ));
                      },
                    ),
                    // Padding(
                    //   padding: const EdgeInsets.only(
                    //       left: PsDimens.space16,
                    //       right: PsDimens.space16,
                    //       bottom: PsDimens.space16),
                    //   child:
                    Container(
                        height: 900,
                        width: MediaQuery.of(context).size.width,
                        child: RefreshIndicator(
                          child: CustomScrollView(
                            physics: const AlwaysScrollableScrollPhysics(),
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                            slivers: <Widget>[
                              SliverPadding(
                                  padding: const EdgeInsets.fromLTRB(
                                      PsDimens.space16, 0, PsDimens.space16, 0),
                                  sliver: SliverGrid(
                                      gridDelegate:
                                          const SliverGridDelegateWithMaxCrossAxisExtent(
                                              maxCrossAxisExtent: 350,
                                              childAspectRatio: 1.0),
                                      delegate: SliverChildBuilderDelegate(
                                        (BuildContext context, int index) {
                                          if (shopProvider.shopList.status ==
                                              PsStatus.BLOCK_LOADING) {
                                            return Shimmer.fromColors(
                                                baseColor: PsColors.grey,
                                                highlightColor: PsColors.white,
                                                child: Row(
                                                    children: const <Widget>[
                                                      PsFrameUIForLoading(),
                                                    ]));
                                          } else {
                                            return ShopHorizontalListItem(
                                              shop: shopProvider
                                                  .shopList.data[index],
                                              onTap: () async {
                                                final String loginUserId =
                                                    Utils.checkUserLoginId(
                                                        widget.psValueHolder);

                                                final TouchCountParameterHolder
                                                    touchCountParameterHolder =
                                                    TouchCountParameterHolder(
                                                        typeId: shopProvider
                                                            .shopList
                                                            .data[index]
                                                            .id,
                                                        typeName: PsConst
                                                            .FILTERING_TYPE_NAME_SHOP,
                                                        userId: loginUserId,
                                                        shopId: shopProvider
                                                            .shopList
                                                            .data[index]
                                                            .id);
                                                shopProvider.postTouchCount(
                                                    touchCountParameterHolder
                                                        .toMap());

                                                await shopProvider.replaceShop(
                                                    shopProvider.shopList
                                                        .data[index].id,
                                                    shopProvider.shopList
                                                        .data[index].name);
                                                final dynamic result =
                                                    await Navigator.pushNamed(
                                                        context,
                                                        RoutePaths
                                                            .shop_dashboard,
                                                        arguments:
                                                            ShopDataIntentHolder(
                                                                shopId:
                                                                    shopProvider
                                                                        .shopList
                                                                        .data[
                                                                            index]
                                                                        .id,
                                                                shopName:
                                                                    shopProvider
                                                                        .shopList
                                                                        .data[
                                                                            index]
                                                                        .name));

                                                if (result != null && result) {
                                                  setState(() {
                                                    shopProvider
                                                        .refreshShopList();
                                                  });
                                                }
                                              },
                                            );
                                          }
                                        },
                                        childCount:
                                            shopProvider.shopList.data.length,
                                      )))
                            ],
                          ),
                          onRefresh: () {
                            return shopProvider.refreshShopList();
                          },
                        )),
                    // )
                  ])
                : Container(
                    color: PsColors.coreBackgroundColor,
                  ),
            builder: (BuildContext context, Widget child) {
              return FadeTransition(
                opacity: widget.animation,
                child: Transform(
                  transform: Matrix4.translationValues(
                      0.0, 100 * (1.0 - widget.animation.value), 0.0),
                  child: child,
                ),
              );
            },
          );
        },
      ),
    );
  }
}

class HomeTrendingProductHorizontalListWidget extends StatefulWidget {
  const HomeTrendingProductHorizontalListWidget({
    Key key,
    // @required this.animationController,
    // @required this.animation,
  }) : super(key: key);

  // final AnimationController animationController;
  // final Animation<double> animation;

  @override
  State<HomeTrendingProductHorizontalListWidget> createState() =>
      HomeTrendingProductHorizontalListWidgetState();
}

class HomeTrendingProductHorizontalListWidgetState
    extends State<HomeTrendingProductHorizontalListWidget> {
  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Consumer<TrendingProductProvider>(
        builder: (BuildContext context, TrendingProductProvider productProvider,
            Widget child) {
          return (productProvider.productList.data != null &&
                  productProvider.productList.data.isNotEmpty)
              ? Column(
                  children: <Widget>[
                    _MyHeaderWidget(
                      headerName: Utils.getString(
                          context, 'dashboard__trending_product'),
                      viewAllClicked: () {
                        Navigator.pushNamed(
                          context,
                          RoutePaths.filterProductList,
                          arguments: ProductListIntentHolder(
                            appBarTitle: Utils.getString(
                                context, 'dashboard__trending_product'),
                            productParameterHolder: ProductParameterHolder()
                                .getTrendingParameterHolder(),
                          ),
                        );
                      },
                    ),
                    Container(
                      height: 302,
                      color: PsColors.coreBackgroundColor,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: productProvider.productList.data.length,
                        itemBuilder: (BuildContext context, int index) {
                          if (productProvider.productList.status ==
                              PsStatus.BLOCK_LOADING) {
                            return Shimmer.fromColors(
                              baseColor: PsColors.grey,
                              highlightColor: PsColors.white,
                              child: Row(
                                children: const <Widget>[
                                  PsFrameUIForLoading(),
                                ],
                              ),
                            );
                          } else {
                            final Product product =
                                productProvider.productList.data[index];
                            return ProductHorizontalListItem(
                              coreTagKey: productProvider.hashCode.toString() +
                                  product.id,
                              product: productProvider.productList.data[index],
                              productId: product.id,
                              onTap: () {
                                final ProductDetailIntentHolder holder =
                                    ProductDetailIntentHolder(
                                  productId: product.id,
                                  heroTagImage:
                                      productProvider.hashCode.toString() +
                                          product.id +
                                          PsConst.HERO_TAG__IMAGE,
                                  heroTagTitle:
                                      productProvider.hashCode.toString() +
                                          product.id +
                                          PsConst.HERO_TAG__TITLE,
                                  heroTagOriginalPrice:
                                      productProvider.hashCode.toString() +
                                          product.id +
                                          PsConst.HERO_TAG__ORIGINAL_PRICE,
                                  heroTagUnitPrice:
                                      productProvider.hashCode.toString() +
                                          product.id +
                                          PsConst.HERO_TAG__UNIT_PRICE,
                                );
                                Navigator.pushNamed(
                                  context,
                                  RoutePaths.productDetail,
                                  arguments: holder,
                                );
                              },
                            );
                          }
                        },
                      ),
                    ),
                  ],
                )
              : Container(
                  color: PsColors.coreBackgroundColor,
                );
        },
      ),
    );
  }
}

class _DiscountProductHorizontalListWidget extends StatefulWidget {
  const _DiscountProductHorizontalListWidget({
    Key key,
    // @required this.animationController,
    // @required this.animation,
  }) : super(key: key);

  // final AnimationController animationController;
  // final Animation<double> animation;

  @override
  __DiscountProductHorizontalListWidgetState createState() =>
      __DiscountProductHorizontalListWidgetState();
}

class __DiscountProductHorizontalListWidgetState
    extends State<_DiscountProductHorizontalListWidget> {
  bool isConnectedToInternet = false;
  bool isSuccessfullyLoaded = true;

  void checkConnection() {
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
      if (isConnectedToInternet && PsConst.SHOW_ADMOB) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!isConnectedToInternet && PsConst.SHOW_ADMOB) {
      print('loading ads....');
      checkConnection();
    }
    return SliverToBoxAdapter(
        // fdfdf
        child: Consumer<DiscountProductProvider>(builder: (BuildContext context,
            DiscountProductProvider productProvider, Widget child) {
      return (productProvider.productList.data != null &&
              productProvider.productList.data.isNotEmpty)
          ? Column(children: <Widget>[
              _MyHeaderWidget(
                headerName:
                    Utils.getString(context, 'dashboard__discount_product'),
                viewAllClicked: () {
                  Navigator.pushNamed(context, RoutePaths.filterProductList,
                      arguments: ProductListIntentHolder(
                          appBarTitle: Utils.getString(
                              context, 'dashboard__discount_product'),
                          productParameterHolder: ProductParameterHolder()
                              .getDiscountParameterHolder()));
                },
              ),
              Container(
                  height: 300,
                  width: MediaQuery.of(context).size.width,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      padding: const EdgeInsets.only(left: PsDimens.space8),
                      itemCount: productProvider.productList.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        if (productProvider.productList.status ==
                            PsStatus.BLOCK_LOADING) {
                          return Shimmer.fromColors(
                              baseColor: PsColors.grey,
                              highlightColor: PsColors.white,
                              child: Row(children: const <Widget>[
                                PsFrameUIForLoading(),
                              ]));
                        } else {
                          final Product product =
                              productProvider.productList.data[index];
                          return ProductHorizontalListItem(
                            coreTagKey: productProvider.hashCode.toString() +
                                product.id,
                            product: productProvider.productList.data[index],
                            productId: product.id,
                            onTap: () {
                              print(productProvider.productList.data[index]
                                  .defaultPhoto.imgPath);
                              final ProductDetailIntentHolder holder =
                                  ProductDetailIntentHolder(
                                productId: product.id,
                                heroTagImage:
                                    productProvider.hashCode.toString() +
                                        product.id +
                                        PsConst.HERO_TAG__IMAGE,
                                heroTagTitle:
                                    productProvider.hashCode.toString() +
                                        product.id +
                                        PsConst.HERO_TAG__TITLE,
                                heroTagOriginalPrice:
                                    productProvider.hashCode.toString() +
                                        product.id +
                                        PsConst.HERO_TAG__ORIGINAL_PRICE,
                                heroTagUnitPrice:
                                    productProvider.hashCode.toString() +
                                        product.id +
                                        PsConst.HERO_TAG__UNIT_PRICE,
                              );
                              Navigator.pushNamed(
                                  context, RoutePaths.productDetail,
                                  arguments: holder);
                            },
                          );
                        }
                      })),
              const PsAdMobBannerWidget(
                admobSize: NativeAdmobType.full,
              ),
            ])
          : Container(
              color: PsColors.coreBackgroundColor,
            );
    }));
  }
}

class HomeCategoryHorizontalListWidget extends StatefulWidget {
  const HomeCategoryHorizontalListWidget({
    Key key,
    // @required this.animationController,
    // @required this.animation,
    @required this.psValueHolder,
  }) : super(key: key);

  // final AnimationController animationController;
  // final Animation<double> animation;
  final PsValueHolder psValueHolder;
  static int index;

  @override
  HomeCategoryHorizontalListWidgetState createState() =>
      HomeCategoryHorizontalListWidgetState();
}

class HomeCategoryHorizontalListWidgetState
    extends State<HomeCategoryHorizontalListWidget> {
  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Consumer<CategoryProvider>(
        builder: (BuildContext context, CategoryProvider categoryProvider,
            Widget child) {
          return (categoryProvider.categoryList.data != null &&
                  categoryProvider.categoryList.data.isNotEmpty)
              ? Row(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(
                          context,
                          RoutePaths.categoryList,
                        );
                      },
                      child: Hero(
                        tag: 'catgories',
                        child: Container(
                          height: 29.0,
                          padding:
                              const EdgeInsets.only(right: 12.0, left: 8.0),
                          alignment: Alignment.center,
                          child: ImageIcon(
                            const AssetImage(
                              'assets/home_images/categories_white.png',
                            ),
                            color: PsColors.whiteColorWithBlack,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: SizedBox(
                          height: 31,
                          child: ListView.builder(
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            itemCount:
                                categoryProvider.categoryList.data.length,
                            itemBuilder: (BuildContext context, int index) {
                              if (categoryProvider.categoryList.status ==
                                  PsStatus.BLOCK_LOADING) {
                                return Shimmer.fromColors(
                                  baseColor: PsColors.grey,
                                  highlightColor: PsColors.white,
                                  child: Row(
                                    children: const <Widget>[
                                      PsFrameUIForLoading(),
                                      PsFrameUIForLoading(),
                                      PsFrameUIForLoading(),
                                      PsFrameUIForLoading(),
                                    ],
                                  ),
                                );
                              } else {
                                return CategoryHorizontalListItem(
                                  category:
                                      categoryProvider.categoryList.data[index],
                                  onTap: () {
                                    final String loginUserId =
                                        Utils.checkUserLoginId(
                                            widget.psValueHolder);
                                    final TouchCountParameterHolder
                                        touchCountParameterHolder =
                                        TouchCountParameterHolder(
                                            typeId: categoryProvider
                                                .categoryList.data[index].id,
                                            typeName: PsConst
                                                .FILTERING_TYPE_NAME_CATEGORY,
                                            userId: loginUserId,
                                            shopId: '');

                                    categoryProvider.postTouchCount(
                                        touchCountParameterHolder.toMap());

                                    final ProductParameterHolder
                                        productParameterHolder =
                                        ProductParameterHolder()
                                            .getLatestParameterHolder();
                                    productParameterHolder.catId =
                                        categoryProvider
                                            .categoryList.data[index].id;

                                    Navigator.push(
                                      context,
                                      PageRouteBuilder<dynamic>(
                                        pageBuilder: (_, Animation<double> a1,
                                                Animation<double> a2) =>
                                            ProductListWithFilterContainerView(
                                          productParameterHolder:
                                              productParameterHolder,
                                          appBarTitle: categoryProvider
                                              .categoryList.data[index].name,
                                          category: categoryProvider
                                              .categoryList.data[index],
                                        ),
                                      ),
                                    );
                                  },
                                );
                              }
                            },
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              : Container(
                  color: PsColors.coreBackgroundColor,
                );
        },
      ),
    );
  }
}

class _MyHeaderWidget extends StatefulWidget {
  const _MyHeaderWidget({
    Key key,
    @required this.headerName,
    this.productCollectionHeader,
    @required this.viewAllClicked,
  }) : super(key: key);

  final String headerName;
  final Function viewAllClicked;
  final ProductCollectionHeader productCollectionHeader;

  @override
  __MyHeaderWidgetState createState() => __MyHeaderWidgetState();
}

class __MyHeaderWidgetState extends State<_MyHeaderWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.viewAllClicked,
      child: Padding(
        padding: const EdgeInsets.only(
            top: PsDimens.space28,
            left: PsDimens.space20,
            right: PsDimens.space16,
            bottom: PsDimens.space16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Expanded(
              child: Text(
                widget.headerName,
                style: Theme.of(context).textTheme.headline5.copyWith(
                    color: PsColors.textPrimaryDarkColor,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w500),
              ),
            ),
            Icon(
              Icons.arrow_forward_ios,
              size: 17.0,
              color: PsColors.textPrimaryColor,
            ),
            // Text(
            //   Utils.getString(context, 'dashboard__view_all'),
            //   textAlign: TextAlign.start,
            //   style: Theme.of(context)
            //       .textTheme
            //       .caption
            //       .copyWith(color: PsColors.mainColor),
            // ),
          ],
        ),
      ),
    );
  }
}
