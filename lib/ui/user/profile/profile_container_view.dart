import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_constants.dart';
import 'package:biomart/constant/route_paths.dart';
import 'package:biomart/provider/user/user_provider.dart';
import 'package:biomart/repository/user_repository.dart';
import 'package:biomart/ui/dashboard/view_pages/call_bottom_view.dart';
import 'package:biomart/ui/user/profile/profile_view.dart';
import 'package:biomart/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';

class ProfileContainerView extends StatefulWidget {
  @override
  _CityProfileContainerViewState createState() =>
      _CityProfileContainerViewState();
}

class _CityProfileContainerViewState extends State<ProfileContainerView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  UserProvider userProvider;
  UserRepository userRepo;

  @override
  Widget build(BuildContext context) {
    //final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
    print(
        '............................Build UI Again ............................');
    userRepo = Provider.of<UserRepository>(context);
    return Scaffold(
      bottomNavigationBar: const BottomBarViewWidget(
        currentIdex: 2,
      ),
      backgroundColor: PsColors.coreBackgroundColor,
      body: CustomScrollView(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          slivers: <Widget>[
            _SliverAppbar(
              title: Utils.getString(context, 'profile__title'),
              //scaffoldKey: scaffoldKey,
            ),
            ProfileView(
              //scaffoldKey: scaffoldKey,
              animationController: animationController,
              flag: PsConst.REQUEST_CODE__MENU_SELECT_WHICH_USER_FRAGMENT,
            ),
          ]),
    );
  }
}

class _SliverAppbar extends StatefulWidget {
  const _SliverAppbar({
    Key key,
    @required this.title,
    //this.scaffoldKey,
    this.menuDrawer,
  }) : super(key: key);
  final String title;
  //final GlobalKey<ScaffoldState> scaffoldKey;
  final Drawer menuDrawer;
  @override
  _SliverAppbarState createState() => _SliverAppbarState();
}

class _SliverAppbarState extends State<_SliverAppbar> {
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      systemOverlayStyle: SystemUiOverlayStyle(
          statusBarIconBrightness: Utils.getBrightnessForAppBar(context)),
      iconTheme: Theme.of(context)
          .iconTheme
          .copyWith(color: PsColors.mainColorWithWhite),
      title: Text(
        widget.title,
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.headline6.copyWith(
            fontWeight: FontWeight.bold, color: PsColors.mainColorWithWhite),
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.notifications_none,
              color: Theme.of(context).iconTheme.color),
          onPressed: () {
            Navigator.pushNamed(
              context,
              RoutePaths.notiList,
            );
          },
        ),
        IconButton(
          icon:
              Icon(Feather.book_open, color: Theme.of(context).iconTheme.color),
          onPressed: () {},
        )
      ],
      elevation: 0,
    );
  }
}
