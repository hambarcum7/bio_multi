import 'package:biomart/api/common/ps_resource.dart';
import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_constants.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/constant/route_paths.dart';
import 'package:biomart/provider/delete_task/delete_task_provider.dart';
import 'package:biomart/provider/user/user_provider.dart';
import 'package:biomart/repository/delete_task_repository.dart';
import 'package:biomart/repository/user_repository.dart';
import 'package:biomart/ui/common/base/ps_widget_with_appbar.dart';
import 'package:biomart/ui/common/dialog/confirm_dialog_view.dart';
import 'package:biomart/ui/common/dialog/error_dialog.dart';
import 'package:biomart/ui/common/ps_ui_widget.dart';
import 'package:biomart/utils/ps_progress_dialog.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/common/ps_value_holder.dart';
import 'package:biomart/viewobject/user.dart';
import 'package:firebase_auth/firebase_auth.dart' as fb_auth;
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:intl/intl.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

class ProfileView extends StatefulWidget {
  const ProfileView({Key key}) : super(key: key);

  @override
  _ProfileViewState createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  UserRepository userRepo;
  PsValueHolder psValueHolder;
  UserProvider userProvider;
  DeleteTaskProvider deleteTaskProvider;
  DeleteTaskRepository deleteTaskRepository;
  final TextEditingController userNameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController userAddressController = TextEditingController();
  final TextEditingController shippingAreaController = TextEditingController();
  bool bindDataFirstTime = true;

  @override
  Widget build(BuildContext context) {
    userRepo = Provider.of<UserRepository>(context);
    psValueHolder = Provider.of<PsValueHolder>(context);
    deleteTaskRepository = Provider.of<DeleteTaskRepository>(context);

    return MultiProvider(
      providers: <SingleChildWidget>[
        ChangeNotifierProvider<DeleteTaskProvider>(
            lazy: false,
            create: (BuildContext context) {
              deleteTaskProvider = DeleteTaskProvider(
                  repo: deleteTaskRepository, psValueHolder: psValueHolder);
              return deleteTaskProvider;
            }),
      ],
      child: PsWidgetWithAppBar<UserProvider>(
        initProvider: () {
          return UserProvider(repo: userRepo, psValueHolder: psValueHolder);
        },
        onProviderReady: (UserProvider provider) async {
          await provider.getUser(provider.psValueHolder.loginUserId);
          userProvider = provider;
        },
        appBarTitle: Utils.getString(context, 'edit_profile__profile'),
        actions: <Widget>[
          GestureDetector(
            onTap: () {
              showDialog<dynamic>(
                context: context,
                builder: (BuildContext context) {
                  return ConfirmDialogView(
                    description: Utils.getString(
                        context, 'home__logout_dialog_description'),
                    leftButtonText: Utils.getString(
                        context, 'home__logout_dialog_cancel_button'),
                    rightButtonText: Utils.getString(
                        context, 'home__logout_dialog_ok_button'),
                    onAgreeTap: () async {
                      setState(() {
                        Navigator.pushNamed(
                          context,
                          RoutePaths.home,
                        );
                      });
                      await userProvider.replaceLoginUserId('');
                      await deleteTaskProvider.deleteTask();
                      await FacebookAuth.instance.logOut();
                      await GoogleSignIn().signOut();
                      await fb_auth.FirebaseAuth.instance.signOut();
                    },
                  );
                },
              );
            },
            child: Container(
              margin: const EdgeInsets.only(right: 8),
              width: 24.0,
              height: 24.0,
              child: Icon(
                Icons.logout,
                color: PsColors.textPrimaryColor,
              ),
            ),
          )
        ],
        builder: (BuildContext context, UserProvider provider, Widget child) {
          if (userProvider != null &&
              userProvider.user != null &&
              userProvider.user.data != null) {
            if (bindDataFirstTime) {
              userNameController.text = userProvider.user.data.userName;
              emailController.text = userProvider.user.data.userEmail;
              phoneController.text = userProvider.user.data.userPhone;
              userAddressController.text = userProvider.user.data.address;
              userProvider.selectedArea = userProvider.user.data.area;
              shippingAreaController.text =
                  userProvider.user.data.area.areaName;
              bindDataFirstTime = false;
            }
            return Container(
              height: MediaQuery.of(context).size.height,
              color: PsColors.coreBackgroundColor,
              margin: const EdgeInsets.symmetric(horizontal: PsDimens.space16),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    _ImageWidget(
                      userProvider: userProvider,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: PsDimens.space16),
                      child: Column(
                        children: <Widget>[
                          // _UserFirstCardWidget(
                          //   userNameController: userNameController,
                          //   emailController: emailController,
                          //   phoneController: phoneController,
                          // ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width,
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 3,
                                  child: _InfoWidget(
                                    infoText: DateFormat('dd.MM.yyyy')
                                        .format(DateTime.now()),
                                    isBaseFont: false,
                                  ),
                                ),
                                const Spacer(
                                  flex: 2,
                                ),
                                const Expanded(
                                  flex: 2,
                                  child: _InfoWidget(
                                    infoText: 'Male',
                                    isBaseFont: true,
                                  ), //TODO
                                ),
                              ],
                            ),
                          ),
                          _InfoWidget(
                            infoText:
                                Utils.getString(context, 'edit_profile__email'),
                            isBaseFont: true,
                          ),
                          _InfoWidget(
                            infoText:
                                Utils.getString(context, 'edit_profile__phone'),
                            isBaseFont: false,
                          ),
                          const SizedBox(
                            height: PsDimens.space40,
                          ),
                          _InfoWidget(
                            infoText: Utils.getString(
                                context, 'edit_profile__country_name'),
                            isBaseFont: true,
                          ),
                          _InfoWidget(
                            infoText: Utils.getString(
                                context, 'edit_profile__city_name'),
                            isBaseFont: true,
                          ),
                          _InfoWidget(
                            infoText: Utils.getString(
                                context, 'edit_profile__region_name'),
                            isBaseFont: true,
                          ),
                          _InfoWidget(
                            infoText: Utils.getString(
                                context, 'edit_profile__postal_code'),
                            isBaseFont: false,
                          ),
                          _InfoWidget(
                            infoText: Utils.getString(
                                context, 'edit_profile__street_name'),
                            isBaseFont: true,
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width,
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: _InfoWidget(
                                    infoText: Utils.getString(
                                        context, 'edit_profile__house_name'),
                                    isBaseFont: false,
                                  ),
                                ),
                                const SizedBox(
                                  width: PsDimens.space80,
                                ),
                                Expanded(
                                  child: _InfoWidget(
                                    infoText: Utils.getString(context,
                                        'edit_profile__apartment_name'),
                                    isBaseFont: false,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 50.0,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          } else {
            return Stack(
              alignment: Alignment.bottomCenter,
              children: <Widget>[
                Container(
                  color: PsColors.coreBackgroundColor,
                ),
                PSProgressIndicator(provider.user.status)
              ],
            );
          }
        },
      ),
    );
  }
}

class _InfoWidget extends StatelessWidget {
  const _InfoWidget({
    Key key,
    @required this.infoText,
    @required this.isBaseFont,
  }) : super(key: key);

  final String infoText;
  final bool isBaseFont;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: PsDimens.space8),
      child: TextField(
        maxLines: null,
        enabled: false,
        decoration: InputDecoration(
            hintText: infoText,
            hintStyle: Theme.of(context).textTheme.bodyText2.copyWith(
                  color: PsColors.textPrimaryColor,
                  fontSize: 16.0,
                  fontFamily:
                      isBaseFont ? PsConfig.ps_default_font_family : 'Roboto',
                ),
            contentPadding: const EdgeInsets.only(left: PsDimens.space8)),
      ),
    );
  }
}

class _ImageWidget extends StatefulWidget {
  const _ImageWidget({this.userProvider});
  final UserProvider userProvider;

  @override
  __ImageWidgetState createState() => __ImageWidgetState();
}

class __ImageWidgetState extends State<_ImageWidget> {
  List<Asset> images = <Asset>[];
  Future<bool> requestGalleryPermission() async {
    // final Map<PermissionGroup, PermissionStatus> permissionss =
    //     await PermissionHandler()
    //         .requestPermissions(<PermissionGroup>[PermissionGroup.photos]);
    // if (permissionss != null &&
    //     permissionss.isNotEmpty &&
    //     permissionss[PermissionGroup.photos] == PermissionStatus.granted) {
    //   return true;
    // } else {
    //   return false;
    // }
    final Permission _photos = Permission.photos;
    final PermissionStatus permissionss = await _photos.request();

    if (permissionss != null && permissionss == PermissionStatus.granted) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    Future<void> _pickImage() async {
      List<Asset> resultList = <Asset>[];

      try {
        resultList = await MultiImagePicker.pickImages(
          maxImages: 1,
          enableCamera: true,
          // selectedAssets: images,
          cupertinoOptions: CupertinoOptions(
              takePhotoIcon: 'chat',
              backgroundColor: '' +
                  Utils.convertColorToString(PsColors.whiteColorWithBlack)),
          materialOptions: MaterialOptions(
            actionBarColor: Utils.convertColorToString(PsColors.black),
            actionBarTitleColor: Utils.convertColorToString(PsColors.white),
            statusBarColor: Utils.convertColorToString(PsColors.black),
            lightStatusBar: false,
            actionBarTitle: '',
            allViewTitle: 'All Photos',
            useDetailsView: false,
            selectCircleStrokeColor:
                Utils.convertColorToString(PsColors.mainColor),
          ),
        );
      } on Exception catch (e) {
        e.toString();
      }

      // If the widget was removed from the tree while the asynchronous platform
      // message was in flight, we want to discard the reply rather than calling
      // setState to update our non-existent appearance.
      if (!mounted) {
        return;
      }
      images = resultList;
      setState(() {});

      if (images.isNotEmpty) {
        if (images[0].name.contains('.webp')) {
          showDialog<dynamic>(
              context: context,
              builder: (BuildContext context) {
                return ErrorDialog(
                  message: Utils.getString(context, 'error_dialog__webp_image'),
                );
              });
        } else {
          PsProgressDialog.dismissDialog();
          final PsResource<User> _apiStatus = await widget.userProvider
              .postImageUpload(
                  widget.userProvider.psValueHolder.loginUserId,
                  PsConst.PLATFORM,
                  await Utils.getImageFileFromAssets(
                      images[0], PsConfig.profileImageAize));
          if (_apiStatus.data != null) {
            setState(() {
              widget.userProvider.user.data = _apiStatus.data;
            });
          }
          PsProgressDialog.dismissDialog();
        }
      }
    }

    // final Widget _imageWidget =
    //     widget.userProvider.user.data.userProfilePhoto != null
    //         ? PsNetworkImageWithUrlForUser(
    //             photoKey: '',
    //             imagePath: widget.userProvider.user.data.userProfilePhoto,
    //             width: double.infinity,
    //             height: PsDimens.space200,
    //             boxfit: BoxFit.cover,
    //             onTap: () {},
    //           )
    //         : InkWell(
    //             onTap: () {},
    //             child: Ink(
    //                 child:
    //                     AssetThumb(asset: images[0], width: 100, height: 160)),
    //           );

    final Widget _imageInCenterWidget = Container(
      width: 90,
      height: 90,
      child: CircleAvatar(
        backgroundColor: Colors.white,
        child: PsNetworkCircleImageForUser(
          photoKey: '',
          imagePath: widget.userProvider.user.data.userProfilePhoto,
          width: double.infinity,
          height: PsDimens.space200,
          boxfit: BoxFit.cover,
        ),
      ),
    );
    return Container(
      alignment: Alignment.center,
      width: MediaQuery.of(context).size.width,
      height: PsDimens.space175,
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Divider(
                height: 13,
                color: PsColors.greyColorForCustomWidget,
              ),
              const Spacer(),
              Row(
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.only(left: PsDimens.space8),
                    child: _imageInCenterWidget,
                  ),
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(
                          left: PsDimens.space34, right: 53),
                      height: 84.0,
                      child: Text(
                        widget.userProvider.user.data.userName,
                        overflow: TextOverflow.clip,
                        style: Theme.of(context).textTheme.bodyText2.copyWith(
                            fontSize: 24.0, fontWeight: FontWeight.w500),
                      ),
                    ),
                  )
                ],
              ),
              const Spacer(),
              Divider(
                height: 8,
                color: PsColors.greyColorForCustomWidget,
              ),
              const SizedBox(
                height: 20.0,
              )
            ],
          ),
          Positioned(
            right: 40.0,
            bottom: 8,
            child: GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, RoutePaths.editProfile);
              },
              child: Container(
                width: 40.0,
                height: 40.0,
                decoration: BoxDecoration(
                  color: PsColors.greyColorForCustomWidget,
                  shape: BoxShape.circle,
                ),
                child: Icon(
                  Icons.edit,
                  color: PsColors.textPrimaryColor,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

// class _UserFirstCardWidget extends StatefulWidget {
//   const _UserFirstCardWidget({
//     @required this.userNameController,
//     @required this.emailController,
//     @required this.phoneController,
//   });
//   final TextEditingController userNameController;
//   final TextEditingController emailController;
//   final TextEditingController phoneController;

//   @override
//   State<_UserFirstCardWidget> createState() => _UserFirstCardWidgetState();
// }

// class _UserFirstCardWidgetState extends State<_UserFirstCardWidget> {
//   List<String> items = <String>['Male', 'Female'];
//   String gender = 'Male';
//   String birthDate = DateFormat('dd.MM.yyyy').format(DateTime.now());

//   /// TODO stanal userInfo-ic

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: <Widget>[
//           Container(
//             color: PsColors.coreBackgroundColor,
//             height: 72,
//             child: Row(
//               children: <Widget>[
//                 Expanded(
//                   flex: 4,
//                   child: Container(
//                     margin: const EdgeInsets.only(top: 8.0),
//                     padding: const EdgeInsets.symmetric(vertical: 8.0),
//                     decoration: BoxDecoration(
//                       color: PsColors.greyColorForCustomWidget,
//                       borderRadius: BorderRadius.circular(8.0),
//                     ),
//                     child: Column(
//                       children: <Widget>[
//                         Text(
//                           Utils.getString(context, 'edit_profile__birthday'),
//                           style: Theme.of(context).textTheme.bodyText1,
//                         ),
//                         const SizedBox(height: 8.0),
//                         Container(
//                           margin: const EdgeInsets.symmetric(horizontal: 25),
//                           height: 21.0,
//                           decoration: BoxDecoration(
//                             color: PsColors.coreBackgroundColor,
//                             borderRadius: BorderRadius.circular(2.0),
//                           ),
//                           child: Center(
//                             child: Text(
//                               birthDate,
//                               style: TextStyle(
//                                 color: PsColors.textPrimaryColor,
//                                 fontSize: 16.0,
//                                 fontWeight: FontWeight.w400,
//                                 fontFamily: 'Roboto',
//                               ),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//                 const Spacer(
//                   flex: 2,
//                 ),
//                 Expanded(
//                   flex: 2,
//                   child: Container(
//                     margin: const EdgeInsets.only(top: 8.0),
//                     padding: const EdgeInsets.symmetric(vertical: 8.0),
//                     decoration: BoxDecoration(
//                       color: PsColors.greyColorForCustomWidget,
//                       borderRadius: BorderRadius.circular(8.0),
//                     ),
//                     child: Column(
//                       children: <Widget>[
//                         Text(
//                           Utils.getString(context, 'edit_profile__gender'),
//                           style: Theme.of(context).textTheme.bodyText1,
//                         ),
//                         const SizedBox(height: 8.0),
//                         Container(
//                           margin: const EdgeInsets.symmetric(horizontal: 25),
//                           height: 21.0,
//                           decoration: BoxDecoration(
//                             color: PsColors.coreBackgroundColor,
//                             borderRadius: BorderRadius.circular(2.0),
//                           ),
//                           child: Center(
//                             child: Text(
//                               gender,
//                               style: TextStyle(
//                                 color: PsColors.textPrimaryColor,
//                                 fontSize: 16.0,
//                                 fontWeight: FontWeight.w400,
//                                 fontFamily: 'Roboto',
//                               ),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
