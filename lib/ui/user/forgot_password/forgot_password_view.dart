import 'package:biomart/api/common/ps_resource.dart';
import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/provider/user/user_provider.dart';
import 'package:biomart/repository/user_repository.dart';
import 'package:biomart/ui/common/dialog/error_dialog.dart';
import 'package:biomart/ui/common/dialog/success_dialog.dart';
import 'package:biomart/ui/common/dialog/warning_dialog_view.dart';
import 'package:biomart/ui/common/ps_button_widget.dart';
import 'package:biomart/ui/user/login/login_view.dart';
import 'package:biomart/utils/ps_progress_dialog.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/api_status.dart';
import 'package:biomart/viewobject/common/ps_value_holder.dart';
import 'package:biomart/viewobject/holder/forgot_password_parameter_holder.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ForgotPasswordView extends StatefulWidget {
  const ForgotPasswordView({
    Key key,
    this.animationController,
    this.goToLoginSelected,
  }) : super(key: key);
  final AnimationController animationController;
  final Function goToLoginSelected;
  @override
  _ForgotPasswordViewState createState() => _ForgotPasswordViewState();
}

class _ForgotPasswordViewState extends State<ForgotPasswordView>
    with SingleTickerProviderStateMixin {
  final TextEditingController userEmailController = TextEditingController();
  UserRepository repo1;
  PsValueHolder psValueHolder;
  AnimationController animationController;
  final FocusNode _emailFocusNode = FocusNode();
  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Animation<double> animation = Tween<double>(begin: 0.0, end: 1.0)
        .animate(CurvedAnimation(
            parent: animationController,
            curve: const Interval(0.5 * 1, 1.0, curve: Curves.fastOutSlowIn)));

    animationController.forward();
    repo1 = Provider.of<UserRepository>(context);
    psValueHolder = Provider.of<PsValueHolder>(context);

    return SliverToBoxAdapter(
      child: ChangeNotifierProvider<UserProvider>(
        lazy: false,
        create: (BuildContext context) {
          final UserProvider provider =
              UserProvider(repo: repo1, psValueHolder: psValueHolder);
          return provider;
        },
        child: Consumer<UserProvider>(
          builder: (BuildContext context, UserProvider provider, Widget child) {
            return AnimatedBuilder(
              animation: animationController,
              child: GestureDetector(
                onTap: () {
                  _emailFocusNode.unfocus();
                },
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.transparent,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      //_HeaderIconAndTextWidget(),
                      const Expanded(
                        child: Center(
                          child: AppName(),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Column(
                          children: <Widget>[
                            _CardWidget(
                              userEmailController: userEmailController,
                              emailFocusNode: _emailFocusNode,
                            ),
                            const SizedBox(
                              height: PsDimens.space32,
                            ),
                            _SendButtonWidget(
                              provider: provider,
                              userEmailController: userEmailController,
                            ),
                            const SizedBox(
                              height: PsDimens.space16,
                            ),
                            _TextWidget(
                              goToLoginSelected: widget.goToLoginSelected,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              builder: (BuildContext context, Widget child) {
                return FadeTransition(
                  opacity: animation,
                  child: Transform(
                    transform: Matrix4.translationValues(
                        0.0, 100 * (1.0 - animation.value), 0.0),
                    child: child,
                  ),
                );
              },
            );
          },
        ),
      ),
    );
  }
}

class _TextWidget extends StatefulWidget {
  const _TextWidget({this.goToLoginSelected});
  final Function goToLoginSelected;
  @override
  __TextWidgetState createState() => __TextWidgetState();
}

class __TextWidgetState extends State<_TextWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Text(
        Utils.getString(context, 'forgot_psw__login'),
        style: Theme.of(context)
            .textTheme
            .bodyText2
            .copyWith(color: PsColors.mainColor),
      ),
      onTap: () {
        if (widget.goToLoginSelected != null) {
          widget.goToLoginSelected();
        } else {
          Navigator.pop(context);
        }
      },
    );
  }
}

// class _HeaderIconAndTextWidget extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     final Widget _textWidget = Text(Utils.getString(context, 'app_name'),
//         style: Theme.of(context)
//             .textTheme
//             .subtitle1
//             .copyWith(color: PsColors.mainColor));

//     final Widget _imageWidget = Container(
//       width: 90,
//       height: 90,
//       child: Image.asset(
//         'assets/images/fs_android_3x.png',
//       ),
//     );
//     return Column(
//       children: <Widget>[
//         const SizedBox(
//           height: PsDimens.space32,
//         ),
//         _imageWidget,
//         const SizedBox(
//           height: PsDimens.space8,
//         ),
//         _textWidget,
//         const SizedBox(
//           height: PsDimens.space52,
//         ),
//       ],
//     );
//   }
// }

class _CardWidget extends StatelessWidget {
  const _CardWidget({
    @required this.userEmailController,
    @required this.emailFocusNode,
  });

  final TextEditingController userEmailController;
  final FocusNode emailFocusNode;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.symmetric(
            horizontal: PsDimens.space8,
            vertical: PsDimens.space8,
          ),
          child: TextField(
            controller: userEmailController,
            focusNode: emailFocusNode,
            style: Theme.of(context).textTheme.button.copyWith(),
            decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  width: 2,
                  color: PsColors.greyColorForLoginTextField,
                ),
                borderRadius: BorderRadius.circular(24),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  width: 2,
                  color: Color(0xff095F56),
                ),
                borderRadius: BorderRadius.circular(24),
              ),
              //labelText: widget.labelText,
              hintStyle: const TextStyle(
                color: Color(0xff4F4F4F),
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                letterSpacing: 1,
              ),
              hintText: Utils.getString(context, 'forgot_psw__email'),
              labelStyle: const TextStyle(
                color: Color(0xff4F4F4F),
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
              ),
              errorBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  width: 2,
                  color: Colors.red,
                ),
                borderRadius: BorderRadius.circular(24),
              ),
              prefix: const SizedBox(
                width: PsDimens.space20,
              ),
              //suffixIcon: widget.suffixIcon,
            ),
            // decoration: InputDecoration(
            //     border: InputBorder.none,
            //     hintText: Utils.getString(context, 'forgot_psw__email'),
            //     hintStyle: Theme.of(context)
            //         .textTheme
            //         .bodyText2
            //         .copyWith(color: PsColors.textPrimaryLightColor),
            //     icon: Icon(Icons.email,
            //         color: Theme.of(context).iconTheme.color)),
          ),
        ),
      ],
    );
  }
}

class _SendButtonWidget extends StatefulWidget {
  const _SendButtonWidget({
    @required this.provider,
    @required this.userEmailController,
  });
  final UserProvider provider;
  final TextEditingController userEmailController;

  @override
  __SendButtonWidgetState createState() => __SendButtonWidgetState();
}

dynamic callWarningDialog(BuildContext context, String text) {
  showDialog<dynamic>(
      context: context,
      builder: (BuildContext context) {
        return WarningDialog(
          message: Utils.getString(context, text),
          onPressed: () {},
        );
      });
}

class __SendButtonWidgetState extends State<_SendButtonWidget> {
  @override
  Widget build(BuildContext context) {
    return PSButtonWidget(
        hasShadow: true,
        width: double.infinity,
        titleText: Utils.getString(context, 'forgot_psw__send'),
        onPressed: () async {
          if (widget.userEmailController.text.isEmpty) {
            callWarningDialog(context,
                Utils.getString(context, 'warning_dialog__input_email'));
          } else {
            if (Utils.checkEmailFormat(
                widget.userEmailController.text.trim())) {
              if (await Utils.checkInternetConnectivity()) {
                final ForgotPasswordParameterHolder
                    forgotPasswordParameterHolder =
                    ForgotPasswordParameterHolder(
                  userEmail: widget.userEmailController.text.trim(),
                );

                await PsProgressDialog.showDialog(context);
                final PsResource<ApiStatus> _apiStatus = await widget.provider
                    .postForgotPassword(forgotPasswordParameterHolder.toMap());
                PsProgressDialog.dismissDialog();

                if (_apiStatus.data != null) {
                  showDialog<dynamic>(
                      context: context,
                      builder: (BuildContext context) {
                        return SuccessDialog(
                          message: _apiStatus.data.message,
                        );
                      });
                } else {
                  showDialog<dynamic>(
                      context: context,
                      builder: (BuildContext context) {
                        return ErrorDialog(
                          message: _apiStatus.message,
                        );
                      });
                }
              } else {
                showDialog<dynamic>(
                    context: context,
                    builder: (BuildContext context) {
                      return ErrorDialog(
                        message: Utils.getString(
                            context, 'error_dialog__no_internet'),
                      );
                    });
              }
            } else {
              callWarningDialog(context,
                  Utils.getString(context, 'warning_dialog__email_format'));
            }
          }
        });
  }
}
