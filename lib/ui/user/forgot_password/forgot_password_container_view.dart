import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/provider/user/user_provider.dart';
import 'package:biomart/repository/user_repository.dart';
import 'package:biomart/ui/dashboard/view_pages/call_bottom_view.dart';
import 'package:biomart/utils/app_bar_widget.dart';
import 'package:biomart/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'forgot_password_view.dart';

class ForgotPasswordContainerView extends StatefulWidget {
  @override
  _CityForgotPasswordContainerViewState createState() =>
      _CityForgotPasswordContainerViewState();
}

class _CityForgotPasswordContainerViewState
    extends State<ForgotPasswordContainerView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  UserProvider userProvider;
  UserRepository userRepo;

  @override
  Widget build(BuildContext context) {
    print(
        '............................Build UI Again ............................');
    userRepo = Provider.of<UserRepository>(context);
    return Scaffold(
      bottomNavigationBar: const BottomBarViewWidget(
        currentIdex: 2,
      ),
      backgroundColor: PsColors.coreBackgroundColor,
      appBar: customAppBar(
        title: Utils.getString(context, 'forgot_psw__title'),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back_ios,
            size: 17.0,
            color: PsColors.textPrimaryColor,
          ),
        ),
      ),
      body: Stack(children: <Widget>[
        Container(
          color: PsColors.coreBackgroundColor,
          width: double.infinity,
          height: double.maxFinite,
        ),
        CustomScrollView(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            slivers: <Widget>[
              // _SliverAppbar(
              //   title: Utils.getString(context, 'forgot_psw__title'),
              //   scaffoldKey: scaffoldKey,
              // ),
              ForgotPasswordView(
                animationController: animationController,
              ),
            ])
      ]),
    );
  }
}

class _SliverAppbar extends StatefulWidget {
  const _SliverAppbar(
      {Key key, @required this.title, this.scaffoldKey, this.menuDrawer})
      : super(key: key);
  final String title;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Drawer menuDrawer;
  @override
  _SliverAppbarState createState() => _SliverAppbarState();
}

class _SliverAppbarState extends State<_SliverAppbar> {
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      systemOverlayStyle: SystemUiOverlayStyle(
          statusBarIconBrightness: Utils.getBrightnessForAppBar(context)),
      iconTheme: Theme.of(context)
          .iconTheme
          .copyWith(color: PsColors.mainColorWithWhite),
      title: Text(
        widget.title,
        textAlign: TextAlign.center,
        style: Theme.of(context)
            .textTheme
            .headline6
            .copyWith(fontWeight: FontWeight.bold)
            .copyWith(color: PsColors.mainColorWithWhite),
      ),
      // actions: <Widget>[
      //   IconButton(
      //     icon: Icon(Icons.notifications_none,
      //         color: Theme.of(context).iconTheme.color),
      //     onPressed: () {
      //       Navigator.pushNamed(
      //         context,
      //         RoutePaths.notiList,
      //       );
      //     },
      //   ),
      //   IconButton(
      //     icon:
      //         Icon(Feather.book_open, color: Theme.of(context).iconTheme.color),
      //     onPressed: () {
      //       Navigator.pushNamed(context, RoutePaths.blogList,
      //           arguments: const BlogIntentHolder(noBlogListForShop: true));
      //     },
      //   )
      // ],
      elevation: 0,
    );
  }
}
