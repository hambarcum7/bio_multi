import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/provider/user/user_provider.dart';
import 'package:biomart/repository/user_repository.dart';
import 'package:biomart/ui/dashboard/view_pages/call_bottom_view.dart';
import 'package:biomart/utils/app_bar_widget.dart';
import 'package:biomart/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'verify_email_view.dart';

class VerifyEmailContainerView extends StatefulWidget {
  const VerifyEmailContainerView({@required this.userId});
  final String userId;

  @override
  _CityVerifyEmailContainerViewState createState() =>
      _CityVerifyEmailContainerViewState();
}

class _CityVerifyEmailContainerViewState extends State<VerifyEmailContainerView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  UserProvider userProvider;
  UserRepository userRepo;

  @override
  Widget build(BuildContext context) {
    print(
        '............................Build UI Again ............................');
    userRepo = Provider.of<UserRepository>(context);

    return Scaffold(
        bottomNavigationBar: const BottomBarViewWidget(
          currentIdex: 2,
        ),
        backgroundColor: PsColors.coreBackgroundColor,
        appBar: customAppBar(
          title: Utils.getString(context, 'email_verify__title'),
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(
                Icons.arrow_back_ios,
                size: 17.0,
                color: PsColors.textPrimaryColor,
              ),
            ),
          ),
        ),
        // appBar: AppBar(
        //   backgroundColor: PsColors.mainColor,
        //   systemOverlayStyle: SystemUiOverlayStyle(
        //       statusBarIconBrightness:
        //           Utils.getBrightnessForAppBar(context)),
        //   iconTheme: Theme.of(context).iconTheme.copyWith(),
        //   title: Text(
        //     Utils.getString(context, 'email_verify__title'),
        //     textAlign: TextAlign.center,
        //     style: Theme.of(context)
        //         .textTheme
        //         .headline6
        //         .copyWith(fontWeight: FontWeight.bold),
        //   ),
        //   elevation: 0,
        // ),
        body: VerifyEmailView(
            animationController: animationController,
            userId: widget.userId));
  }
}
