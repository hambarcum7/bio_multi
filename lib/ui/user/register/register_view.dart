import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/constant/route_paths.dart';
import 'package:biomart/provider/user/user_provider.dart';
import 'package:biomart/repository/user_repository.dart';
import 'package:biomart/ui/common/dialog/warning_dialog_view.dart';
import 'package:biomart/ui/common/ps_button_widget.dart';
import 'package:biomart/ui/user/login/login_view.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/common/ps_value_holder.dart';
import 'package:biomart/viewobject/holder/intent_holder/privacy_policy_intent_holder.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RegisterView extends StatefulWidget {
  const RegisterView({
    Key key,
    this.animationController,
    this.onRegisterSelected,
    this.goToLoginSelected,
  }) : super(key: key);
  final AnimationController animationController;
  final Function onRegisterSelected, goToLoginSelected;
  @override
  _RegisterViewState createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;

  UserRepository repo1;
  PsValueHolder valueHolder;
  TextEditingController nameController;
  TextEditingController emailController;
  TextEditingController passwordController;
  final FocusNode _nameFocusNode = FocusNode();
  final FocusNode _emailFocusNode = FocusNode();
  final FocusNode _passwordFocusNode = FocusNode();

  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);

    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Animation<double> animation =
        Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: animationController,
        curve: const Interval(0.5 * 1, 1.0, curve: Curves.fastOutSlowIn),
      ),
    );

    animationController.forward();

    repo1 = Provider.of<UserRepository>(context);
    valueHolder = Provider.of<PsValueHolder>(context);

    return SliverToBoxAdapter(
      child: ChangeNotifierProvider<UserProvider>(
        lazy: false,
        create: (BuildContext context) {
          final UserProvider provider =
              UserProvider(repo: repo1, psValueHolder: valueHolder);

          return provider;
        },
        child: Consumer<UserProvider>(builder:
            (BuildContext context, UserProvider provider, Widget child) {
          nameController = TextEditingController(
              text: provider.psValueHolder.userNameToVerify);
          emailController = TextEditingController(
              text: provider.psValueHolder.userEmailToVerify);
          passwordController = TextEditingController(
              text: provider.psValueHolder.userPasswordToVerify);

          return AnimatedBuilder(
              animation: animationController,
              child: GestureDetector(
                onTap: () {
                  _nameFocusNode.unfocus();
                  _emailFocusNode.unfocus();
                  _passwordFocusNode.unfocus();
                },
                child: SingleChildScrollView(
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    //height: MediaQuery.of(context).size.height,
                    color: PsColors.coreBackgroundColor,
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        //_HeaderIconAndTextWidget(),
                        const SizedBox(
                          height: 70.0,
                        ),
                        const Center(
                          child: AppName(),
                        ),
                        const SizedBox(
                          height: 70.0,
                        ),
                        _TextFieldWidget(
                          nameText: nameController,
                          emailText: emailController,
                          passwordText: passwordController,
                          nameFocusNode: _nameFocusNode,
                          emailFocusNode: _emailFocusNode,
                          passwordFocusNode: _passwordFocusNode,
                        ),
                        const SizedBox(
                          height: PsDimens.space8,
                        ),
                        _TermsAndConCheckbox(
                          provider: provider,
                          nameTextEditingController: nameController,
                          emailTextEditingController: emailController,
                          passwordTextEditingController: passwordController,
                        ),
                        const SizedBox(
                          height: PsDimens.space8,
                        ),
                        _SignInButtonWidget(
                          provider: provider,
                          nameTextEditingController: nameController,
                          emailTextEditingController: emailController,
                          passwordTextEditingController: passwordController,
                          onRegisterSelected: widget.onRegisterSelected,
                        ),
                        const SizedBox(
                          height: PsDimens.space16,
                        ),
                        _TextWidget(
                          goToLoginSelected: widget.goToLoginSelected,
                        ),
                        const SizedBox(
                          height: kBottomNavigationBarHeight,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              builder: (BuildContext context, Widget child) {
                return FadeTransition(
                    opacity: animation,
                    child: Transform(
                      transform: Matrix4.translationValues(
                          0.0, 100 * (1.0 - animation.value), 0.0),
                      child: child,
                    ));
              });
        }),
      ),
    );
  }
}

class _TermsAndConCheckbox extends StatefulWidget {
  const _TermsAndConCheckbox({
    @required this.provider,
    @required this.nameTextEditingController,
    @required this.emailTextEditingController,
    @required this.passwordTextEditingController,
  });

  final UserProvider provider;
  final TextEditingController nameTextEditingController,
      emailTextEditingController,
      passwordTextEditingController;
  @override
  __TermsAndConCheckboxState createState() => __TermsAndConCheckboxState();
}

class __TermsAndConCheckboxState extends State<_TermsAndConCheckbox> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        const SizedBox(
          width: PsDimens.space20,
        ),
        Checkbox(
          activeColor: PsColors.mainColor,
          value: widget.provider.isCheckBoxSelect,
          onChanged: (bool value) {
            setState(() {
              updateCheckBox(
                  widget.provider.isCheckBoxSelect,
                  context,
                  widget.provider,
                  widget.nameTextEditingController,
                  widget.emailTextEditingController,
                  widget.passwordTextEditingController);
            });
          },
        ),
        Expanded(
          child: InkWell(
            child: Text(
              Utils.getString(context, 'login__agree_privacy'),
              style: Theme.of(context).textTheme.bodyText2,
            ),
            onTap: () {
              setState(() {
                updateCheckBox(
                    widget.provider.isCheckBoxSelect,
                    context,
                    widget.provider,
                    widget.nameTextEditingController,
                    widget.emailTextEditingController,
                    widget.passwordTextEditingController);
              });
            },
          ),
        ),
      ],
    );
  }
}

void updateCheckBox(
  bool isCheckBoxSelect,
  BuildContext context,
  UserProvider provider,
  TextEditingController nameTextEditingController,
  TextEditingController emailTextEditingController,
  TextEditingController passwordTextEditingController,
) {
  if (isCheckBoxSelect) {
    provider.isCheckBoxSelect = false;
  } else {
    provider.isCheckBoxSelect = true;
    //it is for holder
    provider.psValueHolder.userNameToVerify = nameTextEditingController.text;
    provider.psValueHolder.userEmailToVerify = emailTextEditingController.text;
    provider.psValueHolder.userPasswordToVerify =
        passwordTextEditingController.text;
    Navigator.pushNamed(context, RoutePaths.privacyPolicy,
        arguments: PrivacyPolicyIntentHolder(
            title: Utils.getString(context, 'privacy_policy__toolbar_name'),
            description: ''));
  }
}

class _TextWidget extends StatefulWidget {
  const _TextWidget({this.goToLoginSelected});
  final Function goToLoginSelected;
  @override
  __TextWidgetState createState() => __TextWidgetState();
}

class __TextWidgetState extends State<_TextWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Text(
        Utils.getString(context, 'register__login'),
        maxLines: 2,
        style: Theme.of(context)
            .textTheme
            .bodyText2
            .copyWith(color: PsColors.mainColor),
      ),
      onTap: () {
        if (widget.goToLoginSelected != null) {
          widget.goToLoginSelected();
        } else {
          Navigator.pop(context);
        }
      },
    );
  }
}

class _TextFieldWidget extends StatefulWidget {
  const _TextFieldWidget({
    @required this.nameText,
    @required this.emailText,
    @required this.passwordText,
    @required this.emailFocusNode,
    @required this.nameFocusNode,
    @required this.passwordFocusNode,
  });
  final FocusNode nameFocusNode;
  final FocusNode emailFocusNode;
  final FocusNode passwordFocusNode;

  final TextEditingController nameText, emailText, passwordText;
  @override
  __TextFieldWidgetState createState() => __TextFieldWidgetState();
}

class __TextFieldWidgetState extends State<_TextFieldWidget> {
  @override
  Widget build(BuildContext context) {
    const EdgeInsets _marginEdgeInsetWidget = EdgeInsets.symmetric(
      horizontal: PsDimens.space8,
    );

    // const Widget _dividerWidget = Divider(
    //   height: PsDimens.space1,
    // );
    return Column(
      children: <Widget>[
        Container(
          margin: _marginEdgeInsetWidget,
          child: TextField(
            controller: widget.nameText,
            focusNode: widget.nameFocusNode,
            style: Theme.of(context).textTheme.button.copyWith(),
            decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  width: 2,
                  color: PsColors.greyColorForLoginTextField,
                ),
                borderRadius: BorderRadius.circular(24),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  width: 2,
                  color: Color(0xff095F56),
                ),
                borderRadius: BorderRadius.circular(24),
              ),
              //labelText: widget.labelText,
              hintStyle: const TextStyle(
                color: Color(0xff4F4F4F),
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                letterSpacing: 1,
              ),
              hintText: Utils.getString(context, 'register__user_name'),
              labelStyle: const TextStyle(
                color: Color(0xff4F4F4F),
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
              ),
              errorBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  width: 2,
                  color: Colors.red,
                ),
                borderRadius: BorderRadius.circular(24),
              ),
              prefix: const SizedBox(
                width: PsDimens.space20,
              ),
              //suffixIcon: widget.suffixIcon,
            ),
          ),
        ),
        const SizedBox(height: 24),
        Container(
          margin: _marginEdgeInsetWidget,
          child: TextField(
            controller: widget.emailText,
            focusNode: widget.emailFocusNode,
            style: Theme.of(context).textTheme.button.copyWith(),
            decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  width: 2,
                  color: PsColors.greyColorForLoginTextField,
                ),
                borderRadius: BorderRadius.circular(24),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  width: 2,
                  color: Color(0xff095F56),
                ),
                borderRadius: BorderRadius.circular(24),
              ),
              //labelText: widget.labelText,
              hintStyle: const TextStyle(
                color: Color(0xff4F4F4F),
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                letterSpacing: 1,
              ),
              hintText: Utils.getString(context, 'login__email'),
              labelStyle: const TextStyle(
                color: Color(0xff4F4F4F),
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
              ),
              errorBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  width: 2,
                  color: Colors.red,
                ),
                borderRadius: BorderRadius.circular(24),
              ),
              prefix: const SizedBox(
                width: PsDimens.space20,
              ),
              //suffixIcon: widget.suffixIcon,
            ),
          ),
        ),
        const SizedBox(height: 24),
        Container(
          margin: _marginEdgeInsetWidget,
          child: TextField(
            controller: widget.passwordText,
            focusNode: widget.passwordFocusNode,
            obscureText: true,
            style: Theme.of(context).textTheme.button.copyWith(),
            decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  width: 2,
                  color: PsColors.greyColorForLoginTextField,
                ),
                borderRadius: BorderRadius.circular(24),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  width: 2,
                  color: Color(0xff095F56),
                ),
                borderRadius: BorderRadius.circular(24),
              ),
              //labelText: widget.labelText,
              hintStyle: const TextStyle(
                color: Color(0xff4F4F4F),
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                letterSpacing: 1,
              ),
              hintText: Utils.getString(context, 'login__password'),
              labelStyle: const TextStyle(
                color: Color(0xff4F4F4F),
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
              ),
              errorBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  width: 2,
                  color: Colors.red,
                ),
                borderRadius: BorderRadius.circular(24),
              ),
              prefix: const SizedBox(
                width: PsDimens.space20,
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class _HeaderIconAndTextWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        const SizedBox(
          height: PsDimens.space32,
        ),
        Container(
          width: 90,
          height: 90,
          child: Image.asset(
            'assets/images/fs_android_3x.png',
          ),
        ),
        const SizedBox(
          height: PsDimens.space8,
        ),
        Text(Utils.getString(context, 'app_name'),
            style: Theme.of(context)
                .textTheme
                .subtitle1
                .copyWith(color: PsColors.mainColor)),
        const SizedBox(
          height: PsDimens.space52,
        ),
      ],
    );
  }
}

class _SignInButtonWidget extends StatefulWidget {
  const _SignInButtonWidget(
      {@required this.provider,
      @required this.nameTextEditingController,
      @required this.emailTextEditingController,
      @required this.passwordTextEditingController,
      this.onRegisterSelected});
  final UserProvider provider;
  final Function onRegisterSelected;
  final TextEditingController nameTextEditingController,
      emailTextEditingController,
      passwordTextEditingController;

  @override
  __SignInButtonWidgetState createState() => __SignInButtonWidgetState();
}

dynamic callWarningDialog(BuildContext context, String text) {
  showDialog<dynamic>(
      context: context,
      builder: (BuildContext context) {
        return WarningDialog(
          message: Utils.getString(context, text),
          onPressed: () {},
        );
      });
}

class __SignInButtonWidgetState extends State<_SignInButtonWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: PSButtonWidget(
        hasShadow: true,
        width: double.infinity,
        titleText: Utils.getString(context, 'register__register'),
        onPressed: () async {
          if (widget.nameTextEditingController.text.isEmpty) {
            callWarningDialog(context,
                Utils.getString(context, 'warning_dialog__input_name'));
          } else if (widget.emailTextEditingController.text.isEmpty) {
            callWarningDialog(context,
                Utils.getString(context, 'warning_dialog__input_email'));
          } else if (widget.passwordTextEditingController.text.isEmpty) {
            callWarningDialog(context,
                Utils.getString(context, 'warning_dialog__input_password'));
          } else {
            if (Utils.checkEmailFormat(
                widget.emailTextEditingController.text.trim())) {
              await widget.provider.signUpWithEmailId(
                  context,
                  widget.onRegisterSelected,
                  widget.nameTextEditingController.text,
                  widget.emailTextEditingController.text.trim(),
                  widget.passwordTextEditingController.text);
            } else {
              callWarningDialog(context,
                  Utils.getString(context, 'warning_dialog__email_format'));
            }
          }
        },
      ),
    );
  }
}
