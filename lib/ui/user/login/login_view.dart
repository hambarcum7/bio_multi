import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/constant/ps_constants.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/constant/route_paths.dart';
import 'package:biomart/provider/user/user_provider.dart';
import 'package:biomart/repository/user_repository.dart';
import 'package:biomart/ui/common/dialog/warning_dialog_view.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/common/ps_value_holder.dart';
import 'package:biomart/viewobject/holder/intent_holder/privacy_policy_intent_holder.dart';
import 'package:biomart/viewobject/user.dart';
import 'package:flutter/material.dart';
import 'package:progress_state_button/progress_button.dart';
import 'package:provider/provider.dart';
import 'package:the_apple_sign_in/apple_sign_in_button.dart' as apple;

class LoginView extends StatefulWidget {
  const LoginView({
    Key key,
    this.animationController,
    this.animation,
    this.onProfileSelected,
    this.onForgotPasswordSelected,
    this.onSignInSelected,
    this.onPhoneSignInSelected,
    this.onFbSignInSelected,
    this.onGoogleSignInSelected,
  }) : super(key: key);

  final AnimationController animationController;
  final Animation<double> animation;
  final Function onProfileSelected,
      onForgotPasswordSelected,
      onSignInSelected,
      onPhoneSignInSelected,
      onFbSignInSelected,
      onGoogleSignInSelected;
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  UserRepository repo1;
  PsValueHolder psValueHolder;
  final FocusNode _emailFocusNode = FocusNode();
  final FocusNode _passwordFocusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    widget.animationController.forward();
    const Widget _spacingWidget = SizedBox(
      height: PsDimens.space28,
    );

    repo1 = Provider.of<UserRepository>(context);
    psValueHolder = Provider.of<PsValueHolder>(context);

    return SliverToBoxAdapter(
      child: ChangeNotifierProvider<UserProvider>(
        lazy: false,
        create: (BuildContext context) {
          final UserProvider provider =
              UserProvider(repo: repo1, psValueHolder: psValueHolder);
          print(provider.getCurrentFirebaseUser());
          return provider;
        },
        child: Consumer<UserProvider>(builder:
            (BuildContext context, UserProvider provider, Widget child) {
          return AnimatedBuilder(
            animation: widget.animationController,
            child: GestureDetector(
              onTap: () {
                _emailFocusNode.unfocus();
                _passwordFocusNode.unfocus();
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                color: PsColors.coreBackgroundColor,
                child: SingleChildScrollView(
                  child: Column(
                    // mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      //_HeaderIconAndTextWidget(),
                      const SizedBox(
                        height: 70.0,
                      ),
                      const Center(
                        child: AppName(),
                      ),
                      const SizedBox(
                        height: 70.0,
                      ),
                      _TextFieldAndSignInButtonWidget(
                        provider: provider,
                        text: Utils.getString(context, 'login__submit'),
                        onProfileSelected: widget.onProfileSelected,
                        emailFocusNode: _emailFocusNode,
                        passwordFocusNode: _passwordFocusNode,
                        onForgotPasswordSelected:
                            widget.onForgotPasswordSelected,
                      ),

                      const SizedBox(
                        height: PsDimens.space32,
                      ),
                      // _TermsAndConCheckbox(
                      //   provider: provider,
                      //   onCheckBoxClick: () {
                      //     setState(() {
                      //       updateCheckBox(context, provider);
                      //     });
                      //   },
                      // ),

                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.spaceAround,
                      //   children: <Widget>[
                      //     if (PsConfig.showFacebookLogin)
                      //       _LoginWithFbWidget(
                      //         userProvider: provider,
                      //         onFbSignInSelected: widget.onFbSignInSelected,
                      //       ),
                      //     if (PsConfig.showGoogleLogin)
                      //       _LoginWithGoogleWidget(
                      //         userProvider: provider,
                      //         onGoogleSignInSelected:
                      //             widget.onGoogleSignInSelected,
                      //       ),
                      //     if (PsConfig.showPhoneLogin)
                      //       _LoginWithPhoneWidget(
                      //         onPhoneSignInSelected:
                      //             widget.onPhoneSignInSelected,
                      //         provider: provider,
                      //       ),
                      //     if (Utils.isAppleSignInAvailable == 1 &&
                      //         Platform.isIOS)
                      //       _LoginWithAppleIdWidget(
                      //         onAppleIdSignInSelected:
                      //             widget.onGoogleSignInSelected,
                      //       ),
                      //   ],
                      // ),
                      // const SizedBox(
                      //   height: PsDimens.space43,
                      // ),
                      // _DividerORWidget(),

                      _spacingWidget,
                      _RegisterWidget(
                        provider: provider,
                        animationController: widget.animationController,
                        //onForgotPasswordSelected: widget.onForgotPasswordSelected,
                        onSignInSelected: widget.onSignInSelected,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            builder: (BuildContext context, Widget child) {
              return FadeTransition(
                opacity: widget.animation,
                child: Transform(
                  transform: Matrix4.translationValues(
                      0.0, 100 * (1.0 - widget.animation.value), 0.0),
                  child: child,
                ),
              );
            },
          );
        }),
      ),
    );
  }
}

///my
class AppName extends StatelessWidget {
  const AppName({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'z',
      child: RichText(
        text: const TextSpan(
          children: <TextSpan>[
            TextSpan(
              text: PsConst.appFirstName,
              style: TextStyle(
                color: Color(0xff095F56),
                fontSize: 36.0,
                fontWeight: FontWeight.w700,
              ),
            ),
            TextSpan(
              text: PsConst.appLastName,
              style: TextStyle(
                color: Color(0xffFCC02A),
                fontSize: 36.0,
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

///my
class SignInWithIcon extends StatelessWidget {
  const SignInWithIcon({
    Key key,
    this.onTap,
    @required this.imagePath,
  }) : super(key: key);

  final void Function() onTap;
  final String imagePath;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: SizedBox(
        width: PsDimens.space48,
        height: PsDimens.space48,
        child: Image.asset(
          imagePath,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}

class _TermsAndConCheckbox extends StatefulWidget {
  const _TermsAndConCheckbox(
      {@required this.provider, @required this.onCheckBoxClick});

  final UserProvider provider;
  final Function onCheckBoxClick;

  @override
  __TermsAndConCheckboxState createState() => __TermsAndConCheckboxState();
}

class __TermsAndConCheckboxState extends State<_TermsAndConCheckbox> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        const SizedBox(
          width: PsDimens.space20,
        ),
        Checkbox(
          activeColor: PsColors.mainColor,
          value: widget.provider.isCheckBoxSelect,
          onChanged: (bool value) {
            widget.onCheckBoxClick();
          },
        ),
        Expanded(
          child: InkWell(
            child: Text(
              Utils.getString(context, 'login__agree_privacy'),
              style: Theme.of(context).textTheme.bodyText2,
            ),
            onTap: () {
              widget.onCheckBoxClick();
            },
          ),
        ),
      ],
    );
  }
}

void updateCheckBox(BuildContext context, UserProvider provider) {
  if (provider.isCheckBoxSelect) {
    provider.isCheckBoxSelect = false;
  } else {
    provider.isCheckBoxSelect = true;

    Navigator.pushNamed(context, RoutePaths.privacyPolicy,
        arguments: PrivacyPolicyIntentHolder(
            title: Utils.getString(context, 'privacy_policy__toolbar_name'),
            description: ''));
  }
}

class _HeaderIconAndTextWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Widget _textWidget = Text(Utils.getString(context, 'app_name'),
        style: Theme.of(context)
            .textTheme
            .subtitle1
            .copyWith(color: PsColors.mainColor));

    final Widget _imageWidget = Container(
      width: 90,
      height: 90,
      child: Image.asset(
        'assets/images/fs_android_3x.png',
      ),
    );
    return Column(
      children: <Widget>[
        const SizedBox(
          height: PsDimens.space32,
        ),
        _imageWidget,
        const SizedBox(
          height: PsDimens.space8,
        ),
        _textWidget,
        const SizedBox(
          height: PsDimens.space52,
        ),
      ],
    );
  }
}

class _TextFieldAndSignInButtonWidget extends StatefulWidget {
  _TextFieldAndSignInButtonWidget({
    Key key,
    @required this.provider,
    @required this.text,
    this.onProfileSelected,
    this.emailFocusNode,
    this.passwordFocusNode,
    this.onForgotPasswordSelected,
  }) : super(key: key);

  final UserProvider provider;
  final String text;
  final Function onProfileSelected;
  FocusNode emailFocusNode;
  FocusNode passwordFocusNode;
  final Function onForgotPasswordSelected;

  @override
  CardWidgetState createState() => CardWidgetState();
}

class CardWidgetState extends State<_TextFieldAndSignInButtonWidget> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  static ButtonState stateOnlyText = ButtonState.idle;

  void setCustomButtonState(ButtonState newState) {
    setState(() {
      stateOnlyText = newState;
    });
  }

  void onPressedCustomButton() {
    setState(() {
      switch (stateOnlyText) {
        case ButtonState.idle:
          stateOnlyText = ButtonState.loading;
          break;
        case ButtonState.loading:
          stateOnlyText = ButtonState.fail;
          break;
        case ButtonState.success:
          stateOnlyText = ButtonState.idle;
          break;
        case ButtonState.fail:
          stateOnlyText = ButtonState.success;
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    const EdgeInsets _marginEdgeInsetsforCard = EdgeInsets.symmetric(
      horizontal: PsDimens.space8,
    );
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Container(
          margin: _marginEdgeInsetsforCard,
          child: TextFormField(
            controller: emailController,
            keyboardType: TextInputType.emailAddress,
            focusNode: widget.emailFocusNode,
            decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  width: 2,
                  color: PsColors.greyColorForLoginTextField,
                ),
                borderRadius: BorderRadius.circular(24),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  width: 2,
                  color: PsColors.mainColor,
                ),
                borderRadius: BorderRadius.circular(24),
              ),
              hintStyle: TextStyle(
                color: Colors.grey.shade700,
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                letterSpacing: 1,
              ),
              hintText: Utils.getString(context, 'login__email'),
              labelStyle: const TextStyle(
                color: Color(0xff4F4F4F),
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
              ),
              errorBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  width: 2,
                  color: Colors.red,
                ),
                borderRadius: BorderRadius.circular(24),
              ),
              prefix: const SizedBox(
                width: PsDimens.space20,
              ),
              //suffixIcon: widget.suffixIcon,
            ),
            // decoration: InputDecoration(
            //   border: InputBorder.none,
            //   hintText: Utils.getString(context, 'login__email'),
            //   hintStyle: Theme.of(context)
            //       .textTheme
            //       .bodyText2
            //       .copyWith(color: PsColors.textPrimaryLightColor),
            //   icon: Icon(Icons.email,
            //       color: Theme.of(context).iconTheme.color),
            // ),
          ),
        ),
        const SizedBox(
          height: PsDimens.space24,
        ),
        Container(
          margin: _marginEdgeInsetsforCard,
          child: TextFormField(
            controller: passwordController,
            obscureText: true,
            focusNode: widget.passwordFocusNode,
            //style: Theme.of(context).textTheme.button.copyWith(),
            decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  width: 2,
                  color: PsColors.greyColorForLoginTextField,
                ),
                borderRadius: BorderRadius.circular(24),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  width: 2,
                  color: PsColors.mainColor,
                ),
                borderRadius: BorderRadius.circular(24),
              ),
              //labelText: widget.labelText,
              hintStyle: TextStyle(
                color: Colors.grey.shade700,
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
                letterSpacing: 1,
              ),
              hintText: Utils.getString(context, 'login__password'),
              labelStyle: const TextStyle(
                color: Color(0xff4F4F4F),
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
              ),
              errorBorder: OutlineInputBorder(
                borderSide: const BorderSide(
                  width: 2,
                  color: Colors.red,
                ),
                borderRadius: BorderRadius.circular(24),
              ),
              suffixIcon: Padding(
                padding: const EdgeInsets.only(right: 18.0),
                child: TextButton(
                  onPressed: () {
                    if (widget.onForgotPasswordSelected != null) {
                      widget.onForgotPasswordSelected();
                    } else {
                      Navigator.pushNamed(
                        context,
                        RoutePaths.user_forgot_password_container,
                      );
                    }
                  },
                  child: Text(
                    Utils.getString(context, 'login__reset'),
                    style: TextStyle(
                      color: PsColors.mainColor,
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
              prefix: const SizedBox(
                width: PsDimens.space20,
              ),
            ),
            // decoration: InputDecoration(
            //   border: InputBorder.none,
            //   hintText: Utils.getString(context, 'login__password'),
            //   hintStyle: Theme.of(context)
            //       .textTheme
            //       .bodyText2
            //       .copyWith(color: PsColors.textPrimaryLightColor),
            //   icon: Icon(Icons.lock,
            //       color: Theme.of(context).iconTheme.color),
            // ),
          ),
        ),
        const SizedBox(
          height: PsDimens.space32,
        ),
        ProgressButton(
          stateWidgets: <ButtonState, Widget>{
            ButtonState.idle: Text(
              Utils.getString(context, 'login__sign_in'),
              style: TextStyle(
                color: PsColors.white,
                fontSize: PsDimens.space18,
                fontWeight: FontWeight.w500,
              ),
            ),
            ButtonState.loading: null,
            ButtonState.fail: const Text(
              'Failed',
            ),
            ButtonState.success: const Text(
              'Success',
            )
          },
          stateColors: <ButtonState, Color>{
            ButtonState.idle: PsColors.mainColor,
            ButtonState.loading: PsColors.mainColor,
            ButtonState.fail: PsColors.mainColor,
            ButtonState.success: PsColors.mainColor,
          },
          state: stateOnlyText,
          padding: const EdgeInsets.symmetric(
            horizontal: 18.0,
          ),
          radius: 24.0,
          maxWidth: MediaQuery.of(context).size.width - 2 * 50,
          minWidth: 75,
          progressIndicator: const CircularProgressIndicator(
            strokeWidth: 2,
            color: Colors.white,
          ),
          onPressed: () async {
            //onPressedCustomButton();
            if (emailController.text.isEmpty) {
              callWarningDialog(
                context,
                Utils.getString(
                  context,
                  'warning_dialog__input_email',
                ),
              );
            } else if (passwordController.text.isEmpty) {
              callWarningDialog(
                context,
                Utils.getString(
                  context,
                  'warning_dialog__input_password',
                ),
              );
            } else {
              if (Utils.checkEmailFormat(emailController.text.trim())) {
                onPressedCustomButton();
                await widget.provider.loginWithEmailId(
                  context,
                  emailController.text.trim(),
                  passwordController.text,
                  widget.onProfileSelected,
                );

                setCustomButtonState(ButtonState.idle);
              } else {
                callWarningDialog(
                  context,
                  Utils.getString(
                    context,
                    'warning_dialog__email_format',
                  ),
                );
                //setCustomButtonState(ButtonState.fail);
              }
            }
          },
        ),
        // PSButtonWidget(
        //   hasShadow: true,
        //   width: double.infinity,
        //   titleText: Utils.getString(context, 'login__sign_in'),
        //   onPressed: () async {
        //     if (emailController.text.isEmpty) {
        //       callWarningDialog(context,
        //           Utils.getString(context, 'warning_dialog__input_email'));
        //       setCustomButtonState(ButtonState.fail);
        //     } else if (passwordController.text.isEmpty) {
        //       callWarningDialog(context,
        //           Utils.getString(context, 'warning_dialog__input_password'));
        //       setCustomButtonState(ButtonState.fail);
        //     } else {
        //       if (Utils.checkEmailFormat(emailController.text.trim())) {
        //         await widget.provider.loginWithEmailId(
        //           context,
        //           emailController.text.trim(),
        //           passwordController.text,
        //           widget.onProfileSelected,
        //         );
        //       setCustomButtonState(ButtonState.success);
        //       } else {
        //         callWarningDialog(context,
        //             Utils.getString(context, 'warning_dialog__email_format'));
        //       setCustomButtonState(ButtonState.fail);
        //       }
        //     }
        //   },
        // ),
      ],
    );
  }
}

dynamic callWarningDialog(BuildContext context, String text, [Function onTap]) {
  showDialog<dynamic>(
      context: context,
      builder: (BuildContext context) {
        return WarningDialog(
          message: Utils.getString(context, text),
          onPressed: onTap,
        );
      });
}

class _DividerORWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Widget _dividerWidget = Container(
      width: PsDimens.space108,
      height: PsDimens.space1,
      color: PsColors.textPrimaryColor,
    );

    const Widget _spacingWidget = SizedBox(
      width: PsDimens.space16,
    );

    const Widget _textWidget = Text(
      'OR',
      //style: Theme.of(context).textTheme.subtitle1,
    );
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        _dividerWidget,
        _spacingWidget,
        _textWidget,
        _spacingWidget,
        _dividerWidget,
      ],
    );
  }
}

class _LoginWithPhoneWidget extends StatefulWidget {
  const _LoginWithPhoneWidget(
      {@required this.onPhoneSignInSelected, @required this.provider});
  final Function onPhoneSignInSelected;
  final UserProvider provider;

  @override
  __LoginWithPhoneWidgetState createState() => __LoginWithPhoneWidgetState();
}

class __LoginWithPhoneWidgetState extends State<_LoginWithPhoneWidget> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      // margin: const EdgeInsets.only(
      //     left: PsDimens.space32, right: PsDimens.space32),
      width: PsDimens.space48,
      height: PsDimens.space48,
      child: InkWell(
        //titleText: Utils.getString(context, 'login__phone_signin'),
        //icon: Icons.phone,
        // colorData: widget.provider.isCheckBoxSelect
        //     ? PsColors.mainColor
        //     : PsColors.mainColor,
        child: Image.asset(
          'assets/login_icons/sms.png',
          fit: BoxFit.cover,
        ),
        onTap: () async {
          if (widget.provider.isCheckBoxSelect) {
            if (widget.onPhoneSignInSelected != null) {
              widget.onPhoneSignInSelected();
            } else {
              Navigator.pushReplacementNamed(
                context,
                RoutePaths.user_phone_signin_container,
              );
            }
          } else {
            showDialog<dynamic>(
                context: context,
                builder: (BuildContext context) {
                  return WarningDialog(
                    message: Utils.getString(
                        context, 'login__warning_agree_privacy'),
                    onPressed: () {},
                  );
                });
          }
        },
      ),
    );
  }
}

class _LoginWithFbWidget extends StatefulWidget {
  const _LoginWithFbWidget(
      {@required this.userProvider, @required this.onFbSignInSelected});
  final UserProvider userProvider;
  final Function onFbSignInSelected;

  @override
  __LoginWithFbWidgetState createState() => __LoginWithFbWidgetState();
}

class __LoginWithFbWidgetState extends State<_LoginWithFbWidget> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      // margin: const EdgeInsets.only(
      //     left: PsDimens.space32,
      //     top: PsDimens.space8,
      //     right: PsDimens.space32),
      width: PsDimens.space48,
      height: PsDimens.space48,
      child: InkWell(
          // titleText: Utils.getString(context, 'login__fb_signin'),
          // icon: FontAwesome.facebook_official,
          // colorData: widget.userProvider.isCheckBoxSelect == false
          //     ? PsColors.facebookLoginButtonColor
          //     : PsColors.facebookLoginButtonColor,
          child: Image.asset(
            'assets/login_icons/facebook.png',
            fit: BoxFit.cover,
          ),
          onTap: () async {
            await widget.userProvider
                .loginWithFacebookId(context, widget.onFbSignInSelected);
          }),
    );
  }
}

class _LoginWithAppleIdWidget extends StatelessWidget {
  const _LoginWithAppleIdWidget({@required this.onAppleIdSignInSelected});

  final Function onAppleIdSignInSelected;

  @override
  Widget build(BuildContext context) {
    final UserProvider _userProvider =
        Provider.of<UserProvider>(context, listen: false);
    return Container(
        margin: const EdgeInsets.only(
            left: PsDimens.space32,
            top: PsDimens.space8,
            right: PsDimens.space32),
        child: Directionality(
          textDirection: TextDirection.ltr,
          child: apple.AppleSignInButton(
            style: apple.ButtonStyle.black, // style as needed
            type: apple.ButtonType.signIn, // style as needed
            onPressed: () async {
              await _userProvider.loginWithAppleId(
                  context, onAppleIdSignInSelected);
            },
          ),
        ));
  }
}

class _LoginWithGoogleWidget extends StatefulWidget {
  const _LoginWithGoogleWidget(
      {@required this.userProvider, @required this.onGoogleSignInSelected});
  final UserProvider userProvider;
  final Function onGoogleSignInSelected;

  @override
  __LoginWithGoogleWidgetState createState() => __LoginWithGoogleWidgetState();
}

class __LoginWithGoogleWidgetState extends State<_LoginWithGoogleWidget> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      // margin: const EdgeInsets.only(
      //     left: PsDimens.space32,
      //     top: PsDimens.space8,
      //     right: PsDimens.space32),
      width: PsDimens.space48,
      height: PsDimens.space48,
      child: InkWell(
        // titleText: Utils.getString(context, 'login__google_signin'),
        // icon: FontAwesome.google,
        // colorData: widget.userProvider.isCheckBoxSelect
        //     ? PsColors.googleLoginButtonColor
        //     : PsColors.googleLoginButtonColor,
        child: Image.asset(
          'assets/login_icons/google.png',
          fit: BoxFit.cover,
        ),
        onTap: () async {
          await widget.userProvider
              .loginWithGoogleId(context, widget.onGoogleSignInSelected);
        },
      ),
    );
  }
}

class _RegisterWidget extends StatefulWidget {
  const _RegisterWidget({
    Key key,
    this.provider,
    this.animationController,
    this.onForgotPasswordSelected,
    this.onSignInSelected,
  }) : super(key: key);

  final AnimationController animationController;
  final Function onForgotPasswordSelected;
  final Function onSignInSelected;
  final UserProvider provider;

  @override
  ___RegisterWidgetState createState() => ___RegisterWidgetState();
}

class ___RegisterWidgetState extends State<_RegisterWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(bottom: PsDimens.space40),
      margin: const EdgeInsets.symmetric(horizontal: PsDimens.space20),
      child: Row(
        //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          SizedBox(
            width: (MediaQuery.of(context).size.width - 45) * 0.7,
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                Utils.getString(context, 'login__no_account'),
                textAlign: TextAlign.center,
                //maxLines: 2,
                style: Theme.of(context).textTheme.button.copyWith(
                    color: PsColors.textPrimaryColor,
                    fontSize: PsDimens.space18,
                    fontWeight: FontWeight.w400),
              ),
            ),
          ),
          const SizedBox(width: 5),
          GestureDetector(
            child: SizedBox(
              width: (MediaQuery.of(context).size.width - 45) * 0.3,
              child: FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  Utils.getString(context, 'login__sign_up'),
                  textAlign: TextAlign.center,
                  maxLines: 2,
                  style: Theme.of(context).textTheme.button.copyWith(
                      color: PsColors.mainColor,
                      fontSize: PsDimens.space18,
                      fontWeight: FontWeight.w700),
                ),
              ),
            ),
            onTap: () async {
              if (widget.onSignInSelected != null) {
                widget.onSignInSelected();
              } else {
                final dynamic returnData = await Navigator.pushNamed(
                  context,
                  RoutePaths.user_register_container,
                );
                if (returnData != null && returnData is User) {
                  final User user = returnData;
                  widget.provider.psValueHolder =
                      Provider.of<PsValueHolder>(context, listen: false);
                  widget.provider.psValueHolder.loginUserId = user.userId;
                  widget.provider.psValueHolder.userIdToVerify = '';
                  widget.provider.psValueHolder.userNameToVerify = '';
                  widget.provider.psValueHolder.userEmailToVerify = '';
                  widget.provider.psValueHolder.userPasswordToVerify = '';
                  Navigator.pop(context, user);
                }
              }
            },
          ),
        ],
      ),
    );
  }
}
