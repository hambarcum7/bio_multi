import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/provider/user/user_provider.dart';
import 'package:biomart/repository/user_repository.dart';
import 'package:biomart/ui/dashboard/view_pages/call_bottom_view.dart';
import 'package:biomart/utils/app_bar_widget.dart';
import 'package:biomart/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'login_view.dart';

class LoginContainerView extends StatefulWidget {
  @override
  _CityLoginContainerViewState createState() => _CityLoginContainerViewState();
}

class _CityLoginContainerViewState extends State<LoginContainerView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  UserProvider userProvider;
  UserRepository userRepo;

  @override
  Widget build(BuildContext context) {
    final Animation<double> animation = Tween<double>(begin: 0.0, end: 1.0)
        .animate(CurvedAnimation(
            parent: animationController,
            curve: const Interval(0.5 * 1, 1.0, curve: Curves.fastOutSlowIn)));

    print(
        '............................Build UI Again ............................');
    userRepo = Provider.of<UserRepository>(context);
    return Scaffold(
      bottomNavigationBar: const BottomBarViewWidget(
        currentIdex: 2,
      ),
      backgroundColor: PsColors.coreBackgroundColor,
      appBar: customAppBar(
        title: Utils.getString(context, 'login__title'),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back_ios,
              size: 17.0,
              color: PsColors.textPrimaryColor,
            ),
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            color: PsColors.coreBackgroundColor,
            width: double.infinity,
            height: double.maxFinite,
          ),
          CustomScrollView(scrollDirection: Axis.vertical,
              //shrinkWrap: true,
              slivers: <Widget>[
                // _SliverAppbar(
                //   title: Utils.getString(context, 'login__title'),
                //   scaffoldKey: scaffoldKey,
                // ),
                LoginView(
                  animationController: animationController,
                  animation: animation,
                ),
              ])
        ],
      ),
    );
  }
}

class _SliverAppbar extends StatefulWidget {
  const _SliverAppbar(
      {Key key, @required this.title, this.scaffoldKey, this.menuDrawer})
      : super(key: key);
  final String title;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Drawer menuDrawer;
  @override
  _SliverAppbarState createState() => _SliverAppbarState();
}

class _SliverAppbarState extends State<_SliverAppbar> {
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      systemOverlayStyle: SystemUiOverlayStyle(
          statusBarIconBrightness: Utils.getBrightnessForAppBar(context)),
      iconTheme: Theme.of(context)
          .iconTheme
          .copyWith(color: PsColors.mainColorWithWhite),
      title: Text(
        widget.title,
        textAlign: TextAlign.center,
        style: Theme.of(context)
            .textTheme
            .headline6
            .copyWith(fontWeight: FontWeight.bold)
            .copyWith(color: PsColors.mainColorWithWhite),
      ),
      // actions: <Widget>[
      //   IconButton(
      //     icon: Icon(Icons.notifications_none,
      //         color: Theme.of(context).iconTheme.color),
      //     onPressed: () {
      //       Navigator.pushNamed(
      //         context,
      //         RoutePaths.notiList,
      //       );
      //     },
      //   ),
      //   IconButton(
      //     icon:
      //         Icon(Feather.book_open, color: Theme.of(context).iconTheme.color),
      //     onPressed: () {
      //       Navigator.pushNamed(
      //         context,
      //         RoutePaths.blogList,
      //       );
      //     },
      //   )
      // ],
      elevation: 0,
    );
  }
}
