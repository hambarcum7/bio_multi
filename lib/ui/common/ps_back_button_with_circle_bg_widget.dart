import 'dart:io';

import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:flutter/material.dart';

class PsBackButtonWithCircleBgWidget extends StatelessWidget {
  const PsBackButtonWithCircleBgWidget({
    Key key,
    this.isReadyToShow,
    this.onTap,
  }) : super(key: key);

  final bool isReadyToShow;
  final Function() onTap;
  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: isReadyToShow,
      child: Container(
        margin: const EdgeInsets.only(
          left: PsDimens.space12,
          right: PsDimens.space4,
        ),
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.black26,
        ),
        child: Align(
          alignment: Alignment.center,
          child: Padding(
            padding: EdgeInsets.only(
                left: Platform.isIOS ? PsDimens.space8 : PsDimens.space1),
            child: InkWell(
                child: Icon(
                  Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
                  color: PsColors.white,
                ),
                onTap: () {
                  onTap == null ? Navigator.pop(context) : onTap();
                }),
          ),
        ),
      ),
    );
  }
}
