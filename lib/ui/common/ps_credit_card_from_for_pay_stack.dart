import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/ui/common/base/ps_widget_with_appbar_with_no_provider.dart';
import 'package:biomart/ui/common/ps_button_widget.dart';
import 'package:biomart/ui/common/ps_credit_card_form.dart';
import 'package:biomart/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_credit_card/flutter_credit_card.dart';
import 'package:tinkoff_acquiring/src/core/models/common/card_info.dart';
import 'package:tinkoff_acquiring/tinkoff_acquiring.dart' as tinkoff;

class CheckCard extends StatefulWidget {
  const CheckCard({Key key}) : super(key: key);

  @override
  _CheckCardState createState() => _CheckCardState();
}

class _CheckCardState extends State<CheckCard> {
  List<CardInfo> cards = <CardInfo>[
    CardInfo(
      pan: '84684854616454',
      expDate: '12/23',
    ),
    CardInfo(
      pan: '8465685687698704',
      expDate: '10/23',
    ),
    CardInfo(
      pan: '8133244212454',
      expDate: '12/23',
    ),
    CardInfo(
      pan: '84684854616454',
      expDate: '12/23',
    ),
    CardInfo(
      pan: '849999146544556454',
      expDate: '12/23',
    ),
  ];
  tinkoff.TinkoffAcquiring acquiring;
  tinkoff.InitResponse init;
  int selectedCardIndex = 0;

  Widget createCard({
    @required String cardNumber,
    @required String expiryDate,
    @required String cardHolderName,
    @required Function onTap,
    @required Function onDoubleTap,
  }) {
    return GestureDetector(
      onTap: onTap,
      onDoubleTap: onDoubleTap,
      child: CreditCardWidget(
        cardNumber: cardNumber,
        expiryDate: expiryDate,
        cardHolderName: cardHolderName,
        cvvCode: '',
        showBackView: true,
        height: 175,
        width: MediaQuery.of(context).size.width,
        animationDuration: PsConfig.animation_duration,
        cardType: CardType.visa,
      ),
    );
  }

  Future<void> initTinkoff() async {
    acquiring = tinkoff.TinkoffAcquiring(
      tinkoff.TinkoffAcquiringConfig(
        terminalKey: 'TinkoffBankTest',
        password: 'password',
      ),
    );
    // init = await acquiring.init(
    //   tinkoff.InitRequest(
    //     orderId: '180',
    //     customerKey: '',
    //     amount: 140000,
    //   ),
    // );
    // if(init.success){

    // }

    final tinkoff.GetCardListResponse res = await acquiring.getCardList(
      tinkoff.GetCardListRequest(customerKey: ''),
    );
    //cards = res?.cardInfo;
  }

  @override
  void initState() {
    initTinkoff();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PsWidgetWithAppBarWithNoProvider(
      appBarTitle: Utils.getString(context, 'credit_card__check_credit_card'),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text(
                Utils.getString(context, 'credit_card__check_your_credit_card'),
              ),
            ),
            const SizedBox(
              height: PsDimens.space16,
            ),
            if (cards.isNotEmpty)
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: SizedBox(
                  height: 175.0,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: cards.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        padding: const EdgeInsets.all(2.0),
                        //margin: const EdgeInsets.only(right: 8.0),
                        width: MediaQuery.of(context).size.width - 4 * 16,
                        decoration: selectedCardIndex == index
                            ? BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                border: Border.all(
                                  color: Colors.grey[700],
                                  width: 2.0,
                                ),
                              )
                            : null,
                        child: MyCreditCard(
                          cardNumber: cards[index].pan,
                          expiryDate: cards[index].expDate,
                          cardHolderName: '************************',
                          onTap: () {
                            setState(() {
                              selectedCardIndex = index;
                            });
                          },
                        ),
                      );
                    },
                  ),
                ),
              )
            else
              Center(
                child: Text(
                  Utils.getString(context, 'credit_card__cards_empty_list'),
                ),
              ),
            const SizedBox(
              height: PsDimens.space20,
            ),
            Center(
              child: PSButtonWidget(
                titleText: Utils.getString(context, 'credit_card__pay'),
                onPressed: () {
                  ///  cards[selectedCardIndex]
                },
              ),
            ),
            const SizedBox(
              height: PsDimens.space20,
            ),
            Center(
              child: PSButtonWidget(
                titleText:
                    Utils.getString(context, 'credit_card__pay_with_new_card'),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => const PsCreditCardForm()));
                },
              ),
            ),
            const SizedBox(
              height: PsDimens.space20,
            ),
          ],
        ),
      ),
    );
  }
}

class MyCreditCard extends StatefulWidget {
  MyCreditCard({
    Key key,
    this.cardNumber,
    this.expiryDate,
    this.cardHolderName,
    this.onTap,
    // this.onDoubleTap,
  }) : super(key: key);
  String cardNumber;
  String expiryDate;
  String cardHolderName;
  Function onTap;
  //Function onDoubleTap;

  @override
  _MyCreditCardState createState() => _MyCreditCardState();
}

class _MyCreditCardState extends State<MyCreditCard> {
  bool showBackView = false;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      onDoubleTap: () {
        //widget.onDoubleTap();
        setState(() {
          showBackView = !showBackView;
        });
      },
      child: CreditCardWidget(
        cardNumber: widget.cardNumber,
        expiryDate: widget.expiryDate,
        cardHolderName: widget.cardHolderName,
        cvvCode: '',
        showBackView: showBackView,
        height: 175,
        width: MediaQuery.of(context).size.width - 2 * 16,
        animationDuration: const Duration(milliseconds: 500),
      ),
    );
  }
}
