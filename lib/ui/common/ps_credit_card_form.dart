import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/ui/common/base/ps_widget_with_appbar_with_no_provider.dart';
import 'package:biomart/ui/common/ps_button_widget.dart';
import 'package:biomart/ui/common/ps_textfield_widget.dart';
import 'package:biomart/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_credit_card/flutter_credit_card.dart';

class PsCreditCardForm extends StatefulWidget {
  const PsCreditCardForm({
    Key key,
    this.cardNumber,
    this.expiryDate,
    this.cardHolderName,
    this.cvvCode,
    //@required this.onCreditCardModelChange,
    this.themeColor,
    this.textColor,
    this.cursorColor,
  }) : super(key: key);

  final String cardNumber;
  final String expiryDate;
  final String cardHolderName;
  final String cvvCode;
  //final void Function(CreditCardModel) onCreditCardModelChange;
  final Color themeColor;
  final Color textColor;
  final Color cursorColor;

  @override
  _PsCreditCardFormState createState() => _PsCreditCardFormState();
}

class _PsCreditCardFormState extends State<PsCreditCardForm> {
  String cardNumber;
  String expiryDate;
  String cardHolderName;
  String cvvCode;
  bool isCvvFocused = false;
  Color themeColor;

  //void Function(CreditCardModel) onCreditCardModelChange;
  CreditCardModel creditCardModel;

  final MaskedTextController _cardNumberController =
      MaskedTextController(mask: '0000 0000 0000 0000');
  final TextEditingController _expiryDateController =
      MaskedTextController(mask: '00/00');
  final TextEditingController _cardHolderNameController =
      TextEditingController();
  final TextEditingController _cvvCodeController =
      MaskedTextController(mask: '000');

  FocusNode cvvFocusNode = FocusNode();

  void onCreditCardModelChange(CreditCardModel creditCardModel) {
    setState(() {
      cardNumber = creditCardModel.cardNumber;
      expiryDate = creditCardModel.expiryDate;
      cardHolderName = creditCardModel.cardHolderName;
      cvvCode = creditCardModel.cvvCode;
      isCvvFocused = creditCardModel.isCvvFocused;
    });
  }

  void textFieldFocusDidChange() {
    creditCardModel.isCvvFocused = cvvFocusNode.hasFocus;
    onCreditCardModelChange(creditCardModel);
  }

  void createCreditCardModel() {
    cardNumber = widget.cardNumber ?? '';
    expiryDate = widget.expiryDate ?? '';
    cardHolderName = widget.cardHolderName ?? '';
    cvvCode = widget.cvvCode ?? '';

    creditCardModel = CreditCardModel(
        cardNumber, expiryDate, cardHolderName, cvvCode, isCvvFocused);
  }

  @override
  void initState() {
    super.initState();

    createCreditCardModel();

    // onCreditCardModelChange = widget.onCreditCardModelChange;

    cvvFocusNode.addListener(() {
      print('ffffffffffffffffffffffffffffffffffffff');
      textFieldFocusDidChange();
    });

    _cardNumberController.addListener(() {
      setState(() {
        cardNumber = _cardNumberController.text;
        creditCardModel.cardNumber = cardNumber;
        onCreditCardModelChange(creditCardModel);
      });
    });

    _expiryDateController.addListener(() {
      setState(() {
        expiryDate = _expiryDateController.text;
        creditCardModel.expiryDate = expiryDate;
        onCreditCardModelChange(creditCardModel);
      });
    });

    _cardHolderNameController.addListener(() {
      setState(() {
        cardHolderName = _cardHolderNameController.text;
        creditCardModel.cardHolderName = cardHolderName;
        onCreditCardModelChange(creditCardModel);
      });
    });

    _cvvCodeController.addListener(() {
      setState(() {
        cvvCode = _cvvCodeController.text;
        creditCardModel.cvvCode = cvvCode;
        onCreditCardModelChange(creditCardModel);
      });
    });
  }

  @override
  void didChangeDependencies() {
    themeColor = widget.themeColor ?? Theme.of(context).primaryColor;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return PsWidgetWithAppBarWithNoProvider(
      appBarTitle: Utils.getString(context, 'credit_card__toolbar'),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            CreditCardWidget(
              cardNumber: cardNumber,
              expiryDate: expiryDate,
              cardHolderName: cardHolderName,
              cvvCode: cvvCode,
              showBackView: cvvFocusNode.hasFocus,
              height: 175,
              width: MediaQuery.of(context).size.width,
              animationDuration: PsConfig.animation_duration,
            ),
            PsTextFieldWidget(
              titleText: 'Card Number',
              textAboutMe: false,
              hintText: 'xxxx xxxx xxxx xxxx',
              keyboardType: TextInputType.number,
              phoneInputType: true,
              textEditingController: _cardNumberController,
            ),
            PsTextFieldWidget(
              titleText: 'Expired Date',
              textAboutMe: false,
              hintText: 'MM/YY',
              keyboardType: TextInputType.number,
              phoneInputType: true,
              textEditingController: _expiryDateController,
            ),
            PsTextFieldWidget(
              titleText: 'CVV',
              textAboutMe: false,
              hintText: 'XXX',
              keyboardType: TextInputType.number,
              phoneInputType: true,
              textEditingController: _cvvCodeController,
              focusNode: cvvFocusNode,
            ),
            PsTextFieldWidget(
              titleText: 'Card Holder Name',
              textAboutMe: false,
              hintText: 'Card Holder Name',
              keyboardType: TextInputType.text,
              textEditingController: _cardHolderNameController,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                vertical: PsDimens.space20,
              ),
              child: PSButtonWidget(
                titleText: Utils.getString(context, 'credit_card__pay'),
                onPressed: () {
                  ///  cards[selectedCardIndex]
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
