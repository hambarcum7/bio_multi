import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:flutter/material.dart';

class PsTextFieldWidget extends StatelessWidget {
  const PsTextFieldWidget({
    this.textEditingController,
    this.titleText = '',
    this.hintText,
    this.textAboutMe = false,
    this.height = PsDimens.space44,
    this.showTitle = true,
    this.keyboardType = TextInputType.text,
    this.phoneInputType = false,
    this.isMandatory = false,
    this.prefixIcon,
    this.enabled = true,
    this.focusNode,
  });

  final TextEditingController textEditingController;
  final String titleText;
  final String hintText;
  final double height;
  final bool textAboutMe;
  final TextInputType keyboardType;
  final bool showTitle;
  final bool phoneInputType;
  final bool isMandatory;
  final Widget prefixIcon;
  final bool enabled;
  final FocusNode focusNode;
  @override
  Widget build(BuildContext context) {
    final Widget _productTextWidget =
        Text(titleText, style: Theme.of(context).textTheme.bodyText1);

    return Column(
      children: <Widget>[
        if (showTitle)
          Container(
            margin: const EdgeInsets.only(
                left: PsDimens.space12,
                top: PsDimens.space12,
                right: PsDimens.space12),
            child: Row(
              children: <Widget>[
                if (isMandatory)
                  Row(
                    children: <Widget>[
                      Text(titleText,
                          style: Theme.of(context).textTheme.bodyText1),
                      Text(' *',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1
                              .copyWith(color: PsColors.mainColor))
                    ],
                  )
                else
                  _productTextWidget
              ],
            ),
          )
        else
          Container(
            height: 0,
          ),
        Container(
          width: double.infinity,
          height: height,
          margin: const EdgeInsets.all(PsDimens.space12),
          decoration: BoxDecoration(
            color: PsColors.coreBackgroundColor,
            borderRadius: BorderRadius.circular(PsDimens.space4),
            border: Border.all(color: PsColors.mainDividerColor),
          ),
          child: TextField(
            enabled: enabled,
            keyboardType:
                phoneInputType ? TextInputType.phone : TextInputType.text,
            maxLines: null,
            controller: textEditingController,
            focusNode: focusNode,
            style: Theme.of(context)
                .textTheme
                .bodyText2
                .copyWith(color: PsColors.textPrimaryColor),
            decoration: textAboutMe
                ? InputDecoration(
                    contentPadding: const EdgeInsets.only(
                      left: PsDimens.space12,
                      bottom: PsDimens.space8,
                      top: PsDimens.space10,
                    ),
                    // border: const OutlineInputBorder(
                    //   borderRadius: BorderRadius.all(
                    //     Radius.circular(8),
                    //   ),
                    // ),
                    prefixIcon: prefixIcon,
                    border: InputBorder.none,
                    hintText: hintText,
                    hintStyle: Theme.of(context)
                        .textTheme
                        .bodyText2
                        .copyWith(color: PsColors.textPrimaryColor),
                  )
                : InputDecoration(
                    contentPadding: const EdgeInsets.only(
                      left: PsDimens.space12,
                      bottom: PsDimens.space8,
                    ),
                    // border: const OutlineInputBorder(
                    //   borderRadius: BorderRadius.all(
                    //     Radius.circular(8),
                    //   ),
                    // ),
                    prefixIcon: prefixIcon,
                    border: InputBorder.none,
                    hintText: hintText,
                    hintStyle: Theme.of(context)
                        .textTheme
                        .bodyText2
                        .copyWith(color: PsColors.textPrimaryColor),
                  ),
          ),
        ),
      ],
    );
  }
}
