import 'package:biomart/config/ps_colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

class WidgetWithrWithTwoProvider<T extends ChangeNotifier,
    V extends ChangeNotifier> extends StatefulWidget {
  const WidgetWithrWithTwoProvider({
    Key key,
    @required this.initProvider1,
    @required this.initProvider2,
    this.child,
    this.onProviderReady1,
    this.onProviderReady2,
  }) : super(key: key);

  final Function initProvider1, initProvider2;
  final Widget child;
  final Function(T) onProviderReady1;
  final Function(V) onProviderReady2;

  @override
  _WidgetWithrWithTwoProviderState<T, V> createState() =>
      _WidgetWithrWithTwoProviderState<T, V>();
}

class _WidgetWithrWithTwoProviderState<T extends ChangeNotifier,
    V extends ChangeNotifier> extends State<WidgetWithrWithTwoProvider<T, V>> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: PsColors.coreBackgroundColor,
        body: MultiProvider(providers: <SingleChildWidget>[
          ChangeNotifierProvider<T>(
            lazy: false,
            create: (BuildContext context) {
              final T providerObj1 = widget.initProvider1();

              if (widget.onProviderReady1 != null) {
                widget.onProviderReady1(providerObj1);
              }

              return providerObj1;
            },
          ),
          ChangeNotifierProvider<V>(
            lazy: false,
            create: (BuildContext context) {
              final V providerObj2 = widget.initProvider2();
              if (widget.onProviderReady2 != null) {
                widget.onProviderReady2(providerObj2);
              }

              return providerObj2;
            },
          )
        ], child: widget.child));
  }
}
