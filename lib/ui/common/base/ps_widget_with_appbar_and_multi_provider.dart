// import 'package:biomart/config/ps_colors.dart';
// import 'package:biomart/utils/utils.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';

// class PsWidgetWithAppBarAndMultiProvider extends StatefulWidget {
//   const PsWidgetWithAppBarAndMultiProvider(
//       {Key key,
//       this.child,
//       @required this.appBarTitle,
//       this.actions = const <Widget>[]})
//       : super(key: key);

//   final Widget child;
//   final String appBarTitle;
//   final List<Widget> actions;

//   @override
//   _PsWidgetWithAppBarAndMultiProviderState createState() =>
//       _PsWidgetWithAppBarAndMultiProviderState();
// }

// class _PsWidgetWithAppBarAndMultiProviderState
//     extends State<PsWidgetWithAppBarAndMultiProvider> {
//   @override
//   void initState() {
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: PsColors.coreBackgroundColor,
//       appBar: AppBar(
//         systemOverlayStyle: SystemUiOverlayStyle(
//             statusBarIconBrightness: Utils.getBrightnessForAppBar(context)),
//         iconTheme: IconThemeData(color: PsColors.mainColorWithWhite),
//         title: Text(widget.appBarTitle,
//             style: Theme.of(context).textTheme.headline6.copyWith(
//                 fontWeight: FontWeight.bold,
//                 color: PsColors.mainColorWithWhite)),
//         leading: GestureDetector(
//           onTap: () {
//             Navigator.pop(context);
//           },
//           child: GestureDetector(
//             onTap: () {
//               Navigator.pop(context);
//             },
//             child: Icon(
//               Icons.arrow_back_ios,
//               size: 17.0,
//               color: PsColors.textPrimaryColor,
//             ),
//           ),
//         ),
//         actions: widget.actions,
//         flexibleSpace: Container(
//           height: 200,
//         ),
//         elevation: 0,
//       ),
//       body: widget.child,
//     );
//   }
// }
