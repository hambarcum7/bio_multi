import 'dart:io';

import 'package:biomart/api/common/ps_resource.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_constants.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/constant/route_paths.dart';
import 'package:biomart/provider/basket/basket_provider.dart';
import 'package:biomart/provider/transaction/transaction_header_provider.dart';
import 'package:biomart/provider/user/user_provider.dart';
import 'package:biomart/ui/common/base/ps_widget_with_appbar_with_no_provider.dart';
import 'package:biomart/ui/common/dialog/error_dialog.dart';
import 'package:biomart/ui/common/dialog/warning_dialog_view.dart';
import 'package:biomart/ui/common/ps_button_widget.dart';
import 'package:biomart/ui/common/ps_textfield_widget.dart';
import 'package:biomart/utils/ps_progress_dialog.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/basket.dart';
import 'package:biomart/viewobject/common/ps_value_holder.dart';
import 'package:biomart/viewobject/holder/intent_holder/checkout_status_intent_holder.dart';
import 'package:biomart/viewobject/transaction_header.dart';
import 'package:flutter/material.dart';
import 'package:flutter_credit_card/flutter_credit_card.dart';
import 'package:flutter_paystack/flutter_paystack.dart';
import 'package:provider/provider.dart';

class PayStackView extends StatefulWidget {
  const PayStackView(
      {Key key,
      @required this.basketList,
      @required this.couponDiscount,
      @required this.psValueHolder,
      @required this.transactionSubmitProvider,
      @required this.userLoginProvider,
      @required this.basketProvider,
      @required this.memoText,
      @required this.paystackKey,
      @required this.isClickPickUpButton,
      @required this.deliveryPickUpDate,
      @required this.deliveryPickUpTime})
      : super(key: key);

  final List<Basket> basketList;
  final String couponDiscount;
  final PsValueHolder psValueHolder;
  final TransactionHeaderProvider transactionSubmitProvider;
  final UserProvider userLoginProvider;
  final BasketProvider basketProvider;
  final String memoText;
  final String paystackKey;
  final bool isClickPickUpButton;
  final String deliveryPickUpDate;
  final String deliveryPickUpTime;

  @override
  State<StatefulWidget> createState() {
    return PayStackViewState();
  }
}

dynamic callTransactionSubmitApi(
  BuildContext context,
  BasketProvider basketProvider,
  UserProvider userLoginProvider,
  TransactionHeaderProvider transactionSubmitProvider,
  List<Basket> basketList,
  String token,
  String couponDiscount,
  String memoText,
  bool isClickPickUpButton,
  String deliveryPickUpDate,
  String deliveryPickUpTime,
) async {
  if (await Utils.checkInternetConnectivity()) {
    if (userLoginProvider.user != null && userLoginProvider.user.data != null) {
      final PsValueHolder valueHolder =
          Provider.of<PsValueHolder>(context, listen: false);
      final PsResource<TransactionHeader> _apiStatus =
          await transactionSubmitProvider.postTransactionSubmit(
              userLoginProvider.user.data,
              basketList,
              Platform.isIOS ? token : token,
              couponDiscount.toString(),
              basketProvider.checkoutCalculationHelper.tax.toString(),
              basketProvider.checkoutCalculationHelper.totalDiscount.toString(),
              basketProvider.checkoutCalculationHelper.subTotalPrice.toString(),
              basketProvider.checkoutCalculationHelper.shippingCost.toString(),
              basketProvider.checkoutCalculationHelper.totalPrice.toString(),
              basketProvider.checkoutCalculationHelper.totalOriginalPrice
                  .toString(),
              PsConst.ZERO,
              PsConst.ZERO,
              PsConst.ZERO,
              PsConst.ZERO,
              PsConst.ZERO,
              PsConst.ZERO,
              '',
              PsConst.ONE,
              isClickPickUpButton == true ? PsConst.ONE : PsConst.ZERO,
              deliveryPickUpDate,
              deliveryPickUpTime,
              basketProvider.checkoutCalculationHelper.shippingCost.toString(),
              userLoginProvider.user.data.area.areaName,
              memoText,
              valueHolder);

      PsProgressDialog.dismissDialog();

      if (_apiStatus.data != null) {
        await basketProvider.deleteWholeBasketList();

        await Navigator.pushNamed(context, RoutePaths.checkoutSuccess,
            arguments: CheckoutStatusIntentHolder(
              transactionHeader: _apiStatus.data,
            ));
        //Navigator.pop(context, true);
      } else {
        PsProgressDialog.dismissDialog();

        return showDialog<dynamic>(
            context: context,
            builder: (BuildContext context) {
              return ErrorDialog(
                message: _apiStatus.message,
              );
            });
      }
    }
  } else {
    showDialog<dynamic>(
        context: context,
        builder: (BuildContext context) {
          return ErrorDialog(
            message: Utils.getString(context, 'error_dialog__no_internet'),
          );
        });
  }
}

PaymentCard callCard(
  String cardNumber,
  String expiryDate,
  String cardHolderName,
  String cvvCode,
) {
  final List<String> monthAndYear = expiryDate.split('/');
  return PaymentCard(
      number: cardNumber,
      expiryMonth: int.parse(monthAndYear[0]),
      expiryYear: int.parse(monthAndYear[1]),
      name: cardHolderName,
      cvc: cvvCode);
}

class PayStackViewState extends State<PayStackView> {
  String cardNumber = '';
  String expiryDate = '';
  String cardHolderName = '';
  String cvvCode = '';
  bool isCvvFocused = false;
  final PaystackPlugin plugin = PaystackPlugin();

  @override
  void initState() {
    plugin.initialize(publicKey: widget.paystackKey);
    super.initState();
  }

  void setError(dynamic error) {
    showDialog<dynamic>(
        context: context,
        builder: (BuildContext context) {
          return ErrorDialog(
            message: Utils.getString(context, error.toString()),
          );
        });
  }

  dynamic callWarningDialog(BuildContext context, String text) {
    showDialog<dynamic>(
        context: context,
        builder: (BuildContext context) {
          return WarningDialog(
            message: Utils.getString(context, text),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    dynamic payStackNow(String token) async {
      widget.basketProvider.checkoutCalculationHelper.calculate(
          basketList: widget.basketList,
          couponDiscountString: widget.couponDiscount,
          psValueHolder: widget.psValueHolder,
          shippingPriceStringFormatting:
              widget.userLoginProvider.user.data.area.price);

      await PsProgressDialog.showDialog(context);
      callTransactionSubmitApi(
          context,
          widget.basketProvider,
          widget.userLoginProvider,
          widget.transactionSubmitProvider,
          widget.basketList,
          token,
          widget.couponDiscount,
          widget.memoText,
          widget.isClickPickUpButton,
          widget.deliveryPickUpDate,
          widget.deliveryPickUpTime);
    }

    return PsWidgetWithAppBarWithNoProvider(
      appBarTitle: Utils.getString(context, 'checkout3__paystack'),
      child: Column(
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
                child: Column(
              children: <Widget>[
                CreditCardWidget(
                  cardNumber: cardNumber,
                  expiryDate: expiryDate,
                  cardHolderName: cardHolderName,
                  cvvCode: cvvCode,
                  showBackView: isCvvFocused,
                  height: 175,
                  width: MediaQuery.of(context).size.width,
                  animationDuration: PsConfig.animation_duration,
                ),
                // PayStackCreditCardWidget(
                //   cardNumber: cardNumber,
                //   expiryDate: expiryDate,
                //   cardHolderName: cardHolderName,
                //   cvvCode: cvvCode,

                // ),
                // PsCreditCardForm(
                //   onCreditCardModelChange: onCreditCardModelChange,
                // ),
                PsCreditCardFormForPayStack(
                  onCreditCardModelChange: onCreditCardModelChange,
                  themeColor: Colors.black,
                ),
                Container(
                    margin: const EdgeInsets.only(
                        left: PsDimens.space12, right: PsDimens.space12),
                    child: PSButtonWidget(
                        hasShadow: true,
                        width: double.infinity,
                        titleText: Utils.getString(context, 'credit_card__pay'),
                        onPressed: () async {
                          if (cardNumber.isEmpty) {
                            callWarningDialog(
                                context,
                                Utils.getString(
                                    context, 'warning_dialog__input_number'));
                          } else if (expiryDate.isEmpty) {
                            callWarningDialog(
                                context,
                                Utils.getString(
                                    context, 'warning_dialog__input_date'));
                          } else if (cvvCode.isEmpty) {
                            callWarningDialog(
                                context,
                                Utils.getString(
                                    context, 'warning_dialog__input_cvv'));
                          } else if (cardHolderName.isEmpty) {
                            callWarningDialog(
                                context,
                                Utils.getString(context,
                                    'warning_dialog__input_holder_name'));
                          } else {
                            //TODO tinkoff logikan


                            

                            // final Charge charge = Charge()
                            //   ..amount = (double.parse(Utils.getPriceTwoDecimal(
                            //               widget
                            //                   .basketProvider
                            //                   .checkoutCalculationHelper
                            //                   .totalPrice
                            //                   .toString())) *
                            //           100)
                            //       .round()
                            //   ..email =
                            //       widget.userLoginProvider.user.data.userEmail
                            //   ..reference = _getReference()
                            //   ..card = callCard(cardNumber, expiryDate,
                            //       cardHolderName, cvvCode);
                            // try {
                            //   final CheckoutResponse response =
                            //       await plugin.checkout(
                            //     context,
                            //     method: CheckoutMethod.card,
                            //     charge: charge,
                            //     fullscreen: false,
                            //     // logo: MyLogo(),
                            //   );
                            //   if (response.status) {
                            //     payStackNow(response.reference);
                            //   }
                            // } catch (e) {
                            //   print('Check console for error');
                            //   rethrow;
                            // }
                          }
                        })),
                const SizedBox(height: PsDimens.space40)
              ],
            )),
          ),
        ],
      ),
    );
  }

  String _getReference() {
    String platform;
    if (Platform.isIOS) {
      platform = 'iOS';
    } else {
      platform = 'Android';
    }

    return 'ChargedFrom${platform}_${DateTime.now().millisecondsSinceEpoch}';
  }

  void onCreditCardModelChange(CreditCardModel creditCardModel) {
    setState(() {
      cardNumber = creditCardModel.cardNumber;
      expiryDate = creditCardModel.expiryDate;
      cardHolderName = creditCardModel.cardHolderName;
      cvvCode = creditCardModel.cvvCode;
      isCvvFocused = creditCardModel.isCvvFocused;
    });
  }
}

class PsCreditCardFormForPayStack extends StatefulWidget {
  const PsCreditCardFormForPayStack({
    Key key,
    this.cardNumber,
    this.expiryDate,
    this.cardHolderName,
    this.cvvCode,
    @required this.onCreditCardModelChange,
    this.themeColor,
    this.textColor,
    this.cursorColor,
  }) : super(key: key);

  final String cardNumber;
  final String expiryDate;
  final String cardHolderName;
  final String cvvCode;
  final void Function(CreditCardModel) onCreditCardModelChange;
  final Color themeColor;
  final Color textColor;
  final Color cursorColor;

  @override
  _PsCreditCardFormState createState() => _PsCreditCardFormState();
}

class _PsCreditCardFormState extends State<PsCreditCardFormForPayStack> {
  String cardNumber;
  String expiryDate;
  String cardHolderName;
  String cvvCode;
  bool isCvvFocused = false;
  Color themeColor;

  void Function(CreditCardModel) onCreditCardModelChange;
  CreditCardModel creditCardModel;

  final MaskedTextController _cardNumberController =
      MaskedTextController(mask: '0000 0000 0000 0000 000');
  final TextEditingController _expiryDateController =
      MaskedTextController(mask: '00/00');
  final TextEditingController _cardHolderNameController =
      TextEditingController();
  final TextEditingController _cvvCodeController =
      MaskedTextController(mask: '000');

  FocusNode cvvFocusNode = FocusNode();

  void textFieldFocusDidChange() {
    creditCardModel.isCvvFocused = cvvFocusNode.hasFocus;
    widget.onCreditCardModelChange(creditCardModel);
  }

  void createCreditCardModel() {
    cardNumber = widget.cardNumber ?? '';
    expiryDate = widget.expiryDate ?? '';
    cardHolderName = widget.cardHolderName ?? '';
    cvvCode = widget.cvvCode ?? '';
    themeColor = widget.themeColor ?? '';

    creditCardModel = CreditCardModel(
        cardNumber, expiryDate, cardHolderName, cvvCode, isCvvFocused);
  }

  @override
  void initState() {
    super.initState();

    createCreditCardModel();

    //onCreditCardModelChange = widget.onCreditCardModelChange;

    cvvFocusNode.addListener(textFieldFocusDidChange);

    _cardNumberController.addListener(() {
      setState(() {
        cardNumber = _cardNumberController.text;
        creditCardModel.cardNumber = cardNumber;
        widget.onCreditCardModelChange(creditCardModel);
      });
    });

    _expiryDateController.addListener(() {
      setState(() {
        expiryDate = _expiryDateController.text;
        creditCardModel.expiryDate = expiryDate;
        widget.onCreditCardModelChange(creditCardModel);
      });
    });

    _cardHolderNameController.addListener(() {
      setState(() {
        cardHolderName = _cardHolderNameController.text;
        creditCardModel.cardHolderName = cardHolderName;
        widget.onCreditCardModelChange(creditCardModel);
      });
    });

    _cvvCodeController.addListener(() {
      setState(() {
        cvvCode = _cvvCodeController.text;
        creditCardModel.cvvCode = cvvCode;
        widget.onCreditCardModelChange(creditCardModel);
      });
    });
  }

  @override
  void didChangeDependencies() {
    themeColor = widget.themeColor ?? Theme.of(context).primaryColor;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        PsTextFieldWidget(
            titleText: 'Card Number',
            textAboutMe: false,
            // hintText: 'xxxx xxxx xxxx xxxx xxx',
            keyboardType: TextInputType.number,
            phoneInputType: true,
            textEditingController: _cardNumberController),
        PsTextFieldWidget(
            titleText: 'Expired Date',
            textAboutMe: false,
            hintText: 'MM/YY',
            keyboardType: TextInputType.number,
            phoneInputType: true,
            textEditingController: _expiryDateController),
        PsTextFieldWidget(
          titleText: 'CVV',
          textAboutMe: false,
          hintText: 'XXX',
          keyboardType: TextInputType.number,
          phoneInputType: true,
          textEditingController: _cvvCodeController,
        ),
        PsTextFieldWidget(
            titleText: 'Card Holder Name',
            textAboutMe: false,
            hintText: 'Card Holder Name',
            keyboardType: TextInputType.text,
            textEditingController: _cardHolderNameController),
      ],
    );
  }
}
