import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/ui/common/ps_ui_widget.dart';
import 'package:biomart/viewobject/blog.dart';
import 'package:flutter/material.dart';

class BlogHorizontalListItem extends StatelessWidget {
  const BlogHorizontalListItem({
    Key key,
    @required this.blog,
    this.onTap,
  }) : super(key: key);

  final Blog blog;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 133.0,
      width: PsDimens.space175,
      margin: const EdgeInsets.symmetric(horizontal: PsDimens.space12),
      color: PsColors.coreBackgroundColor,
      child: GestureDetector(
        onTap: onTap,
        child: BlogHorizontalListItemWidget(blog: blog),
      ),
    );
  }
}

class BlogHorizontalListItemWidget extends StatelessWidget {
  const BlogHorizontalListItemWidget({
    Key key,
    @required this.blog,
  }) : super(key: key);

  final Blog blog;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        ClipRRect(
          borderRadius: BorderRadius.circular(PsDimens.space10),
          child: PsNetworkImage(
            height: 87.0,
            width: PsDimens.space175,
            photoKey: blog.id,
            defaultPhoto: blog.defaultPhoto,
            boxfit: BoxFit.cover,
          ),
        ),
        const SizedBox(
          height: PsDimens.space8,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: Text(
            blog.name,
            maxLines: 2,
            style: Theme.of(context).textTheme.headline6.copyWith(
                  fontWeight: FontWeight.w500,
                  fontSize: 16.0,
                ),
          ),
        ),
        // Padding(
        //   padding: const EdgeInsets.only(
        //       top: PsDimens.space4,
        //       bottom: PsDimens.space12,
        //       left: PsDimens.space8,
        //       right: PsDimens.space8),
        //   child: Text(
        //     blog.description,
        //     maxLines: 4,
        //     style: Theme.of(context).textTheme.bodyText1.copyWith(height: 1.4),
        //   ),
        // ),
      ],
    );
  }
}
