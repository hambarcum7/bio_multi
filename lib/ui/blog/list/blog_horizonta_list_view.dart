import 'package:biomart/api/common/ps_status.dart';
import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/constant/route_paths.dart';
import 'package:biomart/provider/blog/blog_provider.dart';
import 'package:biomart/repository/blog_repository.dart';
import 'package:biomart/ui/blog/item/blog_horizontal_list_item.dart';
import 'package:biomart/ui/common/ps_frame_loading_widget.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/holder/intent_holder/blog_intent_holder.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class BlogHorizontalListView extends StatefulWidget {
  const BlogHorizontalListView({
    Key key,
    // @required this.animationController,
    // this.animation,
  }) : super(key: key);
  // final AnimationController animationController;
  // final Animation<double> animation;

  @override
  _BlogHorizontalListViewState createState() => _BlogHorizontalListViewState();
}

class _BlogHorizontalListViewState extends State<BlogHorizontalListView>
    with TickerProviderStateMixin {
  final ScrollController _scrollController = ScrollController();

  BlogProvider _blogProvider;
  Animation<double> animation;

  @override
  void dispose() {
    animation = null;
    super.dispose();
  }

  @override
  void initState() {
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _blogProvider.nextBlogList();
      }
    });

    super.initState();
  }

  bool isConnectedToInternet = false;
  bool isSuccessfullyLoaded = true;

  void checkConnection() {
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
      if (isConnectedToInternet && PsConfig.showAdMob) {
        setState(() {});
      }
    });
  }

  BlogRepository repo1;
  dynamic data;
  @override
  Widget build(BuildContext context) {
    repo1 = Provider.of<BlogRepository>(context);

    if (!isConnectedToInternet && PsConfig.showAdMob) {
      print('loading ads....');
      checkConnection();
    }

    print(
        '............................Build UI Again ............................');
    return SliverToBoxAdapter(
      child: ChangeNotifierProvider<BlogProvider>(
        lazy: false,
        create: (BuildContext context) {
          final BlogProvider provider = BlogProvider(repo: repo1);
          provider.loadBlogList();
          _blogProvider = provider;
          return _blogProvider;
        },
        child: Consumer<BlogProvider>(
          builder: (BuildContext context, BlogProvider provider, Widget child) {
            return (provider.blogList.data != null ||
                    provider.blogList.data.isNotEmpty)
                ? Column(
                    children: <Widget>[
                      _HeaderWidget(
                        headerName: Utils.getString(
                            context, 'home__menu_drawer_blog'),
                        viewAllClicked: () {
                          Navigator.pushNamed(
                            context,
                            RoutePaths.blogList,
                            arguments: const BlogIntentHolder(
                              noBlogListForShop: true,
                            ),
                          );
                        },
                      ),
                      Container(
                        height: 135,
                        //color: PsColors.textPrimaryColor,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: provider.blogList.data.length,
                          itemBuilder: (BuildContext context, int index) {
                            if (provider.blogList.status ==
                                PsStatus.BLOCK_LOADING) {
                              return Shimmer.fromColors(
                                baseColor: PsColors.grey,
                                highlightColor: PsColors.white,
                                child: Row(
                                  children: const <Widget>[
                                    PsFrameUIForLoading(),
                                  ],
                                ),
                              );
                            } else {
                              return BlogHorizontalListItem(
                                  blog: provider.blogList.data[index],
                                  onTap: () {
                                    print(provider.blogList.data[index]
                                        .defaultPhoto.imgPath);
                                    Navigator.pushNamed(
                                        context, RoutePaths.blogDetail,
                                        arguments:
                                            provider.blogList.data[index]);
                                  });
                            }
                          },
                        ),
                      ),
                    ],
                  )
                : Container(
                    color: Colors.red,
                  );
          },
        ),
      ),
    );
  }
}

class _HeaderWidget extends StatefulWidget {
  const _HeaderWidget({
    Key key,
    @required this.headerName,
    @required this.viewAllClicked,
  }) : super(key: key);

  final String headerName;
  final Function viewAllClicked;

  @override
  __HeaderWidgetState createState() => __HeaderWidgetState();
}

class __HeaderWidgetState extends State<_HeaderWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.viewAllClicked,
      child: Padding(
        padding: const EdgeInsets.only(
            top: PsDimens.space28,
            left: PsDimens.space20,
            right: PsDimens.space16,
            bottom: PsDimens.space16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Expanded(
              child: Text(widget.headerName,
                  style: Theme.of(context).textTheme.headline5.copyWith(
                        color: PsColors.textPrimaryDarkColor,
                        fontSize: 18.0,
                        fontWeight: FontWeight.w500,
                      )),
            ),
            Icon(
              Icons.arrow_forward_ios,
              size: 17.0,
              color: PsColors.textPrimaryColor,
            ),
            // Text(
            //   Utils.getString(context, 'dashboard__view_all'),
            //   textAlign: TextAlign.start,
            //   style: Theme.of(context)
            //       .textTheme
            //       .caption
            //       .copyWith(color: PsColors.mainColor),
            // ),
          ],
        ),
      ),
    );
  }
}
