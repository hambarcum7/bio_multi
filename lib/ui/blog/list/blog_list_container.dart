import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/ui/blog/list/blog_list_view.dart';
import 'package:biomart/ui/blog/list/blog_list_view_shop.dart';
import 'package:biomart/ui/dashboard/view_pages/call_bottom_view.dart';
import 'package:biomart/utils/app_bar_widget.dart';
import 'package:biomart/utils/utils.dart';
import 'package:flutter/material.dart';

class BlogListContainerView extends StatefulWidget {
  const BlogListContainerView({@required this.noBlogListForShop, this.shopId});

  final bool noBlogListForShop;
  final String shopId;

  @override
  _BlogListContainerViewState createState() => _BlogListContainerViewState();
}

class _BlogListContainerViewState extends State<BlogListContainerView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print(
        '............................Build UI Again ............................');
    return Scaffold(
        bottomNavigationBar: const BottomBarViewWidget(
          currentIdex: 0,
        ),
        backgroundColor: PsColors.coreBackgroundColor,
        appBar: customAppBar(
          title: Utils.getString(context, 'blog_list__app_bar_name'),
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(
                Icons.arrow_back_ios,
                size: 17.0,
                color: PsColors.textPrimaryColor,
              ),
            ),
          ),
        ),
        // appBar: AppBar(

        //   systemOverlayStyle: SystemUiOverlayStyle(
        //       statusBarIconBrightness: Utils.getBrightnessForAppBar(context)),
        //   iconTheme: Theme.of(context)
        //       .iconTheme
        //       .copyWith(color: PsColors.mainColorWithWhite),
        //   title: Text(Utils.getString(context, 'blog_list__app_bar_name'),
        //       textAlign: TextAlign.center,
        //       style: Theme.of(context)
        //           .textTheme
        //           .headline6
        //           .copyWith(fontWeight: FontWeight.bold)
        //           .copyWith(color: PsColors.mainColorWithWhite)),
        //   elevation: 0,
        // ),
        body: widget.noBlogListForShop
            ? Container(
                color: PsColors.coreBackgroundColor,
                height: double.infinity,
                child: BlogListView(
                  animationController: animationController,
                ),
              )
            : Container(
                color: PsColors.coreBackgroundColor,
                height: double.infinity,
                child: BlogListViewShop(
                  animationController: animationController,
                  shopId: widget.shopId,
                ),
              ));
  }
}
