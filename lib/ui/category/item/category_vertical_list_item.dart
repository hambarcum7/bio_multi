import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/ui/common/ps_ui_widget.dart';
import 'package:biomart/viewobject/category.dart';
import 'package:flutter/material.dart';

class CategoryVerticalListItem extends StatelessWidget {
  const CategoryVerticalListItem({
    Key key,
    @required this.category,
    this.onTap,
    this.animationController,
    this.animation,
  }) : super(key: key);

  final Category category;

  final Function onTap;
  final AnimationController animationController;
  final Animation<double> animation;

  @override
  Widget build(BuildContext context) {
    animationController.forward();
    return AnimatedBuilder(
      animation: animationController,
      child: Card(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(14.0)),
        color: PsColors.ps_category_color,
        child: GestureDetector(
          onTap: onTap,
          child: Column(
            children: <Widget>[
              Expanded(
                child: ClipRRect(
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(14.0),
                    topRight: Radius.circular(14.0),
                  ),
                  child: Container(
                    child: PsNetworkImage(
                      photoKey: '',
                      defaultPhoto: category.defaultPhoto,
                      width: PsDimens.space200,
                      height: double.infinity,
                      boxfit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 5),
                //color: PsColors.white,
                child: Text(
                  category.name,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: PsColors.black,
                    fontWeight: FontWeight.w500,
                    fontSize: 18.0,
                    overflow: TextOverflow.visible,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
          opacity: animation,
          child: Transform(
            transform: Matrix4.translationValues(
                0.0, 100 * (1.0 - animation.value), 0.0),
            child: child,
          ),
        );
      },
    );
  }
}

//  Align(
//                 alignment: Alignment.bottomCenter,
//                 child: Container(
//                   margin: const EdgeInsets.only(
//                     bottom: PsDimens.space8,
//                   ),
//                   child: Text(
//                     category.name,
//                     textAlign: TextAlign.center,
//                     style: TextStyle(
//                       color: PsColors.black,
//                       fontWeight: FontWeight.w500,
//                       fontSize: 18.0,
//                       overflow: TextOverflow.visible,
//                     ),
//                   ),
//                 ),
//               ),