import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/provider/basket/basket_provider.dart';
import 'package:biomart/repository/basket_repository.dart';
import 'package:biomart/ui/dashboard/view_pages/call_bottom_view.dart';
import 'package:biomart/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'category_list_view.dart';

class CategoryListViewContainerView extends StatefulWidget {
  const CategoryListViewContainerView({this.appBarTitle});

  final String appBarTitle;
  @override
  _CategoryListWithFilterContainerViewState createState() =>
      _CategoryListWithFilterContainerViewState();
}

class _CategoryListWithFilterContainerViewState
    extends State<CategoryListViewContainerView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  BasketRepository basketRepository;

  @override
  Widget build(BuildContext context) {
    basketRepository = Provider.of<BasketRepository>(context);

    print(
        '............................Build UI Again ............................');
    return Scaffold(
      bottomNavigationBar: const BottomBarViewWidget(
        currentIdex: 0,
      ),
      backgroundColor: PsColors.coreBackgroundColor,
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle(
            statusBarIconBrightness: Utils.getBrightnessForAppBar(context)),
        automaticallyImplyLeading: false,
        leadingWidth: (24 + 8).toDouble(),
        leading: Hero(
          tag: 'catgories',
          child: Container(
            margin: const EdgeInsets.only(left: 8.0),
            child: const ImageIcon(
              AssetImage(
                'assets/home_images/categories_white.png',
              ),
            ),
          ),
        ),
        iconTheme: Theme.of(context)
            .iconTheme
            .copyWith(color: PsColors.textPrimaryColor),
        title: Text(
          widget.appBarTitle,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: PsColors.textPrimaryColor,
            fontSize: PsDimens.space24,
            fontWeight: FontWeight.w400,
          ),
        ),
        elevation: 0,
        actions: <Widget>[
          ChangeNotifierProvider<BasketProvider>(
            lazy: false,
            create: (BuildContext context) {
              final BasketProvider provider =
                  BasketProvider(repo: basketRepository);
              // provider.loadBasketList();
              return provider;
            },
            child: Consumer<BasketProvider>(
              builder: (
                BuildContext context,
                BasketProvider basketProvider,
                Widget child,
              ) {
                return InkWell(
                  child: Container(
                    width: PsDimens.space40,
                    height: PsDimens.space40,
                    margin: const EdgeInsets.only(
                      top: PsDimens.space8,
                      left: PsDimens.space8,
                      right: PsDimens.space8,
                    ),
                    child: Hero(
                      tag: 'search',
                      child: Align(
                        alignment: Alignment.center,
                        child: Icon(
                          Icons.close,
                          color: PsColors.textPrimaryColor,
                        ),
                      ),
                    ),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                );
              },
            ),
          )
        ],
      ),
      body: const CategoryListView(),
    );
  }
}
