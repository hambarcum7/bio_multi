import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_constants.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/constant/route_paths.dart';
import 'package:biomart/provider/category/category_provider.dart';
import 'package:biomart/repository/category_repository.dart';
import 'package:biomart/ui/category/item/category_vertical_list_item.dart';
import 'package:biomart/ui/common/ps_ui_widget.dart';
import 'package:biomart/ui/product/list_with_filter/product_list_with_filter_container.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/common/ps_value_holder.dart';
import 'package:biomart/viewobject/holder/category_parameter_holder.dart';
import 'package:biomart/viewobject/holder/product_parameter_holder.dart';
import 'package:biomart/viewobject/holder/touch_count_parameter_holder.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CategoryListView extends StatefulWidget {
  const CategoryListView({
    Key key,
  }) : super(key: key);

  @override
  _CategoryListViewState createState() {
    return _CategoryListViewState();
  }
}

class _CategoryListViewState extends State<CategoryListView>
    with TickerProviderStateMixin {
  final ScrollController _scrollController = ScrollController();

  CategoryProvider _categoryProvider;
  final CategoryParameterHolder categoryParameterHolder =
      CategoryParameterHolder().getLatestParameterHolder();

  AnimationController animationController;
  Animation<double> animation;

  @override
  void dispose() {
    animationController.dispose();
    animation = null;
    super.dispose();
  }

  @override
  void initState() {
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _categoryProvider.nextCategoryList(categoryParameterHolder);
      }
    });

    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);

    super.initState();
  }

  CategoryRepository repo1;
  PsValueHolder psValueHolder;
  dynamic data;

  bool isConnectedToInternet = false;
  bool isSuccessfullyLoaded = true;

  void checkConnection() {
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
      if (isConnectedToInternet && PsConfig.showAdMob) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!isConnectedToInternet && PsConfig.showAdMob) {
      print('loading ads....');
      checkConnection();
    }

    repo1 = Provider.of<CategoryRepository>(context);
    psValueHolder = Provider.of<PsValueHolder>(context);
    print(
        '............................Build UI Again ............................');
    return ChangeNotifierProvider<CategoryProvider>(
      lazy: false,
      create: (BuildContext context) {
        final CategoryProvider provider =
            CategoryProvider(repo: repo1, psValueHolder: psValueHolder);
        provider.loadCategoryList(categoryParameterHolder);
        _categoryProvider = provider;
        return _categoryProvider;
      },
      child: Consumer<CategoryProvider>(
        builder:
            (BuildContext context, CategoryProvider provider, Widget child) {
          return Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(
                        left: PsDimens.space8,
                        right: PsDimens.space8,
                        top: PsDimens.space8,
                        bottom: PsDimens.space8,
                      ),
                      child: RefreshIndicator(
                        child: CustomScrollView(
                          controller: _scrollController,
                          physics: const AlwaysScrollableScrollPhysics(),
                          scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          slivers: <Widget>[
                            SliverGrid(
                              gridDelegate:
                                  const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 3,
                                mainAxisExtent: 190,
                                mainAxisSpacing: 10.0,
                              ),
                              delegate: SliverChildBuilderDelegate(
                                (BuildContext context, int index) {
                                  if (provider.categoryList.data != null ||
                                      provider.categoryList.data.isNotEmpty) {
                                    final int count =
                                        provider.categoryList.data.length;
                                    return CategoryVerticalListItem(
                                        animationController:
                                            animationController,
                                        animation:
                                            Tween<double>(begin: 0.0, end: 1.0)
                                                .animate(
                                          CurvedAnimation(
                                            parent: animationController,
                                            curve: Interval(
                                                (1 / count) * index, 1.0,
                                                curve: Curves.fastOutSlowIn),
                                          ),
                                        ),
                                        category:
                                            provider.categoryList.data[index],
                                        onTap: () {
                                          if (PsConfig.isShowSubCategory) {
                                            Navigator.pushNamed(
                                              context,
                                              RoutePaths.subCategoryGrid,
                                              arguments: provider
                                                  .categoryList.data[index],
                                            );
                                          } else {
                                            final String loginUserId =
                                                Utils.checkUserLoginId(
                                                    psValueHolder);
                                            final TouchCountParameterHolder
                                                touchCountParameterHolder =
                                                TouchCountParameterHolder(
                                                    typeId: provider
                                                        .categoryList
                                                        .data[index]
                                                        .id,
                                                    typeName: PsConst
                                                        .FILTERING_TYPE_NAME_CATEGORY,
                                                    userId: loginUserId,
                                                    shopId: '');

                                            provider.postTouchCount(
                                                touchCountParameterHolder
                                                    .toMap());

                                            final ProductParameterHolder
                                                productParameterHolder =
                                                ProductParameterHolder()
                                                    .getLatestParameterHolder();
                                            productParameterHolder.catId =
                                                provider.categoryList
                                                    .data[index].id;
                                            // Navigator.pushNamed(
                                            //   context,
                                            //   RoutePaths.filterProductList,
                                            //   arguments:
                                            //      <dynamic>[ ProductListIntentHolder(
                                            //     appBarTitle: provider
                                            //         .categoryList
                                            //         .data[index]
                                            //         .name,
                                            //     productParameterHolder:
                                            //         productParameterHolder,
                                            //   ),

                                            //   ]
                                            // );

                                            Navigator.push(
                                        context,
                                        PageRouteBuilder<dynamic>(
                                          pageBuilder: (_, Animation<double> a1,
                                                  Animation<double> a2) =>
                                              ProductListWithFilterContainerView(
                                            productParameterHolder:
                                                productParameterHolder,
                                            appBarTitle: provider
                                                    .categoryList
                                                    .data[index]
                                                    .name,
                                            category: provider
                                                    .categoryList.data[index],
                                          ),
                                        ),
                                      );
                                          }
                                        });
                                  } else {
                                    return null;
                                  }
                                },
                                childCount: provider.categoryList.data.length,
                              ),
                            ),
                          ],
                        ),
                        onRefresh: () {
                          return provider
                              .resetCategoryList(categoryParameterHolder);
                        },
                      ),
                    ),
                  ),
                ],
              ),
              PSProgressIndicator(provider.categoryList.status)
            ],
          );
        },
      ),
    );
  }
}
