import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/ui/collection/header_list/collection_header_list_view.dart';
import 'package:biomart/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CollectionHeaderListContainerView extends StatefulWidget {
  @override
  _CollectionHeaderListContainerViewState createState() =>
      _CollectionHeaderListContainerViewState();
}

class _CollectionHeaderListContainerViewState
    extends State<CollectionHeaderListContainerView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
  
    print(
        '............................Build UI Again ............................');
    return Scaffold(
      backgroundColor: PsColors.coreBackgroundColor,
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle(
            statusBarIconBrightness: Utils.getBrightnessForAppBar(context)),
        iconTheme: Theme.of(context)
            .iconTheme
            .copyWith(color: PsColors.mainColorWithWhite),
        title: Text(
          Utils.getString(context, 'collection_header__app_bar_name'),
          textAlign: TextAlign.center,
          style: Theme.of(context)
              .textTheme
              .headline6
              .copyWith(fontWeight: FontWeight.bold)
              .copyWith(color: PsColors.mainColorWithWhite),
        ),
        elevation: 0,
      ),
      body: CollectionHeaderListView(
        animationController: animationController,
        shopId: '',
      ),
    );
  }
}
