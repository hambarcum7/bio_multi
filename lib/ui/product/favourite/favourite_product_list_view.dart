import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_constants.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/constant/route_paths.dart';
import 'package:biomart/provider/product/favourite_product_provider.dart';
import 'package:biomart/provider/product/product_provider.dart';
import 'package:biomart/repository/product_repository.dart';
import 'package:biomart/ui/common/ps_admob_banner_widget.dart';
import 'package:biomart/ui/common/ps_button_widget.dart';
import 'package:biomart/ui/common/ps_ui_widget.dart';
import 'package:biomart/ui/product/item/product_vertical_list_item_for_home.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/common/ps_value_holder.dart';
import 'package:biomart/viewobject/holder/intent_holder/product_detail_intent_holder.dart';
import 'package:biomart/viewobject/product.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FavouriteProductListView extends StatefulWidget {
  const FavouriteProductListView({
    Key key,
    @required this.animationController,
    this.productDetailProvider,
  }) : super(key: key);
  final AnimationController animationController;
  final ProductDetailProvider productDetailProvider;
  @override
  _FavouriteProductListView createState() => _FavouriteProductListView();
}

class _FavouriteProductListView extends State<FavouriteProductListView>
    with TickerProviderStateMixin {
  final ScrollController _scrollController = ScrollController();

  FavouriteProductProvider _favouriteProductProvider;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _favouriteProductProvider.nextFavouriteProductList();
      }
    });

    super.initState();
  }

  ProductRepository repo1;
  PsValueHolder psValueHolder;
  dynamic data;
  bool isConnectedToInternet = false;
  bool isSuccessfullyLoaded = true;

  void checkConnection() {
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
      if (isConnectedToInternet && PsConfig.showAdMob) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    repo1 = Provider.of<ProductRepository>(context);
    psValueHolder = Provider.of<PsValueHolder>(context);

    if (!isConnectedToInternet && PsConfig.showAdMob) {
      print('loading ads....');
      checkConnection();
    }
    print(
        '............................Build UI Again ............................');
    return ChangeNotifierProvider<FavouriteProductProvider>(
      lazy: false,
      create: (BuildContext context) {
        final FavouriteProductProvider provider =
            FavouriteProductProvider(repo: repo1, psValueHolder: psValueHolder);
        provider.loadFavouriteProductList();
        _favouriteProductProvider = provider;
        return _favouriteProductProvider;
      },
      child: Consumer<FavouriteProductProvider>(
        builder: (BuildContext context, FavouriteProductProvider provider,
            Widget child) {
          if (provider.favouriteProductList != null &&
              provider.favouriteProductList.data != null &&
              provider.favouriteProductList.data.isNotEmpty) {
            return Column(
              children: <Widget>[
                const PsAdMobBannerWidget(),
                Expanded(
                  child: Stack(
                    children: <Widget>[
                      Container(
                        color: PsColors.coreBackgroundColor,
                        height: MediaQuery.of(context).size.height,
                        child: RefreshIndicator(
                          child: CustomScrollView(
                            controller: _scrollController,
                            physics: const AlwaysScrollableScrollPhysics(),
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            slivers: <Widget>[
                              SliverGrid(
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  // maxCrossAxisExtent: 220,
                                  crossAxisCount:
                                      MediaQuery.of(context).size.width ~/ 160,
                                  crossAxisSpacing: 8,
                                  childAspectRatio: 1 / 1.85,
                                  mainAxisSpacing: 0.0,
                                ),
                                delegate: SliverChildBuilderDelegate(
                                  (BuildContext context, int index) {
                                    if (provider.favouriteProductList.data !=
                                            null ||
                                        provider.favouriteProductList.data
                                            .isNotEmpty) {
                                      // final int count = provider
                                      //     .favouriteProductList.data.length;
                                      final Product product = provider
                                          .favouriteProductList.data[index];
                                      return ProductVerticalListItem(
                                        coreTagKey:
                                            provider.hashCode.toString() +
                                                product.id,
                                        product: provider
                                            .favouriteProductList.data[index],
                                        productId: product.id,
                                        onHeartTap: () {
                                          setState(() {
                                            print('Rebuild favorites');
                                          });
                                        },
                                        onTap: () {
                                          final ProductDetailIntentHolder
                                              holder =
                                              ProductDetailIntentHolder(
                                            productId: product.id,
                                            heroTagImage:
                                                provider.hashCode.toString() +
                                                    product.id +
                                                    PsConst.HERO_TAG__IMAGE,
                                            heroTagTitle:
                                                provider.hashCode.toString() +
                                                    product.id +
                                                    PsConst.HERO_TAG__TITLE,
                                            heroTagOriginalPrice: provider
                                                    .hashCode
                                                    .toString() +
                                                product.id +
                                                PsConst
                                                    .HERO_TAG__ORIGINAL_PRICE,
                                            heroTagUnitPrice: provider.hashCode
                                                    .toString() +
                                                product.id +
                                                PsConst.HERO_TAG__UNIT_PRICE,
                                          );
                                          Navigator.pushNamed(
                                              context, RoutePaths.productDetail,
                                              arguments: holder);
                                        },
                                      );
                                    } else {
                                      return null;
                                    }
                                  },
                                  childCount:
                                      provider.favouriteProductList.data.length,
                                ),
                              ),
                            ],
                          ),
                          onRefresh: () {
                            return provider.resetFavouriteProductList();
                          },
                        ),
                      ),
                      PSProgressIndicator(provider.favouriteProductList.status)
                    ],
                  ),
                )
              ],
            );
          } else {
            widget.animationController.forward();
            final Animation<double> animation =
                Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
                    parent: widget.animationController,
                    curve: const Interval(0.5 * 1, 1.0,
                        curve: Curves.fastOutSlowIn)));
            return AnimatedBuilder(
              animation: widget.animationController,
              child: Container(
                height: MediaQuery.of(context).size.height,
                color: PsColors.coreBackgroundColor,
                child: SingleChildScrollView(
                  child: Column(
                    //mainAxisAlignment: MainAxisAlignment.center,
                    //crossAxisAlignment: CrossAxisAlignment.center,
                    //mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 18.0,
                          vertical: 21.0,
                        ),
                        child: Image.asset(
                          'assets/images/empty_fav.png',
                        ),
                      ),
                      const SizedBox(
                        height: PsDimens.space32,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 27.5),
                        child: FittedBox(
                          child: Text(
                            Utils.getString(context, 'fav_list__empty_title'),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: PsColors.textPrimaryColor,
                                fontSize: PsDimens.space24,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: PsDimens.space28,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                            // style: Theme.of(context).textTheme.body1,
                            children: <InlineSpan>[
                              TextSpan(
                                text: Utils.getString(context, 'click'),
                                style: TextStyle(
                                  fontSize: PsDimens.space18,
                                  color: PsColors.textPrimaryColor,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                              WidgetSpan(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 12.0,
                                  ),
                                  child: Container(
                                    width: 21.0,
                                    height: 21.0,
                                    child: const Icon(
                                      Icons.favorite_border,
                                      color: Color(0xFFEF473F),
                                    ),
                                  ),
                                ),
                              ),
                              TextSpan(
                                text: Utils.getString(
                                    context, 'fav_list__empty_desc'),
                                style: TextStyle(
                                  fontSize: PsDimens.space18,
                                  color: PsColors.textPrimaryColor,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: PsDimens.space32,
                      ),
                      Container(
                        margin: const EdgeInsets.only(
                          left: PsDimens.space12,
                          right: PsDimens.space12,
                          bottom: PsDimens.space20,
                        ),
                        child: PSButtonWidget(
                          hasShadow: false,
                          colorData: PsColors.grey,
                          width: double.infinity,
                          titleText: Utils.getString(context, 'i_ll_go_press'),
                          onPressed: () {
                            Navigator.pushNamed(
                              context,
                              RoutePaths.home,
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              builder: (BuildContext context, Widget child) {
                return FadeTransition(
                    opacity: animation,
                    child: Transform(
                      transform: Matrix4.translationValues(
                          0.0, 100 * (1.0 - animation.value), 0.0),
                      child: child,
                    ));
              },
            );
          }
        },
      ),
    );
  }
}
