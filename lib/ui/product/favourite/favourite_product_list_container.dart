import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/ui/dashboard/view_pages/call_bottom_view.dart';
import 'package:biomart/utils/app_bar_widget.dart';
import 'package:biomart/utils/utils.dart';
import 'package:flutter/material.dart';

import 'favourite_product_list_view.dart';

class FavouriteProductListContainerView extends StatefulWidget {
  @override
  _FavouriteProductListContainerViewState createState() =>
      _FavouriteProductListContainerViewState();
}

class _FavouriteProductListContainerViewState
    extends State<FavouriteProductListContainerView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    print(
        '............................Build UI Again ............................');
    return Scaffold(
      bottomNavigationBar: const BottomBarViewWidget(
        currentIdex: 2,
      ),
      backgroundColor: PsColors.coreBackgroundColor,
      appBar: customAppBar(
        title: Utils.getString(context, 'home__menu_drawer_favourite'),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back_ios,
              size: 17.0,
              color: PsColors.textPrimaryColor,
            ),
          ),
        ),
      ),
      // appBar: AppBar(
      //   systemOverlayStyle: SystemUiOverlayStyle(
      //       statusBarIconBrightness: Utils.getBrightnessForAppBar(context)),
      //   iconTheme: Theme.of(context)
      //       .iconTheme
      //       .copyWith(color: PsColors.mainColorWithWhite),
      //   title: Text(
      //     Utils.getString(context, 'home__menu_drawer_favourite'),
      //     textAlign: TextAlign.center,
      //     style: Theme.of(context).textTheme.headline6.copyWith(
      //         fontWeight: FontWeight.bold,
      //         color: PsColors.mainColorWithWhite),
      //   ),
      //   elevation: 0,
      // ),
      body: FavouriteProductListView(
        animationController: animationController,
      ),
    );
  }
}
