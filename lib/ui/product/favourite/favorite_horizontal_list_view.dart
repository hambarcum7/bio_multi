import 'package:biomart/api/common/ps_status.dart';
import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/constant/ps_constants.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/constant/route_paths.dart';
import 'package:biomart/provider/product/favourite_product_provider.dart';
import 'package:biomart/repository/product_repository.dart';
import 'package:biomart/ui/common/ps_frame_loading_widget.dart';
import 'package:biomart/ui/product/item/product_horizontal_list_item.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/common/ps_value_holder.dart';
import 'package:biomart/viewobject/holder/intent_holder/product_detail_intent_holder.dart';
import 'package:biomart/viewobject/product.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class FavoriteHorizontalListView extends StatefulWidget {
  const FavoriteHorizontalListView({
    Key key,
    @required this.animationController,
    @required this.animation,
  }) : super(key: key);

  final AnimationController animationController;
  final Animation<double> animation;

  @override
  State<FavoriteHorizontalListView> createState() =>
      FavoriteHorizontalListViewState();
}

class FavoriteHorizontalListViewState
    extends State<FavoriteHorizontalListView> {
  ProductRepository repo1;
  PsValueHolder psValueHolder;
  FavouriteProductProvider _favouriteProductProvider;
  @override
  Widget build(BuildContext context) {
    repo1 = Provider.of<ProductRepository>(context);
    psValueHolder = Provider.of<PsValueHolder>(context);

    return ChangeNotifierProvider<FavouriteProductProvider>(
      lazy: false,
      create: (BuildContext context) {
        final FavouriteProductProvider provider =
            FavouriteProductProvider(repo: repo1, psValueHolder: psValueHolder);
        provider.loadFavouriteProductList();
        _favouriteProductProvider = provider;
        return _favouriteProductProvider;
      },
      child: SliverToBoxAdapter(
        child: Consumer<FavouriteProductProvider>(
          builder: (BuildContext context,
              FavouriteProductProvider favoriteProvider, Widget child) {
            return AnimatedBuilder(
              animation: widget.animationController,
              child: (favoriteProvider.favouriteProductList.data != null &&
                      favoriteProvider.favouriteProductList.data.isNotEmpty)
                  ? Column(
                      children: <Widget>[
                        _MyHeaderWidget(
                          headerName: Utils.getString(
                            context,
                            'profile__favourite',
                          ),
                          viewAllClicked: () {
                            Navigator.pushNamed(
                                context, RoutePaths.favouriteProductList);
                          },
                        ),
                        Container(
                          height: 300,
                          //width: MediaQuery.of(context).size.width,
                          //padding: const EdgeInsets.only(left: PsDimens.space8),
                          color: PsColors.coreBackgroundColor,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: favoriteProvider
                                .favouriteProductList.data.length,
                            itemBuilder: (BuildContext context, int index) {
                              if (favoriteProvider
                                      .favouriteProductList.status ==
                                  PsStatus.BLOCK_LOADING) {
                                return Shimmer.fromColors(
                                  baseColor: PsColors.grey,
                                  highlightColor: PsColors.white,
                                  child: Row(
                                    children: const <Widget>[
                                      PsFrameUIForLoading(),
                                    ],
                                  ),
                                );
                              } else {
                                final Product product = favoriteProvider
                                    .favouriteProductList.data[index];
                                return ProductHorizontalListItem(
                                  coreTagKey:
                                      favoriteProvider.hashCode.toString() +
                                          product.id,
                                  product: favoriteProvider
                                      .favouriteProductList.data[index],
                                  productId: product.id,
                                  onTap: () {
                                    final ProductDetailIntentHolder holder =
                                        ProductDetailIntentHolder(
                                      productId: product.id,
                                      heroTagImage:
                                          favoriteProvider.hashCode.toString() +
                                              product.id +
                                              PsConst.HERO_TAG__IMAGE,
                                      heroTagTitle:
                                          favoriteProvider.hashCode.toString() +
                                              product.id +
                                              PsConst.HERO_TAG__TITLE,
                                      heroTagOriginalPrice:
                                          favoriteProvider.hashCode.toString() +
                                              product.id +
                                              PsConst.HERO_TAG__ORIGINAL_PRICE,
                                      heroTagUnitPrice:
                                          favoriteProvider.hashCode.toString() +
                                              product.id +
                                              PsConst.HERO_TAG__UNIT_PRICE,
                                    );
                                    Navigator.pushNamed(
                                      context,
                                      RoutePaths.productDetail,
                                      arguments: holder,
                                    );
                                  },
                                );
                              }
                            },
                          ),
                        ),
                      ],
                    )
                  : Container(
                      color: PsColors.coreBackgroundColor,
                    ),
              builder: (BuildContext context, Widget child) {
                return FadeTransition(
                  opacity: widget.animation,
                  child: Transform(
                    transform: Matrix4.translationValues(
                        0.0, 100 * (1.0 - widget.animation.value), 0.0),
                    child: child,
                  ),
                );
              },
            );
          },
        ),
      ),
    );
  }
}

class _MyHeaderWidget extends StatefulWidget {
  const _MyHeaderWidget({
    Key key,
    @required this.headerName,
    @required this.viewAllClicked,
  }) : super(key: key);

  final String headerName;
  final Function viewAllClicked;

  @override
  __MyHeaderWidgetState createState() => __MyHeaderWidgetState();
}

class __MyHeaderWidgetState extends State<_MyHeaderWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.viewAllClicked,
      child: Padding(
        padding: const EdgeInsets.only(
            top: PsDimens.space28,
            left: PsDimens.space20,
            right: PsDimens.space16,
            bottom: PsDimens.space16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Expanded(
              child: Text(
                widget.headerName,
                style: Theme.of(context).textTheme.headline5.copyWith(
                    color: PsColors.textPrimaryDarkColor,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w500),
              ),
            ),
            Icon(
              Icons.arrow_forward_ios,
              size: 17.0,
              color: PsColors.textPrimaryColor,
            ),
          ],
        ),
      ),
    );
  }
}
