import 'package:biomart/api/common/ps_resource.dart';
import 'package:biomart/api/common/ps_status.dart';
import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/constant/ps_constants.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/provider/basket/basket_provider.dart';
import 'package:biomart/provider/product/favourite_product_provider.dart';
import 'package:biomart/provider/product/product_provider.dart';
import 'package:biomart/repository/basket_repository.dart';
import 'package:biomart/repository/product_repository.dart';
import 'package:biomart/ui/common/dialog/confirm_dialog_view.dart';
import 'package:biomart/ui/common/dialog/error_dialog.dart';
import 'package:biomart/ui/common/dialog/warning_dialog_view.dart';
import 'package:biomart/ui/common/ps_hero.dart';
import 'package:biomart/ui/common/ps_ui_widget.dart';
import 'package:biomart/ui/product/detail/product_detail_view.dart';
//import 'package:biomart/ui/product/detail/product_detail_view.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/basket.dart';
import 'package:biomart/viewobject/basket_selected_add_on.dart';
import 'package:biomart/viewobject/basket_selected_attribute.dart';
import 'package:biomart/viewobject/common/ps_value_holder.dart';
import 'package:biomart/viewobject/holder/favourite_parameter_holder.dart';
import 'package:biomart/viewobject/product.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

class ProductHorizontalListItem extends StatefulWidget {
  const ProductHorizontalListItem({
    Key key,
    @required this.product,
    @required this.productId,
    @required this.coreTagKey,
    this.heroTagTitle,
    this.onTap,
    this.intentBasketPrice, //TODO petqa erevi poxancvi
  }) : super(key: key);

  final Product product;
  final Function onTap;
  final String coreTagKey;
  final String productId;
  final String heroTagTitle;
  final String intentBasketPrice;

  @override
  State<ProductHorizontalListItem> createState() =>
      _ProductHorizontalListItemState();
}

class _ProductHorizontalListItemState extends State<ProductHorizontalListItem> {
  ProductDetailProvider productDetailProvider;
  ProductRepository productRepo;
  BasketProvider basketProvider;
  PsValueHolder psValueHolder;
  BasketRepository basketRepository;
  BasketSelectedAttribute basketSelectedAttribute = BasketSelectedAttribute();
  BasketSelectedAddOn basketSelectedAddOn = BasketSelectedAddOn();
  bool isPurchased = false;
  String qty;
  String colorId = '';
  String colorValue;
  bool checkAttribute;
  Basket basket;
  String id;
  double bottomSheetPrice;
  double totalOriginalPrice = 0.0;
  double totalPrice;
  double selectedAddOnPrice = 0.0;
  double selectedAttributePrice = 0.0;

  @override
  Widget build(BuildContext context) {
    psValueHolder = Provider.of<PsValueHolder>(context);
    productRepo = Provider.of<ProductRepository>(context);
    basketRepository = Provider.of<BasketRepository>(context);

    if (widget.intentBasketPrice != null &&
        widget.intentBasketPrice != '' &&
        bottomSheetPrice == null) {
      bottomSheetPrice = double.parse(widget.intentBasketPrice);
    }
    // Future<void> updateAttributePrice(
    //     BasketSelectedAttribute basketSelectedAttribute) async {
    //   // this.totalOriginalPrice = totalOriginalPrice;
    //   // Get Total Selected Attribute Price
    //   selectedAttributePrice =
    //       basketSelectedAttribute.getTotalSelectedAttributePrice();

    //   // Update Price
    //   totalPrice =
    //       double.parse(productDetailProvider.productDetail.data.unitPrice) +
    //           selectedAddOnPrice +
    //           selectedAttributePrice;
    //   totalOriginalPrice =
    //       double.parse(productDetailProvider.productDetail.data.originalPrice) +
    //           selectedAddOnPrice +
    //           selectedAttributePrice;
    //   setState(() {
    //     bottomSheetPrice = totalPrice;
    //   });
    // }

    Future<void> addToBasketAndBuyClickEvent(bool isBuyButtonType) async {
      // if (widget.product.itemColorList.isNotEmpty &&
      //     widget.product.itemColorList[0].id != '') {
      //   if (colorId == null || colorId == '') {
      //     await showDialog<dynamic>(
      //         context: context,
      //         builder: (BuildContext context) {
      //           return WarningDialog(
      //             message: Utils.getString(
      //                 context, 'product_detail__please_select_color'),
      //             onPressed: () {},
      //           );
      //         });
      //     return;
      //   }
      // }
      id =
          '${widget.product.id}$colorId${basketSelectedAddOn.getSelectedaddOnIdByHeaderId()}${basketSelectedAttribute.getSelectedAttributeIdByHeaderId()}';
      // Check All Attribute is selected
      if (widget.product.customizedHeaderList != null) {
        if (widget.product.customizedHeaderList.isNotEmpty &&
            widget.product.customizedHeaderList[0].id != '' &&
            widget.product.customizedHeaderList[0].customizedDetail != null &&
            // widget.product.customizedHeaderList[0].customizedDetail[0].id !=
            //     '' &&
            !basketSelectedAttribute.isAllAttributeSelected(
                widget.product.customizedHeaderList.length)) {
          await showDialog<dynamic>(
              context: context,
              builder: (BuildContext context) {
                return WarningDialog(
                  message: Utils.getString(
                      context, 'product_detail__please_choose_customize'),
                  onPressed: () {},
                );
              });
          return;
        }
      }

      basket = Basket(
          id: id,
          productId: widget.product.id,
          qty: qty ?? widget.product.minimumOrder,
          shopId: widget.product.shop.id,
          selectedColorId: colorId,
          selectedColorValue: colorValue,
          basketPrice: bottomSheetPrice == null
              ? widget.product.unitPrice
              : bottomSheetPrice.toString(),
          basketOriginalPrice: totalOriginalPrice == 0.0
              ? widget.product.originalPrice
              : totalOriginalPrice.toString(),
          selectedAttributeTotalPrice: basketSelectedAttribute
              .getTotalSelectedAttributePrice()
              .toString(),
          product: widget.product,
          basketSelectedAttributeList:
              basketSelectedAttribute.getSelectedAttributeList(),
          basketSelectedAddOnList: basketSelectedAddOn.getSelectedAddOnList());

      await basketProvider.addBasket(basket);

      Fluttertoast.showToast(
          msg:
              Utils.getString(context, 'product_detail__success_add_to_basket'),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: PsColors.mainColor,
          textColor: PsColors.white);
      setState(() {
        isPurchased = true;
      });

      // if (isBuyButtonType) {
      //   final dynamic result = await Navigator.pushNamed(
      //     context,
      //     RoutePaths.basketList,
      //   );
      //   if (result != null && result) {
      //     productDetailProvider.loadProduct(
      //         widget.product.id, psValueHolder.loginUserId);
      //   }
      // }
    }

    return MultiProvider(
      providers: <SingleChildWidget>[
        ChangeNotifierProvider<ProductDetailProvider>(
          lazy: false,
          create: (BuildContext context) {
            productDetailProvider = ProductDetailProvider(
                repo: productRepo, psValueHolder: psValueHolder);

            final String loginUserId = Utils.checkUserLoginId(psValueHolder);
            productDetailProvider.loadProduct(widget.productId, loginUserId);

            return productDetailProvider;
          },
        ),
        ChangeNotifierProvider<BasketProvider>(
            lazy: false,
            create: (BuildContext context) {
              basketProvider = BasketProvider(repo: basketRepository);
              return basketProvider;
            }),
      ],
      child: Consumer<ProductDetailProvider>(builder:
          (BuildContext context, ProductDetailProvider provider, Widget child) {
        if (provider != null &&
            provider.productDetail != null &&
            provider.productDetail.data != null) {
          return Stack(
            children: <Widget>[
              GestureDetector(
                onTap: widget.onTap,
                child: Container(
                  width: PsDimens.space175,
                  margin: const EdgeInsets.symmetric(
                    horizontal: PsDimens.space12,
                    //vertical: PsDimens.space12,
                  ),
                  decoration: BoxDecoration(
                    color: PsColors.coreBackgroundColor,
                    borderRadius: const BorderRadius.all(
                        Radius.circular(PsDimens.space6)),
                  ),
                  child: Column(
                    //mainAxisSize: MainAxisSize.max,
                    //mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          color: PsColors.coreBackgroundColor,
                          borderRadius: const BorderRadius.all(
                              Radius.circular(PsDimens.space6)),
                        ),
                        child: ClipPath(
                          child: PsNetworkImage(
                            photoKey:
                                '${widget.coreTagKey}${PsConst.HERO_TAG__IMAGE}',
                            defaultPhoto: widget.product.defaultPhoto,
                            width: PsDimens.space175,
                            height: PsDimens.space175,
                            boxfit: BoxFit.cover,
                            onTap: () {
                              Utils.psPrint(
                                  widget.product.defaultPhoto.imgParentId);
                              widget.onTap();
                            },
                          ),
                          clipper: const ShapeBorderClipper(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(PsDimens.space6),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        height: 45,
                        margin: const EdgeInsets.only(
                          top: PsDimens.space8,
                          right: PsDimens.space8,
                          //bottom: PsDimens.space18,
                        ),
                        child: Text(
                          widget.product.name,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context).textTheme.bodyText1.copyWith(
                                fontSize: 16.0,
                                fontWeight: FontWeight.w500,
                              ),
                          maxLines: 2,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 12.0,
                          left: PsDimens.space8,
                          //right: PsDimens.space8,
                        ),
                        child: PsHero(
                          tag:
                              '${widget.coreTagKey}$PsConst.HERO_TAG__ORIGINAL_PRICE',
                          flightShuttleBuilder: Utils.flightShuttleBuilder,
                          child: Text(
                            widget.product.isDiscount == PsConst.ONE
                                ? '${Utils.getPriceFormat(widget.product.originalPrice)}${widget.product.currencySymbol}'
                                : '',
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              color: PsColors.textPrimaryColor,
                              fontWeight: FontWeight.w400,
                              fontSize: PsDimens.space12,
                              fontFamily: 'Roboto',
                              decoration: TextDecoration.lineThrough,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          //bottom: PsDimens.space4,
                          right: PsDimens.space8,
                          top: PsDimens.space4,
                        ),
                        child: PsHero(
                          tag:
                              '${widget.coreTagKey}$PsConst.HERO_TAG__UNIT_PRICE',
                          flightShuttleBuilder: Utils.flightShuttleBuilder,
                          child: Text(
                            '${Utils.getPriceFormat(widget.product.unitPrice)}${widget.product.currencySymbol}',
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              color: PsColors.textPrimaryColor,
                              fontWeight: FontWeight.w500,
                              fontSize: PsDimens.space16,
                              fontFamily: 'Roboto',
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              if (widget.product.isDiscount == PsConst.ONE)
                Positioned(
                  top: 0.0,
                  left: 12.0,
                  child: Container(
                    width: 39.0,
                    height: 23.0,
                    // margin: const EdgeInsets.symmetric(
                    //     horizontal: 8.0, vertical: 16.0),
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(6.0),
                        bottomRight: Radius.circular(8.0),
                      ),
                    ),
                    child: Text(
                      '-${widget.product.discountPercent}%',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        color: PsColors.white,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ),
              Positioned(
                right: 10.0,
                top: 0.0,
                child: HeartIcon(
                  productDetail: provider,
                  product: provider.productDetail.data,
                  heroTagTitle: widget.heroTagTitle,
                ),
              ),
              Positioned(
                bottom: 0,
                right: 17,
                child: GestureDetector(
                  onTap: () async {
                    if (await Utils.checkInternetConnectivity()) {
                      Utils.navigateOnUserVerificationView(
                        context,
                        () async {
                          await addToBasketAndBuyClickEvent(true);

                          /// arnelu gorcoxutyun@///////////////////////////////
                          if (basketProvider.basketList.data.isNotEmpty &&
                              widget.product.shop.id !=
                                  basketProvider.basketList.data[0].shopId) {
                            showDialog<dynamic>(
                              context: context,
                              builder: (BuildContext context) {
                                return ConfirmDialogView(
                                  description: Utils.getString(
                                      context, 'warning_dialog__change_shop'),
                                  leftButtonText: Utils.getString(context,
                                      'basket_list__comfirm_dialog_cancel_button'),
                                  rightButtonText: Utils.getString(context,
                                      'basket_list__comfirm_dialog_ok_button'),
                                  onAgreeTap: () async {
                                    await basketProvider
                                        .deleteWholeBasketList();
                                    Navigator.of(context).pop();
                                    //_showDrawer(false);
                                  },
                                );
                              },
                            );
                          } else {
                            if (widget.product.isAvailable == '1') {
                              if (widget.product.customizedHeaderList
                                      .isNotEmpty &&
                                  widget.product.customizedHeaderList[0].id !=
                                      '' &&
                                  widget.product.customizedHeaderList[0]
                                          .customizedDetail !=
                                      null &&
                                  widget.product.customizedHeaderList[0]
                                          .customizedDetail[0].id !=
                                      '' &&
                                  !basketSelectedAttribute
                                      .isAllAttributeSelected(widget.product
                                          .customizedHeaderList.length)) {
                                await showDialog<dynamic>(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return WarningDialog(
                                      message: Utils.getString(context,
                                          'product_detail__please_choose_customize'),
                                      onPressed: () {},
                                    );
                                  },
                                );
                              } else {
                                //_showDrawer(false);
                              }
                            } else {
                              await showDialog<dynamic>(
                                context: context,
                                builder: (BuildContext context) {
                                  return WarningDialog(
                                    message: Utils.getString(context,
                                        'product_detail__is_not_available'),
                                    onPressed: () {},
                                  );
                                },
                              );
                            }
                          }
                        },
                      );
                    } else {
                      showDialog<dynamic>(
                          context: context,
                          builder: (BuildContext context) {
                            return ErrorDialog(
                              message: Utils.getString(
                                  context, 'error_dialog__no_internet'),
                            );
                          });
                    }
                  },
                  child: Container(
                    width: 63.0,
                    height: 33.0,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4.0),
                      color: const Color(0xff095F56),
                    ),
                    child: !isPurchased
                        ? Image.asset(
                            'assets/home_images/buy.png',
                            width: PsDimens.space24,
                            height: PsDimens.space24,
                          )
                        : Image.asset(
                            'assets/home_images/okay.png',
                            width: PsDimens.space24,
                            height: PsDimens.space24,
                          ),
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                left: 12,
                child: HeaderRatingWidget(
                  productDetail: productDetailProvider,
                ),
              )
            ],
          );
        } else {
          return Container(
            color: PsColors.coreBackgroundColor,
          );
        }
      }),
    );
  }
}

class HeartIcon extends StatefulWidget {
  const HeartIcon(
      {Key key,
      @required this.productDetail,
      @required this.product,
      @required this.heroTagTitle})
      : super(key: key);

  final ProductDetailProvider productDetail;
  final Product product;
  final String heroTagTitle;

  @override
  HeartIconState createState() => HeartIconState();
}

class HeartIconState extends State<HeartIcon> {
  Widget icon;
  ProductRepository favouriteRepo;
  PsValueHolder psValueHolder;

  @override
  Widget build(BuildContext context) {
    favouriteRepo = Provider.of<ProductRepository>(context);
    psValueHolder = Provider.of<PsValueHolder>(context);

    if (widget.product != null &&
        widget.productDetail != null &&
        widget.productDetail.productDetail != null &&
        widget.productDetail.productDetail.data != null &&
        widget.productDetail.productDetail.data.isFavourited != null) {
      return ChangeNotifierProvider<FavouriteProductProvider>(
        lazy: false,
        create: (BuildContext context) {
          final FavouriteProductProvider provider = FavouriteProductProvider(
              repo: favouriteRepo, psValueHolder: psValueHolder);

          return provider;
        },
        child: Consumer<FavouriteProductProvider>(
          builder: (BuildContext context, FavouriteProductProvider provider,
              Widget child) {
            return GestureDetector(
              onTap: () async {
                if (await Utils.checkInternetConnectivity()) {
                  Utils.navigateOnUserVerificationView(context, () async {
                    if (widget.productDetail.productDetail.data.isFavourited ==
                        '0') {
                      setState(() {
                        widget.productDetail.productDetail.data.isFavourited =
                            '1';
                      });
                    } else {
                      setState(() {
                        widget.productDetail.productDetail.data.isFavourited =
                            '0';
                      });
                    }

                    final FavouriteParameterHolder favouriteParameterHolder =
                        FavouriteParameterHolder(
                            userId: provider.psValueHolder.loginUserId,
                            productId: widget.product.id,
                            shopId: widget.product.shop.id);

                    final PsResource<Product> _apiStatus = await provider
                        .postFavourite(favouriteParameterHolder.toMap());

                    if (_apiStatus.data != null) {
                      if (_apiStatus.status == PsStatus.SUCCESS) {
                        await widget.productDetail.loadProductForFav(
                            widget.product.id,
                            provider.psValueHolder.loginUserId);
                      }
                      if (widget.productDetail != null &&
                          widget.productDetail.productDetail != null &&
                          widget.productDetail.productDetail.data != null) {
                        if (widget.productDetail.productDetail.data
                                .isFavourited ==
                            PsConst.ONE) {
                          icon = Container(
                            width: 41.0,
                            height: 41.0,
                            // decoration: const BoxDecoration(
                            //   shape: BoxShape.circle,
                            //   color: Colors.black26,
                            // ),
                            child: const Icon(
                              Icons.favorite,
                              //color: Colors.red,
                            ),
                          );
                        } else {
                          icon = Container(
                            width: 41.0,
                            height: 41.0,
                            // decoration: const BoxDecoration(
                            //   shape: BoxShape.circle,
                            //   color: Colors.black26,
                            // ),
                            child: Icon(
                              Icons.favorite_border,
                              color: PsColors.black,
                            ),
                          );
                        }
                      }
                    } else {
                      print('There is no comment');
                    }
                  });
                } else {
                  showDialog<dynamic>(
                    context: context,
                    builder: (BuildContext context) {
                      return ErrorDialog(
                        message: Utils.getString(
                            context, 'error_dialog__no_internet'),
                      );
                    },
                  );
                }
              },
              child: (widget.productDetail != null &&
                      widget.productDetail.productDetail != null &&
                      widget.productDetail.productDetail.data != null)
                  ? widget.productDetail.productDetail.data.isFavourited ==
                          PsConst.ZERO
                      ? icon = Container(
                          width: 41.0,
                          height: 41.0,
                          // decoration: const BoxDecoration(
                          //   shape: BoxShape.circle,
                          //   color: Colors.black26,
                          // ),
                          child: const Icon(
                            Icons.favorite_border,
                            color: Colors.black,
                          ),
                        )
                      : icon = Container(
                          width: 41.0,
                          height: 41.0,
                          // decoration: const BoxDecoration(
                          //   shape: BoxShape.circle,
                          //   color: Colors.black26,
                          // ),
                          child: const Icon(
                            Icons.favorite,
                            color: Colors.red,
                          ),
                        )
                  : Container(
                      color: PsColors.coreBackgroundColor,
                    ),
            );
          },
        ),
      );
    } else {
      return Container(
        color: PsColors.coreBackgroundColor,
      );
    }
  }
}
