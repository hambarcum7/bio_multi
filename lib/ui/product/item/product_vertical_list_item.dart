import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/constant/ps_constants.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/provider/basket/basket_provider.dart';
import 'package:biomart/provider/product/product_provider.dart';
import 'package:biomart/repository/basket_repository.dart';
import 'package:biomart/repository/product_repository.dart';
import 'package:biomart/ui/common/dialog/confirm_dialog_view.dart';
import 'package:biomart/ui/common/dialog/error_dialog.dart';
import 'package:biomart/ui/common/dialog/warning_dialog_view.dart';
import 'package:biomart/ui/common/ps_hero.dart';
import 'package:biomart/ui/common/ps_ui_widget.dart';
import 'package:biomart/ui/product/detail/product_detail_view.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/basket.dart';
import 'package:biomart/viewobject/basket_selected_add_on.dart';
import 'package:biomart/viewobject/basket_selected_attribute.dart';
import 'package:biomart/viewobject/common/ps_value_holder.dart';
import 'package:biomart/viewobject/product.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ProductVeticalListItem extends StatefulWidget {
  const ProductVeticalListItem({
    Key key,
    @required this.product,
    this.onTap,
    this.animationController,
    this.animation,
    this.coreTagKey,
    this.intentBasketPrice,
    this.productDetailProvider, //TODO petqa erevi poxancvi
  }) : super(key: key);

  final Product product;
  final Function onTap;
  final String coreTagKey;
  final AnimationController animationController;
  final Animation<double> animation;
  final String intentBasketPrice;
  final ProductDetailProvider productDetailProvider;

  @override
  State<ProductVeticalListItem> createState() => _ProductVeticalListItemState();
}

class _ProductVeticalListItemState extends State<ProductVeticalListItem> {
  bool isPurchased = false;
  //ProductDetailProvider productDetailProvider;
  ProductRepository productRepo;
  PsValueHolder psValueHolder;
  BasketProvider basketProvider;
  BasketRepository basketRepository;
  BasketSelectedAttribute basketSelectedAttribute = BasketSelectedAttribute();
  BasketSelectedAddOn basketSelectedAddOn = BasketSelectedAddOn();
  Basket basket;
  String colorId = '';
  String colorValue;
  String qty;
  String id;
  double bottomSheetPrice;
  double totalOriginalPrice = 0.0;
  double totalPrice;
  double selectedAddOnPrice = 0.0;
  double selectedAttributePrice = 0.0;

  @override
  Widget build(BuildContext context) {
    widget.animationController.forward();

    if (widget.intentBasketPrice != null &&
        widget.intentBasketPrice != '' &&
        bottomSheetPrice == null) {
      bottomSheetPrice = double.parse(widget.intentBasketPrice);
    }

    // Future<void> updateAttributePrice(
    //     BasketSelectedAttribute basketSelectedAttribute) async {
    //   // this.totalOriginalPrice = totalOriginalPrice;
    //   // Get Total Selected Attribute Price
    //   selectedAttributePrice =
    //       basketSelectedAttribute.getTotalSelectedAttributePrice();

    //   // Update Price
    //   totalPrice = double.parse(
    //           widget.productDetailProvider.productDetail.data.unitPrice) +
    //       selectedAddOnPrice +
    //       selectedAttributePrice;
    //   totalOriginalPrice = double.parse(
    //           widget.productDetailProvider.productDetail.data.originalPrice) +
    //       selectedAddOnPrice +
    //       selectedAttributePrice;
    //   setState(() {
    //     bottomSheetPrice = totalPrice;
    //   });
    // }

    Future<void> addToBasketAndBuyClickEvent(bool isBuyButtonType) async {
      // if (widget.product.itemColorList.isNotEmpty &&
      //     widget.product.itemColorList[0].id != '') {
      //   if (colorId == null || colorId == '') {
      //     await showDialog<dynamic>(
      //         context: context,
      //         builder: (BuildContext context) {
      //           return WarningDialog(
      //             message: Utils.getString(
      //                 context, 'product_detail__please_select_color'),
      //             onPressed: () {},
      //           );
      //         });
      //     return;
      //   }
      // }
      id =
          '${widget.product.id}$colorId${basketSelectedAddOn.getSelectedaddOnIdByHeaderId()}${basketSelectedAttribute.getSelectedAttributeIdByHeaderId()}';
      // Check All Attribute is selected
      if (widget.product.customizedHeaderList != null) {
        if (widget.product.customizedHeaderList.isNotEmpty &&
            widget.product.customizedHeaderList[0].id != '' &&
            widget.product.customizedHeaderList[0].customizedDetail != null &&
            // widget.product.customizedHeaderList[0].customizedDetail[0].id !=
            //     '' &&
            !basketSelectedAttribute.isAllAttributeSelected(
                widget.product.customizedHeaderList.length)) {
          await showDialog<dynamic>(
              context: context,
              builder: (BuildContext context) {
                return WarningDialog(
                  message: Utils.getString(
                      context, 'product_detail__please_choose_customize'),
                  onPressed: () {},
                );
              });
          return;
        }
      }

      basket = Basket(
          id: id,
          productId: widget.product.id,
          qty: qty ?? widget.product.minimumOrder,
          shopId: widget.product.shop.id,
          selectedColorId: colorId,
          selectedColorValue: colorValue,
          basketPrice: bottomSheetPrice == null
              ? widget.product.unitPrice
              : bottomSheetPrice.toString(),
          basketOriginalPrice: totalOriginalPrice == 0.0
              ? widget.product.originalPrice
              : totalOriginalPrice.toString(),
          selectedAttributeTotalPrice: basketSelectedAttribute
              .getTotalSelectedAttributePrice()
              .toString(),
          product: widget.product,
          basketSelectedAttributeList:
              basketSelectedAttribute.getSelectedAttributeList(),
          basketSelectedAddOnList: basketSelectedAddOn.getSelectedAddOnList());

      await basketProvider.addBasket(basket);

      Fluttertoast.showToast(
          msg:
              Utils.getString(context, 'product_detail__success_add_to_basket'),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: PsColors.mainColor,
          textColor: PsColors.white);
      setState(() {
        isPurchased = true;
      });

      // if (isBuyButtonType) {
      //   final dynamic result = await Navigator.pushNamed(
      //     context,
      //     RoutePaths.basketList,
      //   );
      //   if (result != null && result) {
      //     productDetailProvider.loadProduct(
      //         widget.product.id, psValueHolder.loginUserId);
      //   }
      // }
    }

    return AnimatedBuilder(
      animation: widget.animationController,
      child: Container(
        padding: const EdgeInsets.all(PsDimens.space1),
        margin: const EdgeInsets.all(PsDimens.space4),
        child: GridTile(
          child: Stack(
            children: <Widget>[
              GestureDetector(
                onTap: widget.onTap,
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Container(
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(
                              Radius.circular(PsDimens.space6)),
                        ),
                        child: ClipPath(
                          child: PsNetworkImage(
                            photoKey:
                                '${widget.coreTagKey}${PsConst.HERO_TAG__IMAGE}',
                            defaultPhoto: widget.product.defaultPhoto,
                            width: PsDimens.space175,
                            height: PsDimens.space175,
                            boxfit: BoxFit.cover,
                            onTap: () {
                              Utils.psPrint(
                                  widget.product.defaultPhoto.imgParentId);
                              widget.onTap();
                            },
                          ),
                          clipper: const ShapeBorderClipper(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(PsDimens.space6),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        height: 38.0,
                        margin: const EdgeInsets.only(top: 5.0
                            //bottom: PsDimens.space18,
                            ),
                        child: PsHero(
                          tag: '${widget.coreTagKey}${PsConst.HERO_TAG__TITLE}',
                          child: Text(
                            widget.product.name,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontSize: PsDimens.space16,
                              color: PsColors.textPrimaryColor,
                              fontWeight: FontWeight.w500,
                            ),
                            maxLines: 2,
                          ),
                        ),
                      ),
                      if (widget.product.isDiscount == PsConst.ONE)
                        Padding(
                          padding: const EdgeInsets.only(
                            top: PsDimens.space2,
                            left: PsDimens.space8,
                          ),
                          child: PsHero(
                            tag:
                                '${widget.coreTagKey}$PsConst.HERO_TAG__ORIGINAL_PRICE',
                            flightShuttleBuilder: Utils.flightShuttleBuilder,
                            child: Text(
                              '${Utils.getPriceFormat(widget.product.originalPrice)}${widget.product.currencySymbol}',
                              textAlign: TextAlign.start,
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle2
                                  .copyWith(
                                    color: PsColors.textPrimaryColor,
                                    fontWeight: FontWeight.w400,
                                    fontSize: PsDimens.space12,
                                    decoration: TextDecoration.lineThrough,
                                  ),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                            ),
                          ),
                        )
                      else
                        Container(
                          color: PsColors.coreBackgroundColor,
                          margin: const EdgeInsets.only(
                            top: PsDimens.space2,
                            right: PsDimens.space8,
                            bottom: PsDimens.space18,
                          ),
                        ),
                      PsHero(
                        tag:
                            '${widget.coreTagKey}$PsConst.HERO_TAG__UNIT_PRICE',
                        flightShuttleBuilder: Utils.flightShuttleBuilder,
                        child: Text(
                          '${Utils.getPriceFormat(widget.product.unitPrice)}${widget.product.currencySymbol}',
                          textAlign: TextAlign.start,
                          style: Theme.of(context).textTheme.subtitle2.copyWith(
                                color: PsColors.textPrimaryColor,
                                fontWeight: FontWeight.w500,
                                fontSize: PsDimens.space16,
                              ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              if (widget.product.isDiscount == PsConst.ONE)
                Positioned(
                  top: 0.0,
                  left: 0.0,
                  child: Container(
                    width: 39.0,
                    height: 23.0,
                    // margin: const EdgeInsets.symmetric(
                    //     horizontal: 8.0, vertical: 16.0),
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(6.0),
                        bottomRight: Radius.circular(8.0),
                      ),
                    ),
                    child: Text(
                      '-${widget.product.discountPercent}%',
                      textAlign: TextAlign.start,
                      style: Theme.of(context).textTheme.bodyText2.copyWith(
                            color: PsColors.white,
                            fontWeight: FontWeight.w500,
                          ),
                    ),
                  ),
                ),
              // Positioned(
              //   right: 2.0,
              //   top: 0.0,
              //   child: HeartIcon(
              //     productDetail: provider,
              //     product: provider.productDetail.data,
              //     heroTagTitle: '',
              //   ),
              // ),
              Positioned(
                bottom: 0,
                right: 0,
                child: GestureDetector(
                  onTap: () async {
                    if (await Utils.checkInternetConnectivity()) {
                      Utils.navigateOnUserVerificationView(
                        context,
                        () async {
                          await addToBasketAndBuyClickEvent(true);

                          /// arnelu gorcoxutyun@///////////////////////////////
                          if (basketProvider.basketList.data.isNotEmpty &&
                              widget.product.shop.id !=
                                  basketProvider.basketList.data[0].shopId) {
                            showDialog<dynamic>(
                              context: context,
                              builder: (BuildContext context) {
                                return ConfirmDialogView(
                                  description: Utils.getString(
                                      context, 'warning_dialog__change_shop'),
                                  leftButtonText: Utils.getString(context,
                                      'basket_list__comfirm_dialog_cancel_button'),
                                  rightButtonText: Utils.getString(context,
                                      'basket_list__comfirm_dialog_ok_button'),
                                  onAgreeTap: () async {
                                    await basketProvider
                                        .deleteWholeBasketList();
                                    Navigator.of(context).pop();
                                    //_showDrawer(false);
                                  },
                                );
                              },
                            );
                          } else {
                            if (widget.product.isAvailable == '1') {
                              if (widget.product.customizedHeaderList
                                      .isNotEmpty &&
                                  widget.product.customizedHeaderList[0].id !=
                                      '' &&
                                  widget.product.customizedHeaderList[0]
                                          .customizedDetail !=
                                      null &&
                                  widget.product.customizedHeaderList[0]
                                          .customizedDetail[0].id !=
                                      '' &&
                                  !basketSelectedAttribute
                                      .isAllAttributeSelected(widget.product
                                          .customizedHeaderList.length)) {
                                await showDialog<dynamic>(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return WarningDialog(
                                      message: Utils.getString(context,
                                          'product_detail__please_choose_customize'),
                                      onPressed: () {},
                                    );
                                  },
                                );
                              } else {
                                //_showDrawer(false);
                              }
                            } else {
                              await showDialog<dynamic>(
                                context: context,
                                builder: (BuildContext context) {
                                  return WarningDialog(
                                    message: Utils.getString(context,
                                        'product_detail__is_not_available'),
                                    onPressed: () {},
                                  );
                                },
                              );
                            }
                          }
                        },
                      );
                    } else {
                      showDialog<dynamic>(
                          context: context,
                          builder: (BuildContext context) {
                            return ErrorDialog(
                              message: Utils.getString(
                                  context, 'error_dialog__no_internet'),
                            );
                          });
                    }
                  },
                  child: Container(
                    width: 63.0,
                    height: 33.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4.0),
                      color: const Color(0xff095F56),
                    ),
                    alignment: Alignment.center,
                    child: !isPurchased
                        ? Image.asset(
                            'assets/home_images/buy.png',
                            width: 24.0,
                            height: 24.0,
                          )
                        : Image.asset(
                            'assets/home_images/okay.png',
                            width: 24.0,
                            height: 24.0,
                          ),
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                left: 5,
                child: HeaderRatingWidget(
                  productDetail: widget.productDetailProvider,
                ),
              ),
            ],
          ),
        ),
      ),
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
          opacity: widget.animation,
          child: Transform(
            transform: Matrix4.translationValues(
                0.0, 100 * (1.0 - widget.animation.value), 0.0),
            child: child,
          ),
        );
      },
    );
  }
}
