import 'package:biomart/constant/ps_dimens.dart';
//import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/product.dart';
import 'package:flutter/material.dart';

class DescriptionTileView extends StatelessWidget {
  const DescriptionTileView({
    Key key,
    @required this.productDetail,
  }) : super(key: key);

  final Product productDetail;
  @override
  Widget build(BuildContext context) {
    // final Widget _expansionTileTitleWidget = Text(
    //   Utils.getString(context, 'description_tile__product_description'),
    //   style: Theme.of(context).textTheme.subtitle1,
    // );
    if (productDetail != null && productDetail.description != null) {
      return Container(
        margin: const EdgeInsets.only(
            left: PsDimens.space20,
            right: PsDimens.space20,
            bottom: PsDimens.space8),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Text(
            productDetail.description ?? '',
            style: Theme.of(context)
                .textTheme
                .bodyText2
                .copyWith(letterSpacing: 0.8, fontSize: 16, height: 1.3),
          ),
        ),
      );
    } else {
      return const Card();
    }
  }
}
