import 'package:biomart/api/common/ps_status.dart';
import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/constant/route_paths.dart';
import 'package:biomart/provider/basket/basket_provider.dart';
import 'package:biomart/provider/product/product_provider.dart';
import 'package:biomart/provider/subcategory/sub_category_provider.dart';
import 'package:biomart/provider/subsubcategory/sub_sub_category_provider.dart';
import 'package:biomart/repository/basket_repository.dart';
import 'package:biomart/repository/product_repository.dart';
import 'package:biomart/repository/sub_category_repository.dart';
import 'package:biomart/repository/sub_sub_category_repository.dart';
import 'package:biomart/ui/dashboard/view_pages/call_bottom_view.dart';
import 'package:biomart/ui/product/list_with_filter/product_list_with_filter_view.dart';
import 'package:biomart/ui/search/home_item_search_container_view.dart';
import 'package:biomart/ui/subcategory/item/sub_category_vertical_list_item.dart';
import 'package:biomart/ui/subsubcategory/list/sub_sub_category_grid_view.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/category.dart';
import 'package:biomart/viewobject/common/ps_value_holder.dart';
import 'package:biomart/viewobject/holder/intent_holder/product_list_intent_holder.dart';
import 'package:biomart/viewobject/holder/product_parameter_holder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:shimmer/shimmer.dart';

class ProductListWithFilterContainerView extends StatefulWidget {
  const ProductListWithFilterContainerView({
    @required this.productParameterHolder,
    @required this.appBarTitle,
    this.category,
  });
  final ProductParameterHolder productParameterHolder;
  final String appBarTitle;
  final Category category;
  @override
  _ProductListWithFilterContainerViewState createState() =>
      _ProductListWithFilterContainerViewState();
}

class _ProductListWithFilterContainerViewState
    extends State<ProductListWithFilterContainerView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  BasketRepository basketRepository;
  String appBarTitleName;
  ProductRepository productRepo;
  ProductDetailProvider productDetailProvider;
  PsValueHolder psValueHolder;
  SubCategoryRepository repo1;
  SubCategoryProvider _subCategoryProvider;
  //SubSubCategoryRepository repo2;
  //SubSubCategoryProvider _subSubCategoryProvider;
  int selectableIndex = 0;

  void changeAppBarTitle(String categoryName) {
    appBarTitleName = categoryName;
  }

  @override
  Widget build(BuildContext context) {
    basketRepository = Provider.of<BasketRepository>(context);
    productRepo = Provider.of<ProductRepository>(context);
    psValueHolder = Provider.of<PsValueHolder>(context);
    repo1 = Provider.of<SubCategoryRepository>(context);
    //repo2 = Provider.of<SubSubCategoryRepository>(context);

    print(
        '............................Build UI Again< Filter Container > ............................');
    return MultiProvider(
      providers: <SingleChildWidget>[
        ChangeNotifierProvider<ProductDetailProvider>(
          lazy: false,
          create: (BuildContext context) {
            productDetailProvider = ProductDetailProvider(
                repo: productRepo, psValueHolder: psValueHolder);

            //final String loginUserId = Utils.checkUserLoginId(psValueHolder);
            //productDetailProvider.loadProduct(widget.productId, loginUserId);

            return productDetailProvider;
          },
        ),
        if (widget.category != null)
          ChangeNotifierProvider<SubCategoryProvider>(
            lazy: false,
            create: (BuildContext context) {
              _subCategoryProvider = SubCategoryProvider(repo: repo1);
              //_subProvider = SubCategoryProvider(repo: repo1);
              _subCategoryProvider.loadSubCategoryList(widget.category.id);
              return _subCategoryProvider;
            },
          ),
        // ChangeNotifierProvider<SubSubCategoryProvider>(
        //   lazy: false,
        //   create: (BuildContext context) {
        //     _subSubCategoryProvider = SubSubCategoryProvider(repo: repo2);
        //     //_subSubCategoryProvider.loadSubSubCategoryList(widget.category.id);
        //     return _subSubCategoryProvider;
        //   },
        // ),
      ],
      child: Consumer<ProductDetailProvider>(
        builder: (BuildContext context, ProductDetailProvider provider,
            Widget child) {
          return Scaffold(
            bottomNavigationBar: const BottomBarViewWidget(
              currentIdex: 0,
            ),
            backgroundColor: PsColors.coreBackgroundColor,
            appBar: PreferredSize(
              preferredSize: const Size.fromHeight(70),
              child: AppBar(
                backgroundColor: PsColors.mainColor,
                // shape: const RoundedRectangleBorder(
                //   borderRadius: BorderRadius.vertical(
                //     bottom: Radius.circular(16),
                //   ),
                // ),
                systemOverlayStyle: SystemUiOverlayStyle(
                  statusBarIconBrightness:
                      Utils.getBrightnessForAppBar(context),
                  //statusBarColor: PsColors.mainColor,
                ),
                // iconTheme:
                //     Theme.of(context).iconTheme.copyWith(color: PsColors.white),
                title: Text(
                  appBarTitleName ?? widget.appBarTitle,
                  //textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline6.copyWith(
                        fontWeight: FontWeight.w400,
                        fontSize: 24.0,
                        color: PsColors.white,
                      ),
                ),
                elevation: 0,
                actions: <Widget>[
                  if (!(widget.appBarTitle != null &&
                      widget.appBarTitle ==
                          Utils.getString(
                              context, 'home_search__app_bar_title')))
                    ChangeNotifierProvider<BasketProvider>(
                      lazy: false,
                      create: (BuildContext context) {
                        final BasketProvider provider =
                            BasketProvider(repo: basketRepository);
                        provider.loadBasketList();
                        return provider;
                      },
                      child: Consumer<BasketProvider>(builder:
                          (BuildContext context, BasketProvider basketProvider,
                              Widget child) {
                        return GestureDetector(
                            child: Container(
                              width: PsDimens.space24,
                              height: PsDimens.space24,
                              margin: const EdgeInsets.only(
                                top: PsDimens.space8,
                                left: PsDimens.space8,
                                right: PsDimens.space16,
                              ),
                              child: Image.asset('assets/images/menu.png'),
                            ),
                            onTap: () {
                              final ProductParameterHolder
                                  productParameterHolder =
                                  ProductParameterHolder()
                                      .getLatestParameterHolder();
                              productParameterHolder.searchTerm = '';
                              Utils.psPrint(productParameterHolder.searchTerm);

                              Navigator.pushReplacement(
                                context,
                                PageRouteBuilder<dynamic>(
                                  pageBuilder: (_, Animation<double> a1,
                                          Animation<double> a2) =>
                                      HomeItemSearchContainerView(
                                    productParameterHolder:
                                        productParameterHolder,
                                    filterIsOpened: true,
                                  ),
                                ),
                              );

                              // Navigator.pushNamed(
                              //   context,
                              //   RoutePaths.dashboardsearchFood,
                              //   arguments: ProductListIntentHolder(
                              //       appBarTitle: Utils.getString(
                              //           context, 'home_search__app_bar_title'),
                              //       productParameterHolder:
                              //           productParameterHolder),
                              // );
                            });
                      }),
                    )
                ],

                bottom: widget.category != null
                    ? PreferredSize(
                        child: Consumer<SubCategoryProvider>(
                          builder: (BuildContext context,
                              SubCategoryProvider provider, Widget child) {
                            return Builder(
                              builder: (BuildContext newContext) {
                                return Container(
                                  height: 25,
                                  //color: Colors.red,
                                  //margin: const EdgeInsets.only(bottom: 4),
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: provider.subCategoryList.data !=
                                            null
                                        ? provider.subCategoryList.data.length +
                                            1
                                        : 1,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      if (provider.subCategoryList.status ==
                                          PsStatus.BLOCK_LOADING) {
                                        // print(
                                        //     '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  ${provider.subCategoryList.data.length}');
                                        return Shimmer.fromColors(
                                          baseColor: PsColors.grey,
                                          highlightColor: PsColors.white,
                                          child:
                                              Column(children: const <Widget>[
                                            FrameUIForLoading(),
                                            FrameUIForLoading(),
                                            FrameUIForLoading(),
                                            FrameUIForLoading(),
                                            FrameUIForLoading(),
                                          ]),
                                        );
                                      } else {
                                        if (index == 0) {
                                          return GestureDetector(
                                            onTap: () {
                                              // setState(() {
                                              //   selectableIndex = 0;
                                              // });
                                            },
                                            child: Container(
                                              //height: 20,
                                              alignment: Alignment.center,
                                              margin:
                                                  const EdgeInsets.symmetric(
                                                horizontal: 8.0,
                                              ),
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                horizontal: 16.0,
                                              ),
                                              decoration: selectableIndex == 0
                                                  ? BoxDecoration(
                                                      color:
                                                          const Color.fromRGBO(
                                                              255,
                                                              255,
                                                              255,
                                                              0.17),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                        PsDimens.space24,
                                                      ),
                                                    )
                                                  : null,
                                              child: Text(
                                                Utils.getString(context,
                                                    'product_list__category_all'),
                                                textAlign: TextAlign.start,
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bodyText2
                                                    .copyWith(
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: PsColors.white,
                                                      fontSize: 18.0,
                                                    ),
                                              ),
                                            ),
                                          );
                                        } else {
                                          return SubCategoryVerticalListItem(
                                            subCategory: provider
                                                .subCategoryList
                                                .data[index - 1],
                                            onTap: () {
                                              // setState(() {
                                              //   selectableIndex = index;
                                              // });
                                              // _subSubCategoryProvider
                                              //         .subCategoryId =
                                              //     provider.subCategoryList
                                              //         .data[index - 1].id;
                                              // _subSubCategoryProvider
                                              //         .subSubCategoryBySubCatIdParamenterHolder
                                              //         .subSubCatId =
                                              //     provider.subCategoryList
                                              //         .data[index - 1].id;
                                              // print(_subSubCategoryProvider
                                              //     .subSubCategoryBySubCatIdParamenterHolder
                                              //     .subCatId);

                                              final ProductParameterHolder
                                                  productParameterHolder =
                                                  ProductParameterHolder()
                                                      .getLatestParameterHolder();
                                              productParameterHolder.subCatId =
                                                  provider.subCategoryList
                                                      .data[index - 1].id;
                                              Navigator.push(context,
                                                  MaterialPageRoute(builder:
                                                      (BuildContext context) {
                                                return SubSubCategoryProductListWithFilterContainerView(
                                                  productParameterHolder:
                                                      productParameterHolder,
                                                  appBarTitle: provider
                                                      .subCategoryList
                                                      .data[index - 1]
                                                      .name,
                                                  subCategory: provider
                                                      .subCategoryList
                                                      .data[index - 1],
                                                );
                                              }));
                                              // Navigator.pushNamed(
                                              //   context,
                                              //   RoutePaths.filterProductList,
                                              //   arguments: ProductListIntentHolder(
                                              //       appBarTitle: provider
                                              //           .subCategoryList
                                              //           .data[index - 1]
                                              //           .name,
                                              //       productParameterHolder: provider
                                              //           .subCategoryByCatIdParamenterHolder),
                                              // );
                                            },
                                            boxDecoration: selectableIndex ==
                                                    index
                                                ? BoxDecoration(
                                                    color: const Color.fromRGBO(
                                                        255, 255, 255, 0.17),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                      PsDimens.space24,
                                                    ),
                                                  )
                                                : null,
                                          );
                                        }
                                      }
                                    },
                                  ),
                                );
                              },
                            );
                          },
                        ),
                        preferredSize: const Size.fromHeight(50),
                      )
                    : PreferredSize(
                        preferredSize: const Size.fromHeight(50),
                        child: Container(
                          color: PsColors.coreBackgroundColor,
                        ),
                      ),
              ),
            ),
            body: Builder(
              builder: (BuildContext context) {
                return Stack(children: <Widget>[
                  Container(
                    height: 80,
                    decoration: BoxDecoration(
                      color: PsColors.mainColor,
                      border: Border.all(
                        width: 0,
                        color: PsColors.mainColor,
                      ),
                    ),
                  ),
                  Container(
                    //padding: const EdgeInsets.symmetric(vertical: 24.0),
                    margin: const EdgeInsets.only(top: 8),
                    decoration: BoxDecoration(
                      color: PsColors.coreBackgroundColor,
                      border: Border.all(
                        width: 0,
                        color: PsColors.coreBackgroundColor,
                      ),
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(16.0),
                          topRight: Radius.circular(16.0)),
                    ),
                    child: ProductListWithFilterView(
                      animationController: animationController,
                      productParameterHolder: widget.productParameterHolder,
                      changeAppBarTitle: changeAppBarTitle,
                    ),
                  ),
                ]);
              },
            ),
          );
        },
      ),
    );
  }
}

class FrameUIForLoading extends StatelessWidget {
  const FrameUIForLoading({
    Key key,
  }) : super(key: key);
//
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
            height: 70,
            width: 70,
            margin: const EdgeInsets.all(PsDimens.space16),
            decoration: BoxDecoration(color: PsColors.grey)),
        Expanded(
            child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
          Container(
              height: 15,
              margin: const EdgeInsets.all(PsDimens.space8),
              decoration: BoxDecoration(color: PsColors.grey)),
          Container(
              height: 15,
              margin: const EdgeInsets.all(PsDimens.space8),
              decoration: BoxDecoration(color: PsColors.grey)),
        ]))
      ],
    );
  }
}
//
