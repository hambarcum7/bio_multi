import 'package:biomart/api/common/ps_status.dart';
import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_constants.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/constant/route_paths.dart';
import 'package:biomart/provider/product/product_provider.dart';
import 'package:biomart/provider/product/search_product_provider.dart';
import 'package:biomart/repository/product_repository.dart';
import 'package:biomart/ui/common/ps_ui_widget.dart';
import 'package:biomart/ui/product/item/product_vertical_list_item_for_home.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/common/ps_value_holder.dart';
import 'package:biomart/viewobject/holder/intent_holder/product_detail_intent_holder.dart';
import 'package:biomart/viewobject/holder/product_parameter_holder.dart';
import 'package:biomart/viewobject/product.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

class ProductListWithFilterView extends StatefulWidget {
  const ProductListWithFilterView({
    Key key,
    @required this.productParameterHolder,
    @required this.animationController,
    this.changeAppBarTitle,
    //@required this.productDetailProvider,
  }) : super(key: key);

  final ProductParameterHolder productParameterHolder;
  //final ProductDetailProvider productDetailProvider;
  final AnimationController animationController;
  final Function changeAppBarTitle;

  @override
  _ProductListWithFilterViewState createState() =>
      _ProductListWithFilterViewState();
}

class _ProductListWithFilterViewState extends State<ProductListWithFilterView>
    with TickerProviderStateMixin {
  final ScrollController _scrollController = ScrollController();

  SearchProductProvider _searchProductProvider;
  ProductDetailProvider productDetailProvider;

  bool isVisible = true;
  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    _offset = 0;
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _searchProductProvider.nextProductListByKey(
            _searchProductProvider.productParameterHolder);
      }
      //setState(() {
      final double offset = _scrollController.offset;
      _delta += offset - _oldOffset;
      if (_delta > _containerMaxHeight)
        _delta = _containerMaxHeight;
      else if (_delta < 0) {
        _delta = 0;
      }
      _oldOffset = offset;
      _offset = -_delta;
    });

    print(' Offset $_offset');
    //});
  }

  final double _containerMaxHeight = 60;
  double _offset, _delta = 0, _oldOffset = 0;
  ProductRepository repo1;
  dynamic data;
  PsValueHolder valueHolder;
  bool isConnectedToInternet = false;
  bool isSuccessfullyLoaded = true;

  void checkConnection() {
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
      if (isConnectedToInternet && PsConfig.showAdMob) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    repo1 = Provider.of<ProductRepository>(context);
    valueHolder = Provider.of<PsValueHolder>(context);

    if (!isConnectedToInternet && PsConfig.showAdMob) {
      print('loading ads....');
      checkConnection();
    }
    print(MediaQuery.of(context).size.width);
    print(
        '............................Build UI Again < Filter View >............................');
    return MultiProvider(
      providers: <SingleChildWidget>[
        ChangeNotifierProvider<ProductDetailProvider>(
          lazy: false,
          create: (BuildContext context) {
            productDetailProvider =
                ProductDetailProvider(repo: repo1, psValueHolder: valueHolder);

            //final String loginUserId = Utils.checkUserLoginId(valueHolder);
            //productDetailProvider.loadProduct(widget.productId, loginUserId);

            return productDetailProvider;
          },
        ),
        ChangeNotifierProvider<SearchProductProvider>(
          lazy: false,
          create: (BuildContext context) {
            final SearchProductProvider provider =
                SearchProductProvider(repo: repo1);
            provider.loadProductListByKey(widget.productParameterHolder);
            _searchProductProvider = provider;
            _searchProductProvider.productParameterHolder =
                widget.productParameterHolder;
            return _searchProductProvider;
          },
        ),
      ],
      child: Consumer<SearchProductProvider>(
        builder: (BuildContext context, SearchProductProvider provider,
            Widget child) {
          return Column(
            children: <Widget>[
              //const PsAdMobBannerWidget(),
              Expanded(
                child: Stack(
                  children: <Widget>[
                    if (provider.productList.data.isNotEmpty &&
                        provider.productList.data != null)
                      Container(
                        decoration: BoxDecoration(
                          color: PsColors.transparent,
                          border: Border.all(
                            width: 0,
                            color: PsColors.coreBackgroundColor,
                          ),
                          borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(16.0),
                              topRight: Radius.circular(16.0)),
                        ),
                        padding: const EdgeInsets.symmetric(
                          horizontal: PsDimens.space12,
                        ),
                        child: RefreshIndicator(
                          child: CustomScrollView(
                            controller: _scrollController,
                            physics: const AlwaysScrollableScrollPhysics(),
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            slivers: <Widget>[
                              SliverGrid(
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount:
                                      MediaQuery.of(context).size.width ~/ 160,
                                  crossAxisSpacing: 0,
                                  mainAxisExtent: 330,
                                  childAspectRatio: 1 / 2.1,
                                  mainAxisSpacing: 0.0,
                                ),
                                delegate: SliverChildBuilderDelegate(
                                  (BuildContext context, int index) {
                                    if (provider.productList.data != null ||
                                        provider.productList.data.isNotEmpty) {
                                      // final int count =
                                      //     provider.productList.data.length;
                                      final Product product =
                                          provider.productList.data[index];
                                      return Padding(
                                        padding:
                                            const EdgeInsets.only(top: 8.0),
                                        child: ProductVerticalListItem(
                                            coreTagKey:
                                                provider.hashCode.toString() +
                                                    product.id,
                                            product: provider
                                                .productList.data[index],
                                            productId: product.id,
                                            onTap: () {
                                              final ProductDetailIntentHolder
                                                  holder =
                                                  ProductDetailIntentHolder(
                                                productId: product.id,
                                                heroTagImage: provider.hashCode
                                                        .toString() +
                                                    product.id +
                                                    PsConst.HERO_TAG__IMAGE,
                                                heroTagTitle: provider.hashCode
                                                        .toString() +
                                                    product.id +
                                                    PsConst.HERO_TAG__TITLE,
                                                heroTagOriginalPrice: provider
                                                        .hashCode
                                                        .toString() +
                                                    product.id +
                                                    PsConst
                                                        .HERO_TAG__ORIGINAL_PRICE,
                                                heroTagUnitPrice: provider
                                                        .hashCode
                                                        .toString() +
                                                    product.id +
                                                    PsConst
                                                        .HERO_TAG__UNIT_PRICE,
                                              );
                                              Navigator.pushNamed(context,
                                                  RoutePaths.productDetail,
                                                  arguments: holder);
                                            }),
                                      );
                                      // return ProductVeticalListItem(
                                      //   productDetailProvider:
                                      //       productDetailProvider,
                                      //   coreTagKey: provider.hashCode
                                      //           .toString() +
                                      //       provider.productList.data[index].id,
                                      //   animationController:
                                      //       widget.animationController,
                                      //   animation:
                                      //       Tween<double>(begin: 0.0, end: 1.0)
                                      //           .animate(
                                      //     CurvedAnimation(
                                      //       parent: widget.animationController,
                                      //       curve: Interval(
                                      //           (1 / count) * index, 1.0,
                                      //           curve: Curves.fastOutSlowIn),
                                      //     ),
                                      //   ),
                                      //   product:
                                      //       provider.productList.data[index],
                                      //   onTap: () {
                                      //     final Product product =
                                      //         provider.productList.data[index];
                                      //     final ProductDetailIntentHolder
                                      //         holder =
                                      //         ProductDetailIntentHolder(
                                      //       productId: product.id,
                                      //       heroTagImage:
                                      //           provider.hashCode.toString() +
                                      //               product.id +
                                      //               PsConst.HERO_TAG__IMAGE,
                                      //       heroTagTitle:
                                      //           provider.hashCode.toString() +
                                      //               product.id +
                                      //               PsConst.HERO_TAG__TITLE,
                                      //       heroTagOriginalPrice: provider
                                      //               .hashCode
                                      //               .toString() +
                                      //           product.id +
                                      //           PsConst
                                      //               .HERO_TAG__ORIGINAL_PRICE,
                                      //       heroTagUnitPrice: provider.hashCode
                                      //               .toString() +
                                      //           product.id +
                                      //           PsConst.HERO_TAG__UNIT_PRICE,
                                      //     );

                                      //     Navigator.pushNamed(
                                      //         context, RoutePaths.productDetail,
                                      //         arguments: holder);
                                      //   },
                                      // );
                                    } else {
                                      return null;
                                    }
                                  },
                                  childCount: provider.productList.data.length,
                                ),
                              ),
                            ],
                          ),
                          onRefresh: () {
                            return provider.resetLatestProductList(
                                _searchProductProvider.productParameterHolder);
                          },
                        ),
                      )
                    else if (provider.productList.status !=
                            PsStatus.PROGRESS_LOADING &&
                        provider.productList.status != PsStatus.BLOCK_LOADING &&
                        provider.productList.status != PsStatus.NOACTION)
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 20.0,
                              vertical: 15.0,
                            ),
                            child: Image.asset(
                              'assets/images/baseline_empty_item_grey_24.png',
                              height: 200,
                              fit: BoxFit.contain,
                            ),
                          ),
                          const SizedBox(
                            height: PsDimens.space20,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 40.0,
                            ),
                            child: Text(
                              Utils.getString(
                                  context, 'procuct_list__no_result_data'),
                              textAlign: TextAlign.center,
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6
                                  .copyWith(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 24.0),
                            ),
                          ),
                          const SizedBox(
                            height: PsDimens.space20,
                          ),
                        ],
                      ),
                    //  TODO: create bottom navigation bar
                    // Positioned(
                    //   bottom: _offset,
                    //   width: MediaQuery.of(context).size.width,
                    //   child: Container(
                    //     margin: const EdgeInsets.only(
                    //         left: PsDimens.space12,
                    //         top: PsDimens.space8,
                    //         right: PsDimens.space12,
                    //         bottom: PsDimens.space16),
                    //     child: Container(
                    //       width: double.infinity,
                    //       height: _containerMaxHeight,
                    //       child: BottomNavigationImageAndText(
                    //         searchProductProvider: _searchProductProvider,
                    //         changeAppBarTitle: widget.changeAppBarTitle,
                    //       ),
                    //     ),
                    //   ),
                    // ),
                    PSProgressIndicator(provider.productList.status),
                  ],
                ),
              )
            ],
          );
        },
      ),
    );
  }
}

// class BottomNavigationImageAndText extends StatefulWidget {
//   const BottomNavigationImageAndText(
//       {this.searchProductProvider, this.changeAppBarTitle});
//   final SearchProductProvider searchProductProvider;
//   final Function changeAppBarTitle;

//   @override
//   _BottomNavigationImageAndTextState createState() =>
//       _BottomNavigationImageAndTextState();
// }

// class _BottomNavigationImageAndTextState
//     extends State<BottomNavigationImageAndText> {
//   bool isClickBaseLineList = false;
//   bool isClickBaseLineTune = false;

//   @override
//   Widget build(BuildContext context) {
//     if (widget.searchProductProvider.productParameterHolder.isFiltered()) {
//       isClickBaseLineTune = true;
//     }

//     if (widget.searchProductProvider.productParameterHolder
//         .isCatAndSubCatFiltered()) {
//       isClickBaseLineList = true;
//     }

//     return Container(
//       decoration: BoxDecoration(
//           border: Border.all(color: PsColors.mainLightShadowColor),
//           boxShadow: <BoxShadow>[
//             BoxShadow(
//                 color: PsColors.mainShadowColor,
//                 offset: const Offset(1.1, 1.1),
//                 blurRadius: 10.0),
//           ],
//           color: PsColors.coreBackgroundColor,
//           borderRadius:
//               const BorderRadius.all(Radius.circular(PsDimens.space8))),
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//         children: <Widget>[
//           GestureDetector(
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: <Widget>[
//                 PsIconWithCheck(
//                   icon: MaterialCommunityIcons.format_list_bulleted_type,
//                   color: isClickBaseLineList
//                       ? PsColors.mainColor
//                       : PsColors.iconColor,
//                 ),
//                 Text(Utils.getString(context, 'search__category'),
//                     style: Theme.of(context).textTheme.bodyText1.copyWith(
//                         color: isClickBaseLineList
//                             ? PsColors.mainColor
//                             : PsColors.textPrimaryColor)),
//               ],
//             ),
//             onTap: () async {
//               final Map<String, String> dataHolder = <String, String>{};
//               dataHolder[PsConst.CATEGORY_ID] =
//                   widget.searchProductProvider.productParameterHolder.catId;
//               dataHolder[PsConst.SUB_CATEGORY_ID] =
//                   widget.searchProductProvider.productParameterHolder.subCatId;
//               final dynamic result = await Navigator.pushNamed(
//                   context, RoutePaths.filterExpantion,
//                   arguments: dataHolder);

//               if (result != null) {
//                 widget.searchProductProvider.productParameterHolder.catId =
//                     result[PsConst.CATEGORY_ID];
//                 widget.searchProductProvider.productParameterHolder.subCatId =
//                     result[PsConst.SUB_CATEGORY_ID];
//                 widget.searchProductProvider.resetLatestProductList(
//                     widget.searchProductProvider.productParameterHolder);

//                 if (result[PsConst.CATEGORY_ID] == '' &&
//                     result[PsConst.SUB_CATEGORY_ID] == '') {
//                   isClickBaseLineList = false;
//                 } else {
//                   widget.changeAppBarTitle(result[PsConst.CATEGORY_NAME]);
//                   isClickBaseLineList = true;
//                 }
//               }
//             },
//           ),
//           GestureDetector(
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: <Widget>[
//                 PsIconWithCheck(
//                   icon: Icons.filter_list,
//                   color: isClickBaseLineTune
//                       ? PsColors.mainColor
//                       : PsColors.iconColor,
//                 ),
//                 Text(Utils.getString(context, 'search__filter'),
//                     style: Theme.of(context).textTheme.bodyText1.copyWith(
//                         color: isClickBaseLineTune
//                             ? PsColors.mainColor
//                             : PsColors.textPrimaryColor))
//               ],
//             ),
//             onTap: () async {
//               final dynamic result = await Navigator.pushNamed(
//                   context, RoutePaths.itemSearch,
//                   arguments:
//                       widget.searchProductProvider.productParameterHolder);
//               if (result != null) {
//                 widget.searchProductProvider.productParameterHolder = result;
//                 widget.searchProductProvider.resetLatestProductList(
//                     widget.searchProductProvider.productParameterHolder);

//                 if (widget.searchProductProvider.productParameterHolder
//                     .isFiltered()) {
//                   isClickBaseLineTune = true;
//                 } else {
//                   isClickBaseLineTune = false;
//                 }
//               }
//             },
//           ),
//           GestureDetector(
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: <Widget>[
//                 PsIconWithCheck(
//                   icon: Icons.sort,
//                   color: PsColors.mainColor,
//                 ),
//                 Text(Utils.getString(context, 'search__sort'),
//                     style: Theme.of(context).textTheme.bodyText1.copyWith(
//                         color: isClickBaseLineTune
//                             ? PsColors.mainColor
//                             : PsColors.textPrimaryColor))
//               ],
//             ),
//             onTap: () async {
//               final dynamic result = await Navigator.pushNamed(
//                   context, RoutePaths.itemSort,
//                   arguments:
//                       widget.searchProductProvider.productParameterHolder);
//               if (result != null) {
//                 widget.searchProductProvider.productParameterHolder = result;
//                 widget.searchProductProvider.resetLatestProductList(
//                     widget.searchProductProvider.productParameterHolder);
//               }
//             },
//           ),
//         ],
//       ),
//     );
//   }
// }

// class PsIconWithCheck extends StatelessWidget {
//   const PsIconWithCheck({Key key, this.icon, this.color}) : super(key: key);
//   final IconData icon;
//   final Color color;

//   @override
//   Widget build(BuildContext context) {
//     return Icon(icon, color: color ?? PsColors.grey);
//   }
// }
