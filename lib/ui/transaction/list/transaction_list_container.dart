import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/ui/dashboard/view_pages/call_bottom_view.dart';
import 'package:biomart/ui/transaction/list/transaction_list_view.dart';
import 'package:biomart/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TransactionListContainerView extends StatefulWidget {
  @override
  _TransactionListContainerViewState createState() =>
      _TransactionListContainerViewState();
}

class _TransactionListContainerViewState
    extends State<TransactionListContainerView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
    print(
        '............................Build UI Again ............................');
    return Scaffold(
      bottomNavigationBar: const BottomBarViewWidget(
                  currentIdex: 2,
                ),
      key: scaffoldKey,
      backgroundColor: PsColors.coreBackgroundColor,
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle(
            statusBarIconBrightness: Utils.getBrightnessForAppBar(context)),
        iconTheme: Theme.of(context)
            .iconTheme
            .copyWith(color: PsColors.mainColorWithWhite),
        title: Text(
          Utils.getString(context, 'transaction_list__title'),
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headline6.copyWith(
              fontWeight: FontWeight.bold,
              color: PsColors.mainColorWithWhite),
        ),
        elevation: 0,
      ),
      body: TransactionListView(
        scaffoldKey: scaffoldKey,
        animationController: animationController,
      ),
    );
  }
}
