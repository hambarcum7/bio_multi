import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_constants.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/constant/route_paths.dart';
import 'package:biomart/provider/product/search_product_provider.dart';
import 'package:biomart/provider/user/user_provider.dart';
import 'package:biomart/repository/product_repository.dart';
import 'package:biomart/repository/user_repository.dart';
import 'package:biomart/ui/common/dialog/error_dialog.dart';
import 'package:biomart/ui/common/ps_admob_banner_widget.dart';
import 'package:biomart/ui/common/ps_button_widget.dart';
import 'package:biomart/ui/common/ps_dropdown_base_widget.dart';
import 'package:biomart/ui/common/ps_special_check_text_widget.dart';
import 'package:biomart/ui/common/ps_textfield_widget.dart';
import 'package:biomart/ui/dashboard/view_pages/call_bottom_view.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/category.dart';
import 'package:biomart/viewobject/holder/intent_holder/product_list_intent_holder.dart';
import 'package:biomart/viewobject/holder/product_parameter_holder.dart';
import 'package:biomart/viewobject/sub_category.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';

class HomeItemSearchContainerView extends StatefulWidget {
  HomeItemSearchContainerView({
    @required this.productParameterHolder,
    this.filterIsOpened = false,
  });
  final ProductParameterHolder productParameterHolder;
  bool filterIsOpened;
  @override
  _CityHomeItemSearchContainerViewState createState() =>
      _CityHomeItemSearchContainerViewState();
}

class _CityHomeItemSearchContainerViewState
    extends State<HomeItemSearchContainerView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  UserProvider userProvider;
  UserRepository userRepo;

  @override
  Widget build(BuildContext context) {
    //final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
    print(
        '............................Build UI Again ............................');
    userRepo = Provider.of<UserRepository>(context);
    return HomeItemSearchView(
      animation: Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
          parent: animationController,
          curve: const Interval((1 / 4) * 2, 1.0, curve: Curves.fastOutSlowIn),
        ),
      ),
      animationController: animationController,
      productParameterHolder: widget.productParameterHolder,
      filterIsOpened: widget.filterIsOpened,
    );
  }
}

class HomeItemSearchView extends StatefulWidget {
  const HomeItemSearchView({
    @required this.productParameterHolder,
    @required this.animation,
    @required this.animationController,
    @required this.filterIsOpened,
  });

  final ProductParameterHolder productParameterHolder;
  final AnimationController animationController;
  final Animation<double> animation;
  final bool filterIsOpened;

  @override
  _ItemSearchViewState createState() => _ItemSearchViewState();
}

class _ItemSearchViewState extends State<HomeItemSearchView> {
  ProductRepository repo1;
  SearchProductProvider _searchProductProvider;

  final TextEditingController userInputItemNameTextEditingController =
      TextEditingController();
  final TextEditingController userInputMaximunPriceEditingController =
      TextEditingController();
  final TextEditingController userInputMinimumPriceEditingController =
      TextEditingController();

  bool isConnectedToInternet = false;
  bool isSuccessfullyLoaded = true;
  bool bindDataFirstTime = true;
  bool isFilterOpened = false;

  void checkConnection() {
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
      if (isConnectedToInternet && PsConfig.showAdMob) {
        setState(() {});
      }
    });
  }

  @override
  void initState() {
    isFilterOpened = widget.filterIsOpened;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (!isConnectedToInternet && PsConfig.showAdMob) {
      print('loading ads....');
      checkConnection();
    }
    print(
        '............................Build UI Again ............................');

    Future<void> searchOnTap() async {
      if (userInputItemNameTextEditingController.text != null &&
          userInputItemNameTextEditingController.text != '') {
        _searchProductProvider.productParameterHolder.searchTerm =
            userInputItemNameTextEditingController.text;
      } else {
        _searchProductProvider.productParameterHolder.searchTerm = '';
      }
      if (userInputMaximunPriceEditingController.text != null) {
        _searchProductProvider.productParameterHolder.maxPrice =
            userInputMaximunPriceEditingController.text;
      } else {
        _searchProductProvider.productParameterHolder.maxPrice = '';
      }
      if (userInputMinimumPriceEditingController.text != null) {
        _searchProductProvider.productParameterHolder.minPrice =
            userInputMinimumPriceEditingController.text;
      } else {
        _searchProductProvider.productParameterHolder.minPrice = '';
      }
      if (_searchProductProvider.isfirstRatingClicked) {
        _searchProductProvider.productParameterHolder.overallRating =
            PsConst.RATING_ONE;
      }

      if (_searchProductProvider.isSecondRatingClicked) {
        _searchProductProvider.productParameterHolder.overallRating =
            PsConst.RATING_TWO;
      }

      if (_searchProductProvider.isThirdRatingClicked) {
        _searchProductProvider.productParameterHolder.overallRating =
            PsConst.RATING_THREE;
      }

      if (_searchProductProvider.isfouthRatingClicked) {
        _searchProductProvider.productParameterHolder.overallRating =
            PsConst.RATING_FOUR;
      }

      if (_searchProductProvider.isFifthRatingClicked) {
        _searchProductProvider.productParameterHolder.overallRating =
            PsConst.RATING_FIVE;
      }

      if (_searchProductProvider.isSwitchedFeaturedProduct) {
        _searchProductProvider.productParameterHolder.isFeatured =
            PsConst.IS_FEATURED;
      } else {
        _searchProductProvider.productParameterHolder.isFeatured = PsConst.ZERO;
      }
      if (_searchProductProvider.isSwitchedDiscountPrice) {
        _searchProductProvider.productParameterHolder.isDiscount =
            PsConst.IS_DISCOUNT;
      } else {
        _searchProductProvider.productParameterHolder.isDiscount = PsConst.ZERO;
      }
      if (_searchProductProvider.categoryId != null) {
        _searchProductProvider.productParameterHolder.catId =
            _searchProductProvider.categoryId;
      }
      if (_searchProductProvider.subCategoryId != null) {
        _searchProductProvider.productParameterHolder.subCatId =
            _searchProductProvider.subCategoryId;
      }
      print('userInputText' + userInputItemNameTextEditingController.text);
      final dynamic result =
          await Navigator.pushNamed(context, RoutePaths.filterProductList,
              arguments: ProductListIntentHolder(
                appBarTitle:
                    Utils.getString(context, 'home_search__app_bar_title'),
                productParameterHolder:
                    _searchProductProvider.productParameterHolder,
              ));
      if (result != null && result is ProductParameterHolder) {
        _searchProductProvider.productParameterHolder = result;
      }
    }

    repo1 = Provider.of<ProductRepository>(context);
    return Scaffold(
      bottomNavigationBar: const BottomBarViewWidget(
        currentIdex: 0,
      ),
      backgroundColor: PsColors.coreBackgroundColor,
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Hero(
            tag: 'search',
            child: Icon(
              Icons.arrow_back_ios,
              size: 17.0,
              color: PsColors.textPrimaryColor,
            ),
          ),
        ),
        title: Container(
          height: 42,
          padding: const EdgeInsets.only(left: 15.0),
          margin: const EdgeInsets.only(right: 8.0, top: 8.0, bottom: 8.0),
          decoration: BoxDecoration(
              color: PsColors.greyColorForCustomWidget,
              borderRadius: BorderRadius.circular(12)),
          child: TextField(
            controller: userInputItemNameTextEditingController,
            decoration: InputDecoration(
              hintText: Utils.getString(context, 'home__bottom_app_bar_search'),
              border: InputBorder.none,
            ),
            style: TextStyle(
              fontSize: 19.0,
              fontWeight: FontWeight.w500,
              color: PsColors.textPrimaryColor,
            ),
          ),
        ),
        actions: <Widget>[
          GestureDetector(
            onTap: searchOnTap,
            child: Container(
              margin: const EdgeInsets.only(right: 8.0),
              width: 24.0,
              height: 24.0,
              child: Icon(
                Icons.search,
                color: PsColors.textPrimaryColor,
              ),
            ),
          ),
        ],
      ),
      body: CustomScrollView(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.only(
                  left: 16.0,
                  top: 30.0,
                  bottom: 30,
                ),
                child: Row(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          isFilterOpened = !isFilterOpened;
                        });
                      },
                      child: Text(
                        isFilterOpened
                            ? '- ' + Utils.getString(context, 'search__filter')
                            : '+ ' + Utils.getString(context, 'search__filter'),
                        style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.w400,
                          color: PsColors.textPrimaryColor,
                          fontFamily: PsConfig.ps_default_font_family,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: ChangeNotifierProvider<SearchProductProvider>(
                lazy: false,
                create: (BuildContext content) {
                  _searchProductProvider = SearchProductProvider(repo: repo1);
                  _searchProductProvider.productParameterHolder =
                      widget.productParameterHolder;
                  _searchProductProvider.loadProductListByKey(
                      _searchProductProvider.productParameterHolder);

                  return _searchProductProvider;
                },
                child: Consumer<SearchProductProvider>(
                  builder: (BuildContext context,
                      SearchProductProvider provider, Widget child) {
                    if (bindDataFirstTime) {
                      userInputItemNameTextEditingController.text =
                          widget.productParameterHolder.searchTerm;
                      bindDataFirstTime = false;
                    }
                    if (_searchProductProvider.productList != null &&
                        _searchProductProvider.productList.data != null) {
                      widget.animationController.forward();
                      return Visibility(
                        visible: isFilterOpened,
                        replacement: Container(
                          margin: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height / 4),
                          child: Column(
                            children: <Widget>[
                              Image.asset(
                                'assets/home_images/empty_search.png',
                                width: 100,
                                height: 100,
                              ),
                              Text(
                                Utils.getString(
                                  context,
                                  'home__bottom_app_bar_search',
                                ),
                                style: TextStyle(
                                  color: PsColors.textPrimaryColor,
                                  fontSize: 17.0,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                        ),
                        child: SingleChildScrollView(
                          child: AnimatedBuilder(
                              animation: widget.animationController,
                              child: Container(
                                color: PsColors.baseColor,
                                child: Column(
                                  children: <Widget>[
                                    const PsAdMobBannerWidget(),
                                    // _ProductNameWidget(
                                    //   userInputItemNameTextEditingController:
                                    //       userInputItemNameTextEditingController,
                                    // ),
                                    _PriceWidget(
                                      userInputMinimumPriceEditingController:
                                          userInputMinimumPriceEditingController,
                                      userInputMaximunPriceEditingController:
                                          userInputMaximunPriceEditingController,
                                    ),
                                    _RatingRangeWidget(),
                                    _SpecialCheckWidget(),
                                    Container(
                                      margin: const EdgeInsets.only(
                                          left: PsDimens.space16,
                                          top: PsDimens.space16,
                                          right: PsDimens.space16,
                                          bottom: PsDimens.space40),
                                      child: PSButtonWidget(
                                        hasShadow: true,
                                        width: double.infinity,
                                        titleText: Utils.getString(
                                            context, 'home_search__search'),
                                        onPressed: searchOnTap,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              builder: (BuildContext context, Widget child) {
                                return FadeTransition(
                                    opacity: widget.animation,
                                    child: Transform(
                                      transform: Matrix4.translationValues(
                                          0.0,
                                          100 * (1.0 - widget.animation.value),
                                          0.0),
                                      child: child,
                                    ));
                              }),
                        ),
                      );
                    } else {
                      return Container(
                        color: PsColors.coreBackgroundColor,
                      );
                    }
                  },
                ),
              ),
            ),
          ]),
    );
  }
}

class _ProductNameWidget extends StatefulWidget {
  const _ProductNameWidget({this.userInputItemNameTextEditingController});

  final TextEditingController userInputItemNameTextEditingController;

  @override
  __ProductNameWidgetState createState() => __ProductNameWidgetState();
}

class __ProductNameWidgetState extends State<_ProductNameWidget> {
  @override
  Widget build(BuildContext context) {
    print('*****' + widget.userInputItemNameTextEditingController.text);
    return Column(
      children: <Widget>[
        PsTextFieldWidget(
            titleText: Utils.getString(context, 'home_search__product_name'),
            textAboutMe: false,
            hintText:
                Utils.getString(context, 'home_search__product_name_hint'),
            textEditingController:
                widget.userInputItemNameTextEditingController),
        PsDropdownBaseWidget(
            title: Utils.getString(context, 'search__category'),
            selectedText:
                Provider.of<SearchProductProvider>(context, listen: false)
                    .selectedCategoryName,
            onTap: () async {
              final SearchProductProvider provider =
                  Provider.of<SearchProductProvider>(context, listen: false);

              final dynamic categoryResult =
                  await Navigator.pushNamed(context, RoutePaths.searchCategory);

              if (categoryResult != null && categoryResult is Category) {
                provider.categoryId = categoryResult.id;
                provider.subCategoryId = '';

                setState(() {
                  provider.selectedCategoryName = categoryResult.name;
                  provider.selectedSubCategoryName = '';
                });
              }
            }),
        PsDropdownBaseWidget(
            title: Utils.getString(context, 'search__sub_category'),
            selectedText:
                Provider.of<SearchProductProvider>(context, listen: false)
                    .selectedSubCategoryName,
            onTap: () async {
              final SearchProductProvider provider =
                  Provider.of<SearchProductProvider>(context, listen: false);
              if (provider.categoryId != '') {
                final dynamic subCategoryResult = await Navigator.pushNamed(
                    context, RoutePaths.searchSubCategory,
                    arguments: provider.categoryId);
                if (subCategoryResult != null &&
                    subCategoryResult is SubCategory) {
                  provider.subCategoryId = subCategoryResult.id;

                  provider.selectedSubCategoryName = subCategoryResult.name;
                }
              } else {
                showDialog<dynamic>(
                    context: context,
                    builder: (BuildContext context) {
                      return ErrorDialog(
                        message: Utils.getString(
                            context, 'home_search__choose_category_first'),
                      );
                    });
                const ErrorDialog(message: 'Choose Category first');
              }
            }),
      ],
    );
  }
}

class _ChangeRatingColor extends StatelessWidget {
  const _ChangeRatingColor({
    Key key,
    @required this.title,
    @required this.checkColor,
  }) : super(key: key);

  final String title;
  final bool checkColor;

  @override
  Widget build(BuildContext context) {
    final Color defaultBackgroundColor = PsColors.coreBackgroundColor;
    return Container(
      width: MediaQuery.of(context).size.width / 5.5,
      height: PsDimens.space104,
      decoration: BoxDecoration(
        color: checkColor ? defaultBackgroundColor : PsColors.mainColor,
      ),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Icon(
              Icons.star,
              color: checkColor ? PsColors.iconColor : PsColors.white,
            ),
            Text(
              title,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.caption.copyWith(
                    color: checkColor ? PsColors.iconColor : PsColors.white,
                  ),
            ),
          ],
        ),
      ),
    );
  }
}

class _RatingRangeWidget extends StatefulWidget {
  @override
  __RatingRangeWidgetState createState() => __RatingRangeWidgetState();
}

class __RatingRangeWidgetState extends State<_RatingRangeWidget> {
  @override
  Widget build(BuildContext context) {
    final SearchProductProvider provider =
        Provider.of<SearchProductProvider>(context);

    dynamic _firstRatingRangeSelected() {
      if (!provider.isfirstRatingClicked) {
        return _ChangeRatingColor(
          title: Utils.getString(context, 'home_search__one_and_higher'),
          checkColor: true,
        );
      } else {
        return _ChangeRatingColor(
          title: Utils.getString(context, 'home_search__one_and_higher'),
          checkColor: false,
        );
      }
    }

    dynamic _secondRatingRangeSelected() {
      if (!provider.isSecondRatingClicked) {
        return _ChangeRatingColor(
          title: Utils.getString(context, 'home_search__two_and_higher'),
          checkColor: true,
        );
      } else {
        return _ChangeRatingColor(
          title: Utils.getString(context, 'home_search__two_and_higher'),
          checkColor: false,
        );
      }
    }

    dynamic _thirdRatingRangeSelected() {
      if (!provider.isThirdRatingClicked) {
        return _ChangeRatingColor(
          title: Utils.getString(context, 'home_search__three_and_higher'),
          checkColor: true,
        );
      } else {
        return _ChangeRatingColor(
          title: Utils.getString(context, 'home_search__three_and_higher'),
          checkColor: false,
        );
      }
    }

    dynamic _fouthRatingRangeSelected() {
      if (!provider.isfouthRatingClicked) {
        return _ChangeRatingColor(
          title: Utils.getString(context, 'home_search__four_and_higher'),
          checkColor: true,
        );
      } else {
        return _ChangeRatingColor(
          title: Utils.getString(context, 'home_search__four_and_higher'),
          checkColor: false,
        );
      }
    }

    dynamic _fifthRatingRangeSelected() {
      if (!provider.isFifthRatingClicked) {
        return _ChangeRatingColor(
          title: Utils.getString(context, 'home_search__five'),
          checkColor: true,
        );
      } else {
        return _ChangeRatingColor(
          title: Utils.getString(context, 'home_search__five'),
          checkColor: false,
        );
      }
    }

    return Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Container(
          margin: const EdgeInsets.all(PsDimens.space12),
          child: Row(
            children: <Widget>[
              Text(Utils.getString(context, 'home_search__rating_range'),
                  style: Theme.of(context).textTheme.bodyText1),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width / 5.5,
              decoration: const BoxDecoration(),
              child: InkWell(
                onTap: () {
                  if (!provider.isfirstRatingClicked) {
                    provider.isfirstRatingClicked = true;
                    provider.isSecondRatingClicked = false;
                    provider.isThirdRatingClicked = false;
                    provider.isfouthRatingClicked = false;
                    provider.isFifthRatingClicked = false;
                  } else {
                    setAllRatingFalse(provider);
                  }

                  setState(() {});
                },
                child: _firstRatingRangeSelected(),
              ),
            ),
            Container(
              margin: const EdgeInsets.all(PsDimens.space4),
              width: MediaQuery.of(context).size.width / 5.5,
              decoration: const BoxDecoration(),
              child: InkWell(
                onTap: () {
                  if (!provider.isSecondRatingClicked) {
                    provider.isfirstRatingClicked = false;
                    provider.isSecondRatingClicked = true;
                    provider.isThirdRatingClicked = false;
                    provider.isfouthRatingClicked = false;
                    provider.isFifthRatingClicked = false;
                  } else {
                    setAllRatingFalse(provider);
                  }

                  setState(() {});
                },
                child: _secondRatingRangeSelected(),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 5.5,
              decoration: const BoxDecoration(),
              child: InkWell(
                onTap: () {
                  if (!provider.isThirdRatingClicked) {
                    provider.isfirstRatingClicked = false;
                    provider.isSecondRatingClicked = false;
                    provider.isThirdRatingClicked = true;
                    provider.isfouthRatingClicked = false;
                    provider.isFifthRatingClicked = false;
                  } else {
                    setAllRatingFalse(provider);
                  }

                  setState(() {});
                },
                child: _thirdRatingRangeSelected(),
              ),
            ),
            Container(
              margin: const EdgeInsets.all(PsDimens.space4),
              width: MediaQuery.of(context).size.width / 5.5,
              decoration: const BoxDecoration(),
              child: InkWell(
                onTap: () {
                  if (!provider.isfouthRatingClicked) {
                    provider.isfirstRatingClicked = false;
                    provider.isSecondRatingClicked = false;
                    provider.isThirdRatingClicked = false;
                    provider.isfouthRatingClicked = true;
                    provider.isFifthRatingClicked = false;
                  } else {
                    setAllRatingFalse(provider);
                  }

                  setState(() {});
                },
                child: _fouthRatingRangeSelected(),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width / 5.5,
              child: InkWell(
                onTap: () {
                  if (!provider.isFifthRatingClicked) {
                    provider.isfirstRatingClicked = false;
                    provider.isSecondRatingClicked = false;
                    provider.isThirdRatingClicked = false;
                    provider.isfouthRatingClicked = false;
                    provider.isFifthRatingClicked = true;
                  } else {
                    setAllRatingFalse(provider);
                  }

                  setState(() {});
                },
                child: _fifthRatingRangeSelected(),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

dynamic setAllRatingFalse(SearchProductProvider provider) {
  provider.isfirstRatingClicked = false;
  provider.isSecondRatingClicked = false;
  provider.isThirdRatingClicked = false;
  provider.isfouthRatingClicked = false;
  provider.isFifthRatingClicked = false;
}

class _PriceWidget extends StatelessWidget {
  const _PriceWidget(
      {this.userInputMinimumPriceEditingController,
      this.userInputMaximunPriceEditingController});
  final TextEditingController userInputMinimumPriceEditingController;
  final TextEditingController userInputMaximunPriceEditingController;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.all(PsDimens.space12),
          child: Row(
            children: <Widget>[
              Text(Utils.getString(context, 'home_search__price'),
                  style: Theme.of(context).textTheme.bodyText1),
            ],
          ),
        ),
        _PriceTextWidget(
            title: Utils.getString(context, 'home_search__lowest_price'),
            textField: TextField(
                maxLines: null,
                style: Theme.of(context).textTheme.bodyText2,
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.only(
                      left: PsDimens.space8, bottom: PsDimens.space12),
                  border: InputBorder.none,
                  hintText: Utils.getString(context, 'home_search__not_set'),
                  hintStyle: Theme.of(context)
                      .textTheme
                      .bodyText2
                      .copyWith(color: PsColors.textPrimaryLightColor),
                ),
                keyboardType: TextInputType.number,
                controller: userInputMinimumPriceEditingController)),
        const Divider(
          height: PsDimens.space1,
        ),
        _PriceTextWidget(
            title: Utils.getString(context, 'home_search__highest_price'),
            textField: TextField(
                maxLines: null,
                style: Theme.of(context).textTheme.bodyText2,
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.only(
                      left: PsDimens.space8, bottom: PsDimens.space12),
                  border: InputBorder.none,
                  hintText: Utils.getString(context, 'home_search__not_set'),
                  hintStyle: Theme.of(context)
                      .textTheme
                      .bodyText2
                      .copyWith(color: PsColors.textPrimaryLightColor),
                ),
                keyboardType: TextInputType.number,
                controller: userInputMaximunPriceEditingController)),
      ],
    );
  }
}

class _PriceTextWidget extends StatelessWidget {
  const _PriceTextWidget({
    Key key,
    @required this.title,
    @required this.textField,
  }) : super(key: key);

  final String title;
  final TextField textField;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: PsColors.coreBackgroundColor,
      child: Container(
        margin: const EdgeInsets.all(PsDimens.space12),
        alignment: Alignment.centerLeft,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(title, style: Theme.of(context).textTheme.bodyText2),
            Container(
                decoration: BoxDecoration(
                  color: PsColors.coreBackgroundColor,
                  borderRadius: BorderRadius.circular(PsDimens.space4),
                  border: Border.all(color: PsColors.mainDividerColor),
                ),
                width: PsDimens.space120,
                height: PsDimens.space36,
                child: textField),
          ],
        ),
      ),
    );
  }
}

class _SpecialCheckWidget extends StatefulWidget {
  @override
  __SpecialCheckWidgetState createState() => __SpecialCheckWidgetState();
}

class __SpecialCheckWidgetState extends State<_SpecialCheckWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.all(PsDimens.space12),
          child: Row(
            children: <Widget>[
              Text(Utils.getString(context, 'home_search__special_check'),
                  style: Theme.of(context).textTheme.bodyText1),
            ],
          ),
        ),
        SpecialCheckTextWidget(
            title: Utils.getString(context, 'home_search__featured_product'),
            icon: FontAwesome5.gem,
            checkTitle: 1,
            size: PsDimens.space18),
        const Divider(
          height: PsDimens.space1,
        ),
        SpecialCheckTextWidget(
            title: Utils.getString(context, 'home_search__discount_price'),
            icon: Feather.percent,
            checkTitle: 2,
            size: PsDimens.space18),
        const Divider(
          height: PsDimens.space1,
        ),
      ],
    );
  }
}

class _SpecialCheckTextWidget extends StatefulWidget {
  const _SpecialCheckTextWidget({
    Key key,
    @required this.title,
    @required this.icon,
    @required this.checkTitle,
  }) : super(key: key);

  final String title;
  final IconData icon;
  final bool checkTitle;

  @override
  __SpecialCheckTextWidgetState createState() =>
      __SpecialCheckTextWidgetState();
}

class __SpecialCheckTextWidgetState extends State<_SpecialCheckTextWidget> {
  @override
  Widget build(BuildContext context) {
    final SearchProductProvider provider =
        Provider.of<SearchProductProvider>(context);

    return Container(
        width: double.infinity,
        height: PsDimens.space52,
        child: Container(
          margin: const EdgeInsets.all(PsDimens.space12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(
                    widget.icon,
                    size: PsDimens.space20,
                  ),
                  const SizedBox(
                    width: PsDimens.space10,
                  ),
                  Text(
                    widget.title,
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2
                        .copyWith(fontWeight: FontWeight.normal),
                  ),
                ],
              ),
              if (widget.checkTitle)
                Switch(
                  value: provider.isSwitchedFeaturedProduct,
                  onChanged: (bool value) {
                    setState(() {
                      provider.isSwitchedFeaturedProduct = value;
                    });
                  },
                  activeTrackColor: PsColors.mainColor,
                  activeColor: PsColors.mainColor,
                )
              else
                Switch(
                  value: provider.isSwitchedDiscountPrice,
                  onChanged: (bool value) {
                    setState(() {
                      provider.isSwitchedDiscountPrice = value;
                    });
                  },
                  activeTrackColor: PsColors.mainColor,
                  activeColor: PsColors.mainColor,
                ),
            ],
          ),
        ));
  }
}
