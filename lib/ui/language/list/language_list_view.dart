import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/provider/language/language_provider.dart';
import 'package:biomart/repository/language_repository.dart';
import 'package:biomart/ui/common/base/ps_widget_with_appbar.dart';
import 'package:biomart/ui/common/dialog/confirm_dialog_view.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/common/language.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

import '../item/language_list_item.dart';

class LanguageListView extends StatefulWidget {
  const LanguageListView({this.settingViewScaffoldKey});

  final GlobalKey<ScaffoldState> settingViewScaffoldKey;

  @override
  _LanguageListViewState createState() => _LanguageListViewState();
}

class _LanguageListViewState extends State<LanguageListView>
    with SingleTickerProviderStateMixin {
  LanguageRepository repo1;

  AnimationController animationController;
  Animation<double> animation;
  Language currentLanguage;
  int currentLanguageIndex;

  @override
  void dispose() {
    animationController.dispose();
    animation = null;
    super.dispose();
  }

  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    repo1 = Provider.of<LanguageRepository>(context);
    timeDilation = 1.0;

    return PsWidgetWithAppBar<LanguageProvider>(
      appBarTitle: Utils.getString(context, 'language_selection__title') ?? '',
      initProvider: () {
        return LanguageProvider(repo: repo1);
      },
      onProviderReady: (LanguageProvider provider) {
        provider.getLanguageList();
      },
      builder: (BuildContext context, LanguageProvider provider, Widget child) {
        currentLanguage = provider.getLanguage();
        List<Language> lList = provider.languageList;
        if (currentLanguage == lList[0]) {
          currentLanguageIndex = 0;
        } else if (currentLanguage == lList[1]) {
          currentLanguageIndex = 1;
        } else {
          currentLanguageIndex = 2;
        }
        return Container(
          height: MediaQuery.of(context).size.height,
          padding: const EdgeInsets.only(
            top: PsDimens.space8,
            bottom: PsDimens.space8,
          ),
          decoration: BoxDecoration(
            color: PsColors.coreBackgroundColor,
          ),
          child: ListView.builder(
            shrinkWrap: true,
            itemCount: provider.languageList.length,
            itemBuilder: (BuildContext context, int index) {
              final int count = provider.languageList.length;

              return LanguageListItem(
                  language: provider.languageList[index],
                  backgroundColor: index == currentLanguageIndex
                      ? const Color(0xff7A7A7A)
                      : PsColors.coreBackgroundColor,
                  animationController: animationController,
                  animation: Tween<double>(begin: 0.0, end: 1.0)
                      .animate(CurvedAnimation(
                    parent: animationController,
                    curve: Interval((1 / count) * index, 1.0,
                        curve: Curves.fastOutSlowIn),
                  )),
                  onTap: () {
                    showDialog<dynamic>(
                        context: context,
                        builder: (BuildContext context) {
                          return ConfirmDialogView(
                              description: Utils.getString(
                                  context, 'home__language_dialog_description'),
                              leftButtonText: Utils.getString(
                                  context, 'app_info__cancel_button_name'),
                              rightButtonText:
                                  Utils.getString(context, 'dialog__ok'),
                              onAgreeTap: () async {
                                Navigator.of(context).pop();
                                final Language result =
                                    provider.languageList[index];
                                await provider.addLanguage(result);
                                EasyLocalization.of(context).locale = Locale(
                                    result.languageCode, result.countryCode);
                                widget.settingViewScaffoldKey.currentState
                                    .setState(() {});
                                setState(() {
                                  currentLanguageIndex = index;
                                });
                                // Navigator.pop(
                                //     context, provider.languageList[index]);
                              });
                        });
                  });
            },
          ),
        );
      },
    );
  }
}
