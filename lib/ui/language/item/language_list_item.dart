import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/viewobject/common/language.dart';
import 'package:flutter/material.dart';

class LanguageListItem extends StatelessWidget {
  const LanguageListItem({
    Key key,
    @required this.language,
    @required this.animation,
    @required this.animationController,
    this.onTap,
    this.backgroundColor,
  }) : super(key: key);

  final Language language;
  final Function onTap;
  final AnimationController animationController;
  final Animation<double> animation;
  final Color backgroundColor;

  @override
  Widget build(BuildContext context) {
    animationController.forward();
    return AnimatedBuilder(
        animation: animationController,
        child: GestureDetector(
          onTap: onTap,
          child: Container(
            height: 50.0,
            //padding: const EdgeInsets.all(PsDimens.space14),
            margin: const EdgeInsets.symmetric(
              horizontal: PsDimens.space8,
              vertical: PsDimens.space8,
            ),
            decoration: BoxDecoration(
              color: backgroundColor,
              borderRadius: BorderRadius.circular(7.0),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: PsDimens.space60,
                  height: PsDimens.space36,
                  margin: const EdgeInsets.only(left: PsDimens.space14),
                  child: Image.asset('${language.flagPath}'),
                ),

                Padding(
                  padding: const EdgeInsets.only(right: 14.0),
                  child: Text(
                    language.name,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      color: PsColors.textPrimaryColor,
                      fontSize: 18.0,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                ),
                // Text(
                //   '${language.languageCode}_${language.countryCode}',
                //   maxLines: 2,
                //   overflow: TextOverflow.ellipsis,
                // ),
              ],
            ),
          ),
        ),
        builder: (BuildContext context, Widget child) {
          return FadeTransition(
            opacity: animation,
            child: Transform(
              transform: Matrix4.translationValues(
                  0.0, 100 * (1.0 - animation.value), 0.0),
              child: child,
            ),
          );
        });
  }
}
