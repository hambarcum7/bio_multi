import 'package:biomart/api/common/ps_status.dart';
import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_constants.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/constant/route_paths.dart';
import 'package:biomart/provider/basket/basket_provider.dart';
import 'package:biomart/provider/product/product_provider.dart';
import 'package:biomart/provider/product/search_product_provider.dart';
import 'package:biomart/provider/subcategory/sub_category_provider.dart';
import 'package:biomart/provider/subsubcategory/sub_sub_category_provider.dart';
import 'package:biomart/repository/basket_repository.dart';
import 'package:biomart/repository/product_repository.dart';
import 'package:biomart/repository/sub_category_repository.dart';
import 'package:biomart/repository/sub_sub_category_repository.dart';
import 'package:biomart/ui/common/ps_ui_widget.dart';
import 'package:biomart/ui/dashboard/view_pages/call_bottom_view.dart';
import 'package:biomart/ui/product/item/product_vertical_list_item_for_home.dart';
import 'package:biomart/ui/product/list_with_filter/product_list_with_filter_view.dart';
import 'package:biomart/ui/search/home_item_search_container_view.dart';
import 'package:biomart/ui/subcategory/item/sub_category_vertical_list_item.dart';
import 'package:biomart/ui/subsubcategory/item/sub_sub_category_vertical_list_item.dart';
import 'package:biomart/ui/subsubcategory/list/sub_sub_category_product_list_view.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/category.dart';
import 'package:biomart/viewobject/common/ps_value_holder.dart';
import 'package:biomart/viewobject/holder/intent_holder/product_detail_intent_holder.dart';
import 'package:biomart/viewobject/holder/intent_holder/product_list_intent_holder.dart';
import 'package:biomart/viewobject/holder/product_parameter_holder.dart';
import 'package:biomart/viewobject/product.dart';
import 'package:biomart/viewobject/sub_category.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:shimmer/shimmer.dart';

class SubSubCategoryProductListWithFilterContainerView extends StatefulWidget {
  const SubSubCategoryProductListWithFilterContainerView({
    @required this.productParameterHolder,
    @required this.appBarTitle,
    this.subCategory,
  });
  final ProductParameterHolder productParameterHolder;
  final String appBarTitle;
  final SubCategory subCategory;
  @override
  _SubSubCategoryProductListWithFilterContainerViewState createState() =>
      _SubSubCategoryProductListWithFilterContainerViewState();
}

class _SubSubCategoryProductListWithFilterContainerViewState
    extends State<SubSubCategoryProductListWithFilterContainerView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;

  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    prodParameterHolder = widget.productParameterHolder;
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  BasketRepository basketRepository;
  String appBarTitleName;
  ProductRepository productRepo;
  ProductDetailProvider productDetailProvider;
  PsValueHolder psValueHolder;
  SubSubCategoryRepository repo2;
  SubSubCategoryProvider _subSubCategoryProvider;
  int selectableIndex = 0;
  ProductParameterHolder prodParameterHolder;
  SearchProductProvider _searchProductProvider;

  void changeAppBarTitle(String categoryName) {
    appBarTitleName = categoryName;
  }

  @override
  Widget build(BuildContext context) {
    basketRepository = Provider.of<BasketRepository>(context);
    productRepo = Provider.of<ProductRepository>(context);
    psValueHolder = Provider.of<PsValueHolder>(context);
    repo2 = Provider.of<SubSubCategoryRepository>(context);

    print(
        '............................Build UI Again< Filter Container > ............................');
    return MultiProvider(
      providers: <SingleChildWidget>[
        ChangeNotifierProvider<SearchProductProvider>(
          lazy: false,
          create: (BuildContext context) {
            final SearchProductProvider provider =
                SearchProductProvider(repo: productRepo);
            //provider.loadProductListByKeyFromDB(widget.productParameterHolder);
            provider.loadProductListByKey(prodParameterHolder);
            _searchProductProvider = provider;
            return _searchProductProvider;
          },
        ),
        if (widget.subCategory != null)
          ChangeNotifierProvider<SubSubCategoryProvider>(
            lazy: false,
            create: (BuildContext context) {
              _subSubCategoryProvider = SubSubCategoryProvider(repo: repo2);
              //_subProvider = SubCategoryProvider(repo: repo1);
              _subSubCategoryProvider.subSubCategoryBySubCatIdParamenterHolder
                  .subCatId = widget.subCategory.id;
              _subSubCategoryProvider
                  .loadSubSubCategoryList(widget.subCategory.id);
              return _subSubCategoryProvider;
            },
          ),
      ],
      child: Scaffold(
        bottomNavigationBar: const BottomBarViewWidget(
          currentIdex: 0,
        ),
        backgroundColor: PsColors.coreBackgroundColor,
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(70),
          child: AppBar(
            backgroundColor: PsColors.mainColor,
            systemOverlayStyle: SystemUiOverlayStyle(
              statusBarIconBrightness: Utils.getBrightnessForAppBar(context),
            ),
            title: Text(
              appBarTitleName ?? widget.appBarTitle,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline6.copyWith(
                    fontWeight: FontWeight.w400,
                    fontSize: 24.0,
                    color: PsColors.white,
                  ),
            ),
            elevation: 0,
            actions: <Widget>[
              if (!(widget.appBarTitle != null &&
                  widget.appBarTitle ==
                      Utils.getString(context, 'home_search__app_bar_title')))
                ChangeNotifierProvider<BasketProvider>(
                  lazy: false,
                  create: (BuildContext context) {
                    final BasketProvider provider =
                        BasketProvider(repo: basketRepository);
                    provider.loadBasketList();
                    return provider;
                  },
                  child: Consumer<BasketProvider>(builder:
                      (BuildContext context, BasketProvider basketProvider,
                          Widget child) {
                    return GestureDetector(
                        child: Container(
                          width: PsDimens.space24,
                          height: PsDimens.space24,
                          margin: const EdgeInsets.only(
                            top: PsDimens.space8,
                            left: PsDimens.space8,
                            right: PsDimens.space16,
                          ),
                          child: Image.asset('assets/images/menu.png'),
                        ),
                        onTap: () {
                          final ProductParameterHolder productParameterHolder =
                              ProductParameterHolder()
                                  .getLatestParameterHolder();
                          productParameterHolder.searchTerm = '';
                          Utils.psPrint(productParameterHolder.searchTerm);

                          Navigator.pushReplacement(
                            context,
                            PageRouteBuilder<dynamic>(
                              pageBuilder: (_, Animation<double> a1,
                                      Animation<double> a2) =>
                                  HomeItemSearchContainerView(
                                productParameterHolder: productParameterHolder,
                                filterIsOpened: true,
                              ),
                            ),
                          );

                          // Navigator.pushNamed(
                          //   context,
                          //   RoutePaths.dashboardsearchFood,
                          //   arguments: ProductListIntentHolder(
                          //       appBarTitle: Utils.getString(
                          //           context, 'home_search__app_bar_title'),
                          //       productParameterHolder:
                          //           productParameterHolder),
                          // );
                        });
                  }),
                )
            ],
            bottom: widget.subCategory != null
                ? PreferredSize(
                    child: Consumer<SubSubCategoryProvider>(
                      builder: (BuildContext newContext,
                          SubSubCategoryProvider provider, Widget child) {
                        return Container(
                          height: 25,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: provider.subSubCategoryList.data != null
                                ? provider.subSubCategoryList.data.length + 1
                                : 1,
                            itemBuilder: (BuildContext context, int index) {
                              if (provider.subSubCategoryList.status ==
                                  PsStatus.BLOCK_LOADING) {
                                // print(
                                //     '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  ${provider.subCategoryList.data.length}');
                                return Shimmer.fromColors(
                                  baseColor: PsColors.grey,
                                  highlightColor: PsColors.white,
                                  child: Column(children: const <Widget>[
                                    FrameUIForLoading(),
                                    FrameUIForLoading(),
                                    FrameUIForLoading(),
                                    FrameUIForLoading(),
                                    FrameUIForLoading(),
                                  ]),
                                );
                              } else {
                                if (index == 0) {
                                  return GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        selectableIndex = 0;
                                        prodParameterHolder =
                                            widget.productParameterHolder;
                                        _searchProductProvider
                                            .loadProductListByKey(
                                                prodParameterHolder);
                                      });
                                    },
                                    child: Container(
                                      //height: 20,
                                      alignment: Alignment.center,
                                      margin: const EdgeInsets.symmetric(
                                        horizontal: 8.0,
                                      ),
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 16.0,
                                      ),
                                      decoration: selectableIndex == 0
                                          ? BoxDecoration(
                                              color: const Color.fromRGBO(
                                                  255, 255, 255, 0.17),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                PsDimens.space24,
                                              ),
                                            )
                                          : null,
                                      child: Text(
                                        Utils.getString(context,
                                            'product_list__category_all'),
                                        textAlign: TextAlign.start,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText2
                                            .copyWith(
                                              fontWeight: FontWeight.w400,
                                              color: PsColors.white,
                                              fontSize: 18.0,
                                            ),
                                      ),
                                    ),
                                  );
                                } else {
                                  return SubSubCategoryVerticalListItem(
                                    subSubCategory: _subSubCategoryProvider
                                        .subSubCategoryList.data[index - 1],
                                    onTap: () {
                                      setState(() {
                                        selectableIndex = index;
                                        prodParameterHolder =
                                            ProductParameterHolder()
                                                .getLatestParameterHolder();
                                        prodParameterHolder.subSubCatId =
                                            _subSubCategoryProvider
                                                .subSubCategoryList
                                                .data[index - 1]
                                                .id;
                                        _searchProductProvider
                                            .loadProductListByKey(
                                                prodParameterHolder);
                                      });
                                    },
                                    boxDecoration: selectableIndex == index
                                        ? BoxDecoration(
                                            color: const Color.fromRGBO(
                                                255, 255, 255, 0.17),
                                            borderRadius: BorderRadius.circular(
                                              PsDimens.space24,
                                            ),
                                          )
                                        : null,
                                  );
                                }
                              }
                            },
                          ),
                        );
                      },
                    ),
                    preferredSize: const Size.fromHeight(50),
                  )
                : PreferredSize(
                    preferredSize: const Size.fromHeight(50),
                    child: Container(
                      color: PsColors.coreBackgroundColor,
                    ),
                  ),
          ),
        ),
        body: Stack(
          children: <Widget>[
            Container(
              height: 80,
              decoration: BoxDecoration(
                color: PsColors.mainColor,
                border: Border.all(
                  width: 0,
                  color: PsColors.mainColor,
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 8),
              decoration: BoxDecoration(
                color: PsColors.coreBackgroundColor,
                border: Border.all(
                  width: 0,
                  color: PsColors.coreBackgroundColor,
                ),
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(16.0),
                    topRight: Radius.circular(16.0)),
              ),
              // child: SubSubCategoryProductListWithFilterView(
              //   animationController: animationController,
              //   productParameterHolder: prodParameterHolder,
              //   changeAppBarTitle: changeAppBarTitle,
              //   productList: _searchProductProvider.productList.data,
              // ),
              child: Column(
                children: <Widget>[
                  //const PsAdMobBannerWidget(),
                  Consumer<SearchProductProvider>(
                      builder: (context, provider, child) {
                    return Expanded(
                      child: Stack(
                        children: <Widget>[
                          if (provider.productList.data.isNotEmpty &&
                              provider.productList.data != null)
                            Container(
                              decoration: BoxDecoration(
                                color: PsColors.transparent,
                                border: Border.all(
                                  width: 0,
                                  color: PsColors.coreBackgroundColor,
                                ),
                                borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(16.0),
                                    topRight: Radius.circular(16.0)),
                              ),
                              padding: const EdgeInsets.symmetric(
                                horizontal: PsDimens.space10,
                              ),
                              child: RefreshIndicator(
                                child: CustomScrollView(
                                  //controller: _scrollController,
                                  physics:
                                      const AlwaysScrollableScrollPhysics(),
                                  scrollDirection: Axis.vertical,
                                  shrinkWrap: true,
                                  slivers: <Widget>[
                                    SliverGrid(
                                      gridDelegate:
                                          SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount:
                                            MediaQuery.of(context).size.width ~/
                                                160,
                                        crossAxisSpacing: 0,
                                        childAspectRatio: 1 / 2.1,
                                        mainAxisSpacing: 0.0,
                                        mainAxisExtent: 330,
                                      ),
                                      delegate: SliverChildBuilderDelegate(
                                        (BuildContext context, int index) {
                                          if (provider.productList.data !=
                                                  null ||
                                              provider.productList.data
                                                  .isNotEmpty) {
                                            // final int count =
                                            //     provider.productList.data.length;
                                            final Product product = provider
                                                .productList.data[index];
                                            return Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 8.0),
                                              child: ProductVerticalListItem(
                                                  coreTagKey: provider.hashCode
                                                          .toString() +
                                                      product.id,
                                                  product: provider
                                                      .productList.data[index],
                                                  productId: product.id,
                                                  onTap: () {
                                                    final ProductDetailIntentHolder
                                                        holder =
                                                        ProductDetailIntentHolder(
                                                      productId: product.id,
                                                      heroTagImage: provider
                                                              .hashCode
                                                              .toString() +
                                                          product.id +
                                                          PsConst
                                                              .HERO_TAG__IMAGE,
                                                      heroTagTitle: provider
                                                              .hashCode
                                                              .toString() +
                                                          product.id +
                                                          PsConst
                                                              .HERO_TAG__TITLE,
                                                      heroTagOriginalPrice: provider
                                                              .hashCode
                                                              .toString() +
                                                          product.id +
                                                          PsConst
                                                              .HERO_TAG__ORIGINAL_PRICE,
                                                      heroTagUnitPrice: provider
                                                              .hashCode
                                                              .toString() +
                                                          product.id +
                                                          PsConst
                                                              .HERO_TAG__UNIT_PRICE,
                                                    );
                                                    Navigator.pushNamed(
                                                        context,
                                                        RoutePaths
                                                            .productDetail,
                                                        arguments: holder);
                                                  }),
                                            );
                                            // return ProductVeticalListItem(
                                            //   productDetailProvider:
                                            //       productDetailProvider,
                                            //   coreTagKey: provider.hashCode
                                            //           .toString() +
                                            //       provider.productList.data[index].id,
                                            //   animationController:
                                            //       widget.animationController,
                                            //   animation:
                                            //       Tween<double>(begin: 0.0, end: 1.0)
                                            //           .animate(
                                            //     CurvedAnimation(
                                            //       parent: widget.animationController,
                                            //       curve: Interval(
                                            //           (1 / count) * index, 1.0,
                                            //           curve: Curves.fastOutSlowIn),
                                            //     ),
                                            //   ),
                                            //   product:
                                            //       provider.productList.data[index],
                                            //   onTap: () {
                                            //     final Product product =
                                            //         provider.productList.data[index];
                                            //     final ProductDetailIntentHolder
                                            //         holder =
                                            //         ProductDetailIntentHolder(
                                            //       productId: product.id,
                                            //       heroTagImage:
                                            //           provider.hashCode.toString() +
                                            //               product.id +
                                            //               PsConst.HERO_TAG__IMAGE,
                                            //       heroTagTitle:
                                            //           provider.hashCode.toString() +
                                            //               product.id +
                                            //               PsConst.HERO_TAG__TITLE,
                                            //       heroTagOriginalPrice: provider
                                            //               .hashCode
                                            //               .toString() +
                                            //           product.id +
                                            //           PsConst
                                            //               .HERO_TAG__ORIGINAL_PRICE,
                                            //       heroTagUnitPrice: provider.hashCode
                                            //               .toString() +
                                            //           product.id +
                                            //           PsConst.HERO_TAG__UNIT_PRICE,
                                            //     );

                                            //     Navigator.pushNamed(
                                            //         context, RoutePaths.productDetail,
                                            //         arguments: holder);
                                            //   },
                                            // );
                                          } else {
                                            return null;
                                          }
                                        },
                                        childCount:
                                            provider.productList.data.length,
                                      ),
                                    ),
                                  ],
                                ),
                                onRefresh: () {
                                  provider.productParameterHolder =
                                      prodParameterHolder;
                                  return provider.resetLatestProductList(
                                      provider.productParameterHolder);
                                },
                              ),
                            )
                          else if (provider.productList.status !=
                                  PsStatus.PROGRESS_LOADING &&
                              provider.productList.status !=
                                  PsStatus.BLOCK_LOADING &&
                              provider.productList.status != PsStatus.NOACTION)
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 20.0,
                                    vertical: 15.0,
                                  ),
                                  child: Image.asset(
                                    'assets/images/baseline_empty_item_grey_24.png',
                                    height: 200,
                                    fit: BoxFit.contain,
                                  ),
                                ),
                                const SizedBox(
                                  height: PsDimens.space20,
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 40.0,
                                  ),
                                  child: Text(
                                    Utils.getString(context,
                                        'procuct_list__no_result_data'),
                                    textAlign: TextAlign.center,
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline6
                                        .copyWith(
                                            fontWeight: FontWeight.w700,
                                            fontSize: 24.0),
                                  ),
                                ),
                                const SizedBox(
                                  height: PsDimens.space20,
                                ),
                              ],
                            ),
                          //  TODO: create bottom navigation bar
                          // Positioned(
                          //   bottom: _offset,
                          //   width: MediaQuery.of(context).size.width,
                          //   child: Container(
                          //     margin: const EdgeInsets.only(
                          //         left: PsDimens.space12,
                          //         top: PsDimens.space8,
                          //         right: PsDimens.space12,
                          //         bottom: PsDimens.space16),
                          //     child: Container(
                          //       width: double.infinity,
                          //       height: _containerMaxHeight,
                          //       child: BottomNavigationImageAndText(
                          //         searchProductProvider: _searchProductProvider,
                          //         changeAppBarTitle: widget.changeAppBarTitle,
                          //       ),
                          //     ),
                          //   ),
                          // ),
                          PSProgressIndicator(provider.productList.status),
                        ],
                      ),
                    );
                  })
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class FrameUIForLoading extends StatelessWidget {
  const FrameUIForLoading({
    Key key,
  }) : super(key: key);
//
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
            height: 70,
            width: 70,
            margin: const EdgeInsets.all(PsDimens.space16),
            decoration: BoxDecoration(color: PsColors.grey)),
        Expanded(
            child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
          Container(
              height: 15,
              margin: const EdgeInsets.all(PsDimens.space8),
              decoration: BoxDecoration(color: PsColors.grey)),
          Container(
              height: 15,
              margin: const EdgeInsets.all(PsDimens.space8),
              decoration: BoxDecoration(color: PsColors.grey)),
        ]))
      ],
    );
  }
}
//

// import 'package:biomart/api/common/ps_status.dart';
// import 'package:biomart/config/ps_colors.dart';
// import 'package:biomart/config/ps_config.dart';
// import 'package:biomart/constant/ps_dimens.dart';
// import 'package:biomart/constant/route_paths.dart';
// import 'package:biomart/provider/subsubcategory/sub_sub_category_provider.dart';
// import 'package:biomart/repository/sub_sub_category_repository.dart';
// import 'package:biomart/ui/common/ps_admob_banner_widget.dart';
// import 'package:biomart/ui/common/ps_ui_widget.dart';
// import 'package:biomart/ui/dashboard/view_pages/call_bottom_view.dart';
// import 'package:biomart/ui/subsubcategory/item/sub_sub_category_grid_item.dart';
// import 'package:biomart/utils/utils.dart';
// import 'package:biomart/viewobject/common/ps_value_holder.dart';
// import 'package:biomart/viewobject/holder/intent_holder/product_list_intent_holder.dart';
// import 'package:biomart/viewobject/sub_category.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/scheduler.dart' show timeDilation;
// import 'package:flutter/services.dart';
// import 'package:provider/provider.dart';
// import 'package:shimmer/shimmer.dart';

// class SubSubCategoryGridView extends StatefulWidget {
//   const SubSubCategoryGridView({this.subCategory});
//   final SubCategory subCategory;
//   @override
//   _ModelGridViewState createState() {
//     return _ModelGridViewState();
//   }
// }

// class _ModelGridViewState extends State<SubSubCategoryGridView>
//     with SingleTickerProviderStateMixin {
//   final ScrollController _scrollController = ScrollController();

//   SubSubCategoryProvider _subSubCategoryProvider;
//   PsValueHolder valueHolder;

//   AnimationController animationController;
//   Animation<double> animation;

//   @override
//   void dispose() {
//     animationController.dispose();
//     animation = null;
//     super.dispose();
//   }

//   @override
//   void initState() {
//     _scrollController.addListener(() {
//       if (_scrollController.position.pixels ==
//           _scrollController.position.maxScrollExtent) {
//         final String subCategId = widget.subCategory.id;
//         Utils.psPrint('CategoryId number is $subCategId');

//         _subSubCategoryProvider.nextSubSubCategoryList(
//           widget.subCategory.id,
//         );
//       }
//     });
//     animationController =
//         AnimationController(duration: PsConfig.animation_duration, vsync: this);
//     super.initState();
//   }

//   SubSubCategoryRepository repo1;
//   bool isConnectedToInternet = false;
//   bool isSuccessfullyLoaded = true;

//   void checkConnection() {
//     Utils.checkInternetConnectivity().then((bool onValue) {
//       isConnectedToInternet = onValue;
//       if (isConnectedToInternet && PsConfig.showAdMob) {
//         if (mounted) {
//           setState(() {});
//         }
//       }
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     if (!isConnectedToInternet && PsConfig.showAdMob) {
//       print('loading ads....');
//       checkConnection();
//     }
//     timeDilation = 1.0;
//     repo1 = Provider.of<SubSubCategoryRepository>(context);
//     valueHolder = Provider.of<PsValueHolder>(context);
//     // final dynamic data = EasyLocalizationProvider.of(context).data;

//     // return EasyLocalizationProvider(
//     //     data: data,
//     //     child:
//     return Scaffold(
//         bottomNavigationBar: const BottomBarViewWidget(
//           currentIdex: 0,
//         ),
//         backgroundColor: PsColors.coreBackgroundColor,
//         appBar: AppBar(
//           backgroundColor: PsColors.mainColor,
//           systemOverlayStyle: SystemUiOverlayStyle(
//               statusBarIconBrightness: Utils.getBrightnessForAppBar(context)),
//           title: Text(
//             widget.subCategory.name,
//             // style: TextStyle(color: PsColors.white),
//           ),
//           iconTheme: IconThemeData(
//             color: PsColors.white,
//           ),
//         ),
//         body: ChangeNotifierProvider<SubSubCategoryProvider>(
//             lazy: false,
//             create: (BuildContext context) {
//               _subSubCategoryProvider =
//                   SubSubCategoryProvider(repo: repo1, psValueHolder: valueHolder);
//               _subSubCategoryProvider.loadAllSubSubCategoryList(
//                 widget.subCategory.id,
//               );
//               return _subSubCategoryProvider;
//             },
//             child: Consumer<SubSubCategoryProvider>(builder: (BuildContext context,
//                 SubSubCategoryProvider provider, Widget child) {
//               return Column(
//                 children: <Widget>[
//                   const PsAdMobBannerWidget(),
//                   Expanded(
//                     child: Stack(children: <Widget>[
//                       Container(
//                           child: RefreshIndicator(
//                         onRefresh: () {
//                           return _subSubCategoryProvider.resetSubSubCategoryList(
//                             widget.subCategory.id,
//                           );
//                         },
//                         child: CustomScrollView(
//                             controller: _scrollController,
//                             physics: const AlwaysScrollableScrollPhysics(),
//                             scrollDirection: Axis.vertical,
//                             shrinkWrap: true,
//                             slivers: <Widget>[
//                               SliverGrid(
//                                 gridDelegate:
//                                     const SliverGridDelegateWithMaxCrossAxisExtent(
//                                         maxCrossAxisExtent: 240,
//                                         childAspectRatio: 1.4),
//                                 delegate: SliverChildBuilderDelegate(
//                                   (BuildContext context, int index) {
//                                     if (provider.subSubCategoryList.status ==
//                                         PsStatus.BLOCK_LOADING) {
//                                       return Shimmer.fromColors(
//                                         baseColor: PsColors.grey,
//                                         highlightColor: PsColors.white,
//                                         child: Column(
//                                           children: const <Widget>[
//                                             FrameUIForLoading(),
//                                             FrameUIForLoading(),
//                                             FrameUIForLoading(),
//                                             FrameUIForLoading(),
//                                             FrameUIForLoading(),
//                                             FrameUIForLoading(),
//                                           ],
//                                         ),
//                                       );
//                                     } else {
//                                       final int count =
//                                           provider.subSubCategoryList.data.length;
//                                       return 
//                                       SubSubCategoryGridItem(
//                                         subSubCategory: provider
//                                             .subSubCategoryList.data[index],
//                                         onTap: () {
//                                           provider.subSubCategoryBySubCatIdParamenterHolder
//                                                   .subCatId =
//                                               provider.subSubCategoryList
//                                                   .data[index].subCatId;
//                                           provider.subSubCategoryBySubCatIdParamenterHolder
//                                                   .subSubCatId =
//                                               provider.subSubCategoryList
//                                                   .data[index].subCatId;
//                                           Navigator.pushNamed(context,
//                                               RoutePaths.filterProductList,
//                                               arguments: ProductListIntentHolder(
//                                                   appBarTitle: provider
//                                                       .subSubCategoryList
//                                                       .data[index]
//                                                       .name,
//                                                   productParameterHolder: provider
//                                                       .subSubCategoryBySubCatIdParamenterHolder));
//                                         },
//                                         animationController:
//                                             animationController,
//                                         animation:
//                                             Tween<double>(begin: 0.0, end: 1.0)
//                                                 .animate(
//                                           CurvedAnimation(
//                                             parent: animationController,
//                                             curve: Interval(
//                                                 (1 / count) * index, 1.0,
//                                                 curve: Curves.fastOutSlowIn),
//                                           ),
//                                         ),
//                                       );
//                                     }
//                                   },
//                                   childCount:
//                                       provider.subSubCategoryList.data.length,
//                                 ),
//                               ),
//                             ]),
//                       )),
//                       PSProgressIndicator(
//                         provider.subSubCategoryList.status,
//                         message: provider.subSubCategoryList.message,
//                       )
//                     ]),
//                   )
//                 ],
//               );
//             }))
//         // )
//         );
//   }
// }

// class FrameUIForLoading extends StatelessWidget {
//   const FrameUIForLoading({
//     Key key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Row(
//       mainAxisSize: MainAxisSize.min,
//       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//       children: <Widget>[
//         Container(
//             height: 70,
//             width: 70,
//             margin: const EdgeInsets.all(PsDimens.space16),
//             decoration: BoxDecoration(color: PsColors.grey)),
//         Expanded(
//             child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
//           Container(
//               height: 15,
//               margin: const EdgeInsets.all(PsDimens.space8),
//               decoration: BoxDecoration(color: Colors.grey[300])),
//           Container(
//               height: 15,
//               margin: const EdgeInsets.all(PsDimens.space8),
//               decoration: const BoxDecoration(color: Colors.grey)),
//         ]))
//       ],
//     );
//   }
// }

