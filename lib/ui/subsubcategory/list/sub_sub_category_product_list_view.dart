import 'package:biomart/api/common/ps_status.dart';
import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_constants.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/constant/route_paths.dart';
import 'package:biomart/provider/product/product_provider.dart';
import 'package:biomart/provider/product/search_product_provider.dart';
import 'package:biomart/repository/product_repository.dart';
import 'package:biomart/ui/common/ps_ui_widget.dart';
import 'package:biomart/ui/product/item/product_vertical_list_item_for_home.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/common/ps_value_holder.dart';
import 'package:biomart/viewobject/holder/intent_holder/product_detail_intent_holder.dart';
import 'package:biomart/viewobject/holder/product_parameter_holder.dart';
import 'package:biomart/viewobject/product.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

class SubSubCategoryProductListWithFilterView extends StatefulWidget {
  SubSubCategoryProductListWithFilterView({
    Key key,
    @required this.productParameterHolder,
    @required this.animationController,
    this.changeAppBarTitle,
    this.productList,
    //@required this.productDetailProvider,
  }) : super(key: key);

  final ProductParameterHolder productParameterHolder;
  //final ProductDetailProvider productDetailProvider;
  final AnimationController animationController;
  final Function changeAppBarTitle;
  List<Product> productList;

  @override
  _SubSubCategoryProductListWithFilterViewState createState() =>
      _SubSubCategoryProductListWithFilterViewState();
}

class _SubSubCategoryProductListWithFilterViewState
    extends State<SubSubCategoryProductListWithFilterView>
    with TickerProviderStateMixin {
  final ScrollController _scrollController = ScrollController();

  SearchProductProvider _searchProductProvider;
  ProductDetailProvider productDetailProvider;

  bool isVisible = true;
  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    _offset = 0;
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _searchProductProvider.nextProductListByKey(
            _searchProductProvider.productParameterHolder);
      }
      //setState(() {
      final double offset = _scrollController.offset;
      _delta += offset - _oldOffset;
      if (_delta > _containerMaxHeight)
        _delta = _containerMaxHeight;
      else if (_delta < 0) {
        _delta = 0;
      }
      _oldOffset = offset;
      _offset = -_delta;
    });

    print(' Offset $_offset');
    //});
  }

  final double _containerMaxHeight = 60;
  double _offset, _delta = 0, _oldOffset = 0;
  ProductRepository repo1;
  dynamic data;
  PsValueHolder valueHolder;
  bool isConnectedToInternet = false;
  bool isSuccessfullyLoaded = true;

  void checkConnection() {
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
      if (isConnectedToInternet && PsConfig.showAdMob) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    repo1 = Provider.of<ProductRepository>(context);
    valueHolder = Provider.of<PsValueHolder>(context);

    if (!isConnectedToInternet && PsConfig.showAdMob) {
      print('loading ads....');
      checkConnection();
    }
    print(MediaQuery.of(context).size.width);
    print(
        '............................Build UI Again < Filter View >............................');
    return ChangeNotifierProvider<SearchProductProvider>(
      lazy: false,
      create: (BuildContext context) {
        final SearchProductProvider provider =
            SearchProductProvider(repo: repo1);
        //provider.loadProductListByKeyFromDB(widget.productParameterHolder);
        provider.loadProductListByKey(widget.productParameterHolder);
        _searchProductProvider = provider;
        _searchProductProvider.productParameterHolder =
            widget.productParameterHolder;
        return _searchProductProvider;
      },
      child: Consumer<SearchProductProvider>(
        builder: (BuildContext context, SearchProductProvider provider,
            Widget child) {
          return Column(
            children: <Widget>[
              //const PsAdMobBannerWidget(),
              Expanded(
                child: Stack(
                  children: <Widget>[
                    if (provider.productList.data.isNotEmpty &&
                        provider.productList.data != null)
                      Container(
                        decoration: BoxDecoration(
                          color: PsColors.transparent,
                          border: Border.all(
                            width: 0,
                            color: PsColors.coreBackgroundColor,
                          ),
                          borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(16.0),
                              topRight: Radius.circular(16.0)),
                        ),
                        padding: const EdgeInsets.symmetric(
                          horizontal: PsDimens.space10,
                        ),
                        child: RefreshIndicator(
                          child: CustomScrollView(
                            controller: _scrollController,
                            physics: const AlwaysScrollableScrollPhysics(),
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            slivers: <Widget>[
                              SliverGrid(
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount:
                                      MediaQuery.of(context).size.width ~/ 160,
                                  crossAxisSpacing: 8,
                                  childAspectRatio: 1 / 1.85,
                                  mainAxisSpacing: 0.0,
                                ),
                                delegate: SliverChildBuilderDelegate(
                                  (BuildContext context, int index) {
                                    if (provider.productList.data != null ||
                                        provider.productList.data.isNotEmpty) {
                                      // final int count =
                                      //     provider.productList.data.length;
                                      final Product product =
                                          provider.productList.data[index];
                                      return Padding(
                                        padding:
                                            const EdgeInsets.only(top: 8.0),
                                        child: ProductVerticalListItem(
                                            coreTagKey:
                                                provider.hashCode.toString() +
                                                    product.id,
                                            product: provider
                                                .productList.data[index],
                                            productId: product.id,
                                            onTap: () {
                                              final ProductDetailIntentHolder
                                                  holder =
                                                  ProductDetailIntentHolder(
                                                productId: product.id,
                                                heroTagImage: provider.hashCode
                                                        .toString() +
                                                    product.id +
                                                    PsConst.HERO_TAG__IMAGE,
                                                heroTagTitle: provider.hashCode
                                                        .toString() +
                                                    product.id +
                                                    PsConst.HERO_TAG__TITLE,
                                                heroTagOriginalPrice: provider
                                                        .hashCode
                                                        .toString() +
                                                    product.id +
                                                    PsConst
                                                        .HERO_TAG__ORIGINAL_PRICE,
                                                heroTagUnitPrice: provider
                                                        .hashCode
                                                        .toString() +
                                                    product.id +
                                                    PsConst
                                                        .HERO_TAG__UNIT_PRICE,
                                              );
                                              Navigator.pushNamed(context,
                                                  RoutePaths.productDetail,
                                                  arguments: holder);
                                            }),
                                      );
                                      // return ProductVeticalListItem(
                                      //   productDetailProvider:
                                      //       productDetailProvider,
                                      //   coreTagKey: provider.hashCode
                                      //           .toString() +
                                      //       provider.productList.data[index].id,
                                      //   animationController:
                                      //       widget.animationController,
                                      //   animation:
                                      //       Tween<double>(begin: 0.0, end: 1.0)
                                      //           .animate(
                                      //     CurvedAnimation(
                                      //       parent: widget.animationController,
                                      //       curve: Interval(
                                      //           (1 / count) * index, 1.0,
                                      //           curve: Curves.fastOutSlowIn),
                                      //     ),
                                      //   ),
                                      //   product:
                                      //       provider.productList.data[index],
                                      //   onTap: () {
                                      //     final Product product =
                                      //         provider.productList.data[index];
                                      //     final ProductDetailIntentHolder
                                      //         holder =
                                      //         ProductDetailIntentHolder(
                                      //       productId: product.id,
                                      //       heroTagImage:
                                      //           provider.hashCode.toString() +
                                      //               product.id +
                                      //               PsConst.HERO_TAG__IMAGE,
                                      //       heroTagTitle:
                                      //           provider.hashCode.toString() +
                                      //               product.id +
                                      //               PsConst.HERO_TAG__TITLE,
                                      //       heroTagOriginalPrice: provider
                                      //               .hashCode
                                      //               .toString() +
                                      //           product.id +
                                      //           PsConst
                                      //               .HERO_TAG__ORIGINAL_PRICE,
                                      //       heroTagUnitPrice: provider.hashCode
                                      //               .toString() +
                                      //           product.id +
                                      //           PsConst.HERO_TAG__UNIT_PRICE,
                                      //     );

                                      //     Navigator.pushNamed(
                                      //         context, RoutePaths.productDetail,
                                      //         arguments: holder);
                                      //   },
                                      // );
                                    } else {
                                      return null;
                                    }
                                  },
                                  childCount: provider.productList.data.length,
                                ),
                              ),
                            ],
                          ),
                          onRefresh: () {
                            return provider.resetLatestProductList(
                                _searchProductProvider.productParameterHolder);
                          },
                        ),
                      )
                    else if (provider.productList.status !=
                            PsStatus.PROGRESS_LOADING &&
                        provider.productList.status != PsStatus.BLOCK_LOADING &&
                        provider.productList.status != PsStatus.NOACTION)
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 20.0,
                              vertical: 15.0,
                            ),
                            child: Image.asset(
                              'assets/images/baseline_empty_item_grey_24.png',
                              height: 200,
                              fit: BoxFit.contain,
                            ),
                          ),
                          const SizedBox(
                            height: PsDimens.space20,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 40.0,
                            ),
                            child: Text(
                              Utils.getString(
                                  context, 'procuct_list__no_result_data'),
                              textAlign: TextAlign.center,
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6
                                  .copyWith(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 24.0),
                            ),
                          ),
                          const SizedBox(
                            height: PsDimens.space20,
                          ),
                        ],
                      ),
                    //  TODO: create bottom navigation bar
                    // Positioned(
                    //   bottom: _offset,
                    //   width: MediaQuery.of(context).size.width,
                    //   child: Container(
                    //     margin: const EdgeInsets.only(
                    //         left: PsDimens.space12,
                    //         top: PsDimens.space8,
                    //         right: PsDimens.space12,
                    //         bottom: PsDimens.space16),
                    //     child: Container(
                    //       width: double.infinity,
                    //       height: _containerMaxHeight,
                    //       child: BottomNavigationImageAndText(
                    //         searchProductProvider: _searchProductProvider,
                    //         changeAppBarTitle: widget.changeAppBarTitle,
                    //       ),
                    //     ),
                    //   ),
                    // ),
                    PSProgressIndicator(provider.productList.status),
                  ],
                ),
              )
            ],
          );
        },
      ),
    );
  }
}


// import 'package:biomart/api/common/ps_status.dart';
// import 'package:biomart/config/ps_colors.dart';
// import 'package:biomart/config/ps_config.dart';
// import 'package:biomart/constant/ps_dimens.dart';
// import 'package:biomart/provider/subcategory/sub_category_provider.dart';
// import 'package:biomart/repository/sub_category_repository.dart';
// import 'package:biomart/ui/common/ps_admob_banner_widget.dart';
// import 'package:biomart/ui/common/ps_ui_widget.dart';
// import 'package:biomart/ui/dashboard/view_pages/call_bottom_view.dart';
// import 'package:biomart/ui/subcategory/item/sub_category_vertical_list_item.dart';
// import 'package:biomart/utils/utils.dart';
// import 'package:biomart/viewobject/sub_category.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/scheduler.dart' show timeDilation;
// import 'package:flutter/services.dart';
// import 'package:provider/provider.dart';
// import 'package:shimmer/shimmer.dart';

// class SubCategoryListView extends StatefulWidget {
//   const SubCategoryListView({this.subCategory});
//   final SubCategory subCategory;

//   @override
//   _SubCategoryListViewState createState() {
//     return _SubCategoryListViewState();
//   }
// }

// class _SubCategoryListViewState extends State<SubCategoryListView>
//     with SingleTickerProviderStateMixin {
//   final ScrollController _scrollController = ScrollController();

//   SubCategoryProvider _subCategoryProvider;

//   Animation<double> animation;

//   @override
//   void dispose() {
//     super.dispose();
//   }

//   @override
//   void initState() {
//     super.initState();
//     _scrollController.addListener(() {
//       if (_scrollController.position.pixels ==
//           _scrollController.position.maxScrollExtent) {
//         final String categId = widget.subCategory.id;
//         Utils.psPrint('CategoryId number is $categId');

//         _subCategoryProvider.nextSubCategoryList(widget.subCategory.id);
//       }
//     });
//   }

//   SubCategoryRepository repo1;
//   bool isConnectedToInternet = false;
//   bool isSuccessfullyLoaded = true;

//   void checkConnection() {
//     Utils.checkInternetConnectivity().then((bool onValue) {
//       isConnectedToInternet = onValue;
//       if (isConnectedToInternet && PsConfig.showAdMob) {
//         setState(() {});
//       }
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     if (!isConnectedToInternet && PsConfig.showAdMob) {
//       print('loading ads....');
//       checkConnection();
//     }
//     timeDilation = 1.0;
//     repo1 = Provider.of<SubCategoryRepository>(context);
//     // final dynamic data = EasyLocalizationProvider.of(context).data;

//     return
//         // EasyLocalizationProvider(
//         //     data: data,
//         //     child:
//         Scaffold(
//       bottomNavigationBar: const BottomBarViewWidget(
//         currentIdex: 0,
//       ),
//       backgroundColor: PsColors.coreBackgroundColor,
//       appBar: AppBar(
//         systemOverlayStyle: SystemUiOverlayStyle(
//             statusBarIconBrightness: Utils.getBrightnessForAppBar(context)),
//         title: Text(
//           Utils.getString(context, 'Sub') ?? '',
//           style: TextStyle(color: PsColors.white),
//         ),
//         iconTheme: IconThemeData(
//           color: PsColors.white,
//         ),
//       ),
//       body: ChangeNotifierProvider<SubCategoryProvider>(
//         lazy: false,
//         create: (BuildContext context) {
//           _subCategoryProvider = SubCategoryProvider(repo: repo1);
//           _subCategoryProvider.loadSubCategoryList(widget.subCategory.id);
//           return _subCategoryProvider;
//         },
//         child: Consumer<SubCategoryProvider>(
//           builder: (BuildContext context, SubCategoryProvider provider,
//               Widget child) {
//             return Column(
//               children: <Widget>[
//                 const PsAdMobBannerWidget(),
//                 Expanded(
//                   child: Stack(
//                     children: <Widget>[
//                       Container(
//                         child: RefreshIndicator(
//                           onRefresh: () {
//                             return _subCategoryProvider
//                                 .resetSubCategoryList(widget.subCategory.id);
//                           },
//                           child: ListView.builder(
//                             physics: const AlwaysScrollableScrollPhysics(),
//                             controller: _scrollController,
//                             itemCount: provider.subCategoryList.data.length,
//                             itemBuilder: (BuildContext context, int index) {
//                               if (provider.subCategoryList.status ==
//                                   PsStatus.BLOCK_LOADING) {
//                                 return Shimmer.fromColors(
//                                   baseColor: PsColors.grey,
//                                   highlightColor: PsColors.white,
//                                   child: Column(
//                                     children: const <Widget>[
//                                       FrameUIForLoading(),
//                                       FrameUIForLoading(),
//                                       FrameUIForLoading(),
//                                       FrameUIForLoading(),
//                                       FrameUIForLoading(),
//                                       FrameUIForLoading(),
//                                       FrameUIForLoading(),
//                                       FrameUIForLoading(),
//                                       FrameUIForLoading(),
//                                       FrameUIForLoading(),
//                                     ],
//                                   ),
//                                 );
//                               } else {
//                                 return SubCategoryVerticalListItem(
//                                   subCategory:
//                                       provider.subCategoryList.data[index],
//                                   onTap: () {
//                                     print(provider.subCategoryList.data[index]
//                                         .defaultPhoto.imgPath);
//                                   },
//                                 );
//                               }
//                             },
//                           ),
//                         ),
//                       ),
//                       PSProgressIndicator(provider.subCategoryList.status)
//                     ],
//                   ),
//                 )
//               ],
//             );
//           },
//         ),
//       ),
//     );
//   }
// }

// class FrameUIForLoading extends StatelessWidget {
//   const FrameUIForLoading({
//     Key key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Row(
//       mainAxisSize: MainAxisSize.min,
//       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//       children: <Widget>[
//         Container(
//             height: 70,
//             width: 70,
//             margin: const EdgeInsets.all(PsDimens.space16),
//             decoration: BoxDecoration(color: PsColors.grey)),
//         Expanded(
//             child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
//           Container(
//               height: 15,
//               margin: const EdgeInsets.all(PsDimens.space8),
//               decoration: BoxDecoration(color: PsColors.grey)),
//           Container(
//               height: 15,
//               margin: const EdgeInsets.all(PsDimens.space8),
//               decoration: BoxDecoration(color: PsColors.grey)),
//         ]))
//       ],
//     );
//   }
// }
