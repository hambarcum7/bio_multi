import 'package:biomart/api/common/ps_status.dart';
import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/provider/subsubcategory/sub_sub_category_provider.dart';
import 'package:biomart/repository/sub_sub_category_repository.dart';
import 'package:biomart/ui/common/base/ps_widget_with_appbar.dart';
import 'package:biomart/ui/common/ps_frame_loading_widget.dart';
import 'package:biomart/ui/common/ps_ui_widget.dart';
import 'package:biomart/ui/subsubcategory/item/sub_sub_category_search_list_item.dart';
import 'package:biomart/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class SubSubCategorySearchListView extends StatefulWidget {
  const SubSubCategorySearchListView({@required this.subCategoryId});

  final String subCategoryId;
  @override
  State<StatefulWidget> createState() {
    return _SubSubCategorySearchListViewState();
  }
}

class _SubSubCategorySearchListViewState extends State<SubSubCategorySearchListView>
    with TickerProviderStateMixin {
  final ScrollController _scrollController = ScrollController();

  SubSubCategoryProvider _subSubCategoryProvider;
  AnimationController animationController;
  Animation<double> animation;

  @override
  void dispose() {
    animationController.dispose();
    animation = null;
    super.dispose();
  }

  @override
  void initState() {
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _subSubCategoryProvider.nextSubSubCategoryList(widget.subCategoryId);
      }
    });

    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    animation = Tween<double>(
      begin: 0.0,
      end: 10.0,
    ).animate(animationController);
    super.initState();
  }

  SubSubCategoryRepository repo1;

  @override
  Widget build(BuildContext context) {
    repo1 = Provider.of<SubSubCategoryRepository>(context);

    print(
        '............................Build UI Again ............................');

    return PsWidgetWithAppBar<SubSubCategoryProvider>(
        appBarTitle: Utils.getString(
                context, 'sub_category_list__sub_category_list') ??
            '',
        initProvider: () {
          return SubSubCategoryProvider(
            repo: repo1,
          );
        },
        onProviderReady: (SubSubCategoryProvider provider) {
          provider.loadAllSubSubCategoryList(widget.subCategoryId);
          _subSubCategoryProvider = provider;
        },
        builder: (BuildContext context, SubSubCategoryProvider provider,
            Widget child) {
          return Stack(children: <Widget>[
            Container(
                child: RefreshIndicator(
              child: ListView.builder(
                  controller: _scrollController,
                  physics: const AlwaysScrollableScrollPhysics(),
                  itemCount: provider.subSubCategoryList.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    if (provider.subSubCategoryList.status ==
                        PsStatus.BLOCK_LOADING) {
                      return Shimmer.fromColors(
                          baseColor: PsColors.grey,
                          highlightColor: PsColors.white,
                          child: Column(children: const <Widget>[
                            PsFrameUIForLoading(),
                            PsFrameUIForLoading(),
                            PsFrameUIForLoading(),
                            PsFrameUIForLoading(),
                            PsFrameUIForLoading(),
                            PsFrameUIForLoading(),
                            PsFrameUIForLoading(),
                            PsFrameUIForLoading(),
                            PsFrameUIForLoading(),
                            PsFrameUIForLoading(),
                          ]));
                    } else {
                      final int count = provider.subSubCategoryList.data.length;
                      animationController.forward();
                      return FadeTransition(
                          opacity: animation,
                          child: SubSubCategorySearchListItem(
                            animationController: animationController,
                            animation:
                                Tween<double>(begin: 0.0, end: 1.0).animate(
                              CurvedAnimation(
                                parent: animationController,
                                curve: Interval((1 / count) * index, 1.0,
                                    curve: Curves.fastOutSlowIn),
                              ),
                            ),
                            subSubCategory: provider.subSubCategoryList.data[index],
                            onTap: () {
                              print(provider.subSubCategoryList.data[index]
                                  .defaultPhoto.imgPath);
                              Navigator.pop(context,
                                  provider.subSubCategoryList.data[index]);
                              print(
                                  provider.subSubCategoryList.data[index].name);
                            },
                          ));
                    }
                  }),
              onRefresh: () {
                return provider.resetSubSubCategoryList(widget.subCategoryId);
              },
            )),
            PSProgressIndicator(provider.subSubCategoryList.status)
          ]);
        });
  }
}
