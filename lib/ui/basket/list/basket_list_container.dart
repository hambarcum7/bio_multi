import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/ui/common/dialog/confirm_dialog_view.dart';
import 'package:biomart/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'basket_list_view.dart';

class BasketListContainerView extends StatefulWidget {
  const BasketListContainerView({
    Key key,
  }) : super(key: key);

  @override
  _CityBasketListContainerViewState createState() =>
      _CityBasketListContainerViewState();
}

class _CityBasketListContainerViewState extends State<BasketListContainerView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;

  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Future<bool> _onWillPop() {
      return showDialog<dynamic>(
            context: context,
            builder: (BuildContext context) {
              return ConfirmDialogView(
                description:
                    Utils.getString(context, 'home__quit_dialog_description'),
                leftButtonText:
                    Utils.getString(context, 'app_info__cancel_button_name'),
                rightButtonText: Utils.getString(context, 'dialog__ok'),
                onAgreeTap: () {
                  SystemNavigator.pop();
                },
              );
            },
          ) ??
          false;
    }

    print(
        '............................Build UI Again ............................');

    // final Animation<double> animation =
    //     Tween<double>(begin: 0.0, end: 1.0).animate(
    //   CurvedAnimation(
    //     parent: animationController,
    //     curve: const Interval(0.5 * 1, 1.0, curve: Curves.fastOutSlowIn),
    //   ),
    // );
    //animationController.forward();

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Container(
        color: PsColors.coreBackgroundColor,
        height: double.infinity,
        child: BasketListView(
          animationController: animationController,
        ),
      ),
    );
  }
}
