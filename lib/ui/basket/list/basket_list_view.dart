import 'package:biomart/api/common/ps_resource.dart';
import 'package:biomart/config/ps_colors.dart';
import 'package:biomart/config/ps_config.dart';
import 'package:biomart/constant/ps_constants.dart';
import 'package:biomart/constant/ps_dimens.dart';
import 'package:biomart/constant/route_paths.dart';
import 'package:biomart/provider/basket/basket_provider.dart';
import 'package:biomart/provider/coupon_discount/coupon_discount_provider.dart';
import 'package:biomart/provider/delivery_cost/delivery_cost_provider.dart';
import 'package:biomart/provider/shop_info/shop_info_provider.dart';
import 'package:biomart/provider/user/user_provider.dart';
import 'package:biomart/repository/basket_repository.dart';
import 'package:biomart/repository/coupon_discount_repository.dart';
import 'package:biomart/repository/delivery_cost_repository.dart';
import 'package:biomart/repository/shop_info_repository.dart';
import 'package:biomart/repository/user_repository.dart';
import 'package:biomart/ui/common/base/ps_widget_with_multi_provider.dart';
import 'package:biomart/ui/common/dialog/confirm_dialog_view.dart';
import 'package:biomart/ui/common/dialog/error_dialog.dart';
import 'package:biomart/ui/common/dialog/success_dialog.dart';
import 'package:biomart/ui/common/dialog/warning_dialog_view.dart';
import 'package:biomart/ui/common/ps_button_widget.dart';
import 'package:biomart/ui/common/ps_ui_widget.dart';
import 'package:biomart/ui/dashboard/view_pages/call_bottom_view.dart';
import 'package:biomart/ui/product/favourite/favorite_horizontal_list_view.dart';
import 'package:biomart/utils/app_bar_widget.dart';
import 'package:biomart/utils/ps_progress_dialog.dart';
import 'package:biomart/utils/utils.dart';
import 'package:biomart/viewobject/basket.dart';
import 'package:biomart/viewobject/common/ps_value_holder.dart';
import 'package:biomart/viewobject/coupon_discount.dart';
import 'package:biomart/viewobject/holder/coupon_discount_holder.dart';
import 'package:biomart/viewobject/holder/intent_holder/checkout_intent_holder.dart';
import 'package:biomart/viewobject/holder/intent_holder/product_detail_intent_holder.dart';
import 'package:biomart/viewobject/holder/intent_holder/shop_data_intent_holder.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

import '../item/basket_list_item.dart';

class BasketListView extends StatefulWidget {
  const BasketListView({
    Key key,
    @required this.animationController,
  }) : super(key: key);

  final AnimationController animationController;
  @override
  _BasketListViewState createState() => _BasketListViewState();
}

class _BasketListViewState extends State<BasketListView>
    with SingleTickerProviderStateMixin {
  BasketRepository basketRepo;
  PsValueHolder valueHolder;
  dynamic data;
  ShopInfoProvider shopInfoProvider;
  ShopInfoRepository shopInfoRepository;
  bool isConnectedToInternet = false;
  bool isSuccessfullyLoaded = true;
  final TextEditingController couponController = TextEditingController();
  CouponDiscountRepository couponDiscountRepo;
  CouponDiscountProvider couponDiscountProvider;
  DeliveryCostProvider deliveryCostProvider;
  DeliveryCostRepository deliveryCostRepository;
  UserProvider userProvider;
  UserRepository userRepository;

  void checkConnection() {
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
      if (isConnectedToInternet && PsConfig.showAdMob) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!isConnectedToInternet && PsConfig.showAdMob) {
      print('loading ads....');
      checkConnection();
    }

    basketRepo = Provider.of<BasketRepository>(context);
    shopInfoRepository = Provider.of<ShopInfoRepository>(context);
    couponDiscountRepo = Provider.of<CouponDiscountRepository>(context);
    userRepository = Provider.of<UserRepository>(context);
    valueHolder = Provider.of<PsValueHolder>(context);
    userProvider =
        UserProvider(repo: userRepository, psValueHolder: valueHolder);
    couponDiscountProvider = CouponDiscountProvider(repo: couponDiscountRepo);
    deliveryCostRepository = Provider.of<DeliveryCostRepository>(context);

    final Animation<double> animation = Tween<double>(begin: 0.0, end: 1.0)
        .animate(CurvedAnimation(
            parent: widget.animationController,
            curve: const Interval(0.5 * 1, 1.0, curve: Curves.fastOutSlowIn)));
    widget.animationController.forward();

    return PsWidgetWithMultiProvider(
      child: MultiProvider(
        providers: <SingleChildWidget>[
          ChangeNotifierProvider<BasketProvider>(
              lazy: false,
              create: (BuildContext context) {
                final BasketProvider provider =
                    BasketProvider(repo: basketRepo);
                provider.loadBasketList();

                return provider;
              }),
          ChangeNotifierProvider<ShopInfoProvider>(
            lazy: false,
            create: (BuildContext context) {
              shopInfoProvider = ShopInfoProvider(
                  ownerCode: 'ShopInfoContainerView',
                  psValueHolder: valueHolder,
                  repo: shopInfoRepository);
              return shopInfoProvider;
            },
          ),
          ChangeNotifierProvider<DeliveryCostProvider>(
            lazy: false,
            create: (BuildContext context) {
              deliveryCostProvider =
                  DeliveryCostProvider(repo: deliveryCostRepository);
              return deliveryCostProvider;
            },
          ),
        ],
        child: Consumer<BasketProvider>(
          builder:
              (BuildContext context, BasketProvider provider, Widget child) {
            return Scaffold(
              backgroundColor: PsColors.coreBackgroundColor,
              floatingActionButton: (provider.basketList != null &&
                      provider.basketList.data != null &&
                      provider.basketList.data.isNotEmpty)
                  ? _CheckoutButtonWidget(
                      provider: provider,
                      shopInfoProvider: shopInfoProvider,
                      couponDiscountProvider: couponDiscountProvider,
                    )
                  : null,
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerFloat,
              bottomNavigationBar: const BottomBarViewWidget(
                currentIdex: 1,
              ),
              appBar: (provider.basketList != null &&
                      provider.basketList.data != null &&
                      provider.basketList.data.isNotEmpty)
                  ? customAppBar(
                      title: Utils.getString(
                          context, 'basket_list_container__app_bar_name'),
                      actions: <Widget>[
                        GestureDetector(
                          onTap: () {
                            showDialog<dynamic>(
                              context: context,
                              builder: (BuildContext context) {
                                return ConfirmDialogView(
                                  description: Utils.getString(context,
                                      'basket_list__delete_all_dialog_text'),
                                  leftButtonText: Utils.getString(
                                      context, 'app_info__cancel_button_name'),
                                  rightButtonText:
                                      Utils.getString(context, 'dialog__ok'),
                                  onAgreeTap: () {
                                    provider.deleteWholeBasketList();
                                    Navigator.pop(context);
                                  },
                                );
                              },
                            );
                          },
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.only(right: 5.0),
                              child: Text(
                                Utils.getString(
                                    context, 'basket_list__clear_basket'),
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                    color: Colors.red,
                                    fontSize: PsDimens.space18,
                                    fontWeight: FontWeight.w400,
                                    fontFamily:
                                        PsConfig.ps_default_font_family),
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  : null,
              body: AnimatedBuilder(
                animation: widget.animationController,
                child: (provider.basketList != null &&
                        provider.basketList.data != null &&
                        provider.basketList.data.isNotEmpty)
                    ? CustomScrollView(
                        slivers: <Widget>[
                          SliverToBoxAdapter(
                            child: Container(
                              color: PsColors.coreBackgroundColor,
                              // height: MediaQuery.of(context).size.height * 0.5,
                              child: RefreshIndicator(
                                child: ListView.separated(
                                  separatorBuilder:
                                      (BuildContext context, int index) {
                                    return Container(
                                      height: 0.5,
                                      color: const Color(0xff333333),
                                    );
                                  },
                                  scrollDirection: Axis.vertical,
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: provider.basketList.data.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    // final int count =
                                    //     provider.basketList.data.length;
                                    widget.animationController.forward();
                                    return BasketListItemView(
                                        // animationController:
                                        //     widget.animationController,
                                        // animation:
                                        //     Tween<double>(begin: 0.0, end: 1.0)
                                        //         .animate(
                                        //   CurvedAnimation(
                                        //     parent: widget.animationController,
                                        //     curve: Interval(
                                        //         (1 / count) * index, 1.0,
                                        //         curve: Curves.fastOutSlowIn),
                                        //   ),
                                        // ),
                                        basket: provider.basketList.data[index],
                                        onTap: () async {
                                          // final Basket intentBasket = provider.basketList.data[index];
                                          final dynamic returnData =
                                              await Navigator.pushNamed(
                                            context,
                                            RoutePaths.productDetail,
                                            arguments:
                                                ProductDetailIntentHolder(
                                              id: provider
                                                  .basketList.data[index].id,
                                              qty: provider
                                                  .basketList.data[index].qty,
                                              selectedColorId: provider
                                                  .basketList
                                                  .data[index]
                                                  .selectedColorId,
                                              selectedColorValue: provider
                                                  .basketList
                                                  .data[index]
                                                  .selectedColorValue,
                                              basketPrice: provider.basketList
                                                  .data[index].basketPrice,
                                              basketSelectedAttributeList: provider
                                                  .basketList
                                                  .data[index]
                                                  .basketSelectedAttributeList,
                                              basketSelectedAddOnList: provider
                                                  .basketList
                                                  .data[index]
                                                  .basketSelectedAddOnList,
                                              productId: provider.basketList
                                                  .data[index].product.id,
                                              heroTagImage: '',
                                              heroTagTitle: '',
                                              heroTagOriginalPrice: '',
                                              heroTagUnitPrice: '',
                                            ),
                                          );
                                          if (returnData == null) {
                                            provider.resetBasketList();
                                          }
                                        },
                                        onDeleteTap: () {
                                          showDialog<dynamic>(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return ConfirmDialogView(
                                                    description: Utils.getString(
                                                        context,
                                                        'basket_list__confirm_dialog_description'),
                                                    leftButtonText: Utils.getString(
                                                        context,
                                                        'basket_list__comfirm_dialog_cancel_button'),
                                                    rightButtonText:
                                                        Utils.getString(context,
                                                            'basket_list__comfirm_dialog_ok_button'),
                                                    onAgreeTap: () async {
                                                      Navigator.of(context)
                                                          .pop();
                                                      provider
                                                          .deleteBasketByProduct(
                                                              provider
                                                                  .basketList
                                                                  .data[index]);
                                                    });
                                              });
                                        });
                                  },
                                ),
                                onRefresh: () {
                                  return provider.resetBasketList();
                                },
                              ),
                            ),
                          ),
                          const SliverToBoxAdapter(
                            child: SizedBox(
                              height: 14,
                            ),
                          ),
                          SliverToBoxAdapter(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Expanded(
                                  child: TextField(
                                    keyboardType: TextInputType.text,
                                    maxLines: null,
                                    controller: couponController,
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText2
                                        .copyWith(
                                            color: PsColors.textPrimaryColor,
                                            fontFamily: 'Roboto'),
                                    decoration: InputDecoration(
                                      contentPadding:
                                          const EdgeInsets.only(top: 15),
                                      hintText: Utils.getString(
                                          context, 'checkout__coupon_code'),
                                      prefixIcon: Icon(
                                        Icons.search,
                                        color: PsColors.mainColor,
                                        size: 25,
                                      ),
                                      border: InputBorder.none,
                                      hintStyle: Theme.of(context)
                                          .textTheme
                                          .bodyText2
                                          .copyWith(
                                            color: PsColors.textPrimaryColor,
                                          ),
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(
                                      right: PsDimens.space8),
                                  height: 41.0,
                                  alignment: Alignment.center,
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 18.0),
                                  decoration: BoxDecoration(
                                    color: PsColors.greyColorForCustomWidget,
                                    borderRadius: BorderRadius.circular(4.0),
                                  ),
                                  child: Row(
                                    children: <Widget>[
                                      Icon(
                                        Icons.verified,
                                        color: PsColors.mainColor,
                                        size: 20.0,
                                      ),
                                      const SizedBox(
                                        width: 8.0,
                                      ),
                                      GestureDetector(
                                        //highlightColor: PsColors.mainColor,
                                        child: Text(
                                          Utils.getString(context,
                                              'checkout__claim_button_name'),
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: PsColors.mainColor,
                                            fontSize: PsDimens.space18,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                        onTap: () async {
                                          final String shopId = provider
                                              .basketList
                                              .data[0]
                                              .product
                                              .shop
                                              .id;
                                          await shopInfoProvider
                                              .loadShopInfo(shopId);
                                          if (couponController
                                              .text.isNotEmpty) {
                                            final CouponDiscountParameterHolder
                                                couponDiscountParameterHolder =
                                                CouponDiscountParameterHolder(
                                                    couponCode:
                                                        couponController.text,
                                                    shopId: userProvider
                                                        .psValueHolder.shopId);

                                            final PsResource<CouponDiscount>
                                                _apiStatus =
                                                await couponDiscountProvider
                                                    .postCouponDiscount(
                                                        couponDiscountParameterHolder
                                                            .toMap());

                                            if (_apiStatus.data != null &&
                                                couponController.text ==
                                                    _apiStatus
                                                        .data.couponCode) {
                                              final BasketProvider
                                                  basketProvider =
                                                  Provider.of<BasketProvider>(
                                                      context,
                                                      listen: false);

                                              if (shopInfoProvider
                                                      .shopInfo.data.isArea ==
                                                  PsConst.ONE) {
                                                basketProvider
                                                    .checkoutCalculationHelper
                                                    .calculate(
                                                        basketList: provider
                                                            .basketList.data,
                                                        couponDiscountString:
                                                            _apiStatus.data
                                                                .couponAmount,
                                                        psValueHolder:
                                                            valueHolder,
                                                        shippingPriceStringFormatting:
                                                            userProvider
                                                                .selectedArea
                                                                .price);
                                              } else if (shopInfoProvider
                                                          .shopInfo
                                                          .data
                                                          .isArea ==
                                                      PsConst.ZERO &&
                                                  shopInfoProvider.shopInfo.data
                                                          .deliFeeByDistance ==
                                                      PsConst.ONE) {
                                                basketProvider
                                                    .checkoutCalculationHelper
                                                    .calculate(
                                                  basketList:
                                                      provider.basketList.data,
                                                  couponDiscountString:
                                                      _apiStatus
                                                          .data.couponAmount,
                                                  psValueHolder: valueHolder,
                                                  shippingPriceStringFormatting:
                                                      deliveryCostProvider
                                                          .deliveryCost
                                                          .data
                                                          .totalCost,
                                                );
                                              } else if (shopInfoProvider
                                                          .shopInfo
                                                          .data
                                                          .isArea ==
                                                      PsConst.ZERO &&
                                                  shopInfoProvider.shopInfo.data
                                                          .fixedDelivery ==
                                                      PsConst.ONE) {
                                                basketProvider
                                                    .checkoutCalculationHelper
                                                    .calculate(
                                                  basketList:
                                                      provider.basketList.data,
                                                  couponDiscountString:
                                                      _apiStatus
                                                          .data.couponAmount,
                                                  psValueHolder: valueHolder,
                                                  shippingPriceStringFormatting:
                                                      deliveryCostProvider
                                                          .deliveryCost
                                                          .data
                                                          .totalCost,
                                                );
                                              } else if (shopInfoProvider
                                                          .shopInfo
                                                          .data
                                                          .isArea ==
                                                      PsConst.ZERO &&
                                                  shopInfoProvider.shopInfo.data
                                                          .freeDelivery ==
                                                      PsConst.ONE) {
                                                basketProvider
                                                    .checkoutCalculationHelper
                                                    .calculate(
                                                        basketList: provider
                                                            .basketList.data,
                                                        couponDiscountString:
                                                            _apiStatus.data
                                                                .couponAmount,
                                                        psValueHolder:
                                                            valueHolder,
                                                        shippingPriceStringFormatting:
                                                            '0.0');
                                              }
                                              showDialog<dynamic>(
                                                  context: context,
                                                  builder:
                                                      (BuildContext context) {
                                                    return SuccessDialog(
                                                      message: Utils.getString(
                                                          context,
                                                          'checkout__couponcode_add_dialog_message'),
                                                    );
                                                  });

                                              couponController.clear();
                                              print(
                                                  _apiStatus.data.couponAmount);
                                              setState(() {
                                                couponDiscountProvider
                                                        .couponDiscount =
                                                    _apiStatus
                                                        .data.couponAmount;
                                              });
                                              print(couponDiscountProvider
                                                  .couponDiscount);
                                              print(
                                                basketProvider
                                                    .checkoutCalculationHelper
                                                    .couponDiscountFormattedString,
                                              );
                                            } else {
                                              showDialog<dynamic>(
                                                  context: context,
                                                  builder:
                                                      (BuildContext context) {
                                                    return ErrorDialog(
                                                      message:
                                                          _apiStatus.message,
                                                    );
                                                  });
                                            }
                                          } else {
                                            showDialog<dynamic>(
                                                context: context,
                                                builder:
                                                    (BuildContext context) {
                                                  return WarningDialog(
                                                    message: Utils.getString(
                                                        context,
                                                        'checkout__warning_dialog_message'),
                                                    onPressed: () {},
                                                  );
                                                });
                                          }
                                          setState(() {});
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SliverToBoxAdapter(
                            child: SizedBox(
                              height: 14,
                            ),
                          ),
                          SliverToBoxAdapter(
                            child: _TotalCountWidget(
                              provider: provider,
                              shopInfoProvider: shopInfoProvider,
                            ),
                          ),
                          const SliverToBoxAdapter(
                            child: SizedBox(
                              height: 24,
                            ),
                          ),
                          FavoriteHorizontalListView(
                            animationController: widget.animationController,
                            animation: Tween<double>(begin: 0.0, end: 1.0)
                                .animate(CurvedAnimation(
                                    parent: widget.animationController,
                                    curve: const Interval((1 / 8) * 2, 1.0,
                                        curve: Curves.fastOutSlowIn))),
                          ),
                          const SliverToBoxAdapter(
                            child: SizedBox(
                              height: 80,
                            ),
                          ),
                        ],
                      )
                    : Container(
                        height: MediaQuery.of(context).size.height,
                        color: PsColors.coreBackgroundColor,
                        child: SingleChildScrollView(
                          child: Column(
                            //mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            //mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: 11.0,
                                  right: 11.0,
                                  top: 50.0,
                                ),
                                child: Image.asset(
                                  'assets/images/empty_basket.png',
                                ),
                              ),
                              const SizedBox(
                                height: PsDimens.space64,
                              ),
                              Text(
                                Utils.getString(
                                    context, 'basket_list__empty_cart_title'),
                                style: TextStyle(
                                  color: PsColors.textPrimaryColor,
                                  fontSize: PsDimens.space24,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              const SizedBox(
                                height: PsDimens.space16,
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: PsDimens.space24,
                                ),
                                child: Text(
                                  Utils.getString(context,
                                      'basket_list__empty_cart_description'),
                                  style: TextStyle(
                                    color: PsColors.textPrimaryColor,
                                    fontWeight: FontWeight.w400,
                                    fontSize: PsDimens.space18,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              const SizedBox(
                                height: PsDimens.space40,
                              ),
                              Container(
                                margin: const EdgeInsets.only(
                                  left: PsDimens.space12,
                                  right: PsDimens.space12,
                                  bottom: PsDimens.space20,
                                ),
                                child: PSButtonWidget(
                                  hasShadow: false,
                                  colorData: PsColors.grey,
                                  width: double.infinity,
                                  titleText:
                                      Utils.getString(context, 'start_shoping'),
                                  onPressed: () {
                                    Navigator.pushNamed(
                                      context,
                                      RoutePaths.home,
                                    );
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                builder: (BuildContext context, Widget child) {
                  return FadeTransition(
                    opacity: animation,
                    child: Transform(
                      transform: Matrix4.translationValues(
                          0.0, 100 * (1.0 - animation.value), 0.0),
                      child: child,
                    ),
                  );
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
//           if (provider.basketList != null && provider.basketList.data != null) {
//             if (provider.basketList.data.isNotEmpty) {
//               return Scaffold(
//                 backgroundColor: PsColors.coreBackgroundColor,
//                 floatingActionButton: _CheckoutButtonWidget(
//                   provider: provider,
//                   shopInfoProvider: shopInfoProvider,
//                 ),
//                 bottomNavigationBar: const BottomBarViewWidget(
//                   currentIdex: 1,
//                 ),
//                 appBar: customAppBar(
//                   title: Utils.getString(
//                       context, 'basket_list_container__app_bar_name'),
//                   actions: <Widget>[
//                     GestureDetector(
//                       onTap: () {
//                         showDialog<dynamic>(
//                           context: context,
//                           builder: (BuildContext context) {
//                             return ConfirmDialogView(
//                               description: Utils.getString(context,
//                                   'basket_list__delete_all_dialog_text'),
//                               leftButtonText: Utils.getString(
//                                   context, 'app_info__cancel_button_name'),
//                               rightButtonText:
//                                   Utils.getString(context, 'dialog__ok'),
//                               onAgreeTap: () {
//                                 provider.deleteWholeBasketList();
//                                 Navigator.pop(context);
//                               },
//                             );
//                           },
//                         );
//                       },
//                       child: Center(
//                         child: Padding(
//                           padding: const EdgeInsets.only(right: 5.0),
//                           child: Text(
//                             Utils.getString(
//                                 context, 'basket_list__clear_basket'),
//                             textAlign: TextAlign.center,
//                             style: const TextStyle(
//                                 color: Colors.red,
//                                 fontSize: PsDimens.space18,
//                                 fontWeight: FontWeight.w400,
//                                 fontFamily: PsConfig.ps_default_font_family),
//                           ),
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//                 floatingActionButtonLocation:
//                     FloatingActionButtonLocation.centerFloat,
//                 body: CustomScrollView(
//                   slivers: <Widget>[
//                     SliverToBoxAdapter(
//                       child: Container(
//                         color: PsColors.coreBackgroundColor,
//                         // height: MediaQuery.of(context).size.height * 0.5,
//                         child: RefreshIndicator(
//                           child: ListView.separated(
//                             separatorBuilder:
//                                 (BuildContext context, int index) {
//                               return Container(
//                                 height: 0.5,
//                                 color: const Color(0xff333333),
//                               );
//                             },
//                             scrollDirection: Axis.vertical,
//                             shrinkWrap: true,
//                             physics: const NeverScrollableScrollPhysics(),
//                             itemCount: provider.basketList.data.length,
//                             itemBuilder: (BuildContext context, int index) {
//                               final int count = provider.basketList.data.length;
//                               widget.animationController.forward();
//                               return BasketListItemView(
//                                   animationController:
//                                       widget.animationController,
//                                   animation: Tween<double>(begin: 0.0, end: 1.0)
//                                       .animate(
//                                     CurvedAnimation(
//                                       parent: widget.animationController,
//                                       curve: Interval((1 / count) * index, 1.0,
//                                           curve: Curves.fastOutSlowIn),
//                                     ),
//                                   ),
//                                   basket: provider.basketList.data[index],
//                                   onTap: () async {
//                                     // final Basket intentBasket = provider.basketList.data[index];
//                                     final dynamic returnData =
//                                         await Navigator.pushNamed(
//                                       context,
//                                       RoutePaths.productDetail,
//                                       arguments: ProductDetailIntentHolder(
//                                         id: provider.basketList.data[index].id,
//                                         qty:
//                                             provider.basketList.data[index].qty,
//                                         selectedColorId: provider.basketList
//                                             .data[index].selectedColorId,
//                                         selectedColorValue: provider.basketList
//                                             .data[index].selectedColorValue,
//                                         basketPrice: provider
//                                             .basketList.data[index].basketPrice,
//                                         basketSelectedAttributeList: provider
//                                             .basketList
//                                             .data[index]
//                                             .basketSelectedAttributeList,
//                                         basketSelectedAddOnList: provider
//                                             .basketList
//                                             .data[index]
//                                             .basketSelectedAddOnList,
//                                         productId: provider
//                                             .basketList.data[index].product.id,
//                                         heroTagImage: '',
//                                         heroTagTitle: '',
//                                         heroTagOriginalPrice: '',
//                                         heroTagUnitPrice: '',
//                                       ),
//                                     );
//                                     if (returnData == null) {
//                                       provider.resetBasketList();
//                                     }
//                                   },
//                                   onDeleteTap: () {
//                                     showDialog<dynamic>(
//                                         context: context,
//                                         builder: (BuildContext context) {
//                                           return ConfirmDialogView(
//                                               description: Utils.getString(
//                                                   context,
//                                                   'basket_list__confirm_dialog_description'),
//                                               leftButtonText: Utils.getString(
//                                                   context,
//                                                   'basket_list__comfirm_dialog_cancel_button'),
//                                               rightButtonText: Utils.getString(
//                                                   context,
//                                                   'basket_list__comfirm_dialog_ok_button'),
//                                               onAgreeTap: () async {
//                                                 Navigator.of(context).pop();
//                                                 provider.deleteBasketByProduct(
//                                                     provider.basketList
//                                                         .data[index]);
//                                               });
//                                         });
//                                   });
//                             },
//                           ),
//                           onRefresh: () {
//                             return provider.resetBasketList();
//                           },
//                         ),
//                       ),
//                     ),
//                     const SliverToBoxAdapter(
//                       child: SizedBox(
//                         height: 14,
//                       ),
//                     ),
//                     SliverToBoxAdapter(
//                       child: Row(
//                         mainAxisSize: MainAxisSize.min,
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: <Widget>[
//                           Expanded(
//                             child: PsTextFieldWidget(
//                               hintText: Utils.getString(
//                                   context, 'checkout__coupon_code'),
//                               textEditingController: couponController,
//                               showTitle: false,
//                             ),
//                           ),
//                           Padding(
//                             padding:
//                                 const EdgeInsets.only(right: PsDimens.space8),
//                             child: PSButtonWidget(
//                               titleText: Utils.getString(
//                                   context, 'checkout__claim_button_name'),
//                               onPressed: () async {
//                                 if (couponController.text.isNotEmpty) {
//                                   final CouponDiscountParameterHolder
//                                       couponDiscountParameterHolder =
//                                       CouponDiscountParameterHolder(
//                                           couponCode: couponController.text,
//                                           shopId: userProvider
//                                               .psValueHolder.shopId);

//                                   final PsResource<CouponDiscount> _apiStatus =
//                                       await couponDiscountProvider
//                                           .postCouponDiscount(
//                                               couponDiscountParameterHolder
//                                                   .toMap());

//                                   if (_apiStatus.data != null &&
//                                       couponController.text ==
//                                           _apiStatus.data.couponCode) {
//                                     final BasketProvider basketProvider =
//                                         Provider.of<BasketProvider>(context,
//                                             listen: false);

//                                     if (shopInfoProvider.shopInfo.data.isArea ==
//                                         PsConst.ONE) {
//                                       basketProvider.checkoutCalculationHelper
//                                           .calculate(
//                                               basketList:
//                                                   provider.basketList.data,
//                                               couponDiscountString:
//                                                   _apiStatus.data.couponAmount,
//                                               psValueHolder: valueHolder,
//                                               shippingPriceStringFormatting:
//                                                   userProvider
//                                                       .selectedArea.price);
//                                     } else if (shopInfoProvider
//                                                 .shopInfo.data.isArea ==
//                                             PsConst.ZERO &&
//                                         shopInfoProvider.shopInfo.data
//                                                 .deliFeeByDistance ==
//                                             PsConst.ONE) {
//                                       basketProvider.checkoutCalculationHelper
//                                           .calculate(
//                                               basketList:
//                                                   provider.basketList.data,
//                                               couponDiscountString:
//                                                   _apiStatus.data.couponAmount,
//                                               psValueHolder: valueHolder,
//                                               shippingPriceStringFormatting:
//                                                   '');
//                                     } else if (shopInfoProvider
//                                                 .shopInfo.data.isArea ==
//                                             PsConst.ZERO &&
//                                         shopInfoProvider
//                                                 .shopInfo.data.fixedDelivery ==
//                                             PsConst.ONE) {
//                                       basketProvider.checkoutCalculationHelper
//                                           .calculate(
//                                               basketList:
//                                                   provider.basketList.data,
//                                               couponDiscountString:
//                                                   _apiStatus.data.couponAmount,
//                                               psValueHolder: valueHolder,
//                                               shippingPriceStringFormatting:
//                                                   '');
//                                     } else if (shopInfoProvider
//                                                 .shopInfo.data.isArea ==
//                                             PsConst.ZERO &&
//                                         shopInfoProvider
//                                                 .shopInfo.data.freeDelivery ==
//                                             PsConst.ONE) {
//                                       basketProvider.checkoutCalculationHelper
//                                           .calculate(
//                                               basketList:
//                                                   provider.basketList.data,
//                                               couponDiscountString:
//                                                   _apiStatus.data.couponAmount,
//                                               psValueHolder: valueHolder,
//                                               shippingPriceStringFormatting:
//                                                   '0.0');
//                                     }
//                                     showDialog<dynamic>(
//                                         context: context,
//                                         builder: (BuildContext context) {
//                                           return SuccessDialog(
//                                             message: Utils.getString(context,
//                                                 'checkout__couponcode_add_dialog_message'),
//                                           );
//                                         });

//                                     couponController.clear();
//                                     print(_apiStatus.data.couponAmount);
//                                     setState(() {
//                                       couponDiscountProvider.couponDiscount =
//                                           _apiStatus.data.couponAmount;
//                                     });
//                                   } else {
//                                     showDialog<dynamic>(
//                                         context: context,
//                                         builder: (BuildContext context) {
//                                           return ErrorDialog(
//                                             message: _apiStatus.message,
//                                           );
//                                         });
//                                   }
//                                 } else {
//                                   showDialog<dynamic>(
//                                       context: context,
//                                       builder: (BuildContext context) {
//                                         return WarningDialog(
//                                           message: Utils.getString(context,
//                                               'checkout__warning_dialog_message'),
//                                           onPressed: () {},
//                                         );
//                                       });
//                                 }
//                               },
//                             ),
//                           ),
//                         ],
//                       ),
//                     ),
//                     const SliverToBoxAdapter(
//                       child: SizedBox(
//                         height: 14,
//                       ),
//                     ),
//                     SliverToBoxAdapter(
//                       child: _TotalCountWidget(
//                         provider: provider,
//                         shopInfoProvider: shopInfoProvider,
//                       ),
//                     ),
//                     const SliverToBoxAdapter(
//                       child: SizedBox(
//                         height: 24,
//                       ),
//                     ),
//                     FavoriteHorizontalListView(
//                       animationController: widget.animationController,
//                       animation: Tween<double>(begin: 0.0, end: 1.0).animate(
//                           CurvedAnimation(
//                               parent: widget.animationController,
//                               curve: const Interval((1 / 8) * 2, 1.0,
//                                   curve: Curves.fastOutSlowIn))),
//                     ),
//                     const SliverToBoxAdapter(
//                       child: SizedBox(
//                         height: 80,
//                       ),
//                     ),
//                   ],
//                 ),
//               );
//             } else {
//               return Container(
//                 height: MediaQuery.of(context).size.height,
//                 color: PsColors.coreBackgroundColor,
//                 child: SingleChildScrollView(
//                   child: Column(
//                     //mainAxisAlignment: MainAxisAlignment.center,
//                     crossAxisAlignment: CrossAxisAlignment.center,
//                     //mainAxisSize: MainAxisSize.min,
//                     children: <Widget>[
//                       Padding(
//                         padding: const EdgeInsets.only(
//                           left: 11.0,
//                           right: 11.0,
//                           top: 50.0,
//                         ),
//                         child: Image.asset(
//                           'assets/images/empty_basket.png',
//                         ),
//                       ),
//                       const SizedBox(
//                         height: PsDimens.space64,
//                       ),
//                       Text(
//                         Utils.getString(
//                             context, 'basket_list__empty_cart_title'),
//                         style: TextStyle(
//                           color: PsColors.textPrimaryColor,
//                           fontSize: PsDimens.space24,
//                           fontWeight: FontWeight.w700,
//                         ),
//                       ),
//                       const SizedBox(
//                         height: PsDimens.space16,
//                       ),
//                       Padding(
//                         padding: const EdgeInsets.symmetric(
//                           horizontal: PsDimens.space24,
//                         ),
//                         child: Text(
//                           Utils.getString(
//                               context, 'basket_list__empty_cart_description'),
//                           style: TextStyle(
//                             color: PsColors.textPrimaryColor,
//                             fontWeight: FontWeight.w400,
//                             fontSize: PsDimens.space18,
//                           ),
//                           textAlign: TextAlign.center,
//                         ),
//                       ),
//                       const SizedBox(
//                         height: PsDimens.space40,
//                       ),
//                       Container(
//                         margin: const EdgeInsets.only(
//                           left: PsDimens.space12,
//                           right: PsDimens.space12,
//                           bottom: PsDimens.space20,
//                         ),
//                         child: PSButtonWidget(
//                           hasShadow: false,
//                           colorData: PsColors.grey,
//                           width: double.infinity,
//                           titleText: Utils.getString(context, 'start_shoping'),
//                           onPressed: () {
//                             Navigator.pushNamed(
//                               context,
//                               RoutePaths.home,
//                             );
//                           },
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               );
//             }
//           } else {
//             return Container(
//               color: PsColors.coreBackgroundColor,
//             );
//           }
//         }),
//       ),
//     );
//   }
// }

class _CheckoutButtonWidget extends StatelessWidget {
  const _CheckoutButtonWidget({
    Key key,
    @required this.provider,
    @required this.shopInfoProvider,
    @required this.couponDiscountProvider,
  }) : super(key: key);

  final BasketProvider provider;
  final ShopInfoProvider shopInfoProvider;
  final CouponDiscountProvider couponDiscountProvider;
  @override
  Widget build(BuildContext context) {
    double totalPrice = 0.0;
    //int qty = 0;
    //String currencySymbol;
    print(couponDiscountProvider.couponDiscount);
    print(provider.checkoutCalculationHelper.couponDiscount);
    for (Basket basket in provider.basketList.data) {
      totalPrice += double.parse(basket.basketPrice) * double.parse(basket.qty);

      // qty += int.parse(basket.qty);
      // currencySymbol = basket.product.currencySymbol;
    }
    return FloatingActionButton.extended(
      heroTag: 'somethingUnique',
      backgroundColor: PsColors.mainColor,
      onPressed: () async {
        // Basket Item Count Check
        // Need to have at least 1 item in basket
        if (await Utils.checkInternetConnectivity()) {
          if (provider.basketList == null || provider.basketList.data.isEmpty) {
            // Show Error Dialog
            showDialog<dynamic>(
                context: context,
                builder: (BuildContext context) {
                  return ErrorDialog(
                    message: Utils.getString(
                        context, 'basket_list__empty_cart_title'),
                  );
                });
          }

          // Get Currency Symbol
          final String currencySymbol =
              provider.basketList.data[0].product.currencySymbol;

          // Try to get Minmium Order Amount
          // If there is no error, allow to call next page.

          final String shopId = provider.basketList.data[0].product.shop.id;

          await PsProgressDialog.showDialog(context);

          await shopInfoProvider.loadShopInfo(shopId);

          PsProgressDialog.dismissDialog();

          if (shopInfoProvider.shopInfo.data != null) {
            double minOrderAmount = 0;
            try {
              minOrderAmount = double.parse(
                  shopInfoProvider.shopInfo.data.minimumOrderAmount);
            } catch (e) {
              minOrderAmount = 0;
            }

            print(shopInfoProvider.shopInfo.data.minimumOrderAmount);

            if (totalPrice >= minOrderAmount || minOrderAmount == 0) {
              Utils.navigateOnUserVerificationView(context, () async {
                await Navigator.pushNamed(
                  context,
                  RoutePaths.checkout_container,
                  arguments: CheckoutIntentHolder(
                      basketList: provider.basketList.data,
                      basketProvider: provider),
                );
              });
            } else {
              showDialog<dynamic>(
                  context: context,
                  builder: (BuildContext context) {
                    return ErrorDialog(
                      message: Utils.getString(context,
                              'basket_list__error_minimum_order_amount') +
                          '$currencySymbol'
                              '${shopInfoProvider.shopInfo.data.minimumOrderAmount}',
                    );
                  });
            }
          }
        } else {
          showDialog<dynamic>(
              context: context,
              builder: (BuildContext context) {
                return ErrorDialog(
                  message:
                      Utils.getString(context, 'error_dialog__no_internet'),
                );
              });
        }
      },
      label: Row(
        children: <Widget>[
          Icon(Icons.payment, color: PsColors.white),
          const SizedBox(
            width: PsDimens.space8,
          ),
          Text(
            Utils.getString(context, 'basket_list__checkout_button_name'),
            style: Theme.of(context)
                .textTheme
                .button
                .copyWith(color: PsColors.white),
          ),
        ],
      ),
    );
  }
}

class _TotalCountWidget extends StatelessWidget {
  const _TotalCountWidget({
    Key key,
    @required this.provider,
    @required this.shopInfoProvider,
  }) : super(key: key);

  final BasketProvider provider;
  final ShopInfoProvider shopInfoProvider;
  @override
  Widget build(BuildContext context) {
    double totalPrice = 0.0;
    int qty = 0;
    String currencySymbol;

    for (Basket basket in provider.basketList.data) {
      totalPrice += double.parse(basket.basketPrice) * double.parse(basket.qty);

      qty += int.parse(basket.qty);
      currencySymbol = basket.product.currencySymbol;
    }
    if (provider.checkoutCalculationHelper.couponDiscount != 0) {
      totalPrice -= provider.checkoutCalculationHelper.couponDiscount;
    }

    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: PsDimens.space24,
        vertical: PsDimens.space16,
      ),
      height: 85.0,
      color: PsColors.greyColorForCustomWidget,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Text(
                '${Utils.getString(context, 'checkout__items')}',
                style: TextStyle(
                  fontSize: PsDimens.space14,
                  fontWeight: FontWeight.w400,
                  color: PsColors.textPrimaryColor,
                ),
              ),
              Text(
                'x $qty',
                style: TextStyle(
                  color: PsColors.textPrimaryColor,
                  fontSize: 14.0,
                  fontWeight: FontWeight.w400,
                  fontFamily: 'Roboto',
                ),
              ),
            ],
          ),
          const SizedBox(
            height: PsDimens.space15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Text(
                '${Utils.getString(context, 'checkout__price')} ',
                style: TextStyle(
                  fontSize: PsDimens.space18,
                  fontWeight: FontWeight.w500,
                  color: PsColors.textPrimaryColor,
                ),
              ),
              Text(
                '${Utils.getPriceFormat(totalPrice.toString())} $currencySymbol',
                style: TextStyle(
                  color: PsColors.textPrimaryColor,
                  fontSize: 18.0,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Roboto',
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class _ShopInfoViewWidget extends StatefulWidget {
  const _ShopInfoViewWidget({
    Key key,
    @required this.basket,
  }) : super(key: key);

  final Basket basket;

  @override
  __ShopInfoViewWidgetState createState() => __ShopInfoViewWidgetState();
}

class __ShopInfoViewWidgetState extends State<_ShopInfoViewWidget> {
  bool isConnectedToInternet = false;
  bool isSuccessfullyLoaded = true;

  void checkConnection() {
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
      if (isConnectedToInternet && PsConfig.showAdMob) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, RoutePaths.shop_dashboard,
              arguments: ShopDataIntentHolder(
                  shopId: widget.basket.product.shop.id,
                  shopName: widget.basket.product.shop.name));
        },
        child: Container(
          color: PsColors.baseColor,
          child: Column(
            children: <Widget>[
              _HeaderBoxWidget(basket: widget.basket),
            ],
          ),
        ));
  }
}

class _HeaderBoxWidget extends StatefulWidget {
  const _HeaderBoxWidget({
    Key key,
    @required this.basket,
  }) : super(key: key);

  final Basket basket;

  @override
  __HeaderBoxWidgetState createState() => __HeaderBoxWidgetState();
}

class __HeaderBoxWidgetState extends State<_HeaderBoxWidget> {
  @override
  Widget build(BuildContext context) {
    if (widget.basket != null && widget.basket.product.shop != null) {
      return Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  top: PsDimens.space8,
                  bottom: PsDimens.space8,
                  left: PsDimens.space8),
              child: Container(
                width: PsDimens.space60,
                height: PsDimens.space60,
                child: PsNetworkImage(
                  photoKey: '',
                  defaultPhoto: widget.basket.product.shop.defaultPhoto,
                  boxfit: BoxFit.cover,
                ),
              ),
            ),
            const SizedBox(
              width: PsDimens.space8,
            ),
            Flexible(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: const EdgeInsets.all(PsDimens.space16),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            widget.basket.product.shop.name,
                            style: Theme.of(context).textTheme.headline6,
                          ),
                          const SizedBox(height: PsDimens.space8),
                          Row(
                            children: <Widget>[
                              Text(
                                '\$',
                                overflow: TextOverflow.ellipsis,
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1
                                    .copyWith(
                                        color: widget.basket.product.shop
                                                        .priceLevel ==
                                                    PsConst.PRICE_LOW ||
                                                widget.basket.product.shop
                                                        .priceLevel ==
                                                    PsConst.PRICE_MEDIUM ||
                                                widget.basket.product.shop
                                                        .priceLevel ==
                                                    PsConst.PRICE_HIGH
                                            ? PsColors.priceLevelColor
                                            : PsColors.grey),
                                maxLines: 2,
                              ),
                              Text(
                                '\$',
                                overflow: TextOverflow.ellipsis,
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1
                                    .copyWith(
                                        color: widget.basket.product.shop
                                                        .priceLevel ==
                                                    PsConst.PRICE_MEDIUM ||
                                                widget.basket.product.shop
                                                        .priceLevel ==
                                                    PsConst.PRICE_HIGH
                                            ? PsColors.priceLevelColor
                                            : PsColors.grey),
                                maxLines: 2,
                              ),
                              Text(
                                '\$',
                                overflow: TextOverflow.ellipsis,
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1
                                    .copyWith(
                                        color: widget.basket.product.shop
                                                    .priceLevel ==
                                                PsConst.PRICE_HIGH
                                            ? PsColors.priceLevelColor
                                            : PsColors.grey),
                                maxLines: 2,
                              ),
                              const SizedBox(width: PsDimens.space8),
                              Text(
                                widget.basket.product.shop.highlightedInfo,
                                overflow: TextOverflow.ellipsis,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    .copyWith(),
                                maxLines: 2,
                              ),
                            ],
                          ),
                        ],
                      )),
                ],
              ),
            )
          ]);
    } else {
      return Container(
        color: PsColors.coreBackgroundColor,
      );
    }
  }
}
